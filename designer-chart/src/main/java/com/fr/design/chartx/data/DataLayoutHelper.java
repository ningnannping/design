package com.fr.design.chartx.data;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.util.Arrays;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2020/7/22
 */
public class DataLayoutHelper {

    public static int WIDTH = 150;
    public static int LABEL_HEIGHT = 20;
    public static int LABEL_WIDTH = 65;

    public static int LEFT_GAP = 15;
    public static int RIGHT_GAP = 10;

    public static void setWIDTH(int WIDTH) {
        DataLayoutHelper.WIDTH = WIDTH;
    }

    public static void setLabelHeight(int labelHeight) {
        LABEL_HEIGHT = labelHeight;
    }

    public static void setLabelWidth(int labelWidth) {
        LABEL_WIDTH = labelWidth;
    }

    public static void setLeftGap(int leftGap) {
        LEFT_GAP = leftGap;
    }

    public static void setRightGap(int rightGap) {
        RIGHT_GAP = rightGap;
    }

    public static JPanel createDataLayoutPane(Component[][] components) {
        int len = components.length;
        double p = TableLayout.PREFERRED;
        double[] columnSize = {DataLayoutHelper.LABEL_WIDTH, DataLayoutHelper.WIDTH};
        double[] rowSize = new double[len];
        Arrays.fill(rowSize, p);

        return TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize);
    }

    public static JPanel createDataLayoutPane(String label, Component component) {
        Component[][] components = new Component[][]{
                new Component[]{new UILabel(label, SwingConstants.LEFT), component}
        };

        return createDataLayoutPane(components);
    }

    public static void addNormalBorder(JComponent component) {
        component.setBorder(BorderFactory.createEmptyBorder(0, DataLayoutHelper.LEFT_GAP, 0, DataLayoutHelper.RIGHT_GAP));
    }
}
