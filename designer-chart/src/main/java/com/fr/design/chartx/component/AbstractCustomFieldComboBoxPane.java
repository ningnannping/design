package com.fr.design.chartx.component;

import com.fr.data.util.function.AbstractDataFunction;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.beans.FurtherBasicBeanPane;
import com.fr.design.chartx.component.correlation.AbstractCorrelationPane;
import com.fr.design.chartx.component.correlation.CalculateComboBoxEditorComponent;
import com.fr.design.chartx.component.correlation.FieldEditorComponentWrapper;
import com.fr.design.chartx.component.correlation.UIComboBoxEditorComponent;
import com.fr.design.chartx.component.correlation.UITextFieldEditorComponent;
import com.fr.design.chartx.data.DataLayoutHelper;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.data.CalculateComboBox;
import com.fr.design.mainframe.chart.gui.data.table.DataPaneHelper;
import com.fr.extended.chart.UIComboBoxWithNone;
import com.fr.general.GeneralUtils;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by shine on 2018/9/12.
 * 系列名使用字段名or字段值的抽象的pane 支持多种属性结构的存取
 */
public abstract class AbstractCustomFieldComboBoxPane<T> extends BasicBeanPane<T> {

    private UIButtonGroup<Boolean> nameOrValue;
    private JPanel cardPane;
    private CardLayout cardLayout;

    private AbstractUseFieldValuePane useFieldValuePane;

    private AbstractCustomFieldNamePane customFieldNamePane;

    private List<String> fieldList = new ArrayList<String>();

    public AbstractCustomFieldComboBoxPane() {

        useFieldValuePane = createUseFieldValuePane();
        customFieldNamePane = createCustomFieldNamePane();

        nameOrValue = new UIButtonGroup<Boolean>(
                new String[]{useFieldValuePane.title4PopupWindow(), customFieldNamePane.title4PopupWindow()},
                new Boolean[]{false, true});
        nameOrValue.setSelectedItem(false);
        nameOrValue.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                checkCardPane();
            }
        });
        JPanel northPane = DataLayoutHelper.createDataLayoutPane(Toolkit.i18nText("Fine-Design_Chart_Series_Name_From"), nameOrValue);

        cardLayout = new CardLayout();
        cardPane = new JPanel(cardLayout);
        cardPane.add(useFieldValuePane, useFieldValuePane.title4PopupWindow());
        cardPane.add(customFieldNamePane, customFieldNamePane.title4PopupWindow());

        this.setLayout(new BorderLayout(0, 6));
        this.add(northPane, BorderLayout.NORTH);
        this.add(cardPane, BorderLayout.CENTER);
    }

    protected abstract AbstractUseFieldValuePane createUseFieldValuePane();

    protected abstract AbstractCustomFieldNamePane createCustomFieldNamePane();

    protected boolean valueComboBoxHasNone() {
        return false;
    }

    protected boolean seriesComboBoxHasNone() {
        return false;
    }

    public void checkBoxUse(boolean hasUse) {
        nameOrValue.setEnabled(hasUse);
        useFieldValuePane.checkBoxUse(hasUse);
    }

    public void clearAllBoxList() {
        useFieldValuePane.clearAllBoxList();
        fieldList.clear();
    }

    public void refreshBoxListWithSelectTableData(List columnNameList) {
        useFieldValuePane.refreshBoxListWithSelectTableData(columnNameList);
        fieldList = columnNameList;
    }

    private void checkCardPane() {
        cardLayout.show(cardPane, nameOrValue.getSelectedItem() ? customFieldNamePane.title4PopupWindow() : useFieldValuePane.title4PopupWindow());
    }

    protected void populateNameOrValue(boolean b) {
        nameOrValue.setSelectedItem(b);
        checkCardPane();
    }

    protected boolean updateNameOrValue() {
        return nameOrValue.getSelectedItem();
    }

    protected void populateCustomFieldNamePane(T t) {
        customFieldNamePane.populateBean(t);
    }

    protected void updateCustomFieldNamePane(T t) {
        customFieldNamePane.updateBean(t);
    }

    protected void populateUseFieldValuePane(T t) {
        useFieldValuePane.populateBean(t);
    }

    protected void updateUseFieldValuePane(T t) {
        useFieldValuePane.updateBean(t);
    }

    @Override
    public T updateBean() {
        return null;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }

    protected abstract class AbstractUseFieldValuePane extends FurtherBasicBeanPane<T> {
        private UIComboBox series;
        private UIComboBox value;
        private CalculateComboBox function;

        public AbstractUseFieldValuePane() {
            initComponents();
        }

        private void initComponents() {

            series = seriesComboBoxHasNone() ? new UIComboBoxWithNone() : new UIComboBox();
            value = valueComboBoxHasNone() ? new UIComboBoxWithNone() : new UIComboBox();
            value.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    function.setEnabled(value.getSelectedItem() != null);
                }
            });


            function = new CalculateComboBox();
            function.setEnabled(false);

            Component[][] components = new Component[][]{
                    new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Series_Name"), SwingConstants.LEFT), series},
                    new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Use_Value"), SwingConstants.LEFT), value},
                    new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Summary_Method"), SwingConstants.LEFT), function},
            };

            JPanel panel = DataLayoutHelper.createDataLayoutPane(components);

            this.setLayout(new BorderLayout(0, 6));
            this.add(panel, BorderLayout.CENTER);
        }

        public void checkBoxUse(boolean hasUse) {
            series.setEnabled(hasUse);
            value.setEnabled(hasUse);
        }

        public void clearAllBoxList() {
            DataPaneHelper.clearBoxItems(series);
            DataPaneHelper.clearBoxItems(value);
        }

        public void refreshBoxListWithSelectTableData(List columnNameList) {
            DataPaneHelper.refreshBoxItems(series, columnNameList);
            DataPaneHelper.refreshBoxItems(value, columnNameList);
        }

        protected void populateSeries(String item) {
            series.setSelectedItem(item);
        }

        protected void populateValue(String item) {
            value.setSelectedItem(item);
        }

        protected void populateFunction(AbstractDataFunction _function) {
            function.populateBean(_function);
        }

        protected String updateSeries() {
            return GeneralUtils.objectToString(series.getSelectedItem());
        }

        protected String updateValue() {
            return GeneralUtils.objectToString(value.getSelectedItem());
        }

        protected AbstractDataFunction updateFunction() {
            return function.updateBean();
        }

        @Override
        public boolean accept(Object ob) {
            return true;
        }

        @Override
        public void reset() {
        }

        @Override
        public String title4PopupWindow() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Enable_Field_Value");
        }

        @Override
        public T updateBean() {
            return null;
        }
    }

    protected abstract class AbstractCustomFieldNamePane extends AbstractCorrelationPane<T> {

        @Override
        protected FieldEditorComponentWrapper[] createFieldEditorComponentWrappers() {
            return new FieldEditorComponentWrapper[]{
                    new UIComboBoxEditorComponent(Toolkit.i18nText("Fine-Design_Chart_Field_Name")) {
                        @Override
                        protected List<String> items() {
                            return fieldList;
                        }
                    },
                    new UITextFieldEditorComponent(Toolkit.i18nText("Fine-Design_Chart_Series_Name")),
                    new CalculateComboBoxEditorComponent(Toolkit.i18nText("Fine-Design_Chart_Summary_Method"))
            };
        }

        @Override
        protected Object[] createLine() {
            return new String[]{StringUtils.EMPTY, StringUtils.EMPTY, Toolkit.i18nText("Fine-Design_Chart_Data_Function_Sum")};
        }

        @Override
        protected String title4PopupWindow() {
            return Toolkit.i18nText("Fine-Design_Chart_Enable_Field_Name");
        }
    }
}
