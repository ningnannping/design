package com.fr.design.mainframe.chart.gui.style;

import com.fr.chart.base.TextAttr;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-03
 */
public class ChartTextAttrPaneWithPreStyle extends ChartTextAttrPane {

    private static final int PREDEFINED_STYLE = 0;
    private static final int CUSTOM = 1;

    private UIButtonGroup<Integer> preButton;
    private JPanel textFontPane;

    public ChartTextAttrPaneWithPreStyle() {
        initListener();
    }

    protected JPanel getContentPane(JPanel buttonPane) {
        preButton = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Predefined"),
                Toolkit.i18nText("Fine-Design_Chart_Custom")});
        double f = TableLayout.FILL;
        double e = getEdithAreaWidth();
        double[] columnSize = {f, e};
        double p = TableLayout.PREFERRED;
        textFontPane = TableLayout4VanChartHelper.createGapTableLayoutPane(getComponents(buttonPane), getRowSize(), columnSize);

        double[] rowSize = {p, p, p};
        UILabel text = new UILabel(Toolkit.i18nText("Fine-Design_Chart_Character"), SwingConstants.LEFT);
        Component[][] components = {
                new Component[]{null, null},
                new Component[]{text, preButton},
                new Component[]{textFontPane, null},
        };
        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
    }

    protected double getEdithAreaWidth() {
        return TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
    }

    protected Component[][] getComponents(JPanel buttonPane) {
        return new Component[][]{
                new Component[]{null, getFontNameComboBox()},
                new Component[]{null, buttonPane}
        };
    }

    private void initListener() {
        preButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkPreButton();
            }
        });
    }

    private void checkPreButton() {
        textFontPane.setVisible(preButton.getSelectedIndex() == CUSTOM);
        textFontPane.setPreferredSize(preButton.getSelectedIndex() == CUSTOM ? new Dimension(0, 60) : new Dimension(0, 0));
    }

    public void populate(TextAttr textAttr) {
        if (textAttr.isPredefinedStyle()) {
            preButton.setSelectedIndex(PREDEFINED_STYLE);
        } else {
            preButton.setSelectedIndex(CUSTOM);
        }
        super.populate(textAttr);
        checkPreButton();
    }

    public void update(TextAttr textAttr) {
        int selectedIndex = preButton.getSelectedIndex();
        if (selectedIndex == PREDEFINED_STYLE) {
            textAttr.setPredefinedStyle(true);
        } else {
            textAttr.setPredefinedStyle(false);
        }
        super.update(textAttr);
    }
}
