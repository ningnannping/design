package com.fr.design.chart;

import com.fr.base.ScreenResolution;
import com.fr.base.chart.BaseChartPainter;
import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.base.chart.result.WebChartIDInfo;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chartx.attr.ChartProvider;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.script.Calculator;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;

import javax.swing.Icon;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;

/**
 * 图表的缩略图Icon, 在选择图表类型界面 用到.
 */
public class ChartIcon implements Icon, XMLable {
    private static final int WIDTH = 400;
    private static final int HEIGHT = 225;


    private ChartCollection chartCollection;
    private CallbackEvent callbackEvent;

    private String chartName;

    /**
     * 构造Chart的缩略图Icon
     */
    public ChartIcon(ChartCollection chartCollection) {
        this.chartCollection = chartCollection;
        initChartName();
    }

    public ChartCollection getChartCollection() {
        return chartCollection;
    }

    public String getChartName() {
        return chartName;
    }

    public void setChartCollection(ChartCollection chartCollection) {
        this.chartCollection = chartCollection;
    }

    public void setChartName(String chartName) {
        this.chartName = chartName;
    }

    private void initChartName() {
        ChartProvider chart = chartCollection.getSelectedChartProvider(ChartProvider.class);
        String[] subName = ChartTypeInterfaceManager.getInstance().getSubName(chart.getID());
        chartName = subName[0];
    }

    public void registerCallBackEvent(CallbackEvent callbackEvent) {
        this.callbackEvent = callbackEvent;
    }

    /**
     * 画出缩略图Icon
     *
     * @param g 图形的上下文
     * @param c 所在的Component
     * @param x 缩略图的起始坐标x
     * @param y 缩略图的起始坐标y
     */
    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {

        BaseChartPainter chartPainter = getChartPainter();

        //插入图表的宽度是固定的，resolution直接获取屏幕分辨率，resolution现在只会影响到老图表
        int resolution = ScreenResolution.getScreenResolution();

        Graphics2D g2d = (Graphics2D) g;
        Paint oldPaint = g2d.getPaint();
        g.translate(x, y);
        g2d.setPaint(Color.white);
        g2d.fillRect(0, 0, getIconWidth(), getIconHeight());

        chartPainter.paint(g2d, getIconWidth(), getIconHeight(), resolution, null, callbackEvent);

        g.translate(-x, -y);
        g2d.setPaint(oldPaint);
    }

    protected BaseChartPainter getChartPainter() {
        BaseChartPainter painter = chartCollection.createResultChartPainterWithOutDealFormula(Calculator.createCalculator(),
                WebChartIDInfo.createEmptyDesignerInfo(), getIconWidth(), getIconHeight());
        return painter;
    }


    /**
     * 返回缩略图的宽度
     *
     * @return int 缩略图宽度
     */
    @Override
    public int getIconWidth() {
        return WIDTH;
    }

    /**
     * 返回缩略图的高度
     *
     * @return int 缩略图高度
     */
    @Override
    public int getIconHeight() {
        return HEIGHT;
    }


    @Override
    public void readXML(XMLableReader reader) {
        //do nothing
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        //do nothing
    }

    /**
     * @return 克隆后的对象
     * @throws CloneNotSupportedException 如果克隆失败则抛出此异常
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        ChartIcon cloned = (ChartIcon) super.clone();
        if (getChartCollection() != null) {
            cloned.setChartCollection(this.getChartCollection());
        }
        cloned.setChartName(this.getChartName());
        ;
        return cloned;
    }

}