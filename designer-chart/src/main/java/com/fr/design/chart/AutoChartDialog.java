package com.fr.design.chart;

import com.fr.base.chart.BaseChartCollection;
import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chartx.TwoTuple;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.vanchart.VanChart;

import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-28
 */
public class AutoChartDialog extends ChartDialog {

    private AutoChartTypePane autoChartTypePane;

    public AutoChartDialog(Frame owner) {
        super(owner);
    }

    public AutoChartDialog(Dialog owner) {
        super(owner);
    }

    protected Component initCenterPane() {
        autoChartTypePane = new AutoChartTypePane();

        getOk().setEnabled(false);

        autoChartTypePane.registsListAction(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                getOk().setEnabled(((JList) e.getSource()).getSelectedIndex() >= 0);
            }
        });
        return autoChartTypePane;
    }

    protected ActionListener getActionListener(final String createTime) {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ChartCollection chartCollection = (ChartCollection) getChartCollection();
                autoChartTypePane.update(chartCollection, createTime);
                if (chartCollection.getChartCount() > 0) {
                    doOK();
                } else {
                    doCancel();
                }
            }
        };
    }

    protected String getDialogTitle() {
        return Toolkit.i18nText("Fine-Design_Chart_Auto_Recommended_Chart");
    }

    /**
     * 更新新建的图表 ChartCollection
     */
    public void populate(BaseChartCollection cc) {
        super.populate(cc);
        ChartCollection chartCollection = (ChartCollection) getChartCollection();
        VanChart vanChart = chartCollection.getSelectedChartProvider(VanChart.class);
        if (vanChart == null) {
            return;
        }
        TopDefinitionProvider filterDefinition = vanChart.getFilterDefinition();
        if (filterDefinition == null) {
            return;
        }
        TwoTuple<String, String[]> tableNameAndDataFields = filterDefinition.getTableNameAndDataFields();
        if (tableNameAndDataFields == null) {
            return;
        }
        String tableName = tableNameAndDataFields.getFirst();
        String[] dataFields = tableNameAndDataFields.getSecond();
        autoChartTypePane.populate(tableName, dataFields);
    }
}
