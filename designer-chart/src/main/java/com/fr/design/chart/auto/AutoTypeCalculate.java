package com.fr.design.chart.auto;

import com.fr.chart.auto.ColumnInfo;
import com.fr.chart.auto.strategy.AutoTypeStrategy;
import com.fr.chart.auto.strategy.imp.AvaStrategy;
import com.fr.chart.auto.strategy.imp.BubbleChartStrategy;
import com.fr.chart.auto.strategy.imp.SingleDimensionStrategy;
import com.fr.chart.auto.strategy.imp.SingleTargetStrategy;
import com.fr.data.TableDataSource;
import com.fr.data.TableDataSourceTailor;
import com.fr.data.impl.EmbeddedTableData;
import com.fr.data.impl.NameTableData;
import com.fr.design.data.DesignTableDataManager;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.general.data.DataModel;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.script.Calculator;
import com.fr.stable.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-05-08
 */
public class AutoTypeCalculate {

    public static Pattern[] dataPatterns;

    static {
        Pattern pattern1 = Pattern.compile("^(19|20)\\d{2}$");
        Pattern pattern2 = Pattern.compile("^\\d{4}(0?[1-9]|1[012])$");
        Pattern pattern3 = Pattern.compile("^\\d{4}(0?[1-9]|1[012])(0?[1-9]|[12]\\d|3[01])$");
        dataPatterns = new Pattern[]{pattern1, pattern2, pattern3};
    }

    public static List<VanChart> calculateType(String tableName, List<String> columns) {
        List<ColumnInfo> columnValue = calculateField(tableName, columns);
        if (columnValue.isEmpty()) {
            return new ArrayList<>();
        }

        List<ColumnInfo> dimensions = new ArrayList<>();
        List<ColumnInfo> targets = new ArrayList<>();
        for (ColumnInfo field : columnValue) {
            if (isTarget(field.getValues())) {
                targets.add(field);
            } else {
                dimensions.add(field);
            }
        }
        AutoTypeStrategy autoTypeStrategy = chooseStrategy(dimensions.size(), targets.size());
        return autoTypeStrategy.rankChart(tableName, dimensions, targets);
    }

    private static AutoTypeStrategy chooseStrategy(int dimensionSize, int targetSize) {
        if (dimensionSize == 0) {
            //没有维度，并且只有一个指标，使用单指标匹配逻辑，大于1个指标，使用气泡图（散点图）匹配逻辑
            if (targetSize == 1) {
                return new SingleTargetStrategy();
            } else {
                return new BubbleChartStrategy();
            }
        } else if (dimensionSize == 1) {
            //1个维度，并且没有指标，使用单维度匹配，2~3个指标，使用气泡图（散点图）匹配逻辑，其余使用ava匹配
            if (targetSize == 0) {
                return new SingleDimensionStrategy();
            } else if (targetSize == 2 || targetSize == 3) {
                return new BubbleChartStrategy();
            } else {
                return new AvaStrategy();
            }
        } else {
            //大与1个维度，并且没有指标，使用单维度匹配（循环），否则使用ava匹配
            if (targetSize == 0) {
                return new SingleDimensionStrategy();
            } else {
                return new AvaStrategy();
            }
        }
    }

    private static boolean isTarget(List<String> values) {
        for (String value : values) {
            if (StringUtils.isEmpty(value)) {
                continue;
            }
            Number number = GeneralUtils.string2Number(value);
            if (number == null) {
                return false;
            }
        }
        //不是日期型数字才是指标
        return !isNumberData(values);
    }

    private static boolean isNumberData(List<String> values) {
        for (Pattern pattern : dataPatterns) {
            if (isNumberData(values, pattern)) {
                return true;
            }
        }
        return false;
    }

    private static boolean isNumberData(List<String> values, Pattern pattern) {
        for (String value : values) {
            Matcher matcher = pattern.matcher(value);
            if (!matcher.matches()) {
                return false;
            }
        }
        return true;
    }

    private static List<ColumnInfo> calculateField(String tableName, List<String> columns) {
        NameTableData nameTableData = new NameTableData(tableName);
        TableDataSource dataSource = TableDataSourceTailor.extractTableData(HistoryTemplateListCache.getInstance().getCurrentEditingTemplate().getTarget());
        Calculator calculator = Calculator.createCalculator();
        calculator.setAttribute(TableDataSource.KEY, dataSource);
        nameTableData.createTableData(calculator);

        EmbeddedTableData tableData;
        try {
            tableData = DesignTableDataManager.previewTableDataNeedInputParameters(dataSource, nameTableData, Integer.MAX_VALUE, false);
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return new ArrayList<>();
        }

        List<ColumnInfo> originalData = new ArrayList<>();
        for (String column : columns) {
            List<String> columnData = getColumnData(tableData, column);
            if (columnData != null && !columnData.isEmpty()) {
                originalData.add(new ColumnInfo(column, columnData));
            }
        }
        return originalData;
    }

    private static List<String> getColumnData(EmbeddedTableData tableData, String columnName) {
        List<String> columnData = new ArrayList<>();

        int colIndex = getColIndex(tableData, columnName);
        if (colIndex == DataModel.COLUMN_NAME_NOT_FOUND) {
            return columnData;
        }

        int size = tableData.getRowCount();
        for (int i = 0; i < size; i++) {
            Object valueAt = tableData.getValueAt(i, colIndex);
            columnData.add(GeneralUtils.objectToString(valueAt));
        }
        return columnData;
    }

    private static int getColIndex(EmbeddedTableData tableData, String columnName) {
        int colIndex = 0;

        for (int count = tableData.getColumnCount(); colIndex < count; ++colIndex) {
            if (ComparatorUtils.tableDataColumnNameEquals(tableData.getColumnName(colIndex), columnName)) {
                return colIndex;
            }
        }
        return DataModel.COLUMN_NAME_NOT_FOUND;
    }
}
