package com.fr.design.module;

import com.fr.base.ChartColorMatching;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.color.ColorAdjustPane;
import com.fr.design.style.background.gradient.FixedGradientBar;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * 预定义的图表配色界面.
 *
 * @author kunsnat E-mail:kunsnat@gmail.com
 * @version 创建时间：2013-8-21 下午03:16:27
 */
public class ChartPreFillStylePane extends BasicBeanPane<ChartColorMatching> {

    private JPanel changeColorSetPane;
    private CardLayout cardLayout;

    private UIButtonGroup groupButton;

    private ColorAdjustPane colorAdjustPane;
    private FixedGradientBar colorGradient;

    public ChartPreFillStylePane() {

        initComponents();

        initListener();
    }

    private void initComponents() {

        JPanel customPane = new JPanel(FRGUIPaneFactory.createBorderLayout());

        groupButton = new UIButtonGroup<>(new String[]{Toolkit.i18nText("Fine-Design_Chart_Custom_Color"), Toolkit.i18nText("Fine-Design_Chart_Gradient_Color")});
        groupButton.setSelectedIndex(0);
        customPane.add(groupButton, BorderLayout.NORTH);

        changeColorSetPane = new JPanel(cardLayout = new CardLayout());
        changeColorSetPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        changeColorSetPane.add(colorGradient = new FixedGradientBar(4, 150), "gradient");
        changeColorSetPane.add(colorAdjustPane = new ColorAdjustPane(), "acc");
        cardLayout.show(changeColorSetPane, "acc");
        customPane.add(changeColorSetPane, BorderLayout.CENTER);

        customPane.setPreferredSize(new Dimension(155, 300));
        colorGradient.setPreferredSize(new Dimension(155, 30));
        colorGradient.getSelectColorPointBtnP1().setColorInner(Color.WHITE);
        colorGradient.getSelectColorPointBtnP2().setColorInner(FixedGradientBar.NEW_CHARACTER);
        
        double p = TableLayout.PREFERRED;
        double[] columnSize = {p, p};
        double[] rowSize = {p, p, p};
        Component[][] components = new Component[][]{
                new Component[]{new UILabel(" " + com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Color_Match")), null},
                new Component[]{null, customPane},
        };

        this.setLayout(new BorderLayout());
        this.add(TableLayoutHelper.createTableLayoutPane(components, rowSize, columnSize), BorderLayout.WEST);
    }

    private void initListener() {
        groupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                checkCardPane();
            }
        });
    }

    private void checkCardPane() {
        if (groupButton.getSelectedIndex() == 0) {
            cardLayout.show(changeColorSetPane, "acc");
        } else {
            cardLayout.show(changeColorSetPane, "gradient");
        }
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_ServerM_Predefined_Styles");
    }

    public void populateBean(ChartColorMatching condition) {
        if (condition == null) {
            return;
        }

        boolean isGradient = condition.getGradient();
        List<Color> colorList = condition.getColorList();
        if (isGradient) {
            groupButton.setSelectedIndex(1);
            cardLayout.show(changeColorSetPane, "gradient");

            if (colorList.size() == 2) {
                colorGradient.getSelectColorPointBtnP1().setColorInner(colorList.get(0));
                colorGradient.getSelectColorPointBtnP2().setColorInner(colorList.get(1));
                colorGradient.repaint();
            }
        } else {
            groupButton.setSelectedIndex(0);
            cardLayout.show(changeColorSetPane, "acc");

            if (colorList.isEmpty()) {
                List<Color> resultList = new ArrayList<>();
                Collections.addAll(resultList, ColorAdjustPane.DEFAULT_COLORS);
                condition.setColorList(resultList);
                colorAdjustPane.updateColor(ColorAdjustPane.DEFAULT_COLORS);
            } else {
                colorAdjustPane.updateColor(colorList.toArray(new Color[colorList.size()]));
            }
        }
    }

    @Override
    public ChartColorMatching updateBean() {
        ChartColorMatching chartColorMatching = new ChartColorMatching();

        List<Color> colorList = new ArrayList<Color>();

        if (groupButton.getSelectedIndex() == 1) {
            chartColorMatching.setGradient(true);

            Color start = colorGradient.getSelectColorPointBtnP1().getColorInner();
            Color end = colorGradient.getSelectColorPointBtnP2().getColorInner();
            colorList.add(start);
            colorList.add(end);
        } else {
            chartColorMatching.setGradient(false);

            Color[] colors = colorAdjustPane.getColors();
            for (Color color : colors) {
                colorList.add(color);
            }
        }

        chartColorMatching.setColorList(colorList);

        return chartColorMatching;
    }
}
