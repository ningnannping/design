package com.fr.design.module;

import com.fr.base.ChartColorMatching;
import com.fr.base.ChartPreStyleConfig;
import com.fr.design.gui.icombobox.ColorSchemeComboBox;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.general.NameObject;
import com.fr.stable.Nameable;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.awt.BorderLayout;
import java.awt.Dimension;

/**
 * 图表预定义管理 界面, 在工具栏-服务器管理中.
 *
 * @author kunsnat E-mail:kunsnat@gmail.com
 * @version 创建时间：2013-8-21 下午02:33:48
 */
public class ChartPreStyleManagerPane extends BasicPane {

    private ColorSchemeComboBox defaultColorBox;

    private ChartPreStyleListPane chartPreStyleListPane;

    public ChartPreStyleManagerPane() {
        initComponent();
    }

    private void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel colorBoxPane = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();

        chartPreStyleListPane = new ChartPreStyleListPane(this);

        initDefaultColorBox();
        colorBoxPane.add(new UILabel(Toolkit.i18nText("Fine-Design_Chart_Match_Default_Color_Scheme") + "："));
        colorBoxPane.add(defaultColorBox);

        this.add(colorBoxPane, BorderLayout.NORTH);
        this.add(chartPreStyleListPane, BorderLayout.CENTER);
    }

    private void initDefaultColorBox() {
        Map<String, ColorSchemeComboBox.ColorInfo> colorSchemes = new LinkedHashMap<>();
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();
        Iterator names = config.names();
        while (names.hasNext()) {
            Object key = names.next();
            ColorSchemeComboBox.ColorInfo colorInfo = new ColorSchemeComboBox.ColorInfo();
            ChartColorMatching colorMatching = (ChartColorMatching) config.getPreStyle(key);
            colorInfo.setGradient(colorMatching.getGradient());
            colorInfo.setColors(colorMatching.getColorList());
            colorSchemes.put(colorMatching.getId(), colorInfo);
        }
        defaultColorBox = new ColorSchemeComboBox(colorSchemes);
        defaultColorBox.setPreferredSize(new Dimension(TableLayout4VanChartHelper.EDIT_AREA_WIDTH, 20));
    }

    private void refreshColorSchemes() {
        Nameable[] nameables = chartPreStyleListPane.update();
        Map<String, ColorSchemeComboBox.ColorInfo> colorSchemes = new LinkedHashMap<>();
        for (Nameable value : nameables) {
            String name = value.getName();
            ChartColorMatching colorMatching = (ChartColorMatching) ((NameObject) value).getObject();
            ColorSchemeComboBox.ColorInfo colorInfo = new ColorSchemeComboBox.ColorInfo();
            colorInfo.setGradient(colorMatching.getGradient());
            colorInfo.setColors(colorMatching.getColorList());
            colorSchemes.put(name, colorInfo);
        }
        defaultColorBox.refresh(colorSchemes);
    }

    public void refreshDefaultColorBox() {
        Object selectedItem = defaultColorBox.getSelectedItem();
        refreshColorSchemes();
        defaultColorBox.setSelectedItem(selectedItem);
    }

    public void refreshDefaultColorBox(String oldName, String newName) {
        Object selectedItem = defaultColorBox.getSelectedItem();
        if (ComparatorUtils.equals(selectedItem, oldName)) {
            selectedItem = newName;
        }
        refreshColorSchemes();
        defaultColorBox.setSelectedItem(selectedItem);
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Report_ServerM_Predefined_Styles");
    }

    public void populateBean() {
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();
        String currentStyle = config.getCurrentStyle();
        defaultColorBox.setSelectedItem(currentStyle);
        chartPreStyleListPane.populateBean();
    }

    public void updateBean() {
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();
        config.setCurrentStyle(GeneralUtils.objectToString(defaultColorBox.getSelectedItem()));
        chartPreStyleListPane.updateBean();
    }
}
