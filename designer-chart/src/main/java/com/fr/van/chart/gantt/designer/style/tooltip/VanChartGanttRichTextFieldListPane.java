package com.fr.van.chart.gantt.designer.style.tooltip;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipDurationFormat;
import com.fr.plugin.chart.base.format.AttrTooltipEndTimeFormat;
import com.fr.plugin.chart.base.format.AttrTooltipProcessesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipProgressFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipStartTimeFormat;
import com.fr.plugin.chart.gantt.attr.AttrGanttTooltipContent;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartGanttRichTextFieldListPane extends VanChartFieldListPane {

    private VanChartFieldButton richTextProcessesFormatPane;
    private VanChartFieldButton richTextStartTimeFormatPane;
    private VanChartFieldButton richTextEndTimeFormatPane;
    private VanChartFieldButton richTextDurationFormatPane;
    private VanChartFieldButton richTextProgressFormatPane;

    public VanChartGanttRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    protected void initDefaultFieldButton() {
        VanChartFieldListener listener = getFieldListener();

        richTextProcessesFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Project_Name"),
                new AttrTooltipProcessesFormat(), false, listener);

        setSeriesNameButton(new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                new AttrTooltipSeriesFormat(), false, listener));

        richTextStartTimeFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Start_Time"),
                new AttrTooltipStartTimeFormat(), false, listener);

        richTextEndTimeFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_End_Time"),
                new AttrTooltipEndTimeFormat(), false, listener);

        richTextDurationFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Duration_Time"),
                new AttrTooltipDurationFormat(), false, true, listener);

        richTextProgressFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Process"),
                new AttrTooltipProgressFormat(), false, listener);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(richTextProcessesFormatPane);
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(richTextStartTimeFormatPane);
        fieldPane.add(richTextEndTimeFormatPane);
        fieldPane.add(richTextDurationFormatPane);
        fieldPane.add(richTextProgressFormatPane);
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(richTextProcessesFormatPane);
        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(richTextStartTimeFormatPane);
        fieldButtonList.add(richTextEndTimeFormatPane);
        fieldButtonList.add(richTextDurationFormatPane);
        fieldButtonList.add(richTextProgressFormatPane);

        return fieldButtonList;
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        super.populateDefaultField(tooltipContent);

        if (tooltipContent instanceof AttrGanttTooltipContent) {
            AttrGanttTooltipContent gantt = (AttrGanttTooltipContent) tooltipContent;

            populateButtonFormat(richTextProcessesFormatPane, gantt.getRichTextProcessesFormat());
            populateButtonFormat(richTextStartTimeFormatPane, gantt.getRichTextStartTimeFormat());
            populateButtonFormat(richTextEndTimeFormatPane, gantt.getRichTextEndTimeFormat());
            populateButtonFormat(richTextDurationFormatPane, gantt.getRichTextDurationFormat());
            populateButtonFormat(richTextProgressFormatPane, gantt.getRichTextProgressFormat());
        }
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        super.updateDefaultField(tooltipContent);

        if (tooltipContent instanceof AttrGanttTooltipContent) {
            AttrGanttTooltipContent gantt = (AttrGanttTooltipContent) tooltipContent;

            updateButtonFormat(richTextProcessesFormatPane, gantt.getRichTextProcessesFormat());
            updateButtonFormat(richTextStartTimeFormatPane, gantt.getRichTextStartTimeFormat());
            updateButtonFormat(richTextEndTimeFormatPane, gantt.getRichTextEndTimeFormat());
            updateButtonFormat(richTextDurationFormatPane, gantt.getRichTextDurationFormat());
            updateButtonFormat(richTextProgressFormatPane, gantt.getRichTextProgressFormat());
        }
    }

}
