package com.fr.van.chart.scatter;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSizeFormat;
import com.fr.plugin.chart.base.format.AttrTooltipXFormat;
import com.fr.plugin.chart.base.format.AttrTooltipYFormat;
import com.fr.plugin.chart.scatter.attr.ScatterAttrTooltipContent;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.XFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.YFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * 散点图标签界面
 */
public class  VanChartScatterTooltipContentPane extends VanChartTooltipContentPane{
    private static final long serialVersionUID = 5595016643808487922L;
    private XFormatPaneWithCheckBox xFormatPane;
    private YFormatPaneWithCheckBox yFormatPane;
    private ValueFormatPaneWithCheckBox sizeFormatPane;

    public XFormatPaneWithCheckBox getXFormatPane() {
        return xFormatPane;
    }

    public YFormatPaneWithCheckBox getYFormatPane() {
        return yFormatPane;
    }

    public ValueFormatPaneWithCheckBox getSizeFormatPane() {
        return sizeFormatPane;
    }

    public VanChartScatterTooltipContentPane(VanChartStylePane parent, JPanel showOnPane){
        super(parent, showOnPane);
    }

    public VanChartScatterTooltipContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition){
        super(parent, showOnPane, inCondition);
    }


    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane){
        super.initFormatPane(parent, showOnPane);
        xFormatPane = new XFormatPaneWithCheckBox(parent, showOnPane);
        yFormatPane = new YFormatPaneWithCheckBox(parent, showOnPane);
        sizeFormatPane = new ValueFormatPaneWithCheckBox(parent, showOnPane);
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartScatterRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    @Override
    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{getSeriesNameFormatPane(),null},
                new Component[]{xFormatPane,null},
                new Component[]{yFormatPane,null},
                new Component[]{sizeFormatPane,null},
        };
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.populateFormatPane(attrTooltipContent);
        if(attrTooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatterAttrTooltipContent = (ScatterAttrTooltipContent) attrTooltipContent;
            xFormatPane.populate(scatterAttrTooltipContent.getXFormat());
            yFormatPane.populate(scatterAttrTooltipContent.getYFormat());
            sizeFormatPane.populate(scatterAttrTooltipContent.getSizeFormat());
        }
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                "x",
                "y",
                Toolkit.i18nText("Fine-Design_Chart_Use_Value")};
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipSeriesFormat(),
                new AttrTooltipXFormat(),
                new AttrTooltipYFormat(),
                new AttrTooltipSizeFormat()};
    }

    @Override
    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        super.updateFormatPane(attrTooltipContent);
        if(attrTooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatterAttrTooltipContent = (ScatterAttrTooltipContent) attrTooltipContent;
            xFormatPane.update(scatterAttrTooltipContent.getXFormat());
            yFormatPane.update(scatterAttrTooltipContent.getYFormat());
            sizeFormatPane.update(scatterAttrTooltipContent.getSizeFormat());
        }
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        super.updateTooltipFormat(target, source);

        if (target instanceof ScatterAttrTooltipContent && source instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent targetScatter = (ScatterAttrTooltipContent) target;
            ScatterAttrTooltipContent sourceScatter = (ScatterAttrTooltipContent) source;

            targetScatter.setRichTextXFormat(sourceScatter.getRichTextXFormat());
            targetScatter.setRichTextYFormat(sourceScatter.getRichTextYFormat());
            targetScatter.setRichTextSizeFormat(sourceScatter.getRichTextSizeFormat());
        }
    }

    @Override
    public void setDirty(boolean isDirty) {
        xFormatPane.setDirty(isDirty);
        yFormatPane.setDirty(isDirty);
        sizeFormatPane.setDirty(isDirty);
        getSeriesNameFormatPane().setDirty(isDirty);
    }

    @Override
    public boolean isDirty() {
        return xFormatPane.isDirty() || yFormatPane.isDirty() || sizeFormatPane.isDirty() || getSeriesNameFormatPane().isDirty();
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        return new ScatterAttrTooltipContent();
    }
}
