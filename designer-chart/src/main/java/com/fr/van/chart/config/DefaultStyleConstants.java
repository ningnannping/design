package com.fr.van.chart.config;

import com.fr.base.ChartPreStyleConfig;
import com.fr.chart.base.ChartBaseUtils;
import com.fr.chart.base.ChartConstants;
import com.fr.general.Background;
import com.fr.general.FRFont;
import com.fr.general.GeneralUtils;
import com.fr.stable.CodeUtils;
import com.fr.stable.Constants;

import java.awt.Color;
import java.awt.Font;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/6/25
 */
public class DefaultStyleConstants {
    static final FRFont TITLE = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.BOLD, 19, new Color(241, 246, 255));
    static final FRFont LEGEND = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 12, new Color(159, 173, 191));

    static final FRFont AXIS_LABEL = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 12, new Color(159, 173, 191));
    static final FRFont AXIS_TITLE = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 12, new Color(200, 211, 228));
    static final Color AXIS_LINE = new Color(46, 75, 102);
    static final Color GRID_LINE = new Color(30, 55, 78);

    static final FRFont ALERT_FONT = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 12, new Color(255, 0, 0));

    static final FRFont DATA_SHEET = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 12, new Color(159, 173, 191));
    static final Color DATA_SHEET_BORDER = new Color(46, 75, 102);

    static final Color BORDER = Color.BLACK;
    static final int BORDER_WIDTH = Constants.LINE_NONE;
    static final int SPECIAL_BORDER_WIDTH = Constants.LINE_THIN;

    static final FRFont PIE_CATEGORY_LABEL = FRFont.getInstance(ChartBaseUtils.getLocalDefaultFont("Microsoft YaHei"), Font.PLAIN, 13, new Color(232, 232, 232));

    static final Color GAUGE_PANE_BACK_COLOR = null;
    static final Color GAUGE_HINGE = null;
    static final Color GAUGE_PANE_BACK_COLOR_4_RING = new Color(72, 73, 79);
    static final Color GAUGE_SLOT_BACKGROUND_COLOR = new Color(72, 73, 79);
    private static final String FONT_NAME = ChartBaseUtils.getLocalDefaultFont("verdana");
    static final FRFont THERMOMETER_LABEL_FONT = FRFont.getInstance(FONT_NAME, Font.BOLD, 12, new Color(232, 232, 232));//试管仪表盘标签的默认样式
    static final FRFont THERMOMETER_AXIS_LABEL = FRFont.getInstance(FONT_NAME, Font.PLAIN, 11, new Color(159, 173, 191));
    static final FRFont RING_VALUE_LABEL_FONT = FRFont.getInstance(FONT_NAME, Font.PLAIN, ChartConstants.AUTO_FONT_SIZE, new Color(232, 232, 232));//百分比圆环仪表盘值标签的默认样式
    static final FRFont POINTER_VALUE_LABEL_FONT = FRFont.getInstance(FONT_NAME, Font.PLAIN, ChartConstants.AUTO_FONT_SIZE, new Color(232, 232, 232));//多指针仪表盘值标签的默认样式
    static final FRFont POINTER_CATE_LABEL_FONT = FRFont.getInstance(FONT_NAME, Font.PLAIN, 13, new Color(232, 232, 232));//多指针仪表盘分类标签的默认样式

    static final Color MAP_NULL_VALUE_COLOR = new Color(168, 168, 168);
    static final Color MAP_BORDER = Color.BLACK;
    static final Color MAP_LEGEND_BACK = Color.BLACK;
    static final Color MAP_TITLE_BACK = Color.BLACK;

    static final Color DRILL_MAP_DRILL_TOOLS_BACK = Color.BLACK;
    static final float DRILL_MAP_DRILL_TOOLS_BACK_OPACITY = 0.8f;

    static final Background BACK = null;

    static String COLORS = null;

    static {
        ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();
        try {
            DefaultStyleConstants.COLORS = CodeUtils.cjkDecode("\u7ecf\u5178\u9ad8\u4eae");
            // 没有经典高亮, 用新特性
            if (config.getPreStyle(DefaultStyleConstants.COLORS) == null) {
                DefaultStyleConstants.COLORS = CodeUtils.cjkDecode("\u65b0\u7279\u6027");
            }
            // 没有新特性, 用第一个配色
            if (config.getPreStyle(DefaultStyleConstants.COLORS) == null) {
                if (config.names().hasNext()) {

                    String name = GeneralUtils.objectToString(config.names().next());
                    if (config.getPreStyle(name) != null) {
                        DefaultStyleConstants.COLORS = name;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
