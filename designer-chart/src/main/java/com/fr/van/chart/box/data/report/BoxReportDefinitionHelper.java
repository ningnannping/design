package com.fr.van.chart.box.data.report;

import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.plugin.chart.box.data.VanBoxReportDefinition;
import com.fr.plugin.chart.box.data.VanBoxReportDetailedDefinition;
import com.fr.plugin.chart.box.data.VanBoxReportResultDefinition;

public class BoxReportDefinitionHelper {

    public static VanBoxReportDefinition getBoxReportDefinition(ChartCollection collection) {
        if (collection != null) {

            Chart chart = collection.getSelectedChart();

            if (chart != null) {
                TopDefinitionProvider definitionProvider = chart.getFilterDefinition();

                if (definitionProvider instanceof VanBoxReportDefinition) {
                    return (VanBoxReportDefinition) definitionProvider;
                }
            }
        }

        return null;
    }

    public static VanBoxReportResultDefinition getBoxReportResultDefinition(ChartCollection collection) {
        VanBoxReportDefinition report = getBoxReportDefinition(collection);

        if (report != null) {
            return report.getResultDefinition();
        }

        return null;
    }

    public static VanBoxReportDetailedDefinition getBoxReportDetailedDefinition(ChartCollection collection) {
        VanBoxReportDefinition report = getBoxReportDefinition(collection);

        if (report != null) {
            return report.getDetailedDefinition();
        }

        return null;
    }

    public static boolean isDetailedReportDataType(ChartCollection collection) {
        VanBoxReportDefinition report = getBoxReportDefinition(collection);

        if (report != null) {
            return report.isDetailed();
        }

        return true;
    }
}
