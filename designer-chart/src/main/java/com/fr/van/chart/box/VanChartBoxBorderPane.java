package com.fr.van.chart.box;

import com.fr.base.background.ColorBackground;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.ispinner.chart.UISpinnerWithPx;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.plugin.chart.base.AttrBorderWithWidth;
import com.fr.stable.StringUtils;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.background.VanChartMarkerBackgroundPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class VanChartBoxBorderPane extends BasicBeanPane<AttrBorderWithWidth> {

    private VanChartMarkerBackgroundPane colorBackground;
    private UISpinner lineWidth;

    public VanChartBoxBorderPane() {
        colorBackground = new VanChartMarkerBackgroundPane() {

            protected Component[][] getPaneComponents() {
                return new Component[][]{
                        new Component[]{null, null},
                        new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Color")), typeComboBox},
                        new Component[]{null, centerPane},
                };
            }
        };
        lineWidth = new UISpinnerWithPx(0.5, Double.MAX_VALUE, 0.5, 0.5);

        Component[][] components = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Line_Width")), lineWidth},
                new Component[]{colorBackground, null}
        };

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p, p};

        JPanel panel = TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    public void populateBean(AttrBorderWithWidth border) {
        if (border.getBorderColor() != null) {
            this.colorBackground.populate(ColorBackground.getInstance(border.getBorderColor()));
        }

        this.lineWidth.setValue(border.getLineWidth());
    }

    public AttrBorderWithWidth updateBean() {
        AttrBorderWithWidth border = new AttrBorderWithWidth();

        ColorBackground colorBackground = this.colorBackground.update();

        if (colorBackground == null) {
            border.setBorderColor(null);
        } else {
            border.setBorderColor(colorBackground.getColor());
        }

        if (this.lineWidth != null) {
            border.setLineWidth(this.lineWidth.getValue());
        }

        return border;
    }

    protected String title4PopupWindow() {
        return StringUtils.EMPTY;
    }
}
