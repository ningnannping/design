package com.fr.van.chart.box.condition;

import com.fr.chart.base.DataSeriesCondition;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.i18n.Toolkit;
import com.fr.plugin.chart.base.VanChartAttrMarker;
import com.fr.plugin.chart.box.VanChartAttrOutlierMarker;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.component.VanChartMarkerPane;
import com.fr.van.chart.designer.other.condition.item.VanChartMarkerConditionPane;

public class VanChartBoxOutlierMarkerConditionPane extends VanChartMarkerConditionPane {

    public VanChartBoxOutlierMarkerConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    public String nameForPopupMenuItem() {
        return Toolkit.i18nText("Fine-Design_Chart_Outlier_Value");
    }

    protected String getItemLabelString() {
        return nameForPopupMenuItem();
    }

    public void setDefault() {
        VanChartAttrOutlierMarker outlierMarker = new VanChartAttrOutlierMarker();
        outlierMarker.setMarkerType(MarkerType.MARKER_CIRCLE_HOLLOW);

        markerPane.populate(outlierMarker);
    }

    protected void initMarkerPane() {
        markerPane = new VanChartMarkerPane() {

            protected VanChartAttrMarker createNewAttrMarker() {
                return new VanChartAttrOutlierMarker();
            }
        };
    }

    public void populate(DataSeriesCondition condition) {
        if (condition instanceof VanChartAttrOutlierMarker) {
            markerPane.populate((VanChartAttrOutlierMarker) condition);
        }
    }

    public DataSeriesCondition update() {
        return markerPane.update();
    }
}
