package com.fr.van.chart.designer.style.series;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ChartStylePane;
import com.fr.plugin.chart.range.VanChartRangeLegend;
import com.fr.plugin.chart.type.LegendType;
import com.fr.van.chart.designer.component.VanChartBeautyPane;
import com.fr.van.chart.designer.component.VanChartValueColorPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.CardLayout;
import java.awt.Dimension;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-03
 */
public abstract class VanChartColorValueSeriesPane extends VanChartMultiColorSeriesPane {

    private VanChartValueColorPane vanChartValueColorPane;

    public VanChartColorValueSeriesPane(ChartStylePane parent, Plot plot) {
        super(parent, plot);
    }


    public VanChartValueColorPane getVanChartValueColorPane() {
        return vanChartValueColorPane;
    }

    public void setVanChartValueColorPane(VanChartValueColorPane vanChartValueColorPane) {
        this.vanChartValueColorPane = vanChartValueColorPane;
    }

    protected JPanel createColorChoosePane() {
        vanChartFillStylePane = getVanChartFillStylePane();
        if (vanChartFillStylePane != null) {
            return super.createColorChoosePane();
        }
        return null;
    }

    protected UIButtonGroup<String> createDivideButton() {
        return new UIButtonGroup<>(new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Series"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value")
        }, new String[]{"series", "value"});
    }

    protected JPanel createColorDividePane() {
        vanChartValueColorPane = new VanChartValueColorPane((VanChartStylePane) parentPane);
        JPanel colorDividePane = new JPanel(new CardLayout()) {
            @Override
            public Dimension getPreferredSize() {
                if (getColorDivideButton().getSelectedIndex() == 0) {
                    return vanChartFillStylePane.getPreferredSize();
                } else {
                    return vanChartValueColorPane.getPreferredSize();
                }
            }
        };
        colorDividePane.add(vanChartFillStylePane, "series");
        colorDividePane.add(vanChartValueColorPane, "value");
        return colorDividePane;
    }

    //风格
    protected VanChartBeautyPane createStylePane() {
        return null;
    }

    /**
     * 保存 系列界面的属性到Plot
     */
    public void updateBean(Plot plot) {
        if (plot == null) {
            return;
        }
        super.updateBean(plot);
        if (getColorDivideButton() != null) {
            VanChartRangeLegend legend = (VanChartRangeLegend) plot.getLegend();
            int selectedIndex = getColorDivideButton().getSelectedIndex();
            if (selectedIndex == 0) {
                legend.setLegendType(LegendType.ORDINARY);
            } else {
                vanChartValueColorPane.updateBean(legend);
            }
        }
    }

    /**
     * 更新Plot的属性到系列界面
     */
    public void populateBean(Plot plot) {
        if (plot == null) {
            return;
        }
        super.populateBean(plot);
        if (getColorDivideButton() != null) {
            VanChartRangeLegend legend = (VanChartRangeLegend) plot.getLegend();
            LegendType legendType = legend.getLegendType();
            if (legendType == LegendType.ORDINARY) {
                getColorDivideButton().setSelectedIndex(0);
            } else {
                getColorDivideButton().setSelectedIndex(1);
            }
            vanChartValueColorPane.populateBean(legend);
            checkCardPane();
        }
    }
}
