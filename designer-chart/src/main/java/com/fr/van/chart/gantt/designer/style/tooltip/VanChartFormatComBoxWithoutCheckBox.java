package com.fr.van.chart.gantt.designer.style.tooltip;

public class VanChartFormatComBoxWithoutCheckBox extends VanChartFormatComBoxWithCheckBox {

    protected boolean showSelectBox() {
        return false;
    }
}
