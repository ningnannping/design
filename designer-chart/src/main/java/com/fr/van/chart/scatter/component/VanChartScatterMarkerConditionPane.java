package com.fr.van.chart.scatter.component;

import com.fr.chart.chartglyph.Marker;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.plugin.chart.marker.type.MarkerType;
import com.fr.van.chart.designer.component.VanChartMarkerPane;
import com.fr.van.chart.designer.component.marker.VanChartCommonMarkerPane;
import com.fr.van.chart.designer.other.condition.item.VanChartMarkerConditionPane;

public class VanChartScatterMarkerConditionPane extends VanChartMarkerConditionPane {

    public VanChartScatterMarkerConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane);
    }

    protected void initMarkerPane() {
        markerPane = new VanChartMarkerPane() {
            protected VanChartCommonMarkerPane createCommonMarkerPane() {

                return new VanChartCommonMarkerPane() {
                    protected Marker[] getMarkers() {
                        return getNormalMarkersWithCustom(new MarkerType[]{MarkerType.MARKER_AUTO});
                    }
                };
            }
        };
    }
}
