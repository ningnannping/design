package com.fr.van.chart.scatter;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipSizeFormat;
import com.fr.plugin.chart.base.format.AttrTooltipXFormat;
import com.fr.plugin.chart.base.format.AttrTooltipYFormat;
import com.fr.plugin.chart.scatter.attr.ScatterAttrTooltipContent;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldButton;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListener;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;

public class VanChartScatterRichTextFieldListPane extends VanChartFieldListPane {

    private VanChartFieldButton richTextXFormatPane;
    private VanChartFieldButton richTextYFormatPane;
    private VanChartFieldButton richTextSizeFormatPane;

    public VanChartScatterRichTextFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditorPane) {
        super(fieldAttrPane, richEditorPane);
    }

    public VanChartFieldButton getRichTextXFormatPane() {
        return richTextXFormatPane;
    }

    public VanChartFieldButton getRichTextYFormatPane() {
        return richTextYFormatPane;
    }

    public VanChartFieldButton getRichTextSizeFormatPane() {
        return richTextSizeFormatPane;
    }

    protected void initDefaultFieldButton() {
        super.initDefaultFieldButton();

        VanChartFieldListener listener = getFieldListener();

        richTextXFormatPane = new VanChartFieldButton("x", new AttrTooltipXFormat(), false, listener);
        richTextYFormatPane = new VanChartFieldButton("y", new AttrTooltipYFormat(), false, listener);
        richTextSizeFormatPane = new VanChartFieldButton(Toolkit.i18nText("Fine-Design_Chart_Use_Value"), new AttrTooltipSizeFormat(), false, listener);
    }

    protected void addDefaultFieldButton(JPanel fieldPane) {
        fieldPane.add(getSeriesNameButton());
        fieldPane.add(richTextXFormatPane);
        fieldPane.add(richTextYFormatPane);
        fieldPane.add(richTextSizeFormatPane);
    }

    protected List<VanChartFieldButton> getDefaultFieldButtonList() {
        List<VanChartFieldButton> fieldButtonList = new ArrayList<>();

        fieldButtonList.add(getSeriesNameButton());
        fieldButtonList.add(richTextXFormatPane);
        fieldButtonList.add(richTextYFormatPane);
        fieldButtonList.add(richTextSizeFormatPane);

        return fieldButtonList;
    }

    public void populateDefaultField(AttrTooltipContent tooltipContent) {
        super.populateDefaultField(tooltipContent);

        if (tooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatter = (ScatterAttrTooltipContent) tooltipContent;

            populateButtonFormat(richTextXFormatPane, scatter.getRichTextXFormat());
            populateButtonFormat(richTextYFormatPane, scatter.getRichTextYFormat());
            populateButtonFormat(richTextSizeFormatPane, scatter.getRichTextSizeFormat());
        }
    }

    public void updateDefaultField(AttrTooltipContent tooltipContent) {
        super.updateDefaultField(tooltipContent);

        if (tooltipContent instanceof ScatterAttrTooltipContent) {
            ScatterAttrTooltipContent scatter = (ScatterAttrTooltipContent) tooltipContent;

            updateButtonFormat(richTextXFormatPane, scatter.getRichTextXFormat());
            updateButtonFormat(richTextYFormatPane, scatter.getRichTextYFormat());
            updateButtonFormat(richTextSizeFormatPane, scatter.getRichTextSizeFormat());
        }
    }
}
