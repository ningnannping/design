package com.fr.van.chart.box;

import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Plot;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.base.VanChartTools;
import com.fr.plugin.chart.box.BoxIndependentVanChart;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.van.chart.designer.type.AbstractVanChartTypePane;

public class VanChartBoxPlotPane extends AbstractVanChartTypePane {

    protected String[] getTypeIconPath() {
        return new String[]{"/com/fr/van/chart/box.images/box.png"
        };
    }

    protected Plot getSelectedClonedPlot() {
        VanChartBoxPlot newPlot = null;

        Chart[] boxChartGroup = BoxIndependentVanChart.BoxVanChartTypes;

        for (int i = 0, len = boxChartGroup.length; i < len; i++) {
            if (typeDemo.get(i).isPressing) {
                newPlot = boxChartGroup[i].getPlot();
            }
        }

        Plot cloned = null;

        try {
            if (newPlot != null) {
                cloned = (Plot) newPlot.clone();
            }
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error In ColumnChart");
        }

        return cloned;
    }

    public Chart getDefaultChart() {
        return BoxIndependentVanChart.BoxVanChartTypes[0];
    }

    protected VanChartTools createVanChartTools() {
        VanChartTools tools = new VanChartTools();
        tools.setSort(false);
        return tools;
    }
}
