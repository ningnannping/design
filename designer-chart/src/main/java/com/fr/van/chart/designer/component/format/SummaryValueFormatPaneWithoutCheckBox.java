package com.fr.van.chart.designer.component.format;

import com.fr.design.i18n.Toolkit;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-10
 */
public class SummaryValueFormatPaneWithoutCheckBox extends VanChartFormatPaneWithoutCheckBox {

    public SummaryValueFormatPaneWithoutCheckBox(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected String getCheckBoxText() {
        return Toolkit.i18nText("Fine-Design_Chart_Use_Summary_Value");
    }
}

