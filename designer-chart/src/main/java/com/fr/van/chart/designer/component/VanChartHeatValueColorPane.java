package com.fr.van.chart.designer.component;

import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.gui.frpane.UINumberDragPane;
import com.fr.design.gui.ilable.BoldFontTextLabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.ColorSelectBoxWithOutTransparent;
import com.fr.design.mainframe.chart.gui.style.series.MapColorPickerPaneWithFormula;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.designer.style.axis.component.MinMaxValuePaneWithOutTick;
import com.fr.van.chart.range.component.GradualIntervalConfigPane;
import com.fr.van.chart.range.component.GradualLegendPane;
import com.fr.van.chart.range.component.LegendGradientBar;
import com.fr.van.chart.range.component.SectionIntervalConfigPaneWithOutNum;
import com.fr.van.chart.range.component.SectionLegendPane;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-08-04
 */
public class VanChartHeatValueColorPane extends VanChartValueColorPane {

    public VanChartHeatValueColorPane(VanChartStylePane parent) {
        super(parent);
    }

    protected GradualLegendPane createGradualLegendPane() {
        return new GradualLegendPane() {
            @Override
            protected GradualIntervalConfigPane createGradualIntervalConfigPane() {
                return new GradualIntervalConfigPane() {
                    @Override
                    protected Component[][] getPaneComponents(MinMaxValuePaneWithOutTick minMaxValuePane, ColorSelectBoxWithOutTransparent colorSelectBox, UINumberDragPane numberDragPane, LegendGradientBar legendGradientBar) {
                        return new Component[][]{
                                new Component[]{minMaxValuePane, null},
                                new Component[]{new BoldFontTextLabel(Toolkit.i18nText("Fine-Design_Chart_Value_Divided_Stage")), numberDragPane},
                                new Component[]{null, legendGradientBar}
                        };
                    }
                };
            }
        };
    }

    protected SectionLegendPane createSectionLegendPane() {
        return new SectionLegendPane(getVanChartStylePane()) {
            @Override
            protected MapColorPickerPaneWithFormula createSectionIntervalConfigPane(AbstractAttrNoScrollPane parent) {
                return new SectionIntervalConfigPaneWithOutNum(parent) {
                    private static final int WIDTH = 227;

                    @Override
                    protected int getBoundX() {
                        return 0;
                    }

                    @Override
                    protected int getBoundY() {
                        return 0;
                    }

                    @Override
                    protected int getBoundWidth() {
                        return WIDTH;
                    }
                };
            }
        };
    }

}
