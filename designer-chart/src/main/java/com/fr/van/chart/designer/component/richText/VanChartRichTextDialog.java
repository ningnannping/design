package com.fr.van.chart.designer.component.richText;

import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.gui.GUICoreUtils;

import java.awt.Dimension;
import java.awt.Frame;

public class VanChartRichTextDialog extends BasicDialog {

    public static final Dimension DEFAULT = new Dimension(960, 700);

    public VanChartRichTextDialog(Frame parent, BasicPane pane) {
        super(parent, pane);

        this.setTitle(Toolkit.i18nText("Fine-Design_Report_RichTextEditor"));
        this.setBasicDialogSize(DEFAULT);
        GUICoreUtils.centerWindow(this);
        this.setResizable(true);
        this.setModal(true);
    }

    protected void applyEnterAction() {

    }

    public void checkValid() {

    }
}
