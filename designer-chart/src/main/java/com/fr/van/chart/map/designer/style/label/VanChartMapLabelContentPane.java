package com.fr.van.chart.map.designer.style.label;

import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipAreaNameFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipMapValueFormat;
import com.fr.plugin.chart.base.format.AttrTooltipPercentFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipValueFormat;
import com.fr.plugin.chart.type.TextAlign;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartLabelContentPane;
import com.fr.van.chart.designer.component.format.MapAreaNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.PercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.map.designer.style.VanChartMapRichTextFieldListPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by Mitisky on 16/5/20.
 */
public class VanChartMapLabelContentPane extends VanChartLabelContentPane {

    private UICheckBox showAllSeries;
    private JPanel checkPane;

    public VanChartMapLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        setCategoryNameFormatPane(new MapAreaNameFormatPaneWithCheckBox(parent, showOnPane));
        setSeriesNameFormatPane(new SeriesNameFormatPaneWithCheckBox(parent, showOnPane));
        setValueFormatPane(new ValueFormatPaneWithCheckBox(parent, showOnPane));
        setPercentFormatPane(new PercentFormatPaneWithCheckBox(parent, showOnPane));
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartMapRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected JPanel getLabelContentPane(JPanel contentPane) {
        showAllSeries = new UICheckBox(Toolkit.i18nText("Fine-Design_Chart_Show_All_Series"));

        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
        double[] columnSize = {f, e};
        double p = TableLayout.PREFERRED;
        double[] row = {p, p};
        Component[][] components = {
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Display_Strategy")), showAllSeries}
        };
        checkPane = TableLayout4VanChartHelper.createGapTableLayoutPane(components, row, columnSize);

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(contentPane, BorderLayout.CENTER);
        panel.add(checkPane, BorderLayout.SOUTH);
        return createTableLayoutPaneWithTitle(Toolkit.i18nText("Fine-Design_Chart_Content"), panel);
    }

    protected void checkCardPane() {
        super.checkCardPane();
        checkPane.setVisible(getContent().getSelectedIndex() == COMMON_INDEX);
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Area_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Use_Percent")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipAreaNameFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipValueFormat(),
                new AttrTooltipPercentFormat()
        };
    }

    @Override
    protected AttrTooltipContent createAttrTooltip() {
        AttrTooltipContent content = new AttrTooltipContent(TextAlign.CENTER);
        content.setCategoryFormat(new AttrTooltipAreaNameFormat());
        content.setValueFormat(new AttrTooltipMapValueFormat());
        content.setRichTextCategoryFormat(new AttrTooltipAreaNameFormat());
        content.setRichTextValueFormat(new AttrTooltipMapValueFormat());
        return content;
    }

    public AttrTooltipContent updateBean() {
        AttrTooltipContent attrTooltipContent = super.updateBean();
        attrTooltipContent.setShowAllSeries(showAllSeries.isSelected());
        return attrTooltipContent;
    }

    public void populateBean(AttrTooltipContent attrTooltipContent) {
        super.populateBean(attrTooltipContent);
        showAllSeries.setSelected(attrTooltipContent.isShowAllSeries());
    }
}
