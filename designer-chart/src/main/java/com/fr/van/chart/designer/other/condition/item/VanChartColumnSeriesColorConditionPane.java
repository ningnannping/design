package com.fr.van.chart.designer.other.condition.item;

import com.fr.base.background.ColorBackground;
import com.fr.chart.base.AttrBackground;
import com.fr.chart.base.DataSeriesCondition;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.style.background.gradient.FixedGradientBar;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.plugin.chart.type.GradientType;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class VanChartColumnSeriesColorConditionPane extends AbstractNormalMultiLineConditionPane {

    private UIButtonGroup matchColorTypeBox;

    private ColorSelectBox colorSelectionBox;
    private FixedGradientBar colorGradient;

    private JPanel colorSelectPane;
    private JPanel colorGradientPane;

    public VanChartColumnSeriesColorConditionPane(ConditionAttributesPane conditionAttributesPane) {
        super(conditionAttributesPane, null);
    }

    protected String getItemLabelString() {
        return nameForPopupMenuItem();
    }

    protected JPanel initContentPane() {
        colorSelectionBox = new ColorSelectBox(80);
        colorGradient = new FixedGradientBar(4, 150);

        colorSelectPane = createJPanelWithComponent(colorSelectionBox);
        colorGradientPane = createJPanelWithComponent(colorGradient);

        JPanel panel = new JPanel();

        panel.setLayout(new BorderLayout());

        panel.add(initColorButtonPane(), BorderLayout.NORTH);
        panel.add(colorSelectPane, BorderLayout.CENTER);
        panel.add(colorGradientPane, BorderLayout.SOUTH);

        initListener();

        return panel;
    }

    private JPanel initColorButtonPane() {
        String[] names = new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Solid_Color"),
                Toolkit.i18nText("Fine-Design_Chart_Style_TopDownShade")
        };

        matchColorTypeBox = new UIButtonGroup(names);

        double p = TableLayout.PREFERRED;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        Component[][] components = new Component[][]{new Component[]{matchColorTypeBox}};

        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, new double[]{p}, new double[]{e});
    }

    private void initListener() {
        matchColorTypeBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                checkColorPaneVisible();
            }
        });
    }

    private void checkColorPaneVisible() {
        if (colorSelectPane != null) {
            colorSelectPane.setVisible(matchColorTypeBox.getSelectedIndex() == 0);
        }
        if (colorGradientPane != null) {
            colorGradientPane.setVisible(matchColorTypeBox.getSelectedIndex() == 1);
        }
    }

    public String nameForPopupMenuItem() {
        return Toolkit.i18nText("Fine-Design_Chart_Match_Color");
    }

    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Chart_Match_Color");
    }

    public void populate(DataSeriesCondition condition) {
        if (condition instanceof AttrBackground) {
            AttrBackground matchColor = (AttrBackground) condition;

            if (matchColor.getGradient() == GradientType.NONE) {
                this.matchColorTypeBox.setSelectedIndex(0);
            } else {
                this.matchColorTypeBox.setSelectedIndex(1);
            }

            ColorBackground seriesColor = (ColorBackground) matchColor.getSeriesBackground();

            if (seriesColor != null) {
                this.colorSelectionBox.setSelectObject(seriesColor.getColor());
            }

            this.colorGradient.updateColor(matchColor.getGradientStartColor(), matchColor.getGradientEndColor());
        }

        checkColorPaneVisible();
    }

    public DataSeriesCondition update() {
        AttrBackground matchColor = new AttrBackground();

        if (this.matchColorTypeBox.getSelectedIndex() == 0) {
            matchColor.setGradient(GradientType.NONE);
        } else {
            matchColor.setGradient(GradientType.CUSTOM);
        }

        matchColor.setSeriesBackground(ColorBackground.getInstance(this.colorSelectionBox.getSelectObject()));
        matchColor.setGradientStartColor(this.colorGradient.getSelectColorPointBtnP1().getColorInner());
        matchColor.setGradientEndColor(this.colorGradient.getSelectColorPointBtnP2().getColorInner());

        return matchColor;
    }

    public void setDefault() {
        this.populate(new AttrBackground());
    }

    private JPanel createJPanelWithComponent(Component component) {
        double p = TableLayout.PREFERRED;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        Component[][] components = new Component[][]{
                new Component[]{null},
                new Component[]{component}
        };

        return TableLayout4VanChartHelper.createGapTableLayoutPane(components, new double[]{p, p}, new double[]{e});
    }
}