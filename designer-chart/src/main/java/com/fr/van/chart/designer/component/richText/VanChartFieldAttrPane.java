package com.fr.van.chart.designer.component.richText;

import com.fr.data.util.function.AbstractDataFunction;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.style.FormatPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.mainframe.chart.gui.data.CalculateComboBox;
import com.fr.plugin.chart.base.format.IntervalTimeFormat;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.format.FormatPaneWithOutFont;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.text.Format;

public class VanChartFieldAttrPane extends JPanel {

    private FormatPane fieldFormatPane;
    private UIComboBox intervalTimeBox;
    private CalculateComboBox dataFunctionBox;

    private JPanel formatPane;
    private JPanel intervalTimePane;
    private JPanel fieldFunctionPane;

    public VanChartFieldAttrPane() {
        initComponents();

        this.setLayout(new BorderLayout());
        this.add(formatPane, BorderLayout.NORTH);
        this.add(fieldFunctionPane, BorderLayout.CENTER);
        this.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
    }

    private void initComponents() {
        double p = TableLayout.PREFERRED;
        double d = TableLayout4VanChartHelper.DESCRIPTION_AREA_WIDTH;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        fieldFormatPane = new FormatPaneWithOutFont() {
            protected JPanel createContentPane(Component[][] components) {
                return TableLayout4VanChartHelper.createGapTableLayoutPane(components, new double[]{p, p, p}, new double[]{d, e});
            }
        };
        intervalTimeBox = new UIComboBox(IntervalTimeFormat.getFormats());
        dataFunctionBox = new CalculateComboBox();

        Component[][] intervalTimeComponents = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Report_Base_Format"), SwingConstants.LEFT), intervalTimeBox}
        };
        Component[][] dataFunctionComponents = new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Summary_Method"), SwingConstants.LEFT), dataFunctionBox}
        };

        intervalTimePane = TableLayout4VanChartHelper.createGapTableLayoutPane(intervalTimeComponents, new double[]{p, p}, new double[]{d, e});

        formatPane = new JPanel(new BorderLayout());
        formatPane.add(fieldFormatPane, BorderLayout.NORTH);
        formatPane.add(intervalTimePane, BorderLayout.CENTER);

        fieldFunctionPane = TableLayout4VanChartHelper.createGapTableLayoutPane(dataFunctionComponents, new double[]{p, p}, new double[]{d, e});
    }

    public void registerFormatListener(UIObserverListener listener) {
        fieldFormatPane.registerChangeListener(listener);
        intervalTimeBox.registerChangeListener(listener);
    }

    public void registerFunctionListener(ActionListener listener) {
        dataFunctionBox.addActionListener(listener);
    }

    public void populate(Format format, IntervalTimeFormat intervalTime, AbstractDataFunction dataFunction, boolean showDataFunction, boolean showIntervalTime) {
        fieldFormatPane.populateBean(format);
        intervalTimeBox.setSelectedItem(intervalTime);
        dataFunctionBox.populateBean(dataFunction);

        fieldFormatPane.setVisible(!showIntervalTime);
        intervalTimePane.setVisible(showIntervalTime);
        fieldFunctionPane.setVisible(showDataFunction);
    }

    public Format updateFormat() {
        return fieldFormatPane.update();
    }

    public IntervalTimeFormat updateIntervalTime() {
        return (IntervalTimeFormat) intervalTimeBox.getSelectedItem();
    }

    public AbstractDataFunction updateDataFunction() {
        return dataFunctionBox.updateBean();
    }
}
