package com.fr.van.chart.range.component;

import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.chart.gui.style.series.MapColorPickerPaneWithFormula;
import com.fr.plugin.chart.range.SectionLegend;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class SectionLegendPane extends JPanel {
    private static final long serialVersionUID = 1614283200308877353L;

    private MapColorPickerPaneWithFormula intervalConfigPane;
    private AbstractAttrNoScrollPane parent;

    public SectionLegendPane(AbstractAttrNoScrollPane parent) {
        this.parent = parent;
        initComponents();
    }

    private void initComponents() {
        intervalConfigPane = createSectionIntervalConfigPane(this.parent);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] col = {f};
        double[] row = {p};
        Component[][] components = new Component[][]{
                new Component[]{intervalConfigPane},
        };

        JPanel panel = TableLayoutHelper.createTableLayoutPane(components, row, col);

        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }

    protected MapColorPickerPaneWithFormula createSectionIntervalConfigPane(AbstractAttrNoScrollPane parent) {
        return new SectionIntervalConfigPane(parent) {
            private static final int WIDTH = 225;

            @Override
            protected int getBoundX() {
                return 0;
            }

            @Override
            protected int getBoundY() {
                return 0;
            }

            @Override
            protected int getBoundWidth() {
                return WIDTH;
            }
        };
    }

    public void populate(SectionLegend sectionLegend) {
        if (intervalConfigPane != null) {
            intervalConfigPane.populateBean(sectionLegend.getMapHotAreaColor());
        }
    }

    public void update(SectionLegend sectionLegend) {
        if (intervalConfigPane != null) {
            intervalConfigPane.updateBean(sectionLegend.getMapHotAreaColor());
        }
    }
}