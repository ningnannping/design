package com.fr.van.chart.box;

import com.fr.chart.chartattr.Plot;
import com.fr.plugin.chart.base.AttrTooltip;
import com.fr.plugin.chart.box.VanChartBoxPlot;
import com.fr.plugin.chart.box.attr.AttrBoxTooltip;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.designer.style.tooltip.VanChartPlotTooltipPane;

public class VanChartBoxPlotTooltipPane extends VanChartPlotTooltipPane {

    public VanChartBoxPlotTooltipPane(Plot plot, VanChartStylePane parent) {
        super(plot, parent);
    }

    protected void initTooltipContentPane(Plot plot) {
        boolean isDetailed = ((VanChartBoxPlot) plot).isDetailed();
        tooltipContentPane = new VanChartBoxTooltipContentPane(parent, VanChartBoxPlotTooltipPane.this, isDetailed);
    }

    protected AttrTooltip getAttrTooltip() {
        return new AttrBoxTooltip();
    }

    protected boolean hasTooltipSeriesType() {
        return false;
    }
}
