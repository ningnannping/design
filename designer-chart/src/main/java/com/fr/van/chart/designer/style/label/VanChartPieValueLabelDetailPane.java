package com.fr.van.chart.designer.style.label;

import com.fr.chart.chartattr.Plot;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.utils.gui.UIComponentUtils;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.background.VanChartBackgroundWithOutImagePane;
import com.fr.van.chart.designer.component.border.VanChartBorderWithShapePane;
import com.fr.van.chart.designer.style.VanChartStylePane;
import com.fr.van.chart.pie.style.VanChartPieValueLabelContentPane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-12-09
 */
public class VanChartPieValueLabelDetailPane extends VanChartPlotLabelDetailPane {

    public VanChartPieValueLabelDetailPane(Plot plot, VanChartStylePane parent, boolean inCondition) {
        super(plot, parent, inCondition);
    }

    protected void initToolTipContentPane(Plot plot) {
        setDataLabelContentPane(new VanChartPieValueLabelContentPane(getParentPane(), this, isInCondition()));
    }

    protected JPanel getLabelLayoutPane(JPanel panel, String title) {
        return panel;
    }

    protected JPanel createBorderAndBackgroundPane() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 10));
        jPanel.add(createLabelBorderPane(), BorderLayout.NORTH);
        jPanel.add(createLabelBackgroundPane(), BorderLayout.CENTER);
        return jPanel;
    }

    protected JPanel createLabelBorderPane() {
        VanChartBorderWithShapePane borderPane = new VanChartBorderWithShapePane() {
            @Override
            protected JPanel createLineTypePane() {
                double p = TableLayout.PREFERRED;
                double f = TableLayout.FILL;
                double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

                double[] columnSize = {f, e};
                double[] rowSize = {p};

                Component[][] components = new Component[][]{
                        new Component[]{FRWidgetFactory.createLineWrapLabel(Toolkit.i18nText("Fine-Design_Chart_Border")),
                                UIComponentUtils.wrapWithBorderLayoutPane(getLineTypeBox())}};

                return TableLayout4VanChartHelper.createGapTableLayoutPane(components, rowSize, columnSize);
            }
        };
        setBorderPane(borderPane);
        return borderPane;
    }

    protected JPanel createLabelBackgroundPane() {
        VanChartBackgroundWithOutImagePane backgroundPane = new VanChartBackgroundWithOutImagePane() {

            @Override
            protected JPanel initContentPanel() {
                double p = TableLayout.PREFERRED;
                double f = TableLayout.FILL;
                double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;
                double[] columnSize = {f, e};
                double[] rowSize = {p, p, p};
                return TableLayout4VanChartHelper.createGapTableLayoutPane(getPaneComponents(), rowSize, columnSize);
            }

            @Override
            protected Component[][] getPaneComponents() {
                return new Component[][]{
                        new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Basic_Utils_Background")), typeComboBox},
                        new Component[]{null, centerPane},
                        new Component[]{getTransparentLabel(), transparent},
                };
            }
        };
        setBackgroundPane(backgroundPane);
        return backgroundPane;
    }
}
