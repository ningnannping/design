package com.fr.van.chart.config;

import com.fr.base.ChartColorMatching;
import com.fr.base.ChartPreStyleConfig;
import com.fr.chart.base.AttrBorder;
import com.fr.chart.base.AttrFillStyle;
import com.fr.chart.base.ChartConstants;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.chart.chartglyph.DataSheet;
import com.fr.config.predefined.ColorFillStyle;
import com.fr.design.mainframe.chart.mode.ChartEditContext;
import com.fr.plugin.chart.PiePlot4VanChart;
import com.fr.plugin.chart.attr.axis.VanChartAlertValue;
import com.fr.plugin.chart.attr.axis.VanChartAxis;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.attr.plot.VanChartRectanglePlot;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.base.AttrLabelDetail;
import com.fr.plugin.chart.custom.type.CustomPlotType;
import com.fr.plugin.chart.gauge.VanChartGaugePlot;
import com.fr.plugin.chart.map.VanChartMapPlot;
import com.fr.plugin.chart.type.GaugeStyle;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/10/22
 * vanchart的抽出来，因为主体代码 新增y轴 组合图新增图表都要用
 */
public class DefaultStyleHelper4Van {

    private static boolean duchampMode() {
        try {
            return ChartEditContext.duchampMode();
        } catch (Throwable e) {
            //tomcat预览 默认数据的图表
            return true;
        }
    }

    public static void dealVanPlot4Custom(VanChartPlot plot, CustomPlotType customPlotType) {
        if (!duchampMode()) {
            return;
        }
        dealVanPlotCommonAttr(plot);
        switch (customPlotType) {
            case PIE:
            case DIFFERENT_PIE:
            case SAME_PIE:
                dealPie(plot);
                break;
            case SLOT:
            case POINTER_360:
            case POINTER_180:
            case CUVETTE:
            case RING:
                dealGauge(plot);
                break;
            case COLUMN:
            case LINE:
            case AREA:
            case BUBBLE:
            case RADAR:
            case STACK_RADAR:
            case SCATTER:
            default:
                break;
        }

    }

    public static VanChartAxis dealAxisDefault(VanChartAxis axis) {
        if (!duchampMode()) {
            return axis;
        }
        axis.getTitle().getTextAttr().setFRFont(DefaultStyleConstants.AXIS_TITLE);
        axis.getTextAttr().setFRFont(DefaultStyleConstants.AXIS_LABEL);
        axis.setAxisColor(DefaultStyleConstants.AXIS_LINE);
        axis.setMainGridColor(DefaultStyleConstants.GRID_LINE);
        return axis;
    }

    public static void dealAxisAlert(VanChartAlertValue vanChartAlertValue) {
        if (!duchampMode()) {
            return;
        }
        vanChartAlertValue.setAlertFont(DefaultStyleConstants.ALERT_FONT);
    }

    static void dealVanPlotCommonAttr(Plot plot) {
        if (!duchampMode()) {
            return;
        }
        if (plot instanceof VanChartPlot) {
            VanChartPlot vanChartPlot = (VanChartPlot) plot;

            ChartPreStyleConfig manager = ChartPreStyleConfig.getInstance();
            Object preStyle = manager.getPreStyle(DefaultStyleConstants.COLORS);
            if (preStyle instanceof ChartColorMatching) {
                ColorFillStyle colorFillStyle = new ColorFillStyle();
                //default是默认的意思，为服务器默认配色方案
                //acc为多个颜色组合
                //gradient为渐变颜色
                colorFillStyle.setColorStyle(ChartConstants.COLOR_ACC);
                colorFillStyle.setFillStyleName(DefaultStyleConstants.COLORS);
                colorFillStyle.setColorList(((ChartColorMatching) preStyle).getColorList());

                AttrFillStyle plotFillStyle = vanChartPlot.getPlotFillStyle();
                if (plotFillStyle == null) {
                    plotFillStyle = new AttrFillStyle();
                    vanChartPlot.setPlotFillStyle(plotFillStyle);
                }
                plotFillStyle.setColorFillStyle(colorFillStyle);
            }


            if (vanChartPlot.getLegend() != null) {
                vanChartPlot.getLegend().setFRFont(DefaultStyleConstants.LEGEND);
            }

            if (vanChartPlot instanceof VanChartRectanglePlot) {
                VanChartRectanglePlot vanChartRectanglePlot = (VanChartRectanglePlot) vanChartPlot;

                for (VanChartAxis axis : vanChartRectanglePlot.getXAxisList()) {
                    dealAxisDefault(axis);
                }
                for (VanChartAxis axis : vanChartRectanglePlot.getYAxisList()) {
                    dealAxisDefault(axis);
                }
            }

            dealBorder(vanChartPlot);

            DataSheet dataSheet = vanChartPlot.getDataSheet();
            dataSheet.getTextAttr().setFRFont(DefaultStyleConstants.DATA_SHEET);
            dataSheet.setBorderColor(DefaultStyleConstants.DATA_SHEET_BORDER);
        }

    }

    private static void dealBorder(VanChartPlot vanChartPlot) {
        ConditionAttr defaultAttr = vanChartPlot.getConditionCollection().getDefaultAttr();
        AttrBorder attrBorder = defaultAttr.getExisted(AttrBorder.class);
        if (attrBorder != null) {
            attrBorder.setBorderColor(DefaultStyleConstants.BORDER);
            attrBorder.setBorderStyle(defaultBorderWidth(vanChartPlot));
        }
    }

    private static int defaultBorderWidth(VanChartPlot vanChartPlot) {
        //pie multipie treemap map drillmap heatmap
        if (vanChartPlot instanceof PiePlot4VanChart
                || vanChartPlot instanceof VanChartMapPlot) {
            return DefaultStyleConstants.SPECIAL_BORDER_WIDTH;
        }
        return DefaultStyleConstants.BORDER_WIDTH;
    }

    static void dealPie(Plot plot) {
        if (plot instanceof PiePlot4VanChart) {
            ConditionAttr defaultAttr = plot.getConditionCollection().getDefaultAttr();
            AttrLabel attrLabel = defaultAttr.getExisted(AttrLabel.class);
            if (attrLabel == null) {
                attrLabel = ((PiePlot4VanChart) plot).getDefaultAttrLabel();
                defaultAttr.addDataSeriesCondition(attrLabel);
            }
            attrLabel.getSecondLabelDetail().getTextAttr().setFRFont(DefaultStyleConstants.PIE_CATEGORY_LABEL);
        }
    }

    static void dealGauge(Plot param) {
        if (!(param instanceof VanChartGaugePlot)) {
            return;
        }
        VanChartGaugePlot plot = (VanChartGaugePlot) param;
        plot.getGaugeDetailStyle().setPaneBackgroundColor(plot.getGaugeStyle() == GaugeStyle.RING ? DefaultStyleConstants.GAUGE_PANE_BACK_COLOR_4_RING : DefaultStyleConstants.GAUGE_PANE_BACK_COLOR);
        plot.getGaugeDetailStyle().setSlotBackgroundColor(DefaultStyleConstants.GAUGE_SLOT_BACKGROUND_COLOR);
        plot.getGaugeDetailStyle().setHingeColor(DefaultStyleConstants.GAUGE_HINGE);

        ConditionAttr defaultAttr = plot.getConditionCollection().getDefaultAttr();
        AttrLabel attrLabel = defaultAttr.getExisted(AttrLabel.class);
        if (attrLabel != null) {
            AttrLabelDetail detail = attrLabel.getAttrLabelDetail();
            AttrLabelDetail valueDetail = attrLabel.getGaugeValueLabelDetail();

            switch (plot.getGaugeStyle()) {
                case POINTER:
                case POINTER_SEMI:
                    detail.getContent().getCategoryFormat().setEnable(false);
                    detail.getTextAttr().setFRFont(DefaultStyleConstants.POINTER_CATE_LABEL_FONT);
                    valueDetail.getTextAttr().setFRFont(DefaultStyleConstants.POINTER_VALUE_LABEL_FONT);
                    break;
                case RING:
                case SLOT:
                    valueDetail.getTextAttr().setFRFont(DefaultStyleConstants.RING_VALUE_LABEL_FONT);
                    break;
                case THERMOMETER:
                    detail.getTextAttr().setFRFont(DefaultStyleConstants.THERMOMETER_LABEL_FONT);
                    valueDetail.getTextAttr().setFRFont(DefaultStyleConstants.THERMOMETER_LABEL_FONT);
                    plot.getGaugeAxis().getTextAttr().setFRFont(DefaultStyleConstants.THERMOMETER_AXIS_LABEL);
                    break;
            }

        }
    }
}
