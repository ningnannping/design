package com.fr.van.chart.designer.component;

import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.type.TextAlign;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;

public class VanChartLabelContentPane extends VanChartTooltipContentPane {

    private static final long serialVersionUID = 5630276526789839288L;

    public VanChartLabelContentPane(VanChartStylePane parent, JPanel showOnPane, boolean inCondition) {
        super(parent, showOnPane, inCondition);
    }

    protected VanChartHtmlLabelPane createHtmlLabelPane() {
        return new VanChartHtmlLabelPaneWithBackGroundLabel();
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new AttrTooltipContent(TextAlign.CENTER);
    }

    //TODO Bjorn 标签面板回退
   /* public JPanel createCommonStylePane() {
        if (isInCondition()) {
            return super.createCommonStylePane();
        }
        setTextAttrPane(new ChartTextAttrPaneWithPreStyle());

        JPanel stylePanel = new JPanel(new BorderLayout());
        stylePanel.add(getTextAttrPane(), BorderLayout.CENTER);

        return stylePanel;
    }

    public void updateTextAttr(AttrTooltipContent attrTooltipContent) {
        if (isInCondition()) {
            super.updateTextAttr(attrTooltipContent);
            return;
        }
        if (hasTextStylePane()) {
            this.getTextAttrPane().update(attrTooltipContent.getTextAttr());
            if (!attrTooltipContent.getTextAttr().isPredefinedStyle()) {
                attrTooltipContent.setCustom(true);
            }
        }
    }*/
}
