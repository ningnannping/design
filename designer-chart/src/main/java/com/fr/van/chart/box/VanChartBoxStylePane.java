package com.fr.van.chart.box;

import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.van.chart.designer.style.VanChartStylePane;

import java.util.List;

public class VanChartBoxStylePane extends VanChartStylePane {

    public VanChartBoxStylePane(AttributeChangeListener listener) {
        super(listener);
    }

    protected void createVanChartLabelPane(List<BasicPane> paneList) {
    }

    protected void addVanChartTooltipPane(List<BasicPane> paneList) {
        paneList.add(new VanChartBoxTooltipPane(VanChartBoxStylePane.this));
    }
}
