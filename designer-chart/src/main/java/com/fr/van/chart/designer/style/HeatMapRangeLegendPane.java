package com.fr.van.chart.designer.style;

import javax.swing.JPanel;

/**
 * Created by Mitisky on 16/10/20.
 * 只有渐变色图例和区域段图例.
 * 没有主题配色
 */
public class HeatMapRangeLegendPane extends MapRangeLegendPane {

    public HeatMapRangeLegendPane() {
    }

    public HeatMapRangeLegendPane(VanChartStylePane parent) {
        super(parent);
    }

    protected JPanel createCommonLegendPane(){
        return this.createLegendPaneWithoutFixedCheck();
    }
}
