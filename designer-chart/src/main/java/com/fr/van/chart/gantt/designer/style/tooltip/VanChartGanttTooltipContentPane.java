package com.fr.van.chart.gantt.designer.style.tooltip;

import com.fr.design.i18n.Toolkit;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipDurationFormat;
import com.fr.plugin.chart.base.format.AttrTooltipEndTimeFormat;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipProcessesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipProgressFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.base.format.AttrTooltipStartTimeFormat;
import com.fr.plugin.chart.gantt.attr.AttrGanttTooltipContent;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.VanChartFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by hufan on 2017/1/13.
 */
public class VanChartGanttTooltipContentPane extends VanChartTooltipContentPane {
    private VanChartFormatPaneWithCheckBox processesFormatPane;
    private VanChartDateFormatPaneWithCheckBox startTimeFormatPane;
    private VanChartDateFormatPaneWithCheckBox endTimeFormatPane;
    private VanChartFormatComBoxWithCheckBox durationFormatPane;
    private VanChartFormatPaneWithCheckBox progressFormatPane;

    public VanChartGanttTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(parent, showOnPane);
    }

    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane){
        processesFormatPane = new VanChartFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Project_Name");
            }
        };
        setSeriesNameFormatPane(new SeriesNameFormatPaneWithCheckBox(parent, showOnPane));
        startTimeFormatPane = new VanChartDateFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Start_Time");
            }
        };
        endTimeFormatPane = new VanChartDateFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_End_Time");
            }
        };
        durationFormatPane = new VanChartFormatComBoxWithCheckBox();
        progressFormatPane = new VanChartFormatPaneWithCheckBox(parent, showOnPane){
            @Override
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Process");
            }
        };
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                return new VanChartGanttRichTextFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Project_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Start_Time"),
                Toolkit.i18nText("Fine-Design_Chart_End_Time"),
                Toolkit.i18nText("Fine-Design_Chart_Duration_Time"),
                Toolkit.i18nText("Fine-Design_Chart_Process")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        return new AttrTooltipFormat[]{
                new AttrTooltipProcessesFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipStartTimeFormat(),
                new AttrTooltipEndTimeFormat(),
                new AttrTooltipDurationFormat(),
                new AttrTooltipProgressFormat()
        };
    }

    protected Component[][] getPaneComponents(){
        return new Component[][]{
                new Component[]{processesFormatPane,null},
                new Component[]{getSeriesNameFormatPane(),null},
                new Component[]{startTimeFormatPane,null},
                new Component[]{endTimeFormatPane,null},
                new Component[]{durationFormatPane, null},
                new Component[]{progressFormatPane, null}
        };
    }

    protected double[] getRowSize(double p){
        return new double[]{p,p,p,p,p,p};
    }

    @Override
    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        // fixme 当前的样式面板设计都是基于一个大而全的父类，在子类中组合需要的属性，导致父类的属性多是protected，且子类不能明确获取自己Model的type
        if (attrTooltipContent instanceof AttrGanttTooltipContent){
            AttrGanttTooltipContent ganttTooltipContent = (AttrGanttTooltipContent) attrTooltipContent;
            processesFormatPane.populate(ganttTooltipContent.getProcessesFormat());
            getSeriesNameFormatPane().populate(ganttTooltipContent.getSeriesFormat());
            startTimeFormatPane.populate(ganttTooltipContent.getStartTimeFormat());
            endTimeFormatPane.populate(ganttTooltipContent.getEndTimeFormat());
            durationFormatPane.populate(ganttTooltipContent.getDurationFormat());
            progressFormatPane.populate(ganttTooltipContent.getProgressFormat());
        }
    }

    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        if (attrTooltipContent instanceof AttrGanttTooltipContent){
            AttrGanttTooltipContent ganttTooltipContent = (AttrGanttTooltipContent) attrTooltipContent;
            processesFormatPane.update(ganttTooltipContent.getProcessesFormat());
            getSeriesNameFormatPane().update(ganttTooltipContent.getSeriesFormat());
            startTimeFormatPane.update(ganttTooltipContent.getStartTimeFormat());
            endTimeFormatPane.update(ganttTooltipContent.getEndTimeFormat());
            durationFormatPane.update(ganttTooltipContent.getDurationFormat());
            progressFormatPane.update(ganttTooltipContent.getProgressFormat());
        }
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        super.updateTooltipFormat(target, source);

        if (target instanceof AttrGanttTooltipContent && source instanceof AttrGanttTooltipContent) {
            AttrGanttTooltipContent targetGantt = (AttrGanttTooltipContent) target;
            AttrGanttTooltipContent sourceGantt = (AttrGanttTooltipContent) source;

            targetGantt.setRichTextProcessesFormat(sourceGantt.getRichTextProcessesFormat());
            targetGantt.setRichTextStartTimeFormat(sourceGantt.getRichTextStartTimeFormat());
            targetGantt.setRichTextEndTimeFormat(sourceGantt.getRichTextEndTimeFormat());
            targetGantt.setRichTextDurationFormat(sourceGantt.getRichTextDurationFormat());
            targetGantt.setRichTextProgressFormat(sourceGantt.getRichTextProgressFormat());
        }
    }

    public boolean isDirty() {
        return processesFormatPane.isDirty()
                || getSeriesNameFormatPane().isDirty()
                || startTimeFormatPane.isDirty()
                || endTimeFormatPane.isDirty()
                || durationFormatPane.isDirty()
                || progressFormatPane.isDirty();
    }

    public void setDirty(boolean isDirty) {
        processesFormatPane.setDirty(isDirty);
        getSeriesNameFormatPane().setDirty(isDirty);
        startTimeFormatPane.setDirty(isDirty);
        endTimeFormatPane.setDirty(isDirty);
        durationFormatPane.setDirty(isDirty);
        progressFormatPane.setDirty(isDirty);
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new AttrGanttTooltipContent();
    }
}
