package com.fr.van.chart.designer.type;

import com.fr.base.BaseUtils;
import com.fr.base.chart.chartdata.TopDefinitionProvider;
import com.fr.chart.base.AttrFillStyle;
import com.fr.chart.chartattr.Chart;
import com.fr.chart.chartattr.Legend;
import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartdata.NormalReportDataDefinition;
import com.fr.chart.chartdata.NormalTableDataDefinition;
import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.chart.chartglyph.ConditionCollection;
import com.fr.chart.chartglyph.DataSheet;
import com.fr.chart.charttypes.ChartTypeManager;
import com.fr.chartx.data.AbstractDataDefinition;
import com.fr.chartx.data.ChartDataDefinitionProvider;
import com.fr.chartx.data.field.AbstractColumnFieldCollection;
import com.fr.chartx.data.field.diff.MultiCategoryColumnFieldCollection;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.MultilineLabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.chart.gui.type.AbstractChartTypePane;
import com.fr.design.mainframe.chart.gui.type.ChartImagePane;
import com.fr.design.mainframe.chart.info.ChartInfoCollector;
import com.fr.general.Background;
import com.fr.js.NameJavaScriptGroup;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.attr.plot.VanChartPlot;
import com.fr.plugin.chart.base.VanChartTools;
import com.fr.plugin.chart.base.VanChartZoom;
import com.fr.plugin.chart.vanchart.VanChart;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;

public abstract class AbstractVanChartTypePane extends AbstractChartTypePane<VanChart> {
    private static final long serialVersionUID = 7743244512351499265L;
    private UICheckBox largeModelCheckBox;

    protected JPanel buttonPane;

    protected UIButton autoButton;

    private boolean samePlot;

    protected boolean isSamePlot() {
        return samePlot;
    }

    public AbstractVanChartTypePane() {
        buttonPane = new JPanel();
        buttonPane.setLayout(new BorderLayout());
        if (ChartTypeManager.AUTO_CHART) {
            autoButton = new UIButton(Toolkit.i18nText("Fine-Design_Chart_Auto_Recommended_Chart"),
                    BaseUtils.readIcon("/com/fr/design/images/m_insert/auto_chart.png")) {
                @Override
                public Dimension getPreferredSize() {
                    return new Dimension((int) super.getPreferredSize().getWidth(), 25);
                }
            };
            buttonPane.setBorder((BorderFactory.createEmptyBorder(5, 0, 0, 0)));
            buttonPane.add(autoButton);
        }
    }

    @Override
    protected Component[][] getPaneComponents(JPanel typePane) {
        return new Component[][]{
                new Component[]{typePane},
                new Component[]{buttonPane},
                new Component[]{stylePane},
        };
    }

    //新图表暂时还没有平面3d，渐变高光等布局。
    @Override
    protected String[] getTypeLayoutPath() {
        return new String[0];
    }

    @Override
    protected String[] getTypeLayoutTipName() {
        return new String[0];
    }

    @Override
    protected String[] getTypeTipName() {
        return ChartTypeInterfaceManager.getInstance().getSubName(getPlotID());
    }

    @Override
    public String title4PopupWindow() {
        return ChartTypeInterfaceManager.getInstance().getName(getPlotID());
    }

    @Override
    protected String getPlotTypeID() {
        return getPlotID();
    }

    protected Component[][] getComponentsWithLargeData(JPanel typePane) {
        largeModelCheckBox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Open_Large_Data_Model"));
        MultilineLabel prompt = new MultilineLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Large_Data_Model_Prompt"));
        prompt.setForeground(Color.red);
        JPanel largeDataPane = new JPanel(new BorderLayout());
        largeDataPane.add(largeModelCheckBox, BorderLayout.CENTER);
        largeDataPane.add(prompt, BorderLayout.SOUTH);

        return new Component[][]{
                new Component[]{typePane},
                new Component[]{largeDataPane}
        };
    }

    /**
     * 更新界面内容
     */
    public void populateBean(VanChart chart) {
        for (ChartImagePane imagePane : typeDemo) {
            imagePane.isPressing = false;
        }
        Plot plot = chart.getPlot();
        typeDemo.get(plot.getDetailType()).isPressing = true;
        checkDemosBackground();
    }

    /**
     * 保存界面属性
     */
    public void updateBean(VanChart chart) {
        VanChartPlot oldPlot = chart.getPlot();
        VanChartPlot newPlot = getSelectedClonedPlot();
        checkTypeChange(oldPlot);
        samePlot = accept(chart);
        if (typeChanged && samePlot) {
            //同一中图表切换不同类型
            chart.setPlot(cloneOldPlot2New(oldPlot, newPlot));
            resetChartAttr4SamePlot(chart);
            ChartInfoCollector.getInstance().updateChartMiniType(chart);
        } else if (!samePlot) {
            //不同的图表类型切換
            resetChartAttr(chart, newPlot);
            //切换图表时，数据配置不变,分类个数也不变
            newPlot.setCategoryNum(oldPlot.getCategoryNum());
            //切换类型埋点
            ChartInfoCollector.getInstance().updateChartTypeTime(chart, oldPlot.getPlotID());
        }
        if (!acceptDefinition(chart.getChartDataDefinition(), newPlot)) {
            chart.setChartDataDefinition(null);
        }
    }

    protected boolean acceptDefinition(ChartDataDefinitionProvider definition, VanChartPlot vanChartPlot) {
        if (definition instanceof AbstractDataDefinition) {
            AbstractColumnFieldCollection columnFieldCollection = ((AbstractDataDefinition) definition).getColumnFieldCollection();
            return columnFieldCollection instanceof MultiCategoryColumnFieldCollection;
        }
        return false;
    }

    protected void resetChartAttr4SamePlot(VanChart chart) {
        resetRefreshMoreLabelAttr(chart);
    }

    protected void resetChartAttr(VanChart chart, VanChartPlot newPlot) {
        resetFilterDefinition(chart);
        //上个图表支持继承部分属性到新图表
        if (supportExtendAttr(chart)) {
            newPlot.extendAttribute(chart);
            chart.setPlot(newPlot);
            return;
        }
        chart.setPlot(newPlot);
        if (newPlot.isSupportZoomDirection() && !newPlot.isSupportZoomCategoryAxis()) {
            //图表缩放新设计 恢复用注释。下面一行删除。
            chart.setVanChartZoom(new VanChartZoom());
            //图表缩放新设计 恢复用注释。下面一行取消注释。
            // ((VanChart) chart).setZoomAttribute(new ZoomAttribute());
        }
        //重置工具栏选项
        chart.setVanChartTools(createVanChartTools());
        //重置标题选项
        resetTitleAttr(chart);
        //重置监控刷新选项
        resetRefreshMoreLabelAttr(chart);
    }

    //是否支持属性的继承
    protected boolean supportExtendAttr(VanChart chart) {
        return false;
    }

    //默认有标题
    protected void resetTitleAttr(Chart chart) {
        VanChartPlot vanChartPlot = chart.getPlot();
        chart.setTitle(vanChartPlot.getDefaultTitle());
    }

    //重置数据配置
    protected void resetFilterDefinition(Chart chart) {

    }

    //重置监控刷新面板
    protected void resetRefreshMoreLabelAttr(VanChart chart) {
        chart.setRefreshMoreLabel(chart.getDefaultAutoAttrtooltip(chart));
    }

    protected VanChartTools createVanChartTools() {
        return new VanChartTools();
    }

    protected void checkTypeChange(Plot oldPlot) {
        for (int i = 0; i < typeDemo.size(); i++) {
            if (typeDemo.get(i).isPressing && i != oldPlot.getDetailType()) {
                typeChanged = true;
                break;
            }
            typeChanged = false;
        }
    }

    /**
     * 同一个图表， 类型之间切换
     */
    protected VanChartPlot cloneOldPlot2New(VanChartPlot oldPlot, VanChartPlot newPlot) {
        try {
            if (oldPlot.getLegend() != null) {
                newPlot.setLegend((Legend) oldPlot.getLegend().clone());
            }
            cloneOldConditionCollection(oldPlot, newPlot);

            cloneHotHyperLink(oldPlot, newPlot);

            if (oldPlot.getPlotFillStyle() != null) {
                newPlot.setPlotFillStyle((AttrFillStyle) oldPlot.getPlotFillStyle().clone());
            }
            newPlot.setPlotStyle(oldPlot.getPlotStyle());
            if (oldPlot.getDataSheet() != null) {
                newPlot.setDataSheet((DataSheet) oldPlot.getDataSheet().clone());
            }

            if (oldPlot.getBackground() != null) {
                newPlot.setBackground((Background) oldPlot.getBackground().clone());
            }
            if (oldPlot.getBorderColor() != null) {
                newPlot.setBorderColor(oldPlot.getBorderColor());
            }
            newPlot.setBorderStyle(oldPlot.getBorderStyle());
            newPlot.setRoundRadius(oldPlot.getRoundRadius());
            newPlot.setAlpha(oldPlot.getAlpha());
            newPlot.setShadow(oldPlot.isShadow());

            newPlot.setCategoryNum(oldPlot.getCategoryNum());

        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error("Error in change plot");
        }
        return newPlot;
    }

    protected void cloneHotHyperLink(Plot oldPlot, Plot newPlot) throws CloneNotSupportedException {
        if (oldPlot.getHotHyperLink() != null) {
            newPlot.setHotHyperLink((NameJavaScriptGroup) oldPlot.getHotHyperLink().clone());
        }
    }

    protected void cloneOldDefaultAttrConditionCollection(Plot oldPlot, Plot newPlot) throws CloneNotSupportedException {
        if (oldPlot.getConditionCollection() != null) {
            ConditionCollection newCondition = new ConditionCollection();
            newCondition.setDefaultAttr((ConditionAttr) oldPlot.getConditionCollection().getDefaultAttr().clone());
            newPlot.setConditionCollection(newCondition);
        }
    }

    protected void cloneOldConditionCollection(Plot oldPlot, Plot newPlot) throws CloneNotSupportedException {
        if (oldPlot.getConditionCollection() != null) {
            newPlot.setConditionCollection((ConditionCollection) oldPlot.getConditionCollection().clone());
        }
    }

    @Override
    public void registerButtonListener(ActionListener autoButtonListener) {
        if (autoButton != null) {
            autoButton.addActionListener(autoButtonListener);
        }
    }

    //部分图表继承到多分类之后会报错
    protected void resetMoreCateDefinition(Chart chart) {
        TopDefinitionProvider filterDefinition = chart.getFilterDefinition();
        if (filterDefinition instanceof NormalTableDataDefinition) {
            ((NormalTableDataDefinition) filterDefinition).clearMoreCate();
            ((VanChartPlot) chart.getPlot()).setCategoryNum(1);
        }
        if (filterDefinition instanceof NormalReportDataDefinition) {
            ((NormalReportDataDefinition) filterDefinition).clearMoreCate();
            ((VanChartPlot) chart.getPlot()).setCategoryNum(1);
        }
    }
}