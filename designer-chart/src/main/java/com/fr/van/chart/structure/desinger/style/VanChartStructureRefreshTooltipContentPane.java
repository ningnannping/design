package com.fr.van.chart.structure.desinger.style;

import com.fr.van.chart.designer.component.format.ChangedPercentFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.ChangedValueFormatPaneWithCheckBox;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.Component;

/**
 * Created by mengao on 2017/6/9.
 */
public class VanChartStructureRefreshTooltipContentPane extends VanChartStructureTooltipContentPane {
    public VanChartStructureRefreshTooltipContentPane(VanChartStylePane parent, JPanel showOnPane) {
        super(null, showOnPane);
    }

    @Override
    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        super.initFormatPane(parent, showOnPane);

        ChangedValueFormatPaneWithCheckBox changedValueFormatPane = new ChangedValueFormatPaneWithCheckBox(parent, showOnPane);
        ChangedPercentFormatPaneWithCheckBox changedPercentFormatPane = new ChangedPercentFormatPaneWithCheckBox(parent, showOnPane);

        setChangedValueFormatPane(changedValueFormatPane);
        setChangedPercentFormatPane(changedPercentFormatPane);
    }

    protected boolean supportRichEditor() {
        return false;
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p, p};
    }

    protected Component[][] getPaneComponents() {
        return new Component[][]{
                new Component[]{getCategoryNameFormatPane(), null},
                new Component[]{getSeriesNameFormatPane(), null},
                new Component[]{getValueFormatPane(), null},
                new Component[]{getChangedValueFormatPane(), null},
                new Component[]{getChangedPercentFormatPane(), null},
        };
    }
}
