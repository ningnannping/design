package com.fr.van.chart.designer.style;

import com.fr.van.chart.designer.component.background.VanChartBackgroundPane;
import com.fr.van.chart.designer.component.background.VanChartBackgroundWithOutImagePane;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-11
 */
public class MapRangeLegendPane extends VanChartRangeLegendPane {

    public MapRangeLegendPane() {
        super();
    }

    public MapRangeLegendPane(VanChartStylePane parent) {
        super(parent);
    }

    @Override
    protected VanChartBackgroundPane creatBackgroundPane() {
        //TODO Bjorn 地图图例背景自动逻辑
        return new VanChartBackgroundWithOutImagePane();
        //return new VanChartBackgroundWithOutImagePane(true);
    }
}
