package com.fr.van.chart.box;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.ui.ModernUIPane;
import com.fr.plugin.chart.base.AttrTooltipContent;
import com.fr.plugin.chart.base.format.AttrTooltipCategoryFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMaxFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMedianFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataMinFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataNumberFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataOutlierFormat;
import com.fr.plugin.chart.base.format.AttrTooltipDataQ1Format;
import com.fr.plugin.chart.base.format.AttrTooltipDataQ3Format;
import com.fr.plugin.chart.base.format.AttrTooltipFormat;
import com.fr.plugin.chart.base.format.AttrTooltipSeriesFormat;
import com.fr.plugin.chart.box.attr.AttrBoxTooltipContent;
import com.fr.van.chart.designer.TableLayout4VanChartHelper;
import com.fr.van.chart.designer.component.VanChartTooltipContentPane;
import com.fr.van.chart.designer.component.format.CategoryNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.SeriesNameFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.format.VanChartFormatPaneWithCheckBox;
import com.fr.van.chart.designer.component.richText.VanChartFieldAttrPane;
import com.fr.van.chart.designer.component.richText.VanChartFieldListPane;
import com.fr.van.chart.designer.component.richText.VanChartRichEditorModel;
import com.fr.van.chart.designer.component.richText.VanChartRichTextPane;
import com.fr.van.chart.designer.style.VanChartStylePane;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;

public class VanChartBoxTooltipContentPane extends VanChartTooltipContentPane {

    private boolean detailed;

    private VanChartFormatPaneWithCheckBox number;
    private VanChartFormatPaneWithCheckBox max;
    private VanChartFormatPaneWithCheckBox q3;
    private VanChartFormatPaneWithCheckBox median;
    private VanChartFormatPaneWithCheckBox q1;
    private VanChartFormatPaneWithCheckBox min;
    private VanChartFormatPaneWithCheckBox outlier;

    private JPanel dataNumberPane;
    private JPanel dataOutlierPane;

    public VanChartBoxTooltipContentPane(VanChartStylePane parent, JPanel showOnPane, boolean isDetailed) {
        super(parent, showOnPane);
        checkFormatVisible(isDetailed);
    }

    protected void initFormatPane(VanChartStylePane parent, JPanel showOnPane) {
        setCategoryNameFormatPane(new CategoryNameFormatPaneWithCheckBox(parent, showOnPane));
        setSeriesNameFormatPane(new SeriesNameFormatPaneWithCheckBox(parent, showOnPane));

        number = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Data_Number");
            }
        };
        max = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Max_Value");
            }
        };
        q3 = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Data_Q3");
            }
        };
        median = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Data_Median");
            }
        };
        q1 = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Data_Q1");
            }
        };
        min = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Min_Value");
            }
        };
        outlier = new VanChartFormatPaneWithCheckBox(parent, showOnPane) {
            protected String getCheckBoxText() {
                return Toolkit.i18nText("Fine-Design_Chart_Outlier_Value");
            }
        };
    }

    protected JPanel createCommonFormatPanel() {
        JPanel commonPanel = new JPanel(new BorderLayout());

        commonPanel.add(createCateAndSeriesPane(), BorderLayout.NORTH);
        commonPanel.add(createDataNumberPane(), BorderLayout.CENTER);
        commonPanel.add(createDataDetailPane(), BorderLayout.SOUTH);

        return commonPanel;
    }

    protected VanChartRichTextPane createRichTextPane(ModernUIPane<VanChartRichEditorModel> richEditorPane) {

        return new VanChartRichTextPane(richEditorPane) {

            protected VanChartFieldListPane createFieldListPane(VanChartFieldAttrPane fieldAttrPane, ModernUIPane<VanChartRichEditorModel> richEditor) {
                if (detailed) {
                    return new VanChartBoxRichTextDetailedFieldListPane(fieldAttrPane, richEditor);
                }

                return new VanChartBoxRichTextResultFieldListPane(fieldAttrPane, richEditor);
            }

            protected AttrTooltipContent getInitialTooltipContent() {
                return createAttrTooltip();
            }
        };
    }

    protected String[] getRichTextFieldNames() {
        if (detailed) {
            return new String[]{
                    Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                    Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                    Toolkit.i18nText("Fine-Design_Chart_Data_Number"),
                    Toolkit.i18nText("Fine-Design_Chart_Max_Value"),
                    Toolkit.i18nText("Fine-Design_Chart_Data_Q3"),
                    Toolkit.i18nText("Fine-Design_Chart_Data_Median"),
                    Toolkit.i18nText("Fine-Design_Chart_Data_Q1"),
                    Toolkit.i18nText("Fine-Design_Chart_Min_Value"),
                    Toolkit.i18nText("Fine-Design_Chart_Outlier_Value")
            };
        }

        return new String[]{
                Toolkit.i18nText("Fine-Design_Chart_Category_Use_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Series_Name"),
                Toolkit.i18nText("Fine-Design_Chart_Max_Value"),
                Toolkit.i18nText("Fine-Design_Chart_Data_Q3"),
                Toolkit.i18nText("Fine-Design_Chart_Data_Median"),
                Toolkit.i18nText("Fine-Design_Chart_Data_Q1"),
                Toolkit.i18nText("Fine-Design_Chart_Min_Value")
        };
    }

    protected AttrTooltipFormat[] getRichTextFieldFormats() {
        if (detailed) {
            return new AttrTooltipFormat[]{
                    new AttrTooltipCategoryFormat(),
                    new AttrTooltipSeriesFormat(),
                    new AttrTooltipDataNumberFormat(),
                    new AttrTooltipDataMaxFormat(),
                    new AttrTooltipDataQ3Format(),
                    new AttrTooltipDataMedianFormat(),
                    new AttrTooltipDataQ1Format(),
                    new AttrTooltipDataMinFormat(),
                    new AttrTooltipDataOutlierFormat()
            };
        }

        return new AttrTooltipFormat[]{
                new AttrTooltipCategoryFormat(),
                new AttrTooltipSeriesFormat(),
                new AttrTooltipDataMaxFormat(),
                new AttrTooltipDataQ3Format(),
                new AttrTooltipDataMedianFormat(),
                new AttrTooltipDataQ1Format(),
                new AttrTooltipDataMinFormat()
        };
    }

    protected double[] getRowSize(double p) {
        return new double[]{p, p, p, p, p, p, p, p, p, p, p, p};
    }

    private JPanel createCateAndSeriesPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p, p};

        Component[][] cateAndSeries = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, getCategoryNameFormatPane()},
                new Component[]{null, getSeriesNameFormatPane()}
        };

        return TableLayoutHelper.createTableLayoutPane(cateAndSeries, rowSize, columnSize);
    }

    private JPanel createDataNumberPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};
        double[] rowSize = {p, p};

        Component[][] dataNumber = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, number},
        };

        dataNumberPane = TableLayoutHelper.createTableLayoutPane(dataNumber, rowSize, columnSize);

        return dataNumberPane;
    }

    private JPanel createDataDetailPane() {
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double e = TableLayout4VanChartHelper.EDIT_AREA_WIDTH;

        double[] columnSize = {f, e};

        JPanel detailPane = new JPanel(new BorderLayout());

        Component[][] dataDetail = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, max},
                new Component[]{new UILabel(getLabelContentTitle()), q3},
                new Component[]{null, median},
                new Component[]{null, q1},
                new Component[]{null, min}
        };

        Component[][] dataOutlier = new Component[][]{
                new Component[]{null, null},
                new Component[]{null, outlier},
        };

        dataOutlierPane = TableLayoutHelper.createTableLayoutPane(dataOutlier, new double[]{p, p}, columnSize);

        detailPane.add(TableLayoutHelper.createTableLayoutPane(dataDetail, new double[]{p, p, p, p, p, p}, columnSize), BorderLayout.NORTH);
        detailPane.add(dataOutlierPane, BorderLayout.CENTER);

        return detailPane;
    }

    public boolean isDirty() {
        return getCategoryNameFormatPane().isDirty()
                || getSeriesNameFormatPane().isDirty()
                || number.isDirty()
                || max.isDirty()
                || q3.isDirty()
                || median.isDirty()
                || q1.isDirty()
                || min.isDirty()
                || outlier.isDirty();
    }

    public void setDirty(boolean isDirty) {
        getCategoryNameFormatPane().setDirty(isDirty);
        getSeriesNameFormatPane().setDirty(isDirty);
        number.setDirty(isDirty);
        max.setDirty(isDirty);
        q3.setDirty(isDirty);
        median.setDirty(isDirty);
        q1.setDirty(isDirty);
        min.setDirty(isDirty);
        outlier.setDirty(isDirty);
    }

    protected AttrTooltipContent createAttrTooltip() {
        return new AttrBoxTooltipContent(detailed);
    }

    protected void populateFormatPane(AttrTooltipContent attrTooltipContent) {
        getCategoryNameFormatPane().populate(attrTooltipContent.getCategoryFormat());
        getSeriesNameFormatPane().populate(attrTooltipContent.getSeriesFormat());

        if (attrTooltipContent instanceof AttrBoxTooltipContent) {
            AttrBoxTooltipContent boxTooltipContent = (AttrBoxTooltipContent) attrTooltipContent;

            number.populate(boxTooltipContent.getNumber());
            max.populate(boxTooltipContent.getMax());
            q3.populate(boxTooltipContent.getQ3());
            median.populate(boxTooltipContent.getMedian());
            q1.populate(boxTooltipContent.getQ1());
            min.populate(boxTooltipContent.getMin());
            outlier.populate(boxTooltipContent.getOutlier());

            checkFormatVisible(boxTooltipContent.isDetailed());
        }
    }

    protected void updateTooltipFormat(AttrTooltipContent target, AttrTooltipContent source) {
        super.updateTooltipFormat(target, source);

        if (target instanceof AttrBoxTooltipContent && source instanceof AttrBoxTooltipContent) {
            AttrBoxTooltipContent targetGantt = (AttrBoxTooltipContent) target;
            AttrBoxTooltipContent sourceGantt = (AttrBoxTooltipContent) source;

            targetGantt.setRichTextNumber(sourceGantt.getRichTextNumber());
            targetGantt.setRichTextMax(sourceGantt.getRichTextMax());
            targetGantt.setRichTextQ3(sourceGantt.getRichTextQ3());
            targetGantt.setRichTextMedian(sourceGantt.getRichTextMedian());
            targetGantt.setRichTextQ1(sourceGantt.getRichTextQ1());
            targetGantt.setRichTextMin(sourceGantt.getRichTextMin());
            targetGantt.setRichTextOutlier(sourceGantt.getRichTextOutlier());
        }
    }

    protected void updateFormatPane(AttrTooltipContent attrTooltipContent) {
        getCategoryNameFormatPane().update(attrTooltipContent.getCategoryFormat());
        getSeriesNameFormatPane().update(attrTooltipContent.getSeriesFormat());

        if (attrTooltipContent instanceof AttrBoxTooltipContent) {
            AttrBoxTooltipContent boxTooltipContent = (AttrBoxTooltipContent) attrTooltipContent;

            number.update(boxTooltipContent.getNumber());
            max.update(boxTooltipContent.getMax());
            q3.update(boxTooltipContent.getQ3());
            median.update(boxTooltipContent.getMedian());
            q1.update(boxTooltipContent.getQ1());
            min.update(boxTooltipContent.getMin());
            outlier.update(boxTooltipContent.getOutlier());

            boxTooltipContent.setDetailed(this.detailed);
        }
    }

    public void checkFormatVisible(boolean detailed) {
        this.detailed = detailed;

        dataNumberPane.setVisible(detailed);
        dataOutlierPane.setVisible(detailed);
    }
}
