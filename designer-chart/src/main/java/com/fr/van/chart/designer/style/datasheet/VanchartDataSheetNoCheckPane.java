package com.fr.van.chart.designer.style.datasheet;

import com.fr.chart.chartglyph.DataSheet;
import com.fr.design.gui.iscrollbar.UIScrollBar;
import com.fr.design.mainframe.chart.gui.style.ChartTextAttrPane;
import com.fr.plugin.chart.base.AttrDataSheet;
import com.fr.van.chart.designer.component.border.VanChartBorderPane;

import javax.swing.JPanel;
import java.awt.Component;
import java.awt.Container;

/**
 * Created by mengao on 2017/5/24.
 */
public class VanchartDataSheetNoCheckPane extends VanChartDataSheetPane {

    protected Component[][] creatComponent(JPanel dataSheetPane) {

        Component[][] components = new Component[][]{
                new Component[]{dataSheetPane}
        };
        return components;
    }

    @Override
    protected void layoutContentPane() {
        leftcontentPane = createContentPane();
        this.add(leftcontentPane);
    }

    @Override
    protected void setLeftContentPaneBounds(Container parent, UIScrollBar scrollBar, int beginY, int maxheight) {
        int width = parent.getWidth();
        int height = parent.getHeight();
        scrollBar.setBounds(0, 0, 0, 0);
        leftcontentPane.setBounds(0, 0, width, height);
    }

    public void populate(AttrDataSheet attrDataSheet) {
        populate(attrDataSheet.getDataSheet());
    }

    public AttrDataSheet update() {
        AttrDataSheet attrDataSheet = new AttrDataSheet();
        DataSheet dataSheet = attrDataSheet.getDataSheet();
        dataSheet.setVisible(true);
        update(dataSheet);
        return attrDataSheet;
    }

    @Override
    protected ChartTextAttrPane createChartTextAttrPane(){
        return new ChartTextAttrPane();
    }

    protected VanChartBorderPane createBorderPanePane() {
        return new VanChartBorderPane();
    }
}
