!(function () {
    var Widget = BI.inherit(BI.RichEditorParamAction, {
        props: {
            baseCls: "bi-design-chart-common-editor-insert-param",
            dimensionIds: [],
            editor: null
        },

        _store: function () {
            return BI.Models.getModel("bi.model.design.chart.common.editor.insert_param");
        },

        watch: {
            isSelectedParam: function () {
                var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
                this.addParam(this.model.param, editorService.encode);
                this.combo.hideView();
                // this._restorePosition();
            },
            items: function () {
                this.adapter.populate(this.model.items);
            }
        },

        render: function () {
            var self = this;

            this.adapter = BI.createWidget({
                type: "bi.button_group",
                items: this.model.items,
                chooseType: BI.ButtonGroup.CHOOSE_TYPE_SINGLE,
                behaviors: {},
                layouts: [{
                    type: "bi.vertical"
                }],
                listeners: [{
                    eventName: BI.ButtonGroup.EVENT_CHANGE,
                    action: function () {
                        self._savePosition();
                        self.store.changeParam(this.getValue()[0]);
                    }
                }]
            });

            var searchPopup = {
                type: "bi.vertical",
                cls: "bi-border",
                tgap: 10,
                bgap: 10,
                items: [this.adapter]
            };

            return {
                type: "bi.combo",
                direction: "bottom,left",
                isNeedAdjustWidth: true,
                invisible: true,
                el: {
                    type: "bi.vertical_adapt",
                    items: [{
                        type: "bi.icon_change_button",
                        iconCls: "editor-insert-param-inactivated-font",
                        iconWidth: 24,
                        iconHeight: 24,
                        ref: function (_ref) {
                            self.iconButtonTrigger = _ref;
                        }
                    }]
                },
                popup: {
                    el: searchPopup,
                    minWidth: 150
                },
                ref: function (_ref) {
                    self.combo = _ref;
                },
                listeners: [{
                    eventName: BI.Combo.EVENT_AFTER_POPUPVIEW,
                    action: function () {
                        self.iconButtonTrigger.setIcon("editor-insert-param-active-font");
                    }
                }, {
                    eventName: BI.Combo.EVENT_BEFORE_HIDEVIEW,
                    action: function () {
                        self.iconButtonTrigger.setIcon("editor-insert-param-inactivated-font");
                    }
                }]
            };
        },

        _savePosition: function () {
            var instance = this._getInstance();
            instance.saveRng();
        },

        _restorePosition: function () {
            var instance = this._getInstance();
            instance.initSelection();
        },

        _getInstance: function () {
            return this.options.editor.selectedInstance || this.options.editor.getInstance();
        },

        selectedParam: function (param) {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            this.addParam(param, editorService.encode);
        }
    });

    BI.shortcut("bi.design.chart.common.editor.insert_param", Widget);
}());