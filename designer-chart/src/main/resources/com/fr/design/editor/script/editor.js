!(function () {
    var Widget = BI.inherit(BI.Widget, {
        props: {
            baseCls: "bi-design-chart-common-editor",
            isAuto: true,
            content: "",
            placeholder: "",
            fontStyle: {},
            dimensionIds: [],
            toolbar: {
                buttons: []
            },
            textAlign: "center"
        },

        _store: function () {
            var o = this.options;

            return BI.Models.getModel("bi.model.design.chart.common.editor", {
                dimensionIds: o.dimensionIds,
                isAuto: o.isAuto
            });
        },

        render: function () {
            var self = this, o = this.options;
            this.editor = BI.createWidget({
                type: "bi.nic_editor",
                $value: "chart-common-editor",
                cls: "editor bi-border bi-focus-shadow " + (o.textAlign === "center" ? "editor-center-text" : ""),
                listeners: [{
                    eventName: BI.NicEditor.EVENT_FOCUS,
                    action: function () {
                        self.clearPlaceholder();
                        self.store.setEditorBlurState(false);
                    }
                }, {
                    eventName: BI.NicEditor.EVENT_BLUR,
                    action: function () {
                        self.setPlaceholder();
                        self.store.setEditorBlurState(true);
                    }
                }]
            });

            return {
                type: "bi.vtape",
                ref: function (_ref) {
                    self.bar = _ref;
                },
                items: [{
                    type: "bi.htape",
                    tgap: 3,
                    items: [{
                        type: "bi.label",
                        text: BI.i18nText("BI-Design_Font_Style") + ": ",
                        textAlign: "left",
                        width: 60,
                        tgap: 2,
                        lgap: 3
                    }, {
                        type: "bi.button_group",
                        items: this.model.fontStyleItems,
                        ref: function (_ref) {
                            self.fontStyleGroup = _ref;
                        },
                        listeners: [{
                            eventName: BI.ButtonGroup.EVENT_CHANGE,
                            action: function () {
                                var isAuto = this.getValue()[0];
                                self.store.changeIsAuto(isAuto);
                                self.changeFontStyleMode(isAuto);
                            }
                        }],
                        layouts: [{
                            type: "bi.left",
                            rgap: 2
                        }],
                        value: this.model.mode,
                        width: 170
                    }, this._getToolBar()],
                    height: 24
                }, {
                    el: this.editor,
                    tgap: 10
                }]
            };
        },

        _getToolBar: function () {
            var self = this;
            return {
                type: "bi.design.chart.common.editor.toolbar",
                cls: "toolbar-region",
                wId: this.options.wId,
                editor: this.editor,
                buttons: this.options.toolbar.buttons,
                ref: function (_ref) {
                    self.toolbar = _ref;
                }
            };
        },

        mounted: function () {
            this.editor.bindToolbar(this.bar);
            this.editor.setValue(this._formatContent(this.options.content));
            this.setFocus();
        },

        _cleanHtml: function (value) {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            var dimensionIds = this.model.dimensionIds;
            var dataIdMap = {};

            for (var i = 0, len = dimensionIds.length; i < len; i++) {
                var dimensionId = dimensionIds[i];
                var key = BI.keys(dimensionId)[0];

                dataIdMap[key] = dimensionId[key];
            }

            var result = BI.replaceAll(value, "<p></p>", "");
            // 去掉image的src属性，因为数据太大了
            result = value.replaceAll("<img.*?>", function (imageStr) {
                var attrs = editorService.getImageAttr(imageStr);
                var str = "<img ";
                BI.each(attrs, function (key, value) {
                    if (key === "src") {
                        return;
                    }
                    str += " " + key + "=\"" + value + "\"";

                    if (key === "alt" && dataIdMap[value.trim()]) {
                        str += " " + "data-id" + "=\"" + dataIdMap[value.trim()] + "\"";
                    }
                });

                return str + " />";
            });

            return result;
        },

        getValue: function () {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            var value = this._cleanHtml(this.editor.getValue(BI.NicEditor.FormatType.ESCAPE));
            // BI.each(this.model.dimensionIds, function (idx, dId) {
            //     var fullName = BI.Utils.getNotGeoDimensionFullName(dId);
            //     value = editorService.appendImageAttr(value, fullName, BICst.RICH_TEXT_INFO.DATA_ID, dId);
            // });

            if (editorService.isEmptyRichText(value)) {
                value = "";
            }

            if (BI.isNotEmptyString(this.options.placeholder) && editorService.isRichTextEqual(value, this.options.placeholder)) {
                value = null;
            }

            return {
                isAuto: this.model.isAuto,
                content: value
            };
        },

        setValue: function (obj) {
            var keys = BI.keys(obj);
            if (BI.contains(keys, "content")) {
                this.editor.setValue(this._formatContent(obj.content));
            }
            if (BI.contains(keys, "isAuto")) {
                this.store.changeIsAuto(obj.isAuto);
                this.fontStyleGroup.setValue(obj.isAuto);
            }
            if (BI.contains(keys, "dimensionIds")) {
                this.store.changeDimensionIds(obj.dimensionIds);
            }
        },

        _formatContent: function (content) {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");

            content = editorService.setImageSrc(content);

            if (editorService.isBlankRichText(content)
                || editorService.isRichTextEqual(content, this.options.placeholder)) {
                content = this.options.placeholder || "";
            }

            return content;
        },

        // 切换到“自动”时的处理方法
        _switchToAutoStyle: function () {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor"),
                content = this.editor.getValue(BI.NicEditor.FormatType.ESCAPE),
                isEqualToPlaceholder = editorService.isRichTextEqual(content, this.options.placeholder);

            var HTML_ONLY_STYLE_TAG = "<font[\\s\\S]*?>|</font>|<span[\\s\\S]*?>|</span>|<b\\s+(.*?)>|<b>|</b>|<u[\\s\\S]*?>|</u>|<i\\s+(.*?)>|<i>|</i>|<strong[\\s\\S]*?>|</strong>|<em[\\s\\S]*?>|</em>|<div[\\s\\S]style(.*?)>|</div>";

            if (!isEqualToPlaceholder) {
                content = content.replaceAll(HTML_ONLY_STYLE_TAG, "");
            }

            return content;
        },

        clearPlaceholder: function () {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            var content = this.editor.getValue(BI.NicEditor.FormatType.ESCAPE);
            var isSameContent = editorService.isRichTextEqual(content, this.options.placeholder);

            if (isSameContent) {
                content = editorService.convertText2RichText("", this.options.fontStyle);
                this.editor.setValue(content);
                this.setFocus();
            }
        },

        setPlaceholder: function () {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            var content = this.editor.getValue(BI.NicEditor.FormatType.ESCAPE);
            var isSameContent = editorService.isBlankRichText(content);

            if (isSameContent) {
                this.editor.setValue(this.options.placeholder);
            }
        },

        // 字体样式切换
        changeFontStyleMode: function (isAuto) {
            var editorService = BI.Services.getService("bi.service.design.chart.common.editor");
            var switchFn = this.options.switchFn,
                content = this.editor.getValue(BI.NicEditor.FormatType.ESCAPE),
                origin = content;
            if (isAuto) {
                content = this._switchToAutoStyle();
                this.setFocus();
            } else if (editorService.isRichTextEqual(content, this.options.placeholder)) {
                content = "";
            }
            if (BI.isFunction(switchFn)) {
                content = switchFn(isAuto, content);
            }
            if (content !== origin) {
                this.editor.setValue(content);
            }
        },

        setFocus: function () {
            var instance = this.editor.instance;
            instance && instance.initSelection();
        }
    });

    Widget.EVENT_CHANGE = "EVENT_CHANGE";

    BI.shortcut("bi.design.chart.common.editor", Widget);

}());
