!(function () {

    var chinese = {
        "BI-Design_Font_Style": "字体样式",
        "BI-Font_Size": "字号",
        "BI-Basic_Auto": "自动",
        "BI-Basic_Custom": "自定义",
        "BI-Basic_Default": "默认",
        "BI-Basic_Sim_Hei": "黑体",
        "BI-Basic_Sim_Sun": "宋体",
        "BI-Basic_Fang_Song": "仿宋",
        "BI-Basic_Kai_Ti": "楷体",
        "BI-Basic_Bold": "加粗",
        "BI-Basic_Italic": "斜体",
        "BI-Basic_Underline": "下划线",
        "BI-Basic_Font_Color": "文字颜色",
        "BI-Word_Align_Left": "文字居左",
        "BI-Word_Align_Center": "文字居中",
        "BI-Word_Align_Right": "文字居右",
        "BI-Basic_Cancel": "取消",
        "BI-Basic_Save": "保存",
        "BI-Basic_More": "更多",
        "BI-Custom_Color": "自定义颜色",
        "BI-Transparent_Color": "透明"
    };

    var taiwan = {
        "BI-Design_Font_Style": "字體樣式",
        "BI-Font_Size": "字號",
        "BI-Basic_Auto": "自動",
        "BI-Basic_Custom": "自定義",
        "BI-Basic_Default": "默認",
        "BI-Basic_Sim_Hei": "黑體",
        "BI-Basic_Sim_Sun": "宋體",
        "BI-Basic_Fang_Song": "仿宋",
        "BI-Basic_Kai_Ti": "楷體",
        "BI-Basic_Bold": "加粗",
        "BI-Basic_Italic": "斜體",
        "BI-Basic_Underline": "下劃線",
        "BI-Basic_Font_Color": "文字顏色",
        "BI-Word_Align_Left": "文字居左",
        "BI-Word_Align_Center": "文字居中",
        "BI-Word_Align_Right": "文字居右",
        "BI-Basic_Cancel": "取消",
        "BI-Basic_Save": "保存",
        "BI-Basic_More": "更多",
        "BI-Custom_Color": "自定義顏色",
        "BI-Transparent_Color": "透明"
    };

    var english = {
        "BI-Design_Font_Style": "Font style",
        "BI-Font_Size": "Font size",
        "BI-Basic_Auto": "auto",
        "BI-Basic_Custom": "custom",
        "BI-Basic_Default": "default",
        "BI-Basic_Sim_Hei": "Sim_Hei",
        "BI-Basic_Sim_Sun": "Sim_Sun",
        "BI-Basic_Fang_Song": "Fang_Song",
        "BI-Basic_Kai_Ti": "Kai_Ti",
        "BI-Basic_Bold": "Bold",
        "BI-Basic_Italic": "Italic",
        "BI-Basic_Underline": "UnderLine",
        "BI-Basic_Font_Color": "Font Color",
        "BI-Word_Align_Left": "Align Left",
        "BI-Word_Align_Center": "Align Center",
        "BI-Word_Align_Right": "Align Right",
        "BI-Basic_Cancel": "Cancel",
        "BI-Basic_Save": "Save",
        "BI-Basic_More": "More",
        "BI-Custom_Color": "Custom Color",
        "BI-Transparent_Color": "Transparent Color"
    };

    var japanese = {
        "BI-Design_Font_Style": "フォントスタイル",
        "BI-Font_Size": "フォントサイズ",
        "BI-Basic_Auto": "自動",
        "BI-Basic_Custom": "カスタマイズ",
        "BI-Basic_Default": "デフォルト",
        "BI-Basic_Sim_Hei": "黒い体",
        "BI-Basic_Sim_Sun": "ソンティ",
        "BI-Basic_Fang_Song": "イミテーションソング",
        "BI-Basic_Kai_Ti": "イタリック",
        "BI-Basic_Bold": "大胆な",
        "BI-Basic_Italic": "イタリック",
        "BI-Basic_Underline": "アンダースコア",
        "BI-Basic_Font_Color": "テキストの色",
        "BI-Word_Align_Left": "左のテキスト",
        "BI-Word_Align_Center": "テキスト中心",
        "BI-Word_Align_Right": "右のテキスト",
        "BI-Basic_Cancel": "キャンセル",
        "BI-Basic_Save": "セーブ",
        "BI-Basic_More": "もっと",
        "BI-Custom_Color": "カスタムカラー",
        "BI-Transparent_Color": "トランスペアレント"
    };

    var korean = {
        "BI-Design_Font_Style": "글꼴 스타일",
        "BI-Font_Size": "글꼴 크기",
        "BI-Basic_Auto": "자동적 인",
        "BI-Basic_Custom": "맞춤 설정",
        "BI-Basic_Default": "기본",
        "BI-Basic_Sim_Hei": "블랙 바디",
        "BI-Basic_Sim_Sun": "송티",
        "BI-Basic_Fang_Song": "모조 노래",
        "BI-Basic_Kai_Ti": "기울임 꼴",
        "BI-Basic_Bold": "굵게",
        "BI-Basic_Italic": "이탤릭체",
        "BI-Basic_Underline": "밑줄",
        "BI-Basic_Font_Color": "텍스트 색상",
        "BI-Word_Align_Left": "왼쪽 텍스트",
        "BI-Word_Align_Center": "텍스트 중심",
        "BI-Word_Align_Right": "오른쪽 텍스트",
        "BI-Basic_Cancel": "취소",
        "BI-Basic_Save": "저장",
        "BI-Basic_More": "더",
        "BI-Custom_Color": "맞춤 색상",
        "BI-Transparent_Color": "투명한"
    };

    function transformI18n(language) {
        var BI = window.BI;

        if (BI == null) {
            return;
        }

        if (language == null) {
            BI.addI18n(chinese);
        }

        if (language.indexOf('zh_TW') > -1) {
            BI.addI18n(taiwan);
        } else if (language.indexOf('en_US') > -1) {
            BI.addI18n(english);
        } else if (language.indexOf('ja_JP') > -1) {
            BI.addI18n(japanese);
        } else if (language.indexOf('ko_KR') > -1) {
            BI.addI18n(korean);
        } else {
            BI.addI18n(chinese);
        }
    }

    window.transformI18n = transformI18n;
}());