package com.fr.grid.help;

import junit.framework.TestCase;
import org.junit.Assert;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/8/7
 */
public class GridHelperTest extends TestCase {

    /**
     * 为空
     */
    private char ch;

    public void testIsKoreanCharacter() {
        Assert.assertFalse(GridHelper.isKoreanCharacter('1'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('A'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('z'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('测'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('測'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('鐪'));
        Assert.assertFalse(GridHelper.isKoreanCharacter('&'));
        Assert.assertFalse(GridHelper.isKoreanCharacter(' '));
        Assert.assertFalse(GridHelper.isKoreanCharacter(ch));
        Assert.assertFalse(GridHelper.isKoreanCharacter('は'));
        Assert.assertTrue(GridHelper.isKoreanCharacter('먀'));
        Assert.assertTrue(GridHelper.isKoreanCharacter('이'));
        Assert.assertTrue(GridHelper.isKoreanCharacter('한'));
        // 韩文音节
        Assert.assertTrue(GridHelper.isKoreanCharacter('휖'));
        // 韩文兼容字母
        Assert.assertTrue(GridHelper.isKoreanCharacter('ㆋ'));
        // 韩文字母
        Assert.assertTrue(GridHelper.isKoreanCharacter('ᇪ'));
        // 韩文字母扩展A
        Assert.assertTrue(GridHelper.isKoreanCharacter('ꥵ'));
        // 韩文字母扩展B
        Assert.assertTrue(GridHelper.isKoreanCharacter('ퟂ'));
        // 韩文货币符号
        Assert.assertTrue(GridHelper.isKoreanCharacter('₩'));
    }


}
