package com.fr.grid.action;

import com.fr.design.os.impl.SupportOSImpl;
import com.fr.grid.Grid;
import com.fr.grid.help.GridHelper;
import com.fr.stable.os.support.OSBasedAction;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/8/12
 */
public class GridInputAction implements OSBasedAction {

    private Grid grid;
    private char ch;

    @Override
    public void execute(Object... objects) {
        // win下韩语输入法与其他输入法不一样 在输入第二个字符时 就开始触发keyType 而其中grid的焦点转移会导致输入失效
        boolean accept = SupportOSImpl.KOREAN_INPUT.support() && grid != null;
        if (accept) {
            grid.setNeedRequestFocus(!GridHelper.isKoreanCharacter(ch));
        }
    }

    public void register(Grid grid, char ch) {
        this.grid = grid;
        this.ch = ch;
    }
}
