package com.fr.design.mainframe.alphafine.preview;

import com.fr.common.util.Strings;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.component.TemplateResourcePageGridPane;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.mainframe.alphafine.model.TemplateResourceDetail;
import com.fr.design.mainframe.alphafine.search.manager.impl.TemplateResourceSearchManager;
import com.fr.log.FineLoggerFactory;
import org.jooq.tools.StringUtils;


import javax.swing.JPanel;
import javax.swing.SwingWorker;
import java.awt.CardLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;


/**
 *
 * alphafine - 模板商城面板
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateShopPane extends JPanel {

    private static final TemplateShopPane INSTANCE = new TemplateShopPane();
    public static TemplateShopPane getInstance() {
        return INSTANCE;
    }

    // public 方便埋点
    public static final String DEFAULT_PAGE_PANEL = "defaultPagePane";
    public static final String PAGE_PANEL = "pagePane";
    public static final String DETAIL_PANEL = "detailPane";
    public static final String LOADING_PANEL = "loadingPane";
    public static final String FAILED_PANEL = "failedPane";
    private String currentCard = StringUtils.EMPTY;
    private static final String SLASH = "/";

    private CardLayout cardLayout = new CardLayout();
    private JPanel defaultPagePane;
    private JPanel pagePane;
    private JPanel detailPane;
    private JPanel loadingPane;
    private JPanel failedPane;

    private TemplateShopPane() {
        setLayout(cardLayout);
        initComponents();
        this.add(defaultPagePane, DEFAULT_PAGE_PANEL);
        this.add(loadingPane, LOADING_PANEL);
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        switchCard(DEFAULT_PAGE_PANEL);
    }

    private void switchCard(String flag) {
        cardLayout.show(this, flag);
        currentCard = flag;
    }

    private void initComponents() {
        defaultPagePane = createDefaultResourcePane();
        loadingPane = createLoadingPane();
    }

    /**
     * 刷新
     * */
    public void refreshPagePane(List<TemplateResource> resourceList) {
        pagePane = createContentPane(resourceList);
        this.add(pagePane, PAGE_PANEL);
        switchCard(PAGE_PANEL);
    }

    /**
     * 退出搜索结果面板
     * */
    public void quitSearchResultPane() {
        if (StringUtils.equals(currentCard,PAGE_PANEL)) {
            switchCard(DEFAULT_PAGE_PANEL);
        }
    }

    /**
     * 显示结果
     * */
    public void showResult() {
        if (Strings.isEmpty(AlphaFineHelper.getAlphaFineDialog().getSearchText())) {
            switchCard(DEFAULT_PAGE_PANEL);
        } else {
            switchCard(PAGE_PANEL);
        }
    }


    /**
     * 打开二级页面，显示详细信息
     * */
    public void searchAndShowDetailPane(TemplateResource resource) {

        changeLabel(resource.getName());

        switchCard(LOADING_PANEL);

        new SwingWorker<TemplateResourceDetail, Void>() {
            @Override
            protected TemplateResourceDetail doInBackground(){
                // 搜搜
                TemplateResourceDetail detail = TemplateResourceSearchManager.getInstance().getDetailSearchResult(resource);
                return detail;
            }

            @Override
            protected void done() {
                TemplateResourceDetail detail = null;
                try {
                    detail = get();
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e, e.getMessage());
                }

                if (detail == null) {
                    setRetryAction(resource);
                    switchCard(FAILED_PANEL);
                } else {
                    // detailpane初始化
                    detailPane = new TemplateResourceDetailPane(detail);
                    // 切换
                    INSTANCE.add(detailPane, DETAIL_PANEL);
                    switchCard(DETAIL_PANEL);
                }
            }


        }.execute();

    }



    private void changeLabel(String resourceName) {
        JPanel labelNamePane = AlphaFineHelper.getAlphaFineDialog().getLabelWestPane();
        UILabel tabLabel = AlphaFineHelper.getAlphaFineDialog().getTabLabel();
        tabLabel.setForeground(AlphaFineConstants.DARK_GRAY);

        UILabel slash = new UILabel(SLASH);
        slash.setForeground(AlphaFineConstants.DARK_GRAY);

        UILabel resourceLabel = new UILabel(resourceName);
        resourceLabel.setForeground(AlphaFineConstants.LABEL_SELECTED);


        labelNamePane.removeAll();
        labelNamePane.add(tabLabel);
        labelNamePane.add(slash);
        labelNamePane.add(resourceLabel);


        tabLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                switchCard(PAGE_PANEL);
                tabLabel.setForeground(AlphaFineConstants.LABEL_SELECTED);
                labelNamePane.remove(slash);
                labelNamePane.remove(resourceLabel);
            }
        });
    }

    /**
     * 方便埋点，勿删
     * */
    public String getCurrentCard() {
        return currentCard;
    }

    private JPanel createContentPane(List<TemplateResource> templateResources) {
        return new TemplateResourcePageGridPane(templateResources);
    }

    private JPanel createDefaultResourcePane() {
        return createContentPane(TemplateResourceSearchManager.getInstance().getDefaultResourceList());
    }


    private JPanel createLoadingPane() {
        return new SearchLoadingPane();
    }

    private void setRetryAction(TemplateResource resource) {
        if (failedPane != null) {
            INSTANCE.remove(failedPane);
        }
        failedPane = createFailedPane(resource);
        INSTANCE.add(failedPane, FAILED_PANEL);
    }

    private JPanel createFailedPane(TemplateResource resource) {
        return new NetWorkFailedPane(()->{this.searchAndShowDetailPane(resource);});
    }



}