package com.fr.design.mainframe.alphafine.component;

import com.fr.base.svg.IconUtils;
import com.fr.common.util.Collections;
import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.DesignUtils;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

/**
 * alphafine - 搜索提示面板
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class SearchHintPane extends JPanel {

    private static final String TITLE = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Search_Title");
    private static final String RECOMMEND = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Recommend_Search");
    private static final Icon ICON = IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/search_hint.svg");
    private static final int HEIGHT = 200;
    private static final int WIDTH = 300;
    private static final int TITLE_LABEL_HEIGHT = 30;
    private static final int TITLE_FONT_SIZE = 14;

    public SearchHintPane() {
        this(new ArrayList<>());
    }

    public SearchHintPane(List<String> hints) {
        this(hints, ICON, TITLE);
    }

    public SearchHintPane(List<String> hints, Icon icon, String title) {
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEmptyBorder(40,0,0,0));
        setBackground(Color.white);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setAlignmentY(SwingConstants.CENTER);
        UILabel image = new UILabel();
        image.setPreferredSize(new Dimension(150, 111));
        image.setHorizontalAlignment(SwingConstants.CENTER);
        image.setIcon(icon);
        image.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        add(image, BorderLayout.NORTH);
        add(generateDescription(title), BorderLayout.CENTER);
        add(generateHintsLabel(hints), BorderLayout.SOUTH);
    }

    protected Component generateDescription(String title) {
        JLabel description = new JLabel(title);
        description.setForeground(AlphaFineConstants.MEDIUM_GRAY);
        description.setFont(DesignUtils.getDefaultGUIFont().applySize(TITLE_FONT_SIZE));
        description.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        description.setHorizontalAlignment(SwingConstants.CENTER);
        description.setPreferredSize(new Dimension(WIDTH, TITLE_LABEL_HEIGHT));
        return description;
    }

    private Component generateHintsLabel(List<String> hints) {
        if (Collections.isEmpty(hints)) {
            return new JLabel();
        }
        return new RecommendSearchLabel(RECOMMEND, hints);
    }

}
