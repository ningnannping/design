package com.fr.design.mainframe.alphafine.exception;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/27
 */
public class AlphaFineNetworkException extends RuntimeException {

    public AlphaFineNetworkException() {
        super("NetWork Error");
    }

}
