package com.fr.design.mainframe.alphafine.component;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.model.TemplateResource;

import javax.swing.BorderFactory;
import java.awt.BorderLayout;
import java.awt.Color;
import java.util.List;

/**
 *
 * alphafine - 推荐搜索面板
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class RecommendSearchPane extends TemplateResourcePanel {

    private static final Color BORDER_WHITE = new Color(0xe8e8e9);

    public RecommendSearchPane(TemplateResource templateResource) {
        super();
        setTemplateResource(templateResource);
        initComponent();
        this.setLayout(new BorderLayout());

        this.setBorder(BorderFactory.createLineBorder(BORDER_WHITE, 1));
        this.add(getNorthPane(), BorderLayout.NORTH);
        this.add(getCenterPane(), BorderLayout.CENTER);
    }

    private void initComponent() {
        createNorthPane();
        createCenterPane();
    }


    private void createCenterPane() {

        String recommend = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Recommend_For_You");
        List<String> searchKeys = getTemplateResource().getRecommendSearchKey();

        setCenterPane(new RecommendSearchLabel(recommend, searchKeys));

    }

}