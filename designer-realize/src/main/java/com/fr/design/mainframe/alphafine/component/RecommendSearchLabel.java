package com.fr.design.mainframe.alphafine.component;

import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import org.jetbrains.annotations.NotNull;

import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * alphaFine - 推荐搜索词标签
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/19
 */
public class RecommendSearchLabel extends JPanel {

    private static final Color RECOMMEND_SEARCH_KEY_BLUE = new Color(0x419bf9);

    public RecommendSearchLabel(String title, @NotNull List<String> tips) {
        this.setLayout(new FlowLayout(FlowLayout.CENTER));
        this.setBackground(Color.WHITE);
        JLabel recommend = new JLabel(title);
        this.add(recommend);

        for (String key : tips) {
            JLabel keyLabel = new SearchKeyLabel(key);
            this.add(keyLabel);
        }
    }


    /**
     * 推荐搜索词，绑定alphaFine搜索事件
     */
    public class SearchKeyLabel extends JLabel {
        String searchKey;

        SearchKeyLabel(String searchKey) {
            this.searchKey = searchKey;
            setText(searchKey);
            setBackground(Color.WHITE);
            setForeground(RECOMMEND_SEARCH_KEY_BLUE);
            addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    AlphaFineHelper.getAlphaFineDialog().fireSearch(searchKey);
                }
            });
        }
    }
}
