package com.fr.design.mainframe.alphafine.model;


import com.fr.design.mainframe.alphafine.search.helper.FineMarketClientHelper;
import com.fr.design.mainframe.alphafine.search.manager.impl.TemplateResourceSearchManager;
import com.fr.json.JSONArray;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * alphafine - 模板资源详细数据
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourceDetail {

    // 与对应的模板资源关联
    private final TemplateResource root;
    private String info;
    private String vendor;
    private String htmlText;
    private List<String> detailInfo;
    private String[] tagsId;
    private List<String> tagsName;
    private double price;
    private String parentPkgName = "";
    private String parentPkgUrl;
    private String resourceUrl;

    public static final String ID = "id";
    public static final String INFO = "description";
    public static final String VENDOR = "vendor";
    public static final String DETAIL_INFO = "text";
    public static final String TAGS_ID = "cid";
    public static final String PRICE = "price";
    public static final String NAME = "name";
    public static final String PARENT_NAME = "parentName";
    public static final String PARENT_URL = "parentUrl";
    public static final String TAGS_NAME = "tagsName";
    public static final String URL = "url";



    public TemplateResourceDetail(TemplateResource resource) {
        this.root = resource;
    }


    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public TemplateResource getRoot() {
        return root;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<String> getDetailInfo() {
        return detailInfo;
    }

    public void setDetailInfo(List<String> detailInfo) {
        this.detailInfo = detailInfo;
    }

    public String[] getTagsId() {
        return tagsId;
    }

    public void setTagsId(String[] tagsId) {
        this.tagsId = tagsId;
    }

    public List<String> getTagsName() {
        return tagsName;
    }

    public String getTagsString() {
        StringBuilder sb = new StringBuilder();
        if (tagsName != null) {
            for (String tag : tagsName) {
                sb.append(tag + " ");
            }
        }
        return sb.toString();
    }

    public void setTagsName(List<String> tagsName) {
        this.tagsName = tagsName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getParentPkgName() {
        return parentPkgName;
    }

    public void setParentPkgName(String parentPkgName) {
        if (StringUtils.isEmpty(parentPkgName)) {
            this.parentPkgName = "";
        } else {
            this.parentPkgName = parentPkgName;
        }
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public String getParentPkgUrl() {
        return parentPkgUrl;
    }

    public void setParentPkgUrl(String parentPkgUrl) {
        this.parentPkgUrl = parentPkgUrl;
    }

    public static TemplateResourceDetail createByTemplateResource(TemplateResource root) {
        return Builder.buildByResource(root);
    }

    public static TemplateResourceDetail createFromEmbedResource(TemplateResource root) {
        return Builder.buildFromEmbedResource(root);
    }

    static class Builder {

        static FineMarketClientHelper helper = FineMarketClientHelper.getInstance();

        static TemplateResourceDetail buildFromEmbedResource(TemplateResource templateResource) {
            TemplateResourceDetail detail = new TemplateResourceDetail(templateResource);
            String resourceId = templateResource.getId();
            JSONArray embedResources = TemplateResourceSearchManager.getInstance().getEmbedResourceJSONArray();
            for (int i = 0; i < embedResources.length(); i++) {
                JSONObject resource = embedResources.getJSONObject(i);
                if (resourceId.equals(resource.getString(ID))) {
                    detail.setInfo(resource.getString(INFO));
                    detail.setHtmlText(reformat(resource.getString(DETAIL_INFO)));
                    detail.setVendor(resource.getString(VENDOR));
                    detail.setPrice(resource.getDouble(PRICE));
                    detail.setResourceUrl(resource.getString(URL));
                    detail.setParentPkgName(resource.getString(PARENT_NAME));
                    detail.setParentPkgUrl(resource.getString(PARENT_URL));
                    detail.setTagsName(Arrays.asList(resource.getString(TAGS_NAME).split(",")));
                    break;
                }
            }
            return detail;
        }

        static TemplateResourceDetail buildByResource(TemplateResource templateResource) {
            TemplateResourceDetail detail = new TemplateResourceDetail(templateResource);
            String resourceId = templateResource.getId();

            // 获取模板详情页的信息一共需要三次请求
            try {
                // 1请求详细信息
                JSONObject info = helper.getTemplateInfoById(resourceId);
                detail.setInfo(info.getString(INFO));
                detail.setHtmlText(reformat(info.getString(DETAIL_INFO)));
                detail.setVendor(info.getString(VENDOR));
                detail.setTagsId(info.getString(TAGS_ID).split(","));
                detail.setPrice(info.getDouble(PRICE));
                detail.setResourceUrl(helper.getTemplateUrlById(templateResource.getId()));

                // 2请求所属模板包信息
                JSONObject parentPkginfo = helper.getTemplateParentPackageByTemplateId(resourceId);
                if (parentPkginfo != null) {
                    detail.setParentPkgName(parentPkginfo.getString(NAME));
                    detail.setParentPkgUrl(FineMarketClientHelper.getInstance().getTemplateUrlById(parentPkginfo.getString(ID)));
                }

                // 3请求标签信息
                detail.setTagsName(helper.getTemplateTagsByTemplateTagIds(detail.getTagsId()));
                return detail;
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e, e.getMessage());
            }

            return null;
        }


        static final String A_TAG_FORMAT = "<a href=%s>%s</a>";
        static final String HTML_TAG_REGX="<[^>]+>";
        static final Pattern A_TAG_PATTERN = Pattern.compile("<a[^>]*href=(\\\"([^\\\"]*)\\\"|\\'([^\\']*)\\'|([^\\\\s>]*))[^>]*>(.*?)</a>");
        static final Pattern HTML_TAG_PATTERN = Pattern.compile(HTML_TAG_REGX);

        /**
         * 数据格式转换
         * 原始数据的格式不统一，纯文本、html都有; 统一转为纯文本，如果有a标签则保留
         * */
        static String reformat(String htmlDetailInfo) {
            String result = HTML_TAG_PATTERN.matcher(htmlDetailInfo).replaceAll("");
            Map<String, String> aMap = getLink(htmlDetailInfo);
            for (Map.Entry<String, String> entry : aMap.entrySet()) {
                String aTag = String.format(A_TAG_FORMAT, entry.getValue(), entry.getKey());
                result = result.replace(entry.getKey(), aTag);
            }
            return result;
        }

        /**
         * 读取dom中的a标签，转换为map
         */
        static Map<String, String> getLink(String html) {
            Map<String, String> map = new HashMap<>();
            Matcher matcher = A_TAG_PATTERN.matcher(html);
            while (matcher.find()) {
                map.put(matcher.group(5), matcher.group(1));
            }
            return map;
        }
    }

}