package com.fr.design.mainframe.alphafine.component;

import com.fr.base.GraphHelper;
import com.fr.base.svg.SVGLoader;
import com.fr.design.DesignerEnvManager;
import com.fr.design.mainframe.alphafine.model.ProductNews;
import com.fr.design.utils.SvgPaintUtils;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import java.awt.RenderingHints;
import java.util.Set;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/15
 */
public class ProductNewsImagePanel extends JPanel {

    private static final Image NEW_TIP_IMAGE = SVGLoader.load("/com/fr/design/mainframe/alphafine/images/new_tip.svg");

    private static final int BACKGROUND_HEIGHT = 20;

    private static final Color BACKGROUND_COLOR = new Color(116, 181, 249);

    private static final Color COVER_COLOR = new Color(116, 181, 249, 26);

    private ProductNews productNews;

    private int width = 200;
    private int height = 150;

    public ProductNewsImagePanel(ProductNews productNews) {
        this.productNews = productNews;
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        Color defaultColor = g2.getColor();

        Image image = productNews.getImage();
        if (image != null) {
            g2.drawImage(productNews.getImage(), 0, 0, getWidth(), getHeight(), this);
        } else {
            g2.setColor(COVER_COLOR);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }
        Set<Long> readSet = DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().getReadSet();
        if (!readSet.contains(productNews.getId())) {
            SvgPaintUtils.beforePaint(g2);
            g2.drawImage(NEW_TIP_IMAGE, 0, 0, this);
            SvgPaintUtils.afterPaint(g2);
        }

        g2.setColor(BACKGROUND_COLOR);
        g2.fillRect(0, getHeight() - BACKGROUND_HEIGHT, getWidth(), BACKGROUND_HEIGHT);
        g2.setColor(Color.WHITE);
        int x = (getWidth() - GraphHelper.getWidth(productNews.getTag().getDesc(), g2.getFont())) / 2;
        g2.drawString(productNews.getTag().getDesc(), x, getHeight() - 5);
        g2.setColor(defaultColor);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }


}
