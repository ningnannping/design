package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.component.AlphaFineFrame;
import com.fr.design.mainframe.alphafine.component.SearchHintPane;
import com.fr.design.mainframe.alphafine.search.SearchWorkerManager;
import com.fr.design.mainframe.alphafine.search.manager.SearchProviderRegistry;
import com.fr.general.CloudCenter;
import com.fr.log.FineLoggerFactory;

import javax.swing.JPanel;
import javax.swing.SwingWorker;
import java.awt.CardLayout;
import java.util.ArrayList;
import java.util.List;

/**
 * alphafine插件默认页
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class DefaultPluginContentPane extends DefaultContentPane {

    private static final String[] HINTS = CloudCenter.getInstance().acquireConf("alphafine.plugin.recommend", "JS,API,JSON").split(",");
    private static final String LOADING_PANE = "loading";
    private static final String NETWORK_ERROR = "networkError";
    private static final String RESULT_PANE = "result";
    private SearchWorkerManager searchWorkerManager;
    private CellType cellType;
    private AlphaFineFrame parentWindow;
    private CardLayout cardLayout;
    private SearchLoadingPane searchLoadingPane;
    private NetWorkFailedPane netWorkFailedPane;
    private SwingWorker<Boolean, Void> worker;


    public DefaultPluginContentPane(CellType cellType, AlphaFineFrame parentWindow) {
        super();
        this.cellType = cellType;
        this.parentWindow = parentWindow;
        this.cardLayout = new CardLayout();
        this.setLayout(cardLayout);
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        initPanel();
        add(searchLoadingPane, LOADING_PANE);
        add(netWorkFailedPane, NETWORK_ERROR);
        worker = createWorker();
        worker.execute();
    }

    private void initPanel() {
        this.searchLoadingPane = new SearchLoadingPane();
        this.netWorkFailedPane = new NetWorkFailedPane(()-> reload());
    }

    private void switchPane(String tag) {
        cardLayout.show(this, tag);
    }

    private SwingWorker<Boolean, Void> createWorker() {
        return new SwingWorker<Boolean, Void>() {
            @Override
            protected Boolean doInBackground() throws Exception {
                switchPane(LOADING_PANE);
                return AlphaFineHelper.isNetworkOk();
            }

            @Override
            protected void done() {
                super.done();
                try {
                    boolean networkOk = get();
                    if (!networkOk) {
                        switchPane(NETWORK_ERROR);
                    } else {
                        List<String> searchKeys = new ArrayList<>();
                        for (String s : HINTS) {
                            searchKeys.add(s);
                        }
                        searchWorkerManager = new SearchWorkerManager(
                                cellType,
                                searchTextBean -> SearchProviderRegistry.getSearchProvider(cellType).getDefaultResult(),
                                parentWindow,
                                new SimpleRightSearchResultPane(new SearchHintPane(searchKeys))
                        );
                        searchWorkerManager.showDefault(DefaultPluginContentPane.this);
                    }
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e, e.getMessage());
                }
            }
        };
    }

    @Override
    public void showResult(JPanel result) {
        add(result, RESULT_PANE);
        switchPane(RESULT_PANE);
    }

    /**
     * 网络异常时，重新加载
     */
    public void reload() {
        worker = createWorker();
        worker.execute();
    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }
}
