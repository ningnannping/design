package com.fr.design.mainframe.alphafine.component;

import com.fr.base.GraphHelper;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.utils.DesignUtils;
import com.fr.general.FRFont;
import com.fr.third.jodd.util.StringUtil;

import javax.swing.JPanel;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;


/**
 *
 * alphafine - 模板资源图片面板
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourceImagePanel extends JPanel {

    private static final int BACKGROUND_HEIGHT = 20;

    private static final Color BACKGROUND_COLOR = new Color(0x419BF9);

    private static final Font TAG_FONT = DesignUtils.getDefaultGUIFont().applySize(12);

    private static final Color COVER_COLOR = new Color(116, 181, 249, 26);

    private TemplateResource templateResource;

    private int width = 200;
    private int height = 100;

    public TemplateResourceImagePanel(TemplateResource templateResource) {
        this.templateResource = templateResource;
    }

    public TemplateResourceImagePanel(TemplateResource templateResource, int width, int height) {
        this(templateResource);
        this.width = width;
        this.height = height;
    }


    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        Color defaultColor = g2.getColor();

        Image image = templateResource.getImage();
        if (image != null) {
            g2.drawImage(templateResource.getImage(), 0, 0, getWidth(), getHeight(), this);
        } else {
            g2.setColor(COVER_COLOR);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

        String tagName = templateResource.getType().getName();

        if (!StringUtil.isEmpty(tagName)) {
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, .8f));
            g2.setColor(BACKGROUND_COLOR);
            g2.fillRect(0, getHeight() - BACKGROUND_HEIGHT, getWidth(), BACKGROUND_HEIGHT);
            g2.setColor(Color.WHITE);
            int x = (getWidth() - GraphHelper.getWidth(tagName, g2.getFont())) / 2;
            g2.setFont(TAG_FONT);
            g2.drawString(tagName, x, getHeight() - 5);
        }
        g2.setColor(defaultColor);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }


}