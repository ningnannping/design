package com.fr.design.mainframe.app;

import com.fr.base.BaseUtils;
import com.fr.base.Style;
import com.fr.base.TempNameStyle;
import com.fr.base.chart.exception.ChartNotFoundException;
import com.fr.base.extension.FileExtension;
import com.fr.base.io.XMLEncryptUtils;
import com.fr.config.ServerPreferenceConfig;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.server.StyleListAction;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DecodeDialog;
import com.fr.design.mainframe.TemplateLockedHandler;
import com.fr.design.ui.util.UIUtil;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.exception.DecryptTemplateException;
import com.fr.exception.RemoteDesignPermissionDeniedException;
import com.fr.exception.TplLockedException;
import com.fr.file.FILE;
import com.fr.log.FineLoggerFactory;
import com.fr.main.impl.WorkBook;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by juhaoyu on 2018/6/27.
 */
class CptApp extends AbstractWorkBookApp {

    @Override
    public String[] defaultExtensions() {

        return new String[]{FileExtension.CPT.getExtension()};
    }

    @Override
    public WorkBook asIOFile(FILE file) {
        return asIOFile(file, true);
    }

    @Override
    public WorkBook asIOFile(FILE file, boolean needCheck) {
        if (XMLEncryptUtils.isCptEncoded() &&
                !XMLEncryptUtils.checkVaild(DesignerEnvManager.getEnvManager().getEncryptionKey())) {
            if (!new DecodeDialog(file).isPwdRight()) {
                FineLoggerFactory.getLogger().error(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_ECP_Error_Pwd"));
                return new WorkBook();
            }
        }

        WorkBook tpl = new WorkBook();
        TempNameStyle namestyle = TempNameStyle.getInstance();
        namestyle.clear();
        try {
            tpl.readStream(file.asInputStream());
        } catch (DecryptTemplateException | ChartNotFoundException e) {
            throw e;
        } catch (RemoteDesignPermissionDeniedException exp) {
            FineLoggerFactory.getLogger().error(Toolkit.i18nText("Fine-Design_Basic_Template_Permission_Denied") + file, exp);
        } catch (TplLockedException exp) {
            FineLoggerFactory.getLogger().error(file + Toolkit.i18nText("Fine-Design_Basic_Template_Status_Locked"), exp);
            TemplateLockedHandler.generateTipAndRefresh();
        } catch (Exception exp) {
            FineLoggerFactory.getLogger().error(Toolkit.i18nText("Fine-Design_Report_NS_Exception_ReadError") + file, exp);
        }
        if (needCheck) {
            checkNameStyle(namestyle);
        }
        return tpl;
    }

    private static void checkNameStyle(TempNameStyle namestyle) {

        Iterator it = namestyle.getIterator();
        ArrayList<String> al = new ArrayList<String>();
        while (it.hasNext()) {
            al.add((String) it.next());
        }
        if (!al.isEmpty()) {
            UIUtil.invokeLaterIfNeeded(new Runnable() {
                @Override
                public void run() {
                    showConfirmDialog(al);
                }
            });
        }
    }

    private static void showConfirmDialog(final ArrayList<String> namelist) {

        final JDialog jd = new JDialog();
        // 模态一下，因为可能会多个样式丢失
        jd.setModal(true);
        jd.setSize(450, 150);
        jd.setResizable(false);
        jd.setIconImage(BaseUtils.readImage("/com/fr/base/images/oem/logo.png"));
        String message = namelist.toString().replaceAll("\\[", "").replaceAll("\\]", "");
        UILabel jl = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Template_Global_Style_Missed", message));
        jl.setHorizontalAlignment(SwingConstants.CENTER);
        jd.add(jl, BorderLayout.CENTER);
        JPanel jp = new JPanel();

        // ”是“按钮，点击之后将生成一个全局样式，并写入xml
        UIButton confirmButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Yes"));
        confirmButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    for (String name : namelist) {
                        ServerPreferenceConfig.getInstance().putStyle(name, Style.DEFAULT_STYLE);
                    }
                } catch (Exception ex) {
                    FineLoggerFactory.getLogger().error(ex.getMessage());
                }
                jd.dispose();
                new StyleListAction().actionPerformed(e);// 弹窗
            }
        });

        UIButton noButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_No"));
        noButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                jd.dispose();
            }
        });

        jp.add(confirmButton);
        jp.add(noButton);
        jd.setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Template_Custom_Style_Missed"));
        jd.add(jp, BorderLayout.SOUTH);
        GUICoreUtils.centerWindow(jd);
        jd.setVisible(true);
    }

}
