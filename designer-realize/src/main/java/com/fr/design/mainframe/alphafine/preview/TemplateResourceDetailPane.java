package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.action.StartUseAction;
import com.fr.design.mainframe.alphafine.component.TemplateResourceImagePanel;
import com.fr.design.mainframe.alphafine.model.TemplateResourceDetail;
import com.fr.design.utils.BrowseUtils;
import com.fr.design.utils.DesignUtils;
import com.fr.design.utils.LinkStrUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * alphafine - 模板资源详情页
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourceDetailPane extends JPanel {


    private TemplateResourceDetail data;

    private TemplateResourceImagePanel imagePane;
    private JPanel contentPane;
    private UIScrollPane infoScrollPane;
    private JPanel operatePane;
    private UIScrollPane detailInfoPane;


    private static final int IMAGE_HEIGHT = 170;
    private static final int IMAGE_WIDTH = 310;
    private static final int SCROLL_PANE_WIDTH = 315;
    private static final int SCROLL_PANE_HEIGHT = 135;
    private static final int CONTENT_PANE_WIDTH = 320;
    private static final int CONTENT_PANE_HEIGHT = 180;
    private static final int DETAIL_PANE_HEIGHT = 95;
    private static final int TEXT_SCROLL_PANE_HEIGHT = 500;
    private static final int PANE_WIDTH = 635;
    private static final int BUTTON_WIDTH = 68;
    private static final int BUTTON_HEIGHT = 20;

    private static final String GOTO_DETAIL = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_GOTO_DETAIL");
    private static final String START_USE = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_START_USE");
    private static final String VENDOR = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_Vendor");
    private static final String TAGS = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_Tags");
    private static final String PARENT_PACKAGE = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_Parent_Package");
    private static final String DETAIL_INFO = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_Info");
    private static final String FREE = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Detail_Price_Free");
    private static final String SPACE = " ";
    private static final String LF = "<br>";
    private static final String RMB = "￥";


    private static final Color INFO_PANE_BACKGROUND = new Color(0xf9f9f9);
    private static final Color INFO_PANE_FOREGROUND = new Color(0x5b5b5c);
    private static final Color MORE_INFO_LINK = new Color(0x419bf9);

    private static final Font HTML_FONT = DesignUtils.getDefaultGUIFont().applySize(12);
    private static final Color HTML_COLOR = new Color(0x5c5c5d);
    private static final String HTML_FORMAT = "<html><style>a {color: #419BF9 !important;text-decoration:none !important;}</style><body style=\"line-height: 20px;"+ LinkStrUtils.generateStyle(HTML_FONT, HTML_COLOR) +  "\">%s</body></html>";
    private static final String DETAIL_INFO_HTML_FORMAT = "<html><style>a {color: #419BF9 !important;text-decoration:none !important;}</style><body style=\"line-height: 20px;" + LinkStrUtils.generateStyle(HTML_FONT, HTML_COLOR) +   "\"><p>" + DETAIL_INFO + "</p>%s</body></html>";
    private static final String HTML_P_TAG_FORMAT = "<p style=\"margin-top:5pt;\">%s</p>";



    public TemplateResourceDetailPane(TemplateResourceDetail detail) {
        this.data = detail;
        initComponent();
        this.setLayout(new FlowLayout(FlowLayout.LEFT));
        this.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));
        this.add(imagePane);
        this.add(contentPane);
        this.add(detailInfoPane);
        this.setBackground(Color.WHITE);
        SwingUtilities.invokeLater(()->{scrollToTop();});
    }

    /**
     * scrollPane创建后会拉到最底下，初始化的时候手动拉到顶
     */
    public void scrollToTop() {
        infoScrollPane.getVerticalScrollBar().setValue(0);
        detailInfoPane.getVerticalScrollBar().setValue(0);
    }

    private void initComponent() {
        createImagePane();
        createContentPane();
        createDetailInfoScrollPane();
    }

    private void createContentPane() {
        createInfoScrollPane();
        createOperatePane();
        contentPane = new JPanel();
        contentPane.setLayout(new FlowLayout(FlowLayout.LEFT));
        contentPane.setPreferredSize(new Dimension(CONTENT_PANE_WIDTH, CONTENT_PANE_HEIGHT));
        contentPane.add(infoScrollPane);
        contentPane.add(operatePane);
        contentPane.setBackground(Color.WHITE);
    }

    /**
     * 操作区：查看详情，立即使用
     */
    private void createOperatePane() {
        operatePane = new JPanel(new FlowLayout(FlowLayout.LEFT));

        JLabel emptyLabel = new JLabel();
        emptyLabel.setPreferredSize(new Dimension(145, 25));
        JLabel priceLabel = new JLabel();
        priceLabel.setForeground(Color.RED);
        if (data.getPrice() == 0) {
            priceLabel.setText(FREE);
        } else {
            priceLabel.setText(RMB + SPACE + data.getPrice());
        }

        operatePane.add(createLinkLabel());
        operatePane.add(emptyLabel);
        operatePane.add(priceLabel);
        operatePane.add(createStartUseButton());
        operatePane.setBackground(Color.WHITE);

    }

    /**
     * 查看详情
     */
    JLabel createLinkLabel() {
        JLabel linkLabel = new JLabel(GOTO_DETAIL);
        linkLabel.setBackground(Color.WHITE);
        linkLabel.setForeground(MORE_INFO_LINK);
        linkLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                openResourceUrl(data.getResourceUrl());
            }
        });
        return linkLabel;
    }

    /**
     * 方便埋点
     */
    void openResourceUrl(String url) {
        BrowseUtils.browser(url);
    }


    /**
     * “立即使用” 按钮
     */
    JButton createStartUseButton() {
        JButton starUseButton = new JButton(START_USE) {
            @Override
            public void paintComponent(Graphics g) {
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                g2d.setColor(UIConstants.FLESH_BLUE);
                g2d.fillRoundRect(0, 0, getWidth(), getHeight(), 4, 4);
                super.paintComponent(g2d);
            }
        };
        starUseButton.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        starUseButton.setForeground(Color.WHITE);
        starUseButton.setFont(AlphaFineConstants.MEDIUM_FONT);
        starUseButton.addActionListener(new StartUseAction(data));
        starUseButton.setPreferredSize(new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT));
        starUseButton.setContentAreaFilled(false);
        return starUseButton;
    }

    private void createImagePane() {
        imagePane = new TemplateResourceImagePanel(data.getRoot(), IMAGE_WIDTH, IMAGE_HEIGHT);
    }

    /**
     * 基本信息页
     */
    private void createInfoScrollPane() {

        StringBuilder sb = new StringBuilder();

        // 开发者
        sb.append(String.format(HTML_P_TAG_FORMAT, VENDOR + data.getVendor()));
        // 标签
        sb.append(String.format(HTML_P_TAG_FORMAT, TAGS + data.getTagsString()));
        // 所属模板包
        if (!StringUtils.isEmpty(data.getParentPkgName())) {
            sb.append(String.format(HTML_P_TAG_FORMAT, PARENT_PACKAGE + LinkStrUtils.generateLinkTagWithoutUnderLine(data.getParentPkgUrl(), data.getParentPkgName())));
        }
        // 信息
        sb.append(String.format(HTML_P_TAG_FORMAT, data.getInfo()));


        MessageWithLink content = new MessageWithLink(String.format(HTML_FORMAT, sb));
        content.setBackground(INFO_PANE_BACKGROUND);
        content.setForeground(INFO_PANE_FOREGROUND);
        infoScrollPane = new UIScrollPane(content);
        infoScrollPane.setForeground(INFO_PANE_FOREGROUND);
        infoScrollPane.setPreferredSize(new Dimension(SCROLL_PANE_WIDTH, SCROLL_PANE_HEIGHT));
        infoScrollPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    }

    /**
     * 详细信息页
     */
    private void createDetailInfoScrollPane() {
        MessageWithLink content = new MessageWithLink(String.format(DETAIL_INFO_HTML_FORMAT, data.getHtmlText()));
        detailInfoPane = new UIScrollPane(content);
        detailInfoPane.setPreferredSize(new Dimension(PANE_WIDTH, DETAIL_PANE_HEIGHT));
        detailInfoPane.setBackground(Color.WHITE);
        detailInfoPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
    }



}