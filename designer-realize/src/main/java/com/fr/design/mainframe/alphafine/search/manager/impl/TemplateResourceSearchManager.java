package com.fr.design.mainframe.alphafine.search.manager.impl;


import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.mainframe.alphafine.model.TemplateResourceDetail;
import com.fr.design.mainframe.alphafine.search.helper.FineMarketClientHelper;
import com.fr.general.CloudCenter;
import com.fr.general.IOUtils;
import com.fr.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * alphafine - 模板资源搜索助手
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourceSearchManager {

    private static final TemplateResourceSearchManager INSTANCE = new TemplateResourceSearchManager();
    public static TemplateResourceSearchManager getInstance() {
        return INSTANCE;
    }

    public static final String LOCAL_RESOURCE_URL = "/com/fr/design/mainframe/alphafine/template_resource/local_templates.json";
    private static final FineMarketClientHelper HELPER = FineMarketClientHelper.getInstance();


    /**
     * 帆软市场暂时没有分页搜索接口，先全量搜，分页展示
     * */
    public List<TemplateResource> getSearchResult(String searchText) {
        List<TemplateResource> resourceList = new ArrayList<>();

        resourceList.addAll(searchOnline(searchText));
        resourceList.addAll(searchLocal(searchText));

        resourceList.stream().forEach(resource -> {resource.setSearchWord(searchText);});

        return resourceList;
    }

    /**
     * 联网搜索
     * */
    private List<TemplateResource> searchOnline(String searchText) {
        List<TemplateResource> resourceList = new ArrayList<>();
        try {
            JSONArray jsonArray = HELPER.getTemplateInfoByName(searchText);
            if (jsonArray != null && !jsonArray.isEmpty()) {
                resourceList.addAll(TemplateResource.createByJson(jsonArray));
            }
        } catch (Exception e) {

        }
        return resourceList;
    }

    /**
     * 本地搜索
     * */
    private List<TemplateResource> searchLocal(String searchText) {
        List<TemplateResource> resourceList = new ArrayList<>();
        if (resourceList.isEmpty()) {
            List<TemplateResource> localResource = getEmbedResourceList();
            localResource.stream().forEach(resource->{
                if (resource.getName().toLowerCase().contains(searchText)) {
                    resourceList.add(resource);
                }
            });
        }
        return resourceList;
    }


    /**
     * 返回默认资源
     * */
    public List<TemplateResource> getDefaultResourceList() {
        List<TemplateResource> resourceList = getEmbedResourceList();
        // 添加推荐搜索卡片
        resourceList.add(TemplateResource.getRecommendSearch());
        return resourceList;
    }

    /**
     * 返回内置资源
     * */
    public List<TemplateResource> getEmbedResourceList() {
        List<TemplateResource> resourceList = new ArrayList<>();
        JSONArray jsonArray = getEmbedResourceJSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            TemplateResource resource = TemplateResource.createByJson(jsonArray.getJSONObject(i));
            resource.setEmbed(true);
            resourceList.add(resource);
        }
        return resourceList;
    }

    public JSONArray getEmbedResourceJSONArray() {
        String jsonString = IOUtils.readResourceAsString(LOCAL_RESOURCE_URL);
        return new JSONArray(jsonString);
    }

    public List<String> getRecommendSearchKeys() {
        List<String> searchKey = new ArrayList<>();
        String[] keys = CloudCenter.getInstance().acquireConf("alphafine.tempalte.recommend", "跑马灯,填报,地图").split(",");
        for (String k : keys) {
            searchKey.add(k);
        }
        return searchKey;
    }


    public TemplateResourceDetail getDetailSearchResult(TemplateResource resource) {
        if (AlphaFineHelper.isNetworkOk()) {
            return TemplateResourceDetail.createByTemplateResource(resource);
        } else {
            return TemplateResourceDetail.createFromEmbedResource(resource);
        }
    }

}