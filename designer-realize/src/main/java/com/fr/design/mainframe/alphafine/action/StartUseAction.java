package com.fr.design.mainframe.alphafine.action;

import com.fr.common.util.Strings;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.download.FineMarketConstants;
import com.fr.design.mainframe.alphafine.download.FineMarketDownloadManager;
import com.fr.design.mainframe.alphafine.model.TemplateResourceDetail;
import com.fr.file.FileFILE;
import com.fr.log.FineLoggerFactory;

import javax.swing.SwingWorker;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;


/**
 *
 * 点击后跳转至帆软市场下载对应模板资源
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/22
 *
 * TODO:可以参考mini组件商城的下载@ComponentsPackageInstallation#install
 * */
public class StartUseAction implements ActionListener {

    TemplateResourceDetail resourceDetail;

    public StartUseAction(TemplateResourceDetail detail) {
        this.resourceDetail = detail;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        new SwingWorker<String, Void>() {

            @Override
            protected String doInBackground() throws Exception {
                return FineMarketDownloadManager.getInstance().installResource(resourceDetail.getRoot(), AlphaFineHelper.getAlphaFineDialog());
            }

            @Override
            protected void done() {
                try {
                    open(get());
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e, e.getMessage());
                }
                super.done();
            }
        }.execute();
    }

    /**
     * 打开模板并打开文件目录
     * */
    void open(String fileName) throws IOException {
        if (Strings.isEmpty(fileName)) {
            return;
        }
        // 打开模板
        openInDesigner(fileName);

        // 打开系统文件夹
        File parentDir = new File(fileName).getParentFile();
        Desktop.getDesktop().open(parentDir);
    }

    void openInDesigner(String fileName) {
        File fileNeedOpen = new File(fileName);
        if (fileName.endsWith(FineMarketConstants.ZIP)) {
            File[] files = fileNeedOpen.getParentFile().listFiles();
            fileNeedOpen = getFirstTemplate(files);
        } else if (fileName.endsWith(FineMarketConstants.RAR)) {
            // rar资源没有解压，所以不用打开模板
            return;
        }

        // 打开模板
        if (fileNeedOpen == null) {
            //有可能压缩包解压出来还是压缩包
            FineLoggerFactory.getLogger().error("AlphaFine open resource error: " + fileName);
        } else {
            DesignerContext.getDesignerFrame().openTemplate(new FileFILE(fileNeedOpen));
        }
    }


    private File getFirstTemplate(File[] files) {
        for (File f : files) {
            if (f.getName().endsWith(FineMarketConstants.CPT) ||
                    f.getName().endsWith(FineMarketConstants.FRM) ||
                    f.getName().endsWith(FineMarketConstants.FVS)) {
                return f;
            }
        }
        return null;
    }
}
