package com.fr.design.mainframe;

import com.fr.design.file.HistoryTemplateListPane;
import com.fr.design.gui.frpane.HyperlinkGroupPane;
import com.fr.design.gui.frpane.HyperlinkGroupPaneActionProvider;
import com.fr.general.ComparatorUtils;
import com.fr.js.FormHyperlinkProvider;
import com.fr.stable.bridge.StableFactory;

/**
 * Created by plough on 2017/7/21.
 */
public class ReportHyperlinkGroupPane extends HyperlinkGroupPane {
    private static ReportHyperlinkGroupPane singleton;

    // 仅供继承使用，外部通过 getInstance 获取实例
    protected ReportHyperlinkGroupPane(HyperlinkGroupPaneActionProvider hyperlinkGroupPaneActionProvider) {
        super(hyperlinkGroupPaneActionProvider);
    }

    public synchronized static ReportHyperlinkGroupPane getInstance(HyperlinkGroupPaneActionProvider hyperlinkGroupPaneActionProvider) {
        if (singleton == null) {
            singleton = new ReportHyperlinkGroupPane(hyperlinkGroupPaneActionProvider);
        }
        singleton.refreshPane();
        return singleton;
    }

    protected void refreshPane() {
        ElementCasePane reportPane = ((JWorkBook) HistoryTemplateListPane.getInstance().getCurrentEditingTemplate()).getEditingElementCasePane();
        if (reportPane == null) {
            return;
        }
        populate(reportPane);
    }


    @Override
    protected void initCreatorsFilter() {
        this.creatorsFilter = nameableCreator -> {
            // 如果是普通报表单元格，那么没有 FormHyperlink 选项
            FormHyperlinkProvider formHyperlink = StableFactory.getMarkedInstanceObjectFromClass(FormHyperlinkProvider.XML_TAG, FormHyperlinkProvider.class);
            return !ComparatorUtils.equals(nameableCreator.getHyperlink(), formHyperlink.getClass());
        };
    }
}
