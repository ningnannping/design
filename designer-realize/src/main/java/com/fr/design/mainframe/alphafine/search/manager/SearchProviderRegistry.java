package com.fr.design.mainframe.alphafine.search.manager;

import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.search.manager.fun.AlphaFineSearchProvider;
import com.fr.design.mainframe.alphafine.search.manager.impl.ActionSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.FileSearchManager;
import com.fr.design.mainframe.alphafine.search.manager.impl.PluginSearchManager;

import java.util.HashMap;
import java.util.Map;

/**
 * alphafine搜索提供者注册到这里
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class SearchProviderRegistry {
    private static Map<CellType, AlphaFineSearchProvider> map;

    static {
        map = new HashMap<>();
        map.put(CellType.PLUGIN, PluginSearchManager.getInstance());
        map.put(CellType.ACTION, ActionSearchManager.getInstance());
        map.put(CellType.FILE, FileSearchManager.getInstance());
    }

    /**
     * 根据celltype获得对应searchProvider
     */
    public static AlphaFineSearchProvider getSearchProvider(CellType cellType) {
        return map.get(cellType);
    }
}
