package com.fr.design.mainframe.alphafine.component;

import com.fr.common.util.Strings;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.mainframe.alphafine.preview.TemplateShopPane;
import com.fr.design.utils.BrowseUtils;
import com.fr.design.utils.DesignUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 *
 * alphafine - 模板资源面板
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourcePanel extends JPanel {

    private JPanel northPane;
    private JPanel centerPane;
    private TemplateResource templateResource;

    private static final Color PANEL_BORDER_COLOR = new Color(0xe8e8e9);
    private static final Color DEMO_LABEL_FOREGROUND = new Color(0x419bf9);
    private static final Font RESOURCE_NAME_FONT = DesignUtils.getDefaultGUIFont().applySize(12);
    private static final Color RESOURCE_NAME_COLOR = new Color(0x5c5c5d);

    protected TemplateResourcePanel() {

    }

    protected TemplateResourcePanel(TemplateResource templateResource) {
        this.templateResource = templateResource;
        initComponent();
        this.setLayout(new BorderLayout());
        this.setBorder(BorderFactory.createLineBorder(PANEL_BORDER_COLOR, 1));
        this.add(northPane, BorderLayout.NORTH);
        this.add(centerPane, BorderLayout.CENTER);
        addAction();
    }

    private void addAction() {
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                TemplateShopPane.getInstance().searchAndShowDetailPane(templateResource);
            }
        });
    }

    /**
     * 通过数据构造面板
     * */
    public static TemplateResourcePanel create(TemplateResource templateResource) {
        if (TemplateResource.Type.RECOMMEND_SEARCH.equals(templateResource.getType())) {
            return new RecommendSearchPane(templateResource);
        } else {
            return new TemplateResourcePanel(templateResource);
        }
    }

    public JPanel getNorthPane() {
        return northPane;
    }
    public JPanel getCenterPane() {
        return centerPane;
    }
    public TemplateResource getTemplateResource() {
        return templateResource;
    }

    public void setNorthPane(JPanel northPane) {
        this.northPane = northPane;
    }

    public void setCenterPane(JPanel centerPane) {
        this.centerPane = centerPane;
    }

    public void setTemplateResource(TemplateResource templateResource) {
        this.templateResource = templateResource;
    }

    private void initComponent() {
        createNorthPane();
        createCenterPane();
    }

    protected void createNorthPane() {
        northPane = new TemplateResourceImagePanel(templateResource);
    }

    private void createCenterPane() {
        JLabel nameLabel = createHtmlNameLabel();


        JLabel demoLabel = new JLabel();
        if (templateResource.hasDemoUrl()) {
            demoLabel.setText(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Demo"));
            demoLabel.setForeground(DEMO_LABEL_FOREGROUND);
            demoLabel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    try {
                        BrowseUtils.browser(templateResource.getDemoUrl());
                    } catch (Exception ex) {
                        FineLoggerFactory.getLogger().error(ex, ex.getMessage());
                    }
                }
            });
        }

        centerPane = new JPanel(new BorderLayout());
        centerPane.setBackground(Color.WHITE);
        centerPane.add(nameLabel, BorderLayout.WEST);
        centerPane.add(demoLabel, BorderLayout.EAST);
    }


    private static final String SEARCH_WORD_FORMAT = "<span style=\"color:#419BF9\">%s</span>";
    private static final String HTML_JLABEL_FORMAT = "<html>%s</html>";

    private JLabel createHtmlNameLabel() {
        JLabel label = new JLabel();
        String htmlText = templateResource.getName();
        String searchWord = templateResource.getSearchWord();
        if (!Strings.isEmpty(searchWord) && htmlText.contains(searchWord)) {
            htmlText = htmlText.replace(searchWord, String.format(SEARCH_WORD_FORMAT, searchWord));
        }
        htmlText = String.format(HTML_JLABEL_FORMAT, htmlText);
        label.setText(htmlText);
        label.setFont(RESOURCE_NAME_FONT);
        label.setForeground(RESOURCE_NAME_COLOR);
        label.setBackground(Color.WHITE);
        label.setBorder(BorderFactory.createEmptyBorder());
        return label;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(180, 90);
    }
}