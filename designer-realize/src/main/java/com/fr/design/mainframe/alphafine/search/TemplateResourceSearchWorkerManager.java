package com.fr.design.mainframe.alphafine.search;

import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.component.AlphaFineFrame;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.mainframe.alphafine.preview.TemplateShopPane;
import com.fr.log.FineLoggerFactory;

import javax.swing.SwingWorker;
import java.util.List;
import java.util.function.Function;

/**
 *
 * alphafine - 模板资源搜索管理
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourceSearchWorkerManager implements SearchManager {

    private final CellType cellType;

    private SwingWorker<List<TemplateResource>, Void> searchWorker;

    private Function<SearchTextBean, List<TemplateResource>> searchFunction;

    private AlphaFineFrame alphaFineFrame;

    private volatile boolean searchResult = true;

    private volatile boolean searchOver = false;

    private volatile boolean networkError = false;

    public TemplateResourceSearchWorkerManager(CellType cellType, Function<SearchTextBean, List<TemplateResource>> searchFunction, AlphaFineFrame alphaFineFrame) {
        this.cellType = cellType;
        this.searchFunction = searchFunction;
        this.alphaFineFrame = alphaFineFrame;
    }

    @Override
    public void doSearch(SearchTextBean searchTextBean) {
        checkSearchWork();
        searchOver = false;

        this.searchWorker = new SwingWorker<List<TemplateResource>, Void>() {
            @Override
            protected List<TemplateResource> doInBackground() {
                List<TemplateResource> list;
                if (!AlphaFineHelper.isNetworkOk()) {
                    networkError = true;
                    FineLoggerFactory.getLogger().warn("alphaFine network error");
                }
                list = searchFunction.apply(searchTextBean);
                return list;
            }

            @Override
            protected void done() {
                searchOver = true;
                if (!isCancelled()) {
                    try {
                        List<TemplateResource> list = get();
                        searchResult = !list.isEmpty();
                        showResult(list);
                    } catch (Exception e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                }
            }
        };
        this.searchWorker.execute();
    }

    void showResult(List<TemplateResource> list) {
        if (networkError && !searchResult) {
            alphaFineFrame.showResult(AlphaFineConstants.NETWORK_ERROR);
            return;
        }

        if (alphaFineFrame.getSelectedType() == cellType) {
            if (!searchResult) {
                alphaFineFrame.showResult(CellType.NO_RESULT.getFlagStr4None());
            } else {
                TemplateShopPane.getInstance().refreshPagePane(list);
                AlphaFineHelper.getAlphaFineDialog().showResult(cellType.getFlagStr4None());
            }
        }
    }

    @Override
    public boolean hasSearchResult() {
        return searchResult;
    }

    @Override
    public boolean isSearchOver() {
        return searchOver;
    }

    private void checkSearchWork() {
        if (this.searchWorker != null && !this.searchWorker.isDone()) {
            this.searchWorker.cancel(true);
            this.searchWorker = null;
        }
    }

    @Override
    public boolean isNetWorkError() {
        return networkError;
    }
}