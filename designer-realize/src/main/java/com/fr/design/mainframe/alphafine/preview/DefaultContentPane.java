package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.component.AlphaFineFrame;
import com.fr.design.mainframe.alphafine.component.SearchHintPane;
import com.fr.design.mainframe.alphafine.search.SearchWorkerManager;
import com.fr.design.mainframe.alphafine.search.manager.SearchProviderRegistry;

import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * alphaFine - 默认展示页面
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class DefaultContentPane extends JPanel {

    // 左边展示内容，右边展示搜索提示
    private SearchWorkerManager searchWorkerManager;
    private CellType cellType;

    public DefaultContentPane(CellType cellType, AlphaFineFrame parentWindow) {
        this.setLayout(new BorderLayout());
        this.setPreferredSize(AlphaFineConstants.PREVIEW_SIZE);
        this.searchWorkerManager = new SearchWorkerManager(
                cellType,
                searchTextBean -> SearchProviderRegistry.getSearchProvider(cellType).getDefaultResult(),
                parentWindow,
                new SimpleRightSearchResultPane(new SearchHintPane())
        );
        this.searchWorkerManager.showDefault(this);
    }

    /**
     * 显示搜索结果
     */
    public void showResult(JPanel result) {
        add(result);
        repaint();
    }

    public DefaultContentPane() {

    }

    public CellType getCellType() {
        return cellType;
    }

    public void setCellType(CellType cellType) {
        this.cellType = cellType;
    }
}
