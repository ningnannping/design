package com.fr.design.mainframe.alphafine.preview;

import com.fr.design.mainframe.alphafine.cell.model.AlphaCellModel;
import javax.swing.JPanel;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public abstract class ResultShowPane extends JPanel {

    public abstract void showResult(AlphaCellModel selectedValue);
}
