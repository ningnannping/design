package com.fr.design.mainframe.alphafine.component;

import com.fr.design.constants.UIConstants;
import com.fr.design.gui.ilable.UILabel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/22
 */
public class LineCellRender implements ListCellRenderer<String> {

    @Override
    public Component getListCellRendererComponent(JList<? extends String> list, String value, int index,
                                                  boolean isSelected, boolean cellHasFocus) {
        JPanel panel = new JPanel(new BorderLayout());
        UILabel splitLabel = new UILabel();
        panel.setBackground(null);
        splitLabel.setBackground(UIConstants.BARNOMAL);
        splitLabel.setBorder(BorderFactory.createEmptyBorder(0, 10, 5,0));
        panel.setPreferredSize(new Dimension(640, 1));
        panel.add(splitLabel);
        return panel;
    }
}
