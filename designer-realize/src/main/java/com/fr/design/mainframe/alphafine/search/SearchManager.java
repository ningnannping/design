package com.fr.design.mainframe.alphafine.search;

import com.fr.design.mainframe.alphafine.preview.DefaultContentPane;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/17
 */
public interface SearchManager {

    /**
     * 搜索
     * */
    void doSearch(SearchTextBean searchTextBean);

    /**
     * 展示默认内容
     * */
    default void showDefault(DefaultContentPane defaultContentPane) {

    };

    boolean hasSearchResult();

    boolean isSearchOver();

    boolean isNetWorkError();
}
