package com.fr.design.mainframe.alphafine.component;

import com.fr.base.svg.IconUtils;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.alphafine.model.TemplateResource;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Label;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * alphafine - 模板资源最外层面板, 卡片布局，每个卡片里塞了scrollpanel
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResourcePageGridPane extends JPanel {

    private List<TemplateResource> data;
    private CardLayout cardLayout;
    private List<Page> pages;
    private int totalPage;

    List<UIScrollPane> scrollPanes = new ArrayList<>();

    private static final int PAGE_MAX_SIZE = 12;
    private static final int TABLE_MAX_ROW_COUNT = 4;
    private static final int TABLE_COL_COUNT = 3;
    private static final int TABLE_VGAP = 15;
    private static final int TABLE_HGAP = 15;
    private static final int RESOURCE_WIDTH = 197;
    private static final int RESOURCE_HEIGHT = 128;

    public TemplateResourcePageGridPane(List<TemplateResource> templateResourceList) {
        this.data = templateResourceList;
        totalPage = (int) Math.ceil((double)data.size() / PAGE_MAX_SIZE);
        createPages();
        initComponents();
        this.setBackground(Color.WHITE);
        this.setBorder(BorderFactory.createEmptyBorder(10, 20, 0, 20));
        switchPage(1);
    }

    private void initComponents() {
        cardLayout = new CardLayout();
        this.setLayout(cardLayout);
        this.setBackground(Color.WHITE);
        for (int i = 0; i < pages.size(); i++) {
            UIScrollPane scrollPane = new UIScrollPane(pages.get(i));
            scrollPanes.add(scrollPane);
            this.add(scrollPane, String.valueOf(i + 1));
        }
    }


    /**
     * 构建分页，将资源切分到每一页，并在每一页尾部添加分页按钮
     * */
    private void createPages() {
        int dataCnt = data.size();
        List<TemplateResource>[] slice = new ArrayList[totalPage];
        for (int i = 0; i < dataCnt; i++) {
            int index = i / PAGE_MAX_SIZE;
            if (slice[index] == null) {
                slice[index] = new ArrayList<>();
            }
            slice[index].add(data.get(i));
        }
        pages = new ArrayList<>();
        for (int i = 0; i < totalPage; i++) {
            pages.add(new Page(slice[i], i + 1));
        }
    }


    private void switchPage(int pageNumber) {
        if (pageNumber < 1 || pageNumber > this.totalPage) {
            return;
        }
        cardLayout.show(TemplateResourcePageGridPane.this, String.valueOf(pageNumber));
        scrollPanes.get(pageNumber - 1).getVerticalScrollBar().setValue(0);
        // 坑，切换页面会刷新失败，需要手动滚动一下才能刷新
        scrollPanes.get(pageNumber - 1).getVerticalScrollBar().setValue(1);
        scrollPanes.get(pageNumber - 1).getVerticalScrollBar().setValue(0);
    }


    /**
     * 分页panel，borderlayout布局，north为信息页，south为分页按钮区
     * */
    private class Page extends JPanel {
        List<TemplateResource> pageData;
        Component[][] comps;

        JPanel contentPane;

        JPanel pageButtonPane;
        JButton prev, next;
        JTextField pageNumberField;
        JLabel pageCnt;

        int pageNumber;

        Page(List<TemplateResource> pageData, int pageNumber) {
            super();
            this.pageData = pageData;
            this.pageNumber = pageNumber;
            initComponents();
            this.setLayout(new BorderLayout());
            this.add(contentPane, BorderLayout.NORTH);
            if (totalPage > 1) {
                this.add(pageButtonPane, BorderLayout.SOUTH);
            }
            this.setBackground(Color.WHITE);
            this.setBorder(BorderFactory.createEmptyBorder());
        }

        private void initComponents() {
            createContentPane();
            createPageButtonPane();
        }

        void createContentPane() {
            int dataCnt = pageData.size();
            int rowCnt = (int) Math.ceil((double)dataCnt / 3);
            double[] rowHeight = new double[rowCnt];
            double[] colWidth = new double[TABLE_COL_COUNT];
            Arrays.fill(rowHeight, RESOURCE_HEIGHT);
            Arrays.fill(colWidth, RESOURCE_WIDTH);
            comps = new Component[rowCnt][TABLE_COL_COUNT];

            for (int i = 0; i < rowCnt; i++) {
                for (int j = 0; j < TABLE_COL_COUNT; j++) {
                    int which = i * 3 + j;
                    if (which >= dataCnt) {
                        Label empty = new Label();
                        empty.setPreferredSize(new Dimension(RESOURCE_WIDTH, RESOURCE_HEIGHT));
                        empty.setVisible(false);
                        comps[i][j] = empty;
                    } else {
                        TemplateResourcePanel resource = TemplateResourcePanel.create(pageData.get(which));
                        resource.setPreferredSize(new Dimension(RESOURCE_WIDTH, RESOURCE_HEIGHT));
                        comps[i][j] = resource;
                    }
                }
            }
            contentPane = TableLayoutHelper.createGapTableLayoutPane(comps, rowHeight, colWidth, TABLE_HGAP, TABLE_VGAP);
            contentPane.setBackground(Color.WHITE);
        }

        void createPageButtonPane() {
            prev = new JButton(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/prev.svg"));
            next = new JButton(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/next.svg"));
            pageNumberField = new JTextField((int) Math.log10(totalPage) + 1);
            pageNumberField.setText(String.valueOf(this.pageNumber));
            pageCnt = new JLabel("/ " + totalPage);

            pageButtonPane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
            pageButtonPane.add(prev);
            pageButtonPane.add(pageNumberField);
            pageButtonPane.add(pageCnt);
            pageButtonPane.add(next);

            addPageAction();
        }

        // 添加翻页按钮事件
        void addPageAction() {
            addPrevPageAction();
            addNextPageAction();
            addGotoPageAction();
        }

        void addPrevPageAction() {
            prev.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (pageNumber > 1) {
                        switchPage(pageNumber - 1);
                    }
                }
            });
        };
        void addNextPageAction() {
            next.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (pageNumber < totalPage) {
                        switchPage(pageNumber + 1);
                    }
                }
            });

        }
        void addGotoPageAction() {
            pageNumberField.addKeyListener(new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    super.keyPressed(e);
                    String numb = pageNumberField.getText();
                    if (numb != null && !numb.equals(pageNumber)) {
                        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                            switchPage(Integer.parseInt(numb));
                        }
                    }
                }
            });
        }
    }
}