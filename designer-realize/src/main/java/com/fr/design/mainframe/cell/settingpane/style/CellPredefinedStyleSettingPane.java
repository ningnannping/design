package com.fr.design.mainframe.cell.settingpane.style;

import com.fr.base.CellBorderStyle;
import com.fr.base.NameStyle;
import com.fr.base.Style;
import com.fr.config.predefined.PredefinedCellStyle;
import com.fr.config.predefined.PredefinedCellStyleConfig;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.actions.utils.ReportActionUtils;
import com.fr.design.constants.UIConstants;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.MultiTabPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.style.AbstractBasicStylePane;
import com.fr.design.gui.style.AlignmentPane;
import com.fr.design.gui.style.BorderPane;
import com.fr.design.gui.style.FormatPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.ElementCasePane;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.predefined.ui.PredefinedStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.preview.StyleSettingPreviewPane;
import com.fr.design.style.BorderUtils;
//import com.fr.predefined.PredefinedPatternStyleManager;
import com.fr.stable.Constants;
import com.fr.stable.StringUtils;
import com.fr.third.javax.annotation.Nonnull;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kerry on 2020-09-02
 */
public class CellPredefinedStyleSettingPane extends PredefinedStyleSettingPane<NameStyle> {

    private CustomStylePane customPredefinedStylePane;

    private UIComboBox applicationFormat;


    @Override
    protected StyleSettingPreviewPane createPreviewPane() {
        return null;
    }

    @Override
    protected JPanel createCustomDetailPane() {
        JPanel  jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        customPredefinedStylePane = new CustomStylePane();
        jPanel.add(customPredefinedStylePane, BorderLayout.CENTER);
        return jPanel;
    }

    protected JPanel createPredefinedSettingPane() {

        applicationFormat = new UIComboBox();
        applicationFormat.setPreferredSize(new Dimension(140, 20));
        JPanel centerPane = TableLayoutHelper.createGapTableLayoutPane(new Component[][]{
                {new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Applicate_Format")), applicationFormat}
        }, TableLayoutHelper.FILL_NONE, IntervalConstants.INTERVAL_W4, IntervalConstants.INTERVAL_L1);

        return centerPane;
    }


    @Override
    public String title4PopupWindow() {
        return null;
    }

    @Override
    public void populateBean(NameStyle ob) {
        this.setPopulating(true);
//        super.populate(ob);
//        PredefinedStyle currentStyle = PredefinedPatternStyleManager.INSTANCE.getStyleFromName(getPredefinedStyleName());
//        PredefinedCellStyleConfig config = currentStyle.getCellStyleConfig();
//        Map<String, PredefinedCellStyle> allStyle = config.getAllStyles();
//        this.applicationFormat.clearBoxItems();
//        for (String name : allStyle.keySet()) {
//            this.applicationFormat.addItem(name);
//        }
//        if (allStyle.containsKey(ob.getName())) {
//            this.applicationFormat.setSelectedItem(ob.getName());
//        }else {
//            this.applicationFormat.setSelectedItem(config.getDefaultStyleName());
//        }
//        this.customPredefinedStylePane.populateBean(ob);
//        this.setPopulating(false);
    }


    protected void populateCustomPane(){
        this.customPredefinedStylePane.populateBean(updatePredefinedStyle());
    }



    public void dealWithBorder(ElementCasePane ePane) {

        this.customPredefinedStylePane.dealWithBorder(ePane);
    }

    public void updateBorder() {
        this.customPredefinedStylePane.updateBorder();
    }

    @Override
    @Nonnull
    public NameStyle updateBean() {
        Style style = updateStyle();
        if (!(style instanceof NameStyle)) {
//            return NameStyle.createCustomStyle(style);
        }
        return (NameStyle) style;
    }

    @Nonnull
    public Style updateStyle() {
        if (this.predefinedRadioBtn.isSelected()) {
            return updatePredefinedStyle();
        } else {
            return this.customPredefinedStylePane.updateBean();
        }
    }

    private NameStyle updatePredefinedStyle() {
        Object selectItem = this.applicationFormat.getSelectedItem();
//        return NameStyle.createPredefinedStyle(getPredefinedStyleName(), selectItem == null ? StringUtils.EMPTY : selectItem.toString(), Style.getInstance());
        return null;
    }

    class CustomStylePane extends MultiTabPane<Style> {
        private static final int LENGTH_FOUR = 4;
        private static final int THREE_INDEX = 3;
        private static final int ONE_INDEX = 1;
        private ElementCasePane reportPane;


        public CustomStylePane() {
            super();
            tabPane.setOneLineTab(true);
            tabPane.setDrawLine(false);
            tabPane.setBorder(BorderFactory.createLineBorder(UIConstants.SHADOW_GREY));
            tabPane.setLayout(new GridLayout(1, 3, 0, 0));
        }

        /**
         * @return
         */
        public String title4PopupWindow() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Custom_Style");
        }


        /**
         *
         */
        public void reset() {
            populateBean(null);
        }

        @Override
        /**
         *
         */
        public void populateBean(Style ob) {
            for (int i = 0; i < paneList.size(); i++) {
                ((AbstractBasicStylePane) paneList.get(i)).populateBean(ob);
            }
        }

        @Override
        /**
         *
         */
        public Style updateBean() {
            return updateStyle(ReportActionUtils.getCurrentStyle(reportPane));
        }

        /**
         * @param style
         * @return
         */
        public Style updateStyle(Style style) {
            return ((AbstractBasicStylePane) paneList.get(tabPane.getSelectedIndex())).update(style);//只更新当前选中面板的样式
        }


        public boolean isBorderPaneSelected() {
            return tabPane.getSelectedIndex() == ONE_INDEX;
        }

        /**
         * @param ePane
         */
        public void dealWithBorder(ElementCasePane ePane) {
            this.reportPane = ePane;
            Object[] fourObjectArray = BorderUtils.createCellBorderObject(reportPane);

            if (fourObjectArray != null && fourObjectArray.length % LENGTH_FOUR == 0) {
                if (fourObjectArray.length == LENGTH_FOUR) {
                    ((BorderPane) paneList.get(ONE_INDEX)).populateBean((CellBorderStyle) fourObjectArray[0], ((Boolean) fourObjectArray[1]).booleanValue(), ((Integer) fourObjectArray[2]).intValue(),
                            (Color) fourObjectArray[THREE_INDEX]);
                } else {
                    ((BorderPane) paneList.get(ONE_INDEX)).populateBean(new CellBorderStyle(), Boolean.TRUE, Constants.LINE_NONE,
                            (Color) fourObjectArray[THREE_INDEX]);
                }
            }

        }

        /**
         *
         */
        public void updateBorder() {
            BorderUtils.update(reportPane, ((BorderPane) paneList.get(ONE_INDEX)).update());
        }

        /**
         * @param ob
         * @return
         */
        public boolean accept(Object ob) {
            return ob instanceof Style && !(ob instanceof NameStyle);
        }

        @Override
        protected List<BasicPane> initPaneList() {
            paneList = new ArrayList<BasicPane>();
            paneList.add(new FormatPane());
            paneList.add(new BorderPane());
            paneList.add(new AlignmentPane());
            return paneList;
        }

        @Override
        /**
         *
         */
        public void updateBean(Style ob) {
            return;
        }
    }
}
