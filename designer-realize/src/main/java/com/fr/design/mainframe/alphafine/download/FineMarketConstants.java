package com.fr.design.mainframe.alphafine.download;


/**
 *
 * alphafine - 帆软市场常量
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class FineMarketConstants {

    public static final String REPORTLETS = "/reportlets";
    public static final String ZIP = ".zip";
    public static final String RAR = ".rar";
    public static final String CPT = ".cpt";
    public static final String FRM = ".frm";
    public static final String FVS = ".fvs";
}
