package com.fr.design.mainframe.alphafine.model;

import com.fr.common.util.Strings;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.alphafine.download.FineMarketConstants;
import com.fr.design.mainframe.alphafine.search.manager.impl.TemplateResourceSearchManager;
import com.fr.general.IOUtils;
import com.fr.json.JSONArray;
import com.fr.json.JSONObject;
import com.fr.third.jodd.util.StringUtil;

import java.awt.Image;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * alphafine - 模板资源数据
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class TemplateResource {

    /***
     * 模板资源类型：模板，解决方案，推荐搜索
     */
    public enum Type {
        SINGLE_TEMPLATE(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Single_Template")),
        SCENARIO_SOLUTION(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Scenario_Solution")),
        RECOMMEND_SEARCH;

        private String name;

        Type() {
        }

        Type(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    // 模板资源搜索接口返回值字段名
    public static final String ID = "id";
    public static final String UUID = "uuid";
    public static final String NAME = "name";
    public static final String IMAGE_URL = "pic";
    public static final String DEMO_URL = "demoUrl";
    public static final String PKG_SIZE = "pkgsize";
    public static final String FILE_NAME = "fileLoca";
    private static final String RECOMMEND_SEARCH_IMG_URL = "com/fr/design/mainframe/alphafine/images/more.png";


    // 模板资源属性
    private String id;
    private String uuid;
    private Type type;
    private String imageUrl;
    private Image image;
    private String name;
    private String demoUrl;
    private String fileName;
    private int pkgSize;
    private List<String> recommendSearchKey;
    private boolean embed;



    /// 其他属性
    // 当前搜索词
    private String searchWord;

    public static List<TemplateResource> createByJson(JSONArray jsonArray) {
        List<TemplateResource> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i = jsonArray.length() - 1; i >= 0; i--) {
                list.add(createByJson(jsonArray.getJSONObject(i)));
            }
        }
        return list;
    }

    /**
     * json转obj
     * */
    public static TemplateResource createByJson(JSONObject jsonObject) {

        TemplateResource templateResource = new TemplateResource().setId(jsonObject.getString(ID)).setUuid(jsonObject.getString(UUID)).setName(jsonObject.getString(NAME))
                .setDemoUrl(jsonObject.getString(DEMO_URL)).setPkgSize(jsonObject.getInt(PKG_SIZE)).setFileName(jsonObject.getString(FILE_NAME));
        int pkgSize = templateResource.getPkgSize();
        if (pkgSize == 0) {
            templateResource.type = Type.SINGLE_TEMPLATE;
        } else {
            templateResource.type = Type.SCENARIO_SOLUTION;
        }

        templateResource.setImageUrl(parseUrl(jsonObject));

        templateResource.setImage(IOUtils.readImage(templateResource.imageUrl));
        return templateResource;
    }

    /**
     * 商城接口传过来的图片url是特殊格式，需要特殊处理下
     * */
    static String parseUrl(JSONObject jsonObject) {
        String imgUrl = jsonObject.getString(IMAGE_URL);
        int index = imgUrl.indexOf(",");
        if (index != -1) {
            imgUrl = imgUrl.substring(0, imgUrl.indexOf(","));
        }
        return imgUrl;
    }

    public static TemplateResource getRecommendSearch() {
        TemplateResource recommend = new TemplateResource();
        recommend.setType(Type.RECOMMEND_SEARCH);
        recommend.setImageUrl(RECOMMEND_SEARCH_IMG_URL);
        recommend.setImage(IOUtils.readImage(RECOMMEND_SEARCH_IMG_URL));
        recommend.setRecommendSearchKey(TemplateResourceSearchManager.getInstance().getRecommendSearchKeys());
        return recommend;
    }



    public String getFileName() {
        return fileName;
    }

    public TemplateResource setFileName(String fileName) {
        if (Strings.isEmpty(fileName)) {
            this.fileName = getName() + FineMarketConstants.ZIP;
        } else {
            this.fileName = fileName;
        }
        return this;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public List<String> getRecommendSearchKey() {
        return recommendSearchKey;
    }

    public void setRecommendSearchKey(List<String> recommendSearchKey) {
        this.recommendSearchKey = recommendSearchKey;
    }

    public TemplateResource setImage(Image image) {
        this.image = image;
        return this;
    }

    public Image getImage() {
        return image;
    }

    public TemplateResource setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        return this;
    }


    /**
     * 判断是否为内置模板资源
     * */
    public boolean isEmbed() {
        return embed;
    }

    public void setEmbed(boolean embed) {
        this.embed = embed;
    }

    public String getName() {
        return name;
    }

    public TemplateResource setName(String name) {
        this.name = name;
        return this;
    }

    public String getDemoUrl() {
        return demoUrl;
    }

    public boolean hasDemoUrl() {
        return !StringUtil.isEmpty(demoUrl);
    }

    public TemplateResource setDemoUrl(String demoUrl) {
        this.demoUrl = demoUrl;
        return this;
    }

    public int getPkgSize() {
        return pkgSize;
    }

    public TemplateResource setPkgSize(int pkgSize) {
        this.pkgSize = pkgSize;
        return this;
    }

    public String getId() {
        return id;
    }

    public TemplateResource setId(String id) {
        this.id = id;
        return this;
    }

    public String getUuid() {
        return uuid;
    }

    public TemplateResource setUuid(String uuid) {
        this.uuid = uuid;
        return this;
    }

    public String getSearchWord() {
        return searchWord;
    }

    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }
}