package com.fr.design.mainframe.alphafine.search.manager.impl;

import com.fr.common.util.Collections;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.help.alphafine.AlphaFineCloudConstants;
import com.fr.design.actions.help.alphafine.AlphaFineConstants;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.CellType;
import com.fr.design.mainframe.alphafine.cell.model.MoreModel;
import com.fr.design.mainframe.alphafine.cell.model.PluginModel;
import com.fr.design.mainframe.alphafine.model.SearchResult;
import com.fr.design.mainframe.alphafine.search.SearchTextBean;
import com.fr.design.mainframe.alphafine.search.manager.fun.AlphaFineSearchProvider;
import com.fr.general.ComparatorUtils;
import com.fr.general.http.HttpToolbox;
import com.fr.json.JSONArray;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.basic.version.Version;
import com.fr.plugin.basic.version.VersionIntervalFactory;
import com.fr.stable.ArrayUtils;
import com.fr.stable.EncodeConstants;
import com.fr.stable.ProductConstants;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by XiaXiang on 2017/3/27.
 */
public class PluginSearchManager implements AlphaFineSearchProvider {
    private SearchResult lessModelList;
    private SearchResult moreModelList;
    private SearchResult searchResult;
    private SearchResult defaultModelList;

    private static final int DEFAULT_LIST_SIZE = 10;
    private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private static final String UPLOAD_TIME = "uploadTime";

    private PluginSearchManager() {

    }

    public static PluginSearchManager getInstance() {
        return Holder.INSTANCE;

    }

    private static class Holder {
        private static final PluginSearchManager INSTANCE = new PluginSearchManager();
    }

    private static boolean isCompatibleCurrentEnv(String envVersion) {
        return VersionIntervalFactory.create(envVersion).contain(Version.currentEnvVersion());
    }

    private static PluginModel getPluginModel(JSONObject object, boolean isFromCloud) {
        String name = object.optString("name");
        String content = object.optString("description");
        String pluginId = object.optString("pluginid");
        String envVersion = object.optString("envversion");
        if (!isCompatibleCurrentEnv(envVersion)) {
            return null;
        }
        int id = object.optInt("id");
        int searchCount = object.optInt("searchCount");
        String imageUrl = null;
        try {
            imageUrl = isFromCloud ? AlphaFineCloudConstants.getPluginImageUrl() + URLEncoder.encode(object.optString("pic").toString().substring(AlphaFineCloudConstants.getPluginImageUrl().length()), "utf8") : object.optString("pic");
        } catch (UnsupportedEncodingException e) {
            FineLoggerFactory.getLogger().error("plugin icon error: " + e.getMessage());
        }
        String version = null;
        String jartime = null;
        CellType type;
        String link = object.optString("link");
        if (ComparatorUtils.equals(link, "plugin")) {
            version = isFromCloud ? object.optString("pluginversion") : object.optString("version");
            jartime = object.optString("jartime");
            type = CellType.PLUGIN;
        } else {
            type = CellType.REUSE;
        }
        int price = object.optInt("price");
        return new PluginModel(name, content, imageUrl, version, jartime, link, pluginId, type, price, id, searchCount);
    }

    /**
     * 根据json获取对应的插件model
     *
     * @param object
     * @return
     */
    public static PluginModel getModelFromCloud(JSONObject object) {
        JSONObject jsonObject = object.optJSONObject("result");
        if (jsonObject != null) {
            return getPluginModel(jsonObject, true);
        } else {
            return getPluginModel(object, false);
        }

    }

    @Override
    public SearchResult getLessSearchResult(String[] searchText) {
        this.lessModelList = new SearchResult();
        this.moreModelList = new SearchResult();
        this.searchResult = new SearchResult();
        if (DesignerEnvManager.getEnvManager().getAlphaFineConfigManager().isContainPlugin()) {
            if (ArrayUtils.isEmpty(searchText)) {
                lessModelList.add(new MoreModel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Plugin_Addon")));
                return lessModelList;
            }
            SearchResult noConnectList = AlphaFineHelper.getNoConnectList(Holder.INSTANCE);
            if (noConnectList != null) {
                return noConnectList;
            }
            for (int j = 0; j < searchText.length; j++) {
                try {
                    String encodedKey = URLEncoder.encode(searchText[j], EncodeConstants.ENCODING_UTF_8);
                    String url = AlphaFineCloudConstants.getPluginSearchUrl() + "?keyword=" + encodedKey;
                    String result = HttpToolbox.get(url);
                    AlphaFineHelper.checkCancel();
                    JSONObject jsonObject = new JSONObject(result);
                    JSONArray jsonArray = jsonObject.optJSONArray("result");
                    if (jsonArray != null) {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            AlphaFineHelper.checkCancel();
                            PluginModel cellModel = getPluginModel(jsonArray.optJSONObject(i), false);
                            if (cellModel != null && !AlphaFineHelper.getFilterResult().contains(cellModel) && !searchResult.contains(cellModel)) {
                                searchResult.add(cellModel);
                            }
                        }
                    }
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error("plugin search error :" + e.getMessage());
                }
            }
            if (searchResult.isEmpty()) {
                return this.lessModelList;
            } else if (searchResult.size() < AlphaFineConstants.SHOW_SIZE + 1) {
                lessModelList.add(0, new MoreModel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Plugin_Addon")));
                lessModelList.addAll(searchResult);
            } else {
                lessModelList.add(0, new MoreModel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Plugin_Addon"), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_ShowAll"), true, CellType.PLUGIN));
                lessModelList.addAll(searchResult.subList(0, AlphaFineConstants.SHOW_SIZE));
                moreModelList.addAll(searchResult.subList(AlphaFineConstants.SHOW_SIZE, searchResult.size()));
            }
        }
        return this.lessModelList;
    }

    @Override
    public SearchResult getMoreSearchResult(String searchText) {
        return this.moreModelList;
    }


    @Override
    public SearchResult getSearchResult(SearchTextBean searchTextBean) {
        getLessSearchResult(searchTextBean.getSegmentation());
        return searchResult;
    }

    @Override
    public SearchResult getDefaultResult() {
        this.defaultModelList = new SearchResult();
        try {
            String url = AlphaFineCloudConstants.getSearchAllPluginUrl();
            String result = HttpToolbox.get(url);
            AlphaFineHelper.checkCancel();
            JSONObject jsonObject = new JSONObject(result);
            JSONArray jsonArray = jsonObject.optJSONArray("result");
            List<Map> plugins = jsonArray.getList();

            List<PluginModel> pluginModels = new ArrayList<>();
            pluginModels.addAll(parseDefaultPluginModel(plugins));

            pluginModels.forEach(m -> this.defaultModelList.add(m));

        } catch (Exception e) {
            FineLoggerFactory.getLogger().error("plugin search error :" + e.getMessage());
        }

        return this.defaultModelList;
    }


    /**
     * 将jsonobject转化为PluginModel
     * 并按照更新时间排序，取最新的10个，并过滤掉不是当前版本的
     */
    List<PluginModel> parseDefaultPluginModel(List<Map> jsonObjects) {
        List<PluginModel> pluginModels = new ArrayList<>();
        String version = "v" + ProductConstants.MAIN_VERSION;
        if (!Collections.isEmpty(jsonObjects)) {
            pluginModels = jsonObjects.stream()
                    .filter(o -> ((Integer) o.get(version)) == 1)
                    .sorted((Map map1, Map map2) -> Long.compare(parseTime(map2), parseTime(map1)))
                    .limit(DEFAULT_LIST_SIZE)
                    .map(jsonObject -> getPluginModel(new JSONObject(jsonObject), false))
                    .collect(Collectors.toList());
        }
        return pluginModels;
    }

    private static long parseTime(Map value) {
        SimpleDateFormat format = new SimpleDateFormat(TIME_FORMAT);
        long t = 0L;
        try {
            t = format.parse((String) value.get(UPLOAD_TIME)).getTime();
        } catch (ParseException e) {
            FineLoggerFactory.getLogger().error(e, e.getMessage());
        }
        return t;
    }

}
