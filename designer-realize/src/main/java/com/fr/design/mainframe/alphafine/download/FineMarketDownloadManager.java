package com.fr.design.mainframe.alphafine.download;

import com.fr.base.svg.IconUtils;
import com.fr.common.util.Strings;
import com.fr.design.DesignerEnvManager;
import com.fr.design.extra.Process;
import com.fr.design.i18n.Toolkit;
import com.fr.design.login.DesignerLoginHelper;
import com.fr.design.login.DesignerLoginSource;
import com.fr.design.mainframe.alphafine.AlphaFineHelper;
import com.fr.design.mainframe.alphafine.model.TemplateResource;
import com.fr.design.mainframe.alphafine.search.helper.FineMarketClientHelper;
import com.fr.design.mainframe.toast.SimpleToast;
import com.fr.file.FileCommonUtils;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.SwingUtilities;
import javax.swing.filechooser.FileSystemView;
import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;


/**
 * 在这里统一管理帆软市场的下载
 * 下载的流程控制尽量都在这个类内部完成
 * 通过Process类来实现下载流程控制
 *
 * @author Link
 * @version 10.0
 * Created by Link on 2022/9/22
 * */
public class FineMarketDownloadManager {

    private static final FineMarketDownloadManager INSTANCE = new FineMarketDownloadManager();
    public static final FineMarketDownloadManager getInstance() {
        return INSTANCE;
    }


    public static final double PROCESS_SUCCESS = 1d;
    public static final double PROCESS_FAILED = -1d;
    public static final double OPENING_FILE = 2d;

    private static final String OPENING_PLEASE_WAIT = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Opening");
    private static final String DOWNLOAD_FAILED = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Resource_Download_Failed_Check_Network");


    /**
     * 下载资源并解压
     * */
    public String installResource(TemplateResource resource, Window parentWindow){
        // 验证登录
        String token = DesignerEnvManager.getEnvManager().getDesignerLoginRefreshToken();
        if (Strings.isEmpty(token)) {
            DesignerLoginHelper.showLoginDialog(DesignerLoginSource.NORMAL, new HashMap<>(), AlphaFineHelper.getAlphaFineDialog());
            return null;
        }
        return install(resource, parentWindow);
    }

    private String install(TemplateResource resource, Window parentWindow) {
        // 默认下载到桌面
        String workDir = FileSystemView.getFileSystemView().getHomeDirectory().getPath();
        File destDir = new File(workDir);

        DownloadProcess downloadProcess = new DownloadProcess(parentWindow);
        String fileName = null;
        try {
            fileName = FineMarketClientHelper.getInstance().download(resource, destDir, downloadProcess);
            unzip(fileName, downloadProcess);
            return fileName;
        } catch (Exception e) {
            downloadProcess.process(FineMarketDownloadManager.PROCESS_FAILED);
            FineLoggerFactory.getLogger().error(e, e.getMessage());
        }
        return null;
    }

    void unzip(String fileName, DownloadProcess process) throws IOException {
        process.process(OPENING_FILE);

        if (fileName.endsWith(FineMarketConstants.ZIP)) {
            File file = new File(fileName);
            File parentDir = file.getParentFile();
            // TODO:之前用的gbk解码太容易乱码了。这里应该自动识别编码，然后解压，但是要添加1个三方包，可能会赶不上发布。所以先暂时用utf8顶着。
            IOUtils.unzip(file, FileCommonUtils.getAbsolutePath(parentDir), "UTF-8");
        }
    }




    /**
     * 下载流程控制，主要控制ui的显示
     * */
    class DownloadProcess implements Process<Double> {

        SimpleToast downloadingToast;
        SimpleToast openingToast;
        SimpleToast failedToast;
        Window parent;

        public DownloadProcess(Window parentWindow) {
            this.parent = parentWindow;
            init();
        }

        void init() {
            showLoadingToast();
        }

        @Override
        public void process(Double aDouble) {
            SwingUtilities.invokeLater(()->{
                if (aDouble == PROCESS_FAILED) {
                    downloadFailed();
                } else if (aDouble == PROCESS_SUCCESS) {
                    downloadSuccess();
                } else if (aDouble == OPENING_FILE) {
                    openingFile();
                }
            });
        }

        /**
        * 下载失败
        */
        public void downloadFailed() {
            downloadingToast.setVisible(false);
            showFailedToast();
        }

        /**
        * 下载成功
        */
        public void downloadSuccess() {
            downloadingToast.setVisible(false);
        }


        private void openingFile() {
            downloadingToast.setVisible(false);
            openingToast = new SimpleToast(AlphaFineHelper.getAlphaFineDialog(), IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/loading.svg"), OPENING_PLEASE_WAIT, true);
            openingToast.setVisible(true);
        }

        private void showLoadingToast() {
            downloadingToast = new SimpleToast(AlphaFineHelper.getAlphaFineDialog(), IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/loading.svg"), OPENING_PLEASE_WAIT, false);
            downloadingToast.setVisible(true);
        }

        private void showFailedToast() {
            failedToast = new SimpleToast(AlphaFineHelper.getAlphaFineDialog(), IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/caution.svg"), DOWNLOAD_FAILED, true);
            failedToast.setVisible(true);
        }

    }
}
