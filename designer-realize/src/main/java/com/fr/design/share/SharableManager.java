package com.fr.design.share;

import com.fr.design.base.clipboard.ClipboardFilter;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.mainframe.FormWidgetDetailPane;
import com.fr.design.mainframe.JForm;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.collect.SharableCollectorManager;
import com.fr.design.mainframe.share.encrypt.clipboard.impl.EncryptSelectionClipboardHandler;
import com.fr.design.mainframe.share.encrypt.clipboard.impl.EncryptTransferableClipboardHandler;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.design.share.utils.EffectItemUtils;
import com.fr.design.widget.FormWidgetDefinePaneFactoryBase;
import com.fr.form.share.encrypt.engine.ClazzCreatorFactory;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.cardlayout.WCardMainBorderLayout;
import com.fr.general.GeneralContext;
import com.fr.json.JSONArray;
import com.fr.stable.EnvChangedListener;
import com.fr.stable.bridge.BridgeMark;
import com.fr.stable.bridge.StableFactory;

import java.util.List;

public class SharableManager {
    public static void start() {
        SharableCollectorManager.getInstance().execute();
        listenEnv();
        ClipboardFilter.registerClipboardHandler(EncryptSelectionClipboardHandler.getInstance());
        ClipboardFilter.registerClipboardHandler(EncryptTransferableClipboardHandler.getInstance());
        registerSharableEncryptDefinePanes();
    }

    public static void saveTemplate(JTemplate jt) {
        if (jt instanceof JForm) {
            int showCount = 0;
            int useCount = 0;
            if (!jt.getProcessInfo().isTestTemplate()) {
                JForm jForm = (JForm) jt;
                XLayoutContainer root = jForm.getFormDesign().getRootComponent();
                List<XCreator> xCreators = ShareComponentUtils.getHelpConfigXCreatorList(root);
                JSONArray helpConfigUseInfo = ComponentCollector.getInstance().getHelpConfigUseInfoWithTemplate(jForm.getTarget().getTemplateID());
                for (XCreator xCreator : xCreators) {
                    if (EffectItemUtils.hasEffectItem(xCreator.toData())) {
                        showCount ++;
                        if (helpConfigUseInfo.contains(ShareComponentUtils.getWidgetId(xCreator.toData()))) {
                            useCount++;
                        }
                    }
                }

                ComponentCollector.getInstance().collectHelpConfigInfo(jForm.getTarget().getTemplateID(), showCount, useCount);
            }
        }
    }

    private static void listenEnv() {
        GeneralContext.addEnvChangedListenerToLast(new EnvChangedListener() {
            @Override
            public void envChanged() {
                FormWidgetDetailPane.getInstance().resetEmptyPane();
                LocalWidgetRepoPane.getInstance().refreshPane();
            }
        });
    }

    private static void registerSharableEncryptDefinePane(Class<? extends Widget> mainClazz) {
        String newClazzName = ClazzCreatorFactory.getInstance().createName(mainClazz);
        Class<? extends Widget> newClazz;
        try {
            newClazz = (Class<? extends Widget>) Class.forName(newClazzName, false, mainClazz.getClassLoader());
        } catch (Exception e) {
            newClazz = (Class<? extends Widget>) ClazzCreatorFactory.getInstance().newSharableClazz(mainClazz.getClassLoader(), mainClazz);
        }
        if (FormWidgetDefinePaneFactoryBase.getDefinePane(newClazz) == null) {
            FormWidgetDefinePaneFactoryBase.registerDefinePane(newClazz, FormWidgetDefinePaneFactoryBase.getDefinePane(mainClazz));
        }

    }

    private static void registerSharableEncryptDefinePanes() {
        registerSharableEncryptDefinePane(ElementCaseEditor.class);
        registerSharableEncryptDefinePane(StableFactory.getMarkedClass(BridgeMark.CHART_EDITOR, AbstractBorderStyleWidget.class));
        registerSharableEncryptDefinePane(WAbsoluteLayout.class);
        registerSharableEncryptDefinePane(WCardMainBorderLayout.class);
    }
}
