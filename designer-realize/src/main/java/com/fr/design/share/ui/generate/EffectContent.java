package com.fr.design.share.ui.generate;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.share.effect.EffectItem;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.design.share.ui.effect.PreviewPane;
import com.fr.design.share.ui.effect.EffectControlUpdatePane;
import com.fr.design.share.ui.effect.EffectPopupEditDialog;
import com.fr.design.share.ui.generate.table.EffectItemRender;
import com.fr.design.share.ui.generate.table.EffectTableModel;
import com.fr.design.share.ui.table.EffectItemEditor;
import com.fr.design.share.utils.ShareDialogUtils;
import com.fr.form.ui.Widget;
import com.fr.locale.InterProviderFactory;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.List;

public class EffectContent extends JPanel {
    static final private Dimension PREVIEW_SIZE = new Dimension(340, 120);

    private EffectItemGroup effectItemGroup;
    private EffectPopupEditDialog effectPopupEditDialog;
    private EffectControlUpdatePane effectControlUpdatePane;

    public EffectContent(EffectItemGroup effectItemGroup) {
        this.effectItemGroup = effectItemGroup;
        this.init();
    }

    private void init() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        if (getEffectItems().size() > 0) {
            // 预览图
            JPanel imageContainer = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
            PreviewPane previewPane = new PreviewPane(getWidget(), PREVIEW_SIZE, false);
            imageContainer.add(previewPane);
            this.add(imageContainer, BorderLayout.NORTH);

            // 效果列表
            Object[] columnNames = {com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_From"), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_Rename")};
            JTable table = new JTable(new EffectTableModel(effectItemGroup, columnNames));
            table.setRowHeight(25);
            table.getColumnModel().getColumn(0).setPreferredWidth(230);
            table.getColumnModel().getColumn(1).setPreferredWidth(320);


            table.setDefaultEditor(EffectTableModel.class, new EffectItemEditor(table));
            table.setDefaultRenderer(EffectTableModel.class, new EffectItemRender());
            table.getTableHeader().setResizingAllowed(false);
            table.getTableHeader().setReorderingAllowed(false);
            table.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    int selectColumn = table.getSelectedColumn();
                    if (selectColumn == 0) {
                        int selectRow = table.getSelectedRow();
                        EffectItem selectEffectItem = getEffectItems().get(selectRow);
                        effectControlUpdatePane = EffectControlUpdatePane.newInstance(selectEffectItem);
                        effectControlUpdatePane.populate();
                        setPaneDisabled(effectControlUpdatePane);
                        effectPopupEditDialog = EffectPopupEditDialog.newInstance(ShareDialogUtils.getInstance().getShareDialog(), effectControlUpdatePane);
                        effectPopupEditDialog.setTitle(selectEffectItem.getName());
                        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                        effectPopupEditDialog.setLocation((d.width - effectPopupEditDialog.getSize().width) / 2, (d.height - effectPopupEditDialog.getSize().height) / 2);
                        effectPopupEditDialog.setVisible(true);
                    }
                }
                public void mouseExited(MouseEvent e) {
                    int tableContentHeight = table.getParent().getHeight() - table.getTableHeader().getHeight();
                    if ((e.getPoint().getY() < 0) || (e.getPoint().getY() > tableContentHeight)) {
                        table.clearSelection();
                    }
                    int row = table.rowAtPoint(e.getPoint());
                    int col = table.columnAtPoint(e.getPoint());
                    if (row < 0 || col < 0) {
                        table.clearSelection();
                    }
                }
            });
            table.addMouseMotionListener(new MouseMotionAdapter() {
                @Override
                public void mouseMoved(MouseEvent e) {
                    int row = table.rowAtPoint(e.getPoint());
                    int col = table.columnAtPoint(e.getPoint());
                    table.setRowSelectionInterval(row, row);
                    table.setColumnSelectionInterval(col, col);
                }
            });

//            if (previewPane.isElementCaseEditor()) {
//                table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
//                    @Override
//                    public void valueChanged(ListSelectionEvent e) {
//                        if (table.getSelectedRow() >= 0) {
//                            EffectItem effectItem= getEffectItems().get(table.getSelectedRow());
//                            SourceNode cellSourceNode = SourceNodeUtils.getCellSourceNode(effectItem.getSourceNode());
//                            if (cellSourceNode != null) {
//                                DefaultTemplateCellElement cellElement = (DefaultTemplateCellElement) cellSourceNode.getTarget();
//                                previewPane.getElementCasePane().setSelection(new CellSelection(cellElement.getColumn(), cellElement.getRow(), cellElement.getColumnSpan(), cellElement.getRowSpan()));
//                            }
//                        }
//                    }
//                });
//            }

            JPanel tableContainer = FRGUIPaneFactory.createBorderLayout_S_Pane();
            tableContainer.setBorder(BorderFactory.createEmptyBorder(10,0,5,0));
            if (getEffectItems().size() > 5) {
                UIScrollPane scrollPane = new UIScrollPane(table);
                scrollPane.setPreferredSize(new Dimension(550, 160));
                tableContainer.add(scrollPane, BorderLayout.CENTER);
            } else {
                JPanel tablePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
                tablePane.add(table.getTableHeader(), BorderLayout.NORTH);
                tablePane.add(table, BorderLayout.CENTER);
                tableContainer.add(tablePane, BorderLayout.CENTER);
            }
            this.add(tableContainer, BorderLayout.CENTER);

        }
    }

    private List<EffectItem> getEffectItems() {
        return effectItemGroup.getEffectItems();
    }

    private Widget getWidget() {
        return effectItemGroup.getWidget();
    }

    private void setPaneDisabled(Component component) {
        if (component instanceof JScrollPane) {
            JScrollPane pane = (JScrollPane) component;
            setPaneDisabled(pane.getViewport().getView());
            return;
        }
        if (component instanceof JTree) {
            JTree tree = (JTree) component;
            TreeSelectionListener[] treeSelectionListeners = tree.getTreeSelectionListeners();
            for (TreeSelectionListener treeSelectionListener : treeSelectionListeners) {
                tree.removeTreeSelectionListener(treeSelectionListener);
            }
            return;
        }
        component.setEnabled(false);
        if (component instanceof JComponent) {
            Component[] components = ((JComponent) component).getComponents();
            for (Component co : components) {
                setPaneDisabled(co);
            }
        }
    }

}
