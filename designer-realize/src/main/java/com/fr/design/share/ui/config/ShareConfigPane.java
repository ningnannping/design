package com.fr.design.share.ui.config;

import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIButtonUI;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.ui.base.PlaceholderTextArea;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.design.share.SharableManager;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.design.share.utils.EffectItemUtils;
import com.fr.design.share.utils.ShareDialogUtils;
import com.fr.design.utils.gui.GUIPaintUtils;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WTitleLayout;
import com.fr.locale.InterProviderFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.Border;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;

public class ShareConfigPane extends BasicPane {
    private static final Dimension DIALOG_SIZE = new Dimension(900, 680);
    private static final Border DIALOG_BORDER = BorderFactory.createEmptyBorder(0, 6, 4, 6);
    private static final Color EMPTY_TEXT_COLOR = new Color(51, 51, 51, 128);

    private Widget widget;
    private List<EffectItemGroup> effectItemGroups;
    private HashMap<String, ShareConfigContentPane> quickConfigContentPaneMap = new HashMap();

    private BasicDialog dialog;
    private int selectIndex = 0;
    private CardLayout cardLayout;
    private JPanel contentPane;
    private UILabel nameLabel;
    private UIButton preButton;
    private UIButton nextButton;



    public ShareConfigPane(Widget widget) {
        this.widget = widget;
        effectItemGroups = EffectItemUtils.getEffectItemGroupsByWidget(widget);
        initComponent();
    }

    private void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(DIALOG_BORDER);
        this.setPreferredSize(DIALOG_SIZE);

        if (effectItemGroups.size() > 0) {
            this.add(createNaviPane(), BorderLayout.NORTH);
            this.add(createContentPane(), BorderLayout.CENTER);
        } else {
            this.add(createInfoPane(), BorderLayout.WEST);
            this.add(createEffectEmptyPane(), BorderLayout.EAST);
        }

    }

    private JPanel createNaviPane() {
        JPanel naviPane = new JPanel();
        naviPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        naviPane.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 0));

        preButton = new UIButton(Toolkit.i18nText("Fine-Design_Share_Prev"));
        preButton.setPreferredSize(new Dimension(62, 20));
        preButton.setUI(disabledUIButtonUI);
        preButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quickConfigContentPaneMap.get(getSelectWidget().getWidgetName()).reset();
                selectIndex--;
                TogglePage();
            }
        });
        preButton.setEnabled(hasPreview());

        nextButton = new UIButton(Toolkit.i18nText("Fine-Design_Share_Next"));
        nextButton.setPreferredSize(new Dimension(62, 20));
        nextButton.setUI(disabledUIButtonUI);
        nextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                quickConfigContentPaneMap.get(getSelectWidget().getWidgetName()).reset();
                selectIndex++;
                TogglePage();
            }
        });
        nextButton.setEnabled(hasNext());

        nameLabel = new UILabel("");
        nameLabel.setPreferredSize(new Dimension(nameLabel.getFont().getSize() * 20, 15));
        
        naviPane.add(preButton);
        naviPane.add(nextButton);
        naviPane.add(nameLabel);

        return naviPane;
    }

    private JPanel createContentPane() {
        contentPane = new JPanel();
        cardLayout = new CardLayout();
        contentPane.setLayout(cardLayout);
        addContent();
        return contentPane;
    }

    private void addContent() {
        ShareConfigContentPane content = new ShareConfigContentPane(getSelectEffectItemGroup(), getDescription());
        contentPane.add(getSelectWidget().getWidgetName(), content);
        quickConfigContentPaneMap.put(getSelectWidget().getWidgetName(), content);
        updateContent();
    }

    private void updateContent() {
        cardLayout.show(contentPane, getSelectWidget().getWidgetName());
        nameLabel.setText(getSelectWidget().getWidgetName());
        nameLabel.setToolTipText(getSelectWidget().getWidgetName());
    }

    private void TogglePage() {
        preButton.setEnabled(hasPreview());
        nextButton.setEnabled(hasNext());
        if (quickConfigContentPaneMap.containsKey(getSelectWidget().getWidgetName())) {
            updateContent();
        } else {
            addContent();
        }
    }

    private JPanel createInfoPane() {
        JPanel infoPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Share_Introduce"));
        PlaceholderTextArea textArea = new PlaceholderTextArea();
        textArea.setText(getDescription());
        textArea.setPlaceholder(Toolkit.i18nText("Fine-Design_Share_No_Introduction"));
        textArea.setEditable(false);
        UIScrollPane scrollPane = new UIScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(373, 176));
        infoPane.add(scrollPane, BorderLayout.CENTER);
        return infoPane;
    }

    private String getDescription() {
        if (this.widget instanceof WTitleLayout) {
            return ((WTitleLayout) this.widget).getBodyBoundsWidget().getWidget().getDescription();
        }
        return this.widget.getDescription();
    }

    private JPanel createEffectEmptyPane() {
        JPanel effectPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Share_Effects"));
        effectPane.setLayout(new VerticalFlowLayout(FlowLayout.CENTER,0 , 0));
        effectPane.setPreferredSize(new Dimension(480, 545));
        UILabel label = new UILabel(Toolkit.i18nText("Fine-Design_Share_Empty"));
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        label.setPreferredSize(new Dimension(480, 22));
        label.setHorizontalAlignment(UILabel.CENTER);
        label.setForeground(EMPTY_TEXT_COLOR);
        effectPane.add(label);
        return effectPane;
    }

    @Override
    protected String title4PopupWindow() {
        return Toolkit.i18nText("Fine-Design_Share_Help_Settings");
    }

    public void show() {
        final DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
        dialog = this.showWindowWithCustomSize(designerFrame, null, DIALOG_SIZE);
        ShareDialogUtils.getInstance().setConfigDialog(dialog);
        dialog.addDialogActionListener(new DialogActionAdapter() {
            @Override
            public void doOk() {
                for(EffectItemGroup effectItemGroup : effectItemGroups) {
                    effectItemGroup.save();
                }
                HistoryTemplateListCache.getInstance().getCurrentEditingTemplate().saveTemplate();
                ComponentCollector.getInstance().setHelpConfigUseInfo(ShareComponentUtils.getCurrentTemplateID(), ShareComponentUtils.getWidgetId(widget));
                SharableManager.saveTemplate(HistoryTemplateListCache.getInstance().getCurrentEditingTemplate());
            }
        });
        dialog.setVisible(true);
    }

    private boolean hasPreview() {
        return selectIndex > 0;
    }

    private boolean hasNext() {
        return selectIndex < effectItemGroups.size() - 1;
    }

    private Widget getSelectWidget() {
        return getSelectEffectItemGroup().getWidget();
    }

    private EffectItemGroup getSelectEffectItemGroup() {
        return effectItemGroups.get(selectIndex);
    }

    private UIButtonUI disabledUIButtonUI = new UIButtonUI() {
        protected void doExtraPainting(UIButton b, Graphics2D g2d, int w, int h, String selectedRoles) {
            if (!b.isEnabled()) {
                GUIPaintUtils.fillPaint(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), new Color(240, 240, 241), UIConstants.ARC);
            } else if (isPressed(b) && b.isPressedPainted()) {
                GUIPaintUtils.fillPressed(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles));
            } else if (isRollOver(b)) {
                GUIPaintUtils.fillRollOver(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles), b.isPressedPainted());
            } else if (b.isNormalPainted()) {
                GUIPaintUtils.fillNormal(g2d, 0, 0, w, h, b.isRoundBorder(), b.getRectDirection(), b.isDoneAuthorityEdited(selectedRoles), b.isPressedPainted());
            }
        }
    };

}
