package com.fr.design.share.ui.effect;

import com.fr.design.DesignerEnvManager;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.data.tabledata.tabledatapane.GlobalMultiTDTableDataPane;
import com.fr.design.data.tabledata.tabledatapane.GlobalTreeTableDataPane;
import com.fr.design.data.tabledata.tabledatapane.MultiTDTableDataPane;
import com.fr.design.data.tabledata.tabledatapane.TreeTableDataPane;
import com.fr.design.env.DesignerWorkspaceInfo;
import com.fr.design.env.RemoteDesignerWorkspaceInfo;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.share.effect.EffectItem;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.JPanel;
import java.awt.BorderLayout;

public class EffectControlUpdatePane extends JPanel {
    private EffectItem effectItem;
    private BasicBeanPane updatePane;

    private EffectControlUpdatePane(EffectItem effectItem) {
        this.effectItem = effectItem;
        initUpdatePane();
    }

    public static EffectControlUpdatePane newInstance(EffectItem effectItem) {
        return new EffectControlUpdatePane(effectItem);
    }

    private void initUpdatePane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        NameableCreator creator = effectItem.creator();
        if (creator == null) {
            return;
        } else {
            if (isMulti(creator.getUpdatePane()) || isTree(creator.getUpdatePane())) {
                updatePane = effectItem.createPaneByCreators(creator, effectItem.getName());
            } else {
                updatePane = effectItem.createPaneByCreators(creator);
            }
            this.add(updatePane, BorderLayout.CENTER);
        }
    }

    public void populate() {
        try{
            updatePane.populateBean(effectItem.getPopulateBean());
        }catch (Exception e){
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }


    public boolean isMulti(Class _class) {
        return ComparatorUtils.equals(_class, GlobalMultiTDTableDataPane.class) || ComparatorUtils.equals(_class, MultiTDTableDataPane.class);
    }

    public boolean isTree(Class _class) {
        return ComparatorUtils.equals(_class, GlobalTreeTableDataPane.class) || ComparatorUtils.equals(_class, TreeTableDataPane.class);
    }

    public void update() {
        BasicBeanPane pane = updatePane;
        if (pane != null && pane.isVisible()) {
            Object bean = pane.updateBean();
            try {
                if (bean instanceof RemoteDesignerWorkspaceInfo) {
                    DesignerWorkspaceInfo info = DesignerEnvManager.getEnvManager().getWorkspaceInfo(effectItem.getName());
                    String remindTime = info.getRemindTime();
                    ((RemoteDesignerWorkspaceInfo) bean).setRemindTime(remindTime);
                }
            }catch (Exception e){
                FineLoggerFactory.getLogger().info("remindTime is not exist");
            }
            effectItem.updateBean(bean);
        }
    }

    public void checkValid() throws Exception {
        updatePane.checkValid();
    }
}
