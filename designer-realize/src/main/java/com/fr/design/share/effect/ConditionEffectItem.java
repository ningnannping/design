package com.fr.design.share.effect;

import com.fr.chart.chartattr.Plot;
import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.design.ChartTypeInterfaceManager;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.condition.ConditionAttributesPane;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.share.effect.source.SourceNode;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ConditionEffectItem extends BaseEffectItem<ConditionAttr> {
    private ConditionAttr condition;
    private Plot plot;
    public ConditionEffectItem(ConditionAttr condition, SourceNode sourceNode, Plot plot) {
        this.condition = condition;
        this.plot = plot;
        this.setSourceNode(sourceNode);
        init();
    }

    private void init() {
        ConditionAttr object;
        try {
            object = (ConditionAttr) condition.clone();
        } catch (CloneNotSupportedException e) {
            object = new ConditionAttr();
            object.setName(condition.getName());
            object.setCondition(condition.getCondition());
            for (int i = 0; i < condition.getDataSeriesConditionCount(); i++) {
                object.addDataSeriesCondition(condition.getDataSeriesCondition(i));
            }
        }
        setObject(object);
        this.setName(condition.getName());
        this.setNameableCreator(new NameObjectCreator(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Condition_Attributes"), ConditionAttr.class, getPlotConditionPane(this.plot)));
    }

    private Class<? extends ConditionAttributesPane> getPlotConditionPane(Plot plot) {
        return ChartTypeInterfaceManager.getInstance().getPlotConditionPane(plot).getClass();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        getObject().setName(name);
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        Constructor<? extends BasicBeanPane> constructor = null;
        try {
            constructor = creator.getUpdatePane().getConstructor(Plot.class);
            return constructor.newInstance(this.plot);

        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void updateBean(Object bean) {
        ConditionAttr conditionAttr = (ConditionAttr) bean;
        conditionAttr.setName(this.getName());
        setObject(conditionAttr);
    }

    public void save() {
        condition.setName(getObject().getName());
        condition.setCondition(getObject().getCondition());
        condition.removeAll();
        for (int i = 0; i < getObject().getDataSeriesConditionCount(); i++) {
            condition.addDataSeriesCondition(getObject().getDataSeriesCondition(i));
        }
    }
}
