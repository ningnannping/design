package com.fr.design.share.effect;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.stable.Nameable;

public interface EffectItem<T> extends Nameable {
    SourceNode getSourceNode();

    String getConfigPath();

    String getDescription();

    void setDescription(String description);

    T getObject();

    void setObject(T ob);

    Object getPopulateBean();

    void save();

    void updateBean(Object bean);

    NameableCreator creator();

    BasicBeanPane createPaneByCreators(NameableCreator creator);

    BasicBeanPane createPaneByCreators(NameableCreator creator, String string);
}
