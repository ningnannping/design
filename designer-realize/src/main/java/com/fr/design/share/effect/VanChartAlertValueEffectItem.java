package com.fr.design.share.effect;

import com.fr.design.share.effect.source.SourceNode;
import com.fr.plugin.chart.attr.DefaultAxisHelper;
import com.fr.plugin.chart.attr.axis.VanChartAlertValue;
import com.fr.plugin.chart.attr.axis.VanChartAxis;
import com.fr.plugin.chart.attr.plot.VanChartRectanglePlot;
import com.fr.van.chart.designer.style.background.ChartNameObjectCreator;
import com.fr.van.chart.designer.style.background.VanChartAlertValuePane;

import java.util.List;

public class VanChartAlertValueEffectItem extends BaseEffectItem<VanChartAlertValue> {
    private VanChartAlertValue alertValue;
    private VanChartRectanglePlot plot;

    public VanChartAlertValueEffectItem(VanChartAlertValue alertValue, SourceNode sourceNode, VanChartRectanglePlot plot) {
        this.alertValue = alertValue;
        this.plot = plot;
        this.setSourceNode(sourceNode);
        init();
    }

    private void init() {
        List<VanChartAxis> xAxisList = plot.getXAxisList();
        List<VanChartAxis> yAxisList = plot.getYAxisList();
        String[] axisNames = DefaultAxisHelper.getAllAxisNames(plot);
        for (VanChartAxis axis : xAxisList) {
            List<VanChartAlertValue> values = axis.getAlertValues();
            for (VanChartAlertValue alertValue : values) {
                alertValue.setAxisNamesArray(axisNames);
                alertValue.setAxisName(plot.getXAxisName(axis));
            }
        }
        for (VanChartAxis axis : yAxisList) {
            List<VanChartAlertValue> values = axis.getAlertValues();
            for (VanChartAlertValue alertValue : values) {
                alertValue.setAxisNamesArray(axisNames);
                alertValue.setAxisName(plot.getYAxisName(axis));
            }
        }

        VanChartAlertValue object;
        try{
            object = (VanChartAlertValue) alertValue.clone();
        }catch (CloneNotSupportedException e) {
            object = new VanChartAlertValue();
            object.setAxisNamesArray(alertValue.getAxisNamesArray());
            object.setAxisName(alertValue.getAxisName());
            object.setAlertContentFormula(alertValue.getAlertContentFormula());
            object.setAlertContent(alertValue.getAlertContent());
            object.setAlertPosition(alertValue.getAlertPosition());
            object.setAlertValueFormula(alertValue.getAlertValueFormula());
            object.setAlertFont(alertValue.getAlertFont());
            object.setAlertLineAlpha(alertValue.getAlertLineAlpha());
            object.setAlertPaneSelectName(alertValue.getAlertPaneSelectName());
            object.setLineColor(alertValue.getLineColor());
            object.setLineStyle(alertValue.getLineStyle());
        }
        setObject(object);

        this.setName(alertValue.getAlertPaneSelectName());
        this.setNameableCreator(new ChartNameObjectCreator(axisNames, com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Alert_Line"), VanChartAlertValue.class, VanChartAlertValuePane.class));
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        getObject().setAlertPaneSelectName(name);
    }

    @Override
    public void updateBean(Object bean) {
        VanChartAlertValue alertValue = (VanChartAlertValue) bean;
        alertValue.setAlertPaneSelectName(getName());
        setObject(alertValue);
    }

    @Override
    public void save() {
        VanChartAlertValue object = getObject();
        alertValue.setAxisNamesArray(object.getAxisNamesArray());
        alertValue.setAxisName(object.getAxisName());
        alertValue.setAlertContentFormula(object.getAlertContentFormula());
        alertValue.setAlertContent(object.getAlertContent());
        alertValue.setAlertPosition(object.getAlertPosition());
        alertValue.setAlertValueFormula(object.getAlertValueFormula());
        alertValue.setAlertFont(object.getAlertFont());
        alertValue.setAlertLineAlpha(object.getAlertLineAlpha());
        alertValue.setAlertPaneSelectName(object.getAlertPaneSelectName());
        alertValue.setLineColor(object.getLineColor());
        alertValue.setLineStyle(object.getLineStyle());
    }
}
