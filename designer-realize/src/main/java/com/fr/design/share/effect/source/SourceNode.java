package com.fr.design.share.effect.source;



public interface SourceNode<T> {
    String toString();

    T getTarget();

    SourceNode getParent();

    void setParent(SourceNode parent);
}

