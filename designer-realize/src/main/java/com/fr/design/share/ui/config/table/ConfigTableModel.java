package com.fr.design.share.ui.config.table;

import com.fr.design.share.effect.EffectItem;
import com.fr.design.share.effect.ListenerEffectItem;

import javax.swing.table.AbstractTableModel;
import java.util.List;

public class ConfigTableModel extends AbstractTableModel {
    private Object[] columnNames;
    private List<? extends EffectItem> effectItems;

    public ConfigTableModel(List<? extends EffectItem> effectItems, Object[] columnNames) {
        this.effectItems = effectItems;
        this.columnNames = columnNames;
    }
    @Override
    public int getRowCount() {
        return this.effectItems.size();
    }

    @Override
    public int getColumnCount() {
        return this.columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnNames == null) {
            return null;
        }
        return columnNames[columnIndex].toString();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return ConfigTableModel.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex == 1;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex >= getRowCount() || columnIndex >= getColumnCount()) {
            return null;
        }
        EffectItem effectItem = effectItems.get(rowIndex);
        if (columnIndex == 0) {
            return effectItem.getConfigPath();
        } else if (columnIndex == 1) {
            return effectItem.getName();
        }
        return null;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex >= effectItems.size() || columnIndex >= getColumnCount()) {
            return;
        }
        EffectItem effectItem = effectItems.get(rowIndex);
        if (columnIndex == 1) {
            effectItem.setName(aValue.toString());
        }
    }
}
