package com.fr.design.share.ui.config;

import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.VerticalFlowLayout;
import com.fr.design.mainframe.share.ui.base.PlaceholderTextArea;
import com.fr.design.share.effect.EffectItem;
import com.fr.design.share.effect.EffectItemGroup;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.design.share.effect.source.SourceNodeUtils;
import com.fr.design.share.ui.config.table.ExpandEffectTable;
import com.fr.design.share.ui.effect.PreviewPane;
import com.fr.form.ui.Widget;
import com.fr.grid.selection.CellSelection;
import com.fr.locale.InterProviderFactory;
import com.fr.report.cell.DefaultTemplateCellElement;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ShareConfigContentPane extends JPanel {
    private EffectItemGroup effectItemGroup;
    private PreviewPane previewPane;
    private String info;
    private List<ExpandEffectTable> expandEffectTables = new ArrayList<>();

    public ShareConfigContentPane(EffectItemGroup effectItemGroup, String info) {
        this.effectItemGroup = effectItemGroup;
        this.info = info;
        initComponent();
    }

    private void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel leftPane = createLeftPane();
        JPanel rightPane = createRightPane();
        this.add(leftPane, BorderLayout.WEST);
        this.add(rightPane, BorderLayout.CENTER);
    }

    private JPanel createLeftPane() {
        JPanel leftPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        leftPane.add(createPreviewPane(), BorderLayout.NORTH);
        leftPane.add(createInfoPane(), BorderLayout.CENTER);
        return leftPane;
    }

    private JPanel createRightPane() {
        JPanel rightPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        rightPane.setBorder(BorderFactory.createEmptyBorder(0, 3, 0, 0));
        rightPane.add(createEffectPane(), BorderLayout.CENTER);
        return rightPane;
    }

    private JPanel createPreviewPane() {

        JPanel pane= FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText(Toolkit.i18nText("Fine-Design_Share_Preview")));
        previewPane = new PreviewPane(getWidget());
        pane.add(previewPane, BorderLayout.CENTER);
        return pane;
    }

    private JPanel createInfoPane() {
        JPanel infoPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText("Fine-Design_Share_Introduce"));
        PlaceholderTextArea textArea = new PlaceholderTextArea();
        textArea.setText(this.info);
        textArea.setPlaceholder(Toolkit.i18nText("Fine-Design_Share_No_Introduction"));
        textArea.setEditable(false);
        UIScrollPane scrollPane = new UIScrollPane(textArea);
        scrollPane.setPreferredSize(new Dimension(373, 176));
        infoPane.add(scrollPane, BorderLayout.CENTER);
        return infoPane;
    }

    private JPanel createEffectPane() {
        JPanel effectPane = FRGUIPaneFactory.createTitledBorderPane(Toolkit.i18nText(Toolkit.i18nText("Fine-Design_Share_Effects")));
        JPanel effectContainer = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, VerticalFlowLayout.TOP,0, 0);

        EffectItemGroup effectItemGroup = getEffectItemGroup();
        LinkedHashMap<String, List<? extends EffectItem>> effectItemsMap = effectItemGroup.getEffectItemsWithClassify();

        Set<Map.Entry<String, List<? extends EffectItem>>> entrySet = effectItemsMap.entrySet();
        Iterator<Map.Entry<String, List<? extends EffectItem>>> iterator = entrySet.iterator();

        expandEffectTables.clear();
        while (iterator.hasNext()) {
            Map.Entry<String, List<? extends EffectItem>> entry= iterator.next();
            String title = entry.getKey();
            List<? extends EffectItem> effectItems = entry.getValue();
            ExpandEffectTable expandEffectTable= new ExpandEffectTable(effectItems, title);
            expandEffectTables.add(expandEffectTable);
            JTable table =  expandEffectTable.getTable();
            if (previewPane.isElementCaseEditor()) {
                table.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
                    @Override
                    public void valueChanged(ListSelectionEvent e) {
                        if (table.getSelectedRow() >= 0) {
                            EffectItem effectItem= effectItems.get(table.getSelectedRow());
                            SourceNode cellSourceNode = SourceNodeUtils.getCellSourceNode(effectItem.getSourceNode());
                            if (cellSourceNode != null) {
                                DefaultTemplateCellElement cellElement = (DefaultTemplateCellElement) cellSourceNode.getTarget();
                                previewPane.getElementCasePane().setSelection(new CellSelection(cellElement.getColumn(), cellElement.getRow(), cellElement.getColumnSpan(), cellElement.getRowSpan()));
                            }
                        }
                    }
                });
            }
            effectContainer.add(expandEffectTable);

        }

        UIScrollPane scrollPane = new UIScrollPane(effectContainer);
        scrollPane.setPreferredSize(new Dimension(465, 545));
        scrollPane.getViewport().addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                Component component = e.getComponent();
                if (component != null) {
                    for(ExpandEffectTable expandEffectTable : expandEffectTables) {
                        for(Component co : expandEffectTable.getComponents()) {
                            co.setPreferredSize(new Dimension(component.getWidth(), co.getHeight()));
                            co.setSize(new Dimension(component.getWidth(), co.getHeight()));
                        }
                    }
                }
            }
            @Override
            public void componentMoved(ComponentEvent e) {

            }
            @Override
            public void componentShown(ComponentEvent e) {

            }
            @Override
            public void componentHidden(ComponentEvent e) {

            }
        });
        scrollPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));

        effectPane.add(scrollPane, BorderLayout.CENTER);
        return effectPane;
    }

    private Widget getWidget() {
        return getEffectItemGroup().getWidget();
    }

    private EffectItemGroup getEffectItemGroup() {
        return effectItemGroup;
    }

    public void reset() {
        if (previewPane.isElementCaseEditor() && (previewPane.getElementCasePane() != null)) {
            previewPane.getElementCasePane().setSelection(new CellSelection(0, 0, 0, 0));
        }
    }

}
