package com.fr.design.share.ui.effect;

import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.UIDialog;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

public class EffectPopupEditDialog extends UIDialog {
    private JComponent editPane;
    private PopupToolPane popupToolPane;
    private UIButton okButton;
    private static final int WIDTH = 570;
    private static final int HEIGHT = 510;

    public EffectPopupEditDialog(Dialog parent, JComponent pane) {
        super(parent);
        setUndecorated(true);
        pane.setBorder(BorderFactory.createEmptyBorder(20, 10, 10, 10));

        this.editPane = pane;
        JPanel editPaneWrapper = new JPanel(new BorderLayout());
        popupToolPane = new PopupToolPane(this);
        editPaneWrapper.add(popupToolPane, BorderLayout.NORTH);
        editPaneWrapper.add(editPane, BorderLayout.CENTER);
        editPaneWrapper.setBorder(BorderFactory.createLineBorder(UIConstants.POP_DIALOG_BORDER, 1));
        editPaneWrapper.add(this.createControlButtonPane(), BorderLayout.SOUTH);
        this.getContentPane().add(editPaneWrapper, BorderLayout.CENTER);

        setSize(WIDTH, HEIGHT);
//            pack();
        this.setVisible(false);
    }

    public static EffectPopupEditDialog newInstance(Dialog parent, JComponent pane) {
        return new EffectPopupEditDialog(parent, pane);
    }


    @Override
    public void setTitle(String title) {
        popupToolPane.setTitle(title);
    }


    @Override
    public void checkValid() throws Exception {

    }

    // 移动弹出编辑面板的工具条
    private class PopupToolPane extends JPanel {
        private JDialog parentDialog;  // 如果不在对话框中，值为null
        private Color originColor;  // 初始背景
        private JPanel contentPane;
        private UILabel titleLabel;
        private Point mouseDownCompCoords;  // 存储按下左键的位置，移动对话框时会用到

        private static final int MIN_X = -150;
        private static final int MAX_X_SHIFT = 50;
        private static final int MAX_Y_SHIFT = 50;

        private MouseListener mouseListener = new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getDefaultCursor());
                if (mouseDownCompCoords == null) {
                    contentPane.setBackground(originColor);
                }
                repaint();
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mouseDownCompCoords = null;
                if (!getBounds().contains(e.getPoint())) {
                    contentPane.setBackground(originColor);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                mouseDownCompCoords = e.getPoint();
            }
        };

        private MouseMotionListener mouseMotionListener = new MouseMotionListener() {
            @Override
            public void mouseMoved(MouseEvent e) {
                setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
                contentPane.setBackground(UIConstants.POPUP_TITLE_BACKGROUND);
                repaint();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if (mouseDownCompCoords != null) {
                    Point currCoords = e.getLocationOnScreen();
                    int x = currCoords.x - mouseDownCompCoords.x;
                    int y = currCoords.y - mouseDownCompCoords.y;
                    //屏幕可用区域
                    Rectangle screen = GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds();

                    int minY = screen.y;
                    int maxX = Toolkit.getDefaultToolkit().getScreenSize().width - MAX_X_SHIFT;
                    int maxY = Toolkit.getDefaultToolkit().getScreenSize().height - MAX_Y_SHIFT;
                    if (x < MIN_X) {
                        x = MIN_X;
                    } else if (x > maxX) {
                        x = maxX;
                    }
                    if (y < minY) {
                        y = minY;
                    } else if (y > maxY) {
                        y = maxY;
                    }
                    // 移动到屏幕边缘时，需要校正位置
                    parentDialog.setLocation(x, y);
                }
            }
        };

        PopupToolPane(JDialog parentDialog) {
            this(StringUtils.EMPTY, parentDialog);
        }

        PopupToolPane(String title, JDialog parentDialog) {
            super();
            this.parentDialog = parentDialog;
            originColor = UIConstants.DIALOG_TITLEBAR_BACKGROUND;

            contentPane = new JPanel();
            contentPane.setBackground(originColor);
            contentPane.setLayout(new BorderLayout());
            titleLabel = new UILabel(title);
            Font font = new Font("SimSun", Font.PLAIN, 12);
            titleLabel.setFont(font);
            contentPane.add(titleLabel, BorderLayout.WEST);
            contentPane.setBorder(new EmptyBorder(5, 14, 6, 0));

            setLayout(new BorderLayout());
            add(contentPane, BorderLayout.CENTER);
            setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, UIConstants.TOOLBAR_BORDER_COLOR));

            addMouseListener(mouseListener);
            addMouseMotionListener(mouseMotionListener);
        }

        public void setTitle(String title) {
            titleLabel.setText(title);
        }

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(super.getPreferredSize().width, 28);
        }
    }

    private JPanel createControlButtonPane() {
        JPanel controlPane = FRGUIPaneFactory.createBorderLayout_S_Pane();

        JPanel buttonsPane = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        controlPane.add(buttonsPane, BorderLayout.EAST);

        //确定
        addOkButton(buttonsPane);

        return controlPane;
    }

    private void addOkButton(JPanel buttonsPane) {
        okButton = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_OK"));
        okButton.setName(OK_BUTTON);
        okButton.setMnemonic('O');
        buttonsPane.add(okButton);
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                doOK();
            }
        });
    }
}
