package com.fr.design.share.effect;

import com.fr.chart.chartglyph.ConditionAttr;
import com.fr.design.share.effect.source.SourceNode;
import com.fr.plugin.chart.attr.DefaultAxisHelper;
import com.fr.plugin.chart.attr.plot.VanChartRectanglePlot;
import com.fr.plugin.chart.base.AttrSeriesStackAndAxis;
import com.fr.van.chart.column.VanChartCustomStackAndAxisConditionPane;
import com.fr.van.chart.designer.style.series.StackedAndAxisNameObjectCreator;

public class StackAndAxisConditionEffectItem extends  BaseEffectItem<ConditionAttr> {
    private ConditionAttr condition;
    private VanChartRectanglePlot plot;

    public StackAndAxisConditionEffectItem(ConditionAttr condition, SourceNode sourceNode, VanChartRectanglePlot plot) {
        this.condition = condition;
        this.plot = plot;
        this.setSourceNode(sourceNode);

        init();
    }

    private void init() {
        setAxisNamesArray(condition);

        ConditionAttr object;
        try {
            object = (ConditionAttr) condition.clone();
        } catch (CloneNotSupportedException e) {
            object = new ConditionAttr();
            object.setName(condition.getName());
            object.setCondition(condition.getCondition());
            for (int i = 0; i < condition.getDataSeriesConditionCount(); i++) {
                object.addDataSeriesCondition(condition.getDataSeriesCondition(i));
            }
        }
        setObject(object);

        this.setName(condition.getName());
        this.setNameableCreator(new StackedAndAxisNameObjectCreator(new AttrSeriesStackAndAxis(), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Stack_And_Series"), ConditionAttr.class, VanChartCustomStackAndAxisConditionPane.class));
    }

    @Override
    public void setName(String name) {
        super.setName(name);
        getObject().setName(name);
    }

    @Override
    public void updateBean(Object bean) {
        ConditionAttr conditionAttr = (ConditionAttr) bean;
        conditionAttr.setName(getName());
        setAxisNamesArray(conditionAttr);
        setObject(conditionAttr);
    }

    public void save() {
        condition.setName(getObject().getName());
        condition.setCondition(getObject().getCondition());
        condition.removeAll();
        for (int i = 0; i < getObject().getDataSeriesConditionCount(); i++) {
            condition.addDataSeriesCondition(getObject().getDataSeriesCondition(i));
        }
    }

    private void setAxisNamesArray(ConditionAttr conditionAttr) {
        String[] axisXNames = DefaultAxisHelper.getXAxisNames(plot);
        String[] axisYNames = DefaultAxisHelper.getYAxisNames(plot);
        AttrSeriesStackAndAxis stackAndAxis = (AttrSeriesStackAndAxis) conditionAttr.getExisted(AttrSeriesStackAndAxis.class);
        stackAndAxis.setXAxisNamesArray(axisXNames);
        stackAndAxis.setYAxisNameArray(axisYNames);
    }
}
