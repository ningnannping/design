package com.fr.design.webattr;

import com.fr.config.Configuration;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.report.fit.BaseFitAttrPane;
import com.fr.design.report.fit.FitAttrModel;
import com.fr.general.ComparatorUtils;
import com.fr.report.fit.ReportFitAttr;
import com.fr.report.fit.ReportFitConfig;
import com.fr.transaction.Configurations;
import com.fr.transaction.Worker;

import java.awt.Color;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;


public class ServerFitAttrPane extends BaseFitAttrPane {

    public ServerFitAttrPane() {
        super();
        initComponents();
    }

    @Override
    protected void initComponents() {
        super.initComponents();
        populateModel(fitAttrModelList.get(0));
    }


    @Override
    public void populateBean(ReportFitAttr reportFitAttr) {
        reportFitAttr = fitAttrModel.getGlobalReportFitAttr();
        super.populateBean(reportFitAttr);
    }

    public ReportFitAttr updateBean() {
        ReportFitAttr reportFitAttr = super.updateBean();
        Configurations.update(new Worker() {
            @Override
            public void run() {
                fitAttrModel.setGlobalReportFitAttr(reportFitAttr);
            }

            @Override
            public Class<? extends Configuration>[] targets() {
                return new Class[]{ReportFitConfig.class};
            }
        });
        return null;

    }

    @Override
    protected String[] getItemNames() {
        return fitAttrModelList.stream().map(FitAttrModel::getModelName).toArray(String[]::new);
    }

    @Override
    protected ItemListener getItemListener() {
        ServerFitAttrPane self = this;
        return new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    for (FitAttrModel fitAttrModel : fitAttrModelList) {
                        if (ComparatorUtils.equals(e.getItem(), fitAttrModel.getModelName())) {
                            self.populateModel(fitAttrModel);
                            populateBean(null);
                            refresh();
                        }
                    }
                }
            }
        };
    }

    @Override
    protected void initPrompt() {
        if (fitAttrModelList.size() == 3) {
            UILabel uiLabel = new UILabel(Toolkit.i18nText("Fine-Designer_Fit_Attr_Pane_Hint"));
            uiLabel.setForeground(Color.lightGray);
            contentJPanel.add(uiLabel);
        }
    }
}

