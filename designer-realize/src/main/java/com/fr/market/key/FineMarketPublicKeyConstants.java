package com.fr.market.key;


/**
 *
 * 帆软市场公钥常量
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/8/25
 */
public class FineMarketPublicKeyConstants {
    
    public static final String DEFAULT_KEY_KEY = "defaultKey";
    
    public static final String DEFAULT_KEY_DIRECTORY = "/com/fr/market/key";

    /**
     * 公钥第一段
     */
    public static final String FIRST_PROPERTY = "76c1/default";

    /**
     * 公钥第二段
     */
    public static final String SECOND_PROPERTY = "943f/default";

    /**
     * 公钥第三段
     */
    public static final String THIRD_PROPERTY = "d8a3/default";
}
