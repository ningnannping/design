package com.fr.market.key;

import com.fr.stable.StableUtils;

/**
 * 帆软市场公钥Holder
 * @author Link
 * @version 10.0
 * Created by Link on 2022/8/25
 */
public class FineMarketPublicKeyHolder {

    private static FineMarketPublicKeyHolder instance = null;

    private String defaultKey;

    public static FineMarketPublicKeyHolder getInstance() {

        if (instance == null) {
            synchronized (FineMarketPublicKeyHolder.class) {
                if (instance == null) {
                    instance = new FineMarketPublicKeyHolder();
                }
            }
        }
        return instance;
    }

    private FineMarketPublicKeyHolder() {
        init();
    }

    private void init() {
        // 读取三个default.properties文件，组成公钥
        String firstPart = FineMarketDefaultKeyProperties.create(StableUtils.pathJoin(FineMarketPublicKeyConstants.DEFAULT_KEY_DIRECTORY, FineMarketPublicKeyConstants.FIRST_PROPERTY)).getPublicKey();
        String secondPart = FineMarketDefaultKeyProperties.create(StableUtils.pathJoin(FineMarketPublicKeyConstants.DEFAULT_KEY_DIRECTORY, FineMarketPublicKeyConstants.SECOND_PROPERTY)).getPublicKey();
        String thirdPart = FineMarketDefaultKeyProperties.create(StableUtils.pathJoin(FineMarketPublicKeyConstants.DEFAULT_KEY_DIRECTORY, FineMarketPublicKeyConstants.THIRD_PROPERTY)).getPublicKey();
        this.defaultKey = firstPart + secondPart + thirdPart;
    }

    /**
     * 获取默认公钥
     * @return 公钥
     */
    public String getDefaultKey() {
        return this.defaultKey;
    }
}
