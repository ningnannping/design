package com.fr.market.key;

import com.fr.general.IOUtils;
import com.fr.io.utils.ResourceIOUtils;
import com.fr.log.FineLoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * 帆软市场公钥Properties
 *
 * @author Link
 * @version 11.0
 * Created by Yvan on 2022/8/25
 */
public class FineMarketDefaultKeyProperties {

    private Properties properties = new Properties();

    private Map<String, String> publicKeyMap = new HashMap<>();

    private String propertyPath;

    private FineMarketDefaultKeyProperties(String propertyPath) {
        this.propertyPath = propertyPath;
        load();
    }

    /**
     * 构造方法
     */
    public static FineMarketDefaultKeyProperties create(String propertyPath) {
        return new FineMarketDefaultKeyProperties(propertyPath);
    }
    
    private void load() {
        try (InputStream inputStream = IOUtils.readResource(getPropertyPath())) {
            byte[] data = ResourceIOUtils.inputStream2Bytes(inputStream);
            properties.load(new ByteArrayInputStream(data));
            trims(properties);
            publicKeyMap.put(FineMarketPublicKeyConstants.DEFAULT_KEY_KEY, properties.getProperty(FineMarketPublicKeyConstants.DEFAULT_KEY_KEY));
        } catch (IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
    }

    private String getPropertyPath() {
        return this.propertyPath;
    }

    public String getPublicKey() {
        return publicKeyMap.get(FineMarketPublicKeyConstants.DEFAULT_KEY_KEY);
    }

    /**
     * 去除properties中value末尾的空格
     * @param properties
     */
    public static void trims(Properties properties) {
        for (String key : properties.stringPropertyNames()) {
            String value = properties.getProperty(key);
            if (value != null) {
                properties.put(key, value.trim());
            }
        }
    }
}
