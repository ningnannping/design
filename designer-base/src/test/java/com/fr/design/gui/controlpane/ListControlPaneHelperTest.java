package com.fr.design.gui.controlpane;

import com.fr.form.event.Listener;
import com.fr.general.NameObject;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kerry on 5/17/21
 */
public class ListControlPaneHelperTest {

    @Test
    public void testProcessCatalog() {
        ListControlPaneHelper helper = ListControlPaneHelper.newInstance(EasyMock.mock(ListControlPaneProvider.class));
        List<NameObject> nameObjectList = new ArrayList<>();
        Listener listener1 = new Listener();
        listener1.setEventName("click");
        Listener listener2 = new Listener();
        listener2.setEventName("afterInit");
        Listener listener3 = new Listener();
        listener3.setEventName("click");
        Listener listener4 = new Listener();
        listener4.setEventName("afterInit");
        listener4.setName("test");
        nameObjectList.add(new NameObject("click", listener1));
        nameObjectList.add(new NameObject("afterInit", listener2));
        nameObjectList.add(new NameObject("click", listener3));
        nameObjectList.add(new NameObject("afterInit", listener4));
        Map<String, List<NameObject>> map = helper.processCatalog(nameObjectList);
        Assert.assertEquals(2, map.size());
        Assert.assertEquals(2, map.get("afterInit").size());
        Assert.assertEquals(2, map.get("click").size());

    }
}
