package com.fr.design.mainframe;

import com.fr.invoke.Reflect;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kerry on 2020-07-28
 */
public class JFormSliderPaneTest {


    @Test
    public void testGetPreferredValue() {
        JFormSliderPane sliderPane = new JFormSliderPane();
        int result = Reflect.on(sliderPane).call("getPreferredValue", 100).get();
        Assert.assertEquals(100, result);
        result = Reflect.on(sliderPane).call("getPreferredValue", 0).get();
        Assert.assertEquals(10, result);
        result = Reflect.on(sliderPane).call("getPreferredValue", 1000).get();
        Assert.assertEquals(400, result);
    }

    @Test
    public void testCalSliderValue() {
        JFormSliderPane sliderPane = new JFormSliderPane();
        int result = Reflect.on(sliderPane).call("calSliderValue", 10).get();
        Assert.assertEquals(0, result);
        result = Reflect.on(sliderPane).call("calSliderValue", 90).get();
        Assert.assertEquals(44, result);
        result = Reflect.on(sliderPane).call("calSliderValue", 100).get();
        Assert.assertEquals(50, result);
        result = Reflect.on(sliderPane).call("calSliderValue", 200).get();
        Assert.assertEquals(66, result);
        result = Reflect.on(sliderPane).call("calSliderValue", 400).get();
        Assert.assertEquals(100, result);

    }
}
