package com.fr.design.mainframe.reuse;

import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.javax.xml.stream.XMLStreamException;
import org.junit.Assert;
import org.junit.Test;

import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Created by kerry on 5/10/21
 */
public class ComponentReuseNotificationInfoTest {

    @Test
    public void testReadXML() {
        try {
            XMLableReader xmlReader = XMLableReader.createXMLableReader(new StringReader("<ComponentReuseNotificationInfo lastNotifyTime=\"1620612153215\" notifiedNumber=\"2\" clickedWidgetLib=\"true\"/>\n"));
            ComponentReuseNotificationInfo notificationInfo = ComponentReuseNotificationInfo.getInstance();
            notificationInfo.readXML(xmlReader);
            xmlReader.close();
            Assert.assertEquals(2, notificationInfo.getNotifiedNumber());
            Assert.assertEquals(true, notificationInfo.isClickedWidgetLib());
            Assert.assertEquals(1620612153215L, notificationInfo.getLastNotifyTime());
        } catch (XMLStreamException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testWriteXML() {
        StringWriter sw = new StringWriter();
        XMLPrintWriter writer = XMLPrintWriter.create(new PrintWriter(sw));
        ComponentReuseNotificationInfo notificationInfo = ComponentReuseNotificationInfo.getInstance();
        notificationInfo.setNotifiedNumber(1);
        notificationInfo.writeXML(writer);
        writer.flush();
        writer.close();
        Assert.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<ComponentReuseNotificationInfo xmlVersion=\"20170720\" releaseVersion=\"\" lastNotifyTime=\"0\" notifiedNumber=\"1\" clickedWidgetLib=\"false\"/>\n", sw.toString());
    }

}
