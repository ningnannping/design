package com.fr.design.mainframe;

import com.fr.invoke.Reflect;
import junit.framework.TestCase;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/5/8
 */
public class JTemplateNameHelperTest extends TestCase {

    public void testNewTemplateNameByIndex() {

        String name = JTemplateNameHelper.newTemplateNameByIndex("TEST");

        assertEquals("TEST1", name);

        String name1 = JTemplateNameHelper.newTemplateNameByIndex("TEST");

        assertEquals("TEST2", name1);

    }

    public void testGetFileNameIndex() {
        //正常情况
        assertEquals("1", Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "WorkBook", "WorkBook1").toString());

        //正常情况
        assertEquals("8888888888", Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "WorkBook", "WorkBook8888888888").toString());

        //正常情况
        assertEquals("88812214128888881231238123123", Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "WorkBook", "WorkBook88812214128888881231238123123").toString());

        //前缀不匹配
        assertNull(Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "Work123", "WorkBook8888888888").get());

        //前缀为空
        assertNull(Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "", "WorkBook8888888888").get());

        //文件长度小于前缀
        assertNull(Reflect.on(JTemplateNameHelper.class).call("getFileNameIndex", "WorkBook", "").get());


    }
}
