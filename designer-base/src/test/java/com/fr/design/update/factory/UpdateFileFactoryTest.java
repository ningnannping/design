package com.fr.design.update.factory;

import com.fr.decision.update.data.UpdateConstants;
import com.fr.stable.StableUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;

public class UpdateFileFactoryTest {

    @Test
    public void testGetBackupVersions() {
        Assert.assertEquals(0, UpdateFileFactory.getBackupVersions().length);
        File backupDir = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR));
        StableUtils.deleteFile(backupDir);
    }

    @Test
    public void testIsBackupVersionsValid() {
        File des = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR, "test", UpdateConstants.BACKUPPATH, "test"));
        File env = new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR, "test", UpdateConstants.DESIGNERBACKUPPATH, "test"));
        StableUtils.mkdirs(des);
        StableUtils.mkdirs(env);
        Assert.assertTrue(UpdateFileFactory.isBackupVersionsValid(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR, "test")));
        StableUtils.deleteFile(new File(StableUtils.pathJoin(StableUtils.getInstallHome(), UpdateConstants.DESIGNER_BACKUP_DIR)));
    }
}
