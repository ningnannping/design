package com.fr.design.extra;

import com.fr.invoke.Reflect;
import com.fr.json.JSONObject;
import com.fr.plugin.context.PluginContext;
import com.fr.plugin.context.PluginMarker;
import com.fr.plugin.context.PluginMarkerAdapter;
import com.fr.plugin.error.PluginErrorCode;
import com.fr.plugin.manage.PluginManager;
import com.fr.plugin.manage.control.PluginTask;
import com.fr.plugin.manage.control.PluginTaskResult;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lucian.Chen
 * @version 10.0
 * Created by Lucian.Chen on 2020/12/17
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PluginManager.class, PluginUtils.class})
public class PluginOperateUtilsTest {

    @Test
    public void testGetSuccessInfo() {
        PluginTaskResult pluginTaskResult = EasyMock.mock(PluginTaskResult.class);
        PluginTaskResult pluginTaskResult1 = EasyMock.mock(PluginTaskResult.class);
        PluginTaskResult pluginTaskResult2 = EasyMock.mock(PluginTaskResult.class);

        List<PluginTaskResult> pluginTaskResults1 = new ArrayList<>();
        pluginTaskResults1.add(pluginTaskResult1);
        List<PluginTaskResult> pluginTaskResults2 = new ArrayList<>();
        pluginTaskResults2.add(pluginTaskResult1);
        pluginTaskResults2.add(pluginTaskResult2);

        PluginMarker pluginMarker1 = PluginMarker.create("plugin-1", "1.0");
        PluginMarker pluginMarker2 = PluginMarkerAdapter.create("plugin-2", "2.0", "name-2");
        PluginTask pluginTask1 = PluginTask.installTask(pluginMarker1);
        PluginTask pluginTask2 = PluginTask.installTask(pluginMarker2);

        EasyMock.expect(pluginTaskResult.asList()).andReturn(pluginTaskResults1).times(2);
        EasyMock.expect(pluginTaskResult.asList()).andReturn(pluginTaskResults2).times(2);
        EasyMock.expect(pluginTaskResult1.getCurrentTask()).andReturn(pluginTask1).anyTimes();
        EasyMock.expect(pluginTaskResult2.getCurrentTask()).andReturn(pluginTask2).anyTimes();

        EasyMock.expect(pluginTaskResult1.errorCode()).andReturn(PluginErrorCode.BelowSystem).anyTimes();
        EasyMock.expect(pluginTaskResult2.errorCode()).andReturn(PluginErrorCode.BeyondSystem).anyTimes();

        PluginContext plugin1 = EasyMock.mock(PluginContext.class);
        PluginContext plugin2 = EasyMock.mock(PluginContext.class);
        EasyMock.expect(plugin1.getName()).andReturn("context-1").anyTimes();
        EasyMock.expect(plugin2.getName()).andReturn("context-2").anyTimes();
        PowerMock.mockStatic(PluginManager.class);
        EasyMock.expect(PluginManager.getContext(pluginMarker1.getPluginID()))
                .andReturn(plugin1).once().andReturn(null).once().andReturn(plugin1).once().andReturn(null).once();
        EasyMock.expect(PluginManager.getContext(pluginMarker2.getPluginID()))
                .andReturn(plugin2).once().andReturn(null).once();

        EasyMock.replay(pluginTaskResult, pluginTaskResult1, pluginTaskResult2, plugin1, plugin2);
        PowerMock.replayAll();

        // 1个
        Assert.assertEquals(PluginOperateUtils.getSuccessInfo(pluginTaskResult), "context-1Fine-Core_Plugin_Error_BelowSystem");
        Assert.assertEquals(PluginOperateUtils.getSuccessInfo(pluginTaskResult), "plugin-1Fine-Core_Plugin_Error_BelowSystem");


        // 2个
        Assert.assertEquals(PluginOperateUtils.getSuccessInfo(pluginTaskResult), "context-1Fine-Core_Plugin_Error_BelowSystem\ncontext-2Fine-Core_Plugin_Error_BeyondSystem");
        Assert.assertEquals(PluginOperateUtils.getSuccessInfo(pluginTaskResult), "plugin-1Fine-Core_Plugin_Error_BelowSystem\nname-2Fine-Core_Plugin_Error_BeyondSystem");

        EasyMock.verify(pluginTaskResult, pluginTaskResult1, pluginTaskResult2, plugin1, plugin2);
        PowerMock.verifyAll();

    }


    @Test
    public void testGetPluginName() {
        PluginContext pluginContext = EasyMock.mock(PluginContext.class);
        EasyMock.expect(pluginContext.getName()).andReturn("pluginContext").once();

        PluginMarker pluginMarker1 = PluginMarker.create("id-1", "1");
        PluginMarker pluginMarker2 = PluginMarkerAdapter.create("id-2", "2", "name-2");

        EasyMock.replay(pluginContext);

        Assert.assertEquals(Reflect.on(PluginOperateUtils.class).call("getPluginName", null, null).get(), "");
        Assert.assertEquals(Reflect.on(PluginOperateUtils.class).call("getPluginName", pluginContext, pluginMarker1).get(), "pluginContext");
        Assert.assertEquals(Reflect.on(PluginOperateUtils.class).call("getPluginName", null, pluginMarker1).get(), "id-1");
        Assert.assertEquals(Reflect.on(PluginOperateUtils.class).call("getPluginName", null, pluginMarker2).get(), "name-2");

        EasyMock.verify(pluginContext);
    }

    @Test
    public void testUpdateMarker2Online() {

        try {
            PluginMarker pluginMarker = PluginMarker.create("plugin-1", "1.0");
            String pluginJson = "{\"id\": plugin-1,\"name\": \"图表(新特性)\",\"version\": \"8.6.16\"}";
            JSONObject object = new JSONObject(pluginJson);

            PowerMock.mockStatic(PluginUtils.class);
            EasyMock.expect(PluginUtils.getLatestPluginInfo("plugin-1")).andReturn(object).once();
            EasyMock.expect(PluginUtils.getLatestPluginInfo("plugin-1")).andThrow(new NullPointerException()).once();

            PowerMock.replayAll();

            PluginMarker marker1 = PluginOperateUtils.updateMarker2Online(pluginMarker);
            PluginMarker marker2 = PluginOperateUtils.updateMarker2Online(pluginMarker);

            Assert.assertTrue(marker1 instanceof PluginMarkerAdapter);
            Assert.assertEquals(marker1.getPluginID(), "plugin-1");
            Assert.assertEquals(marker1.getVersion(), "1.0");
            Assert.assertEquals(((PluginMarkerAdapter) marker1).getPluginName(), "图表(新特性)");
            Assert.assertEquals(marker2, pluginMarker);

            PowerMock.verifyAll();
        } catch (Exception e) {
            Assert.fail();
        }

    }


    @Test
    public void testUpdatePluginOnline() {
        try {
            PluginMarker pluginMarker = PluginMarker.create("plugin-1", "1.0");
            String pluginJson = "{\"id\": plugin-1,\"name\": \"图表(新特性)\",\"version\": \"8.6.16\"}";
            JSONObject object = new JSONObject(pluginJson);

            PowerMock.mockStatic(PluginUtils.class);
            EasyMock.expect(PluginUtils.getLatestPluginInfo("plugin-1")).andReturn(object).once();
            EasyMock.expect(PluginUtils.getInstalledPluginMarkerByID("plugin-1")).andReturn(pluginMarker).once();

            PowerMock.replayAll();

            PluginOperateUtils.updatePluginOnline(pluginMarker, null);

            PowerMock.verifyAll();
        } catch (Exception e) {
            Assert.fail();
        }
    }

}
