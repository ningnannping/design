package com.fr.design.data;

import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author rinoux
 * @version 10.0
 * Created by rinoux on 2022/3/28
 */
public class MapCompareUtilsTest {

    @Test
    public void contrastMapEntries() {


        Map<String, String> orig = new LinkedHashMap<>();

        orig.put("aaa", "aaa");
        orig.put("bbb", "bbb");
        orig.put("ccc", "ccc");
        orig.put("ddd", "ddd");


        Map<String, String> other = new LinkedHashMap<>();

        other.put("aaa", "111");
        other.put("bbb", "bbb");
        other.put("ccc", "ccc");
        other.put("eee", "eee");


        MapCompareUtils.contrastMapEntries(orig, other, new MapCompareUtils.EventHandler<String, String>() {
            @Override
            public void on(MapCompareUtils.EntryEventKind entryEventKind, String s, String s2) {
                switch (entryEventKind) {
                    case UPDATED:
                        Assert.assertEquals(s, "aaa");
                        Assert.assertEquals(s2, "111");
                        break;
                    case REMOVED:
                        Assert.assertEquals(s, "ddd");
                        break;
                    case ADDED:
                        Assert.assertEquals(s, "eee");
                        Assert.assertEquals(s2, "eee");
                        break;
                    default:
                        Assert.fail();
                }
            }
        });
    }
}