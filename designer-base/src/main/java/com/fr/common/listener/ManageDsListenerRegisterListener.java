package com.fr.common.listener;

import com.fr.design.data.DesignTableDataManager;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ChangeListener;

/**
 * 管理数据集相关监听的注册
 *
 * 原本的监听生命周期注册与销毁：
 *
 * 创建时组件时进行注册，在模板关闭时进行销毁
 * 但是在模板未关闭的这段时间，如果不断的打开和关闭某些弹窗，次数达到一定程度，会导致出现大量内存占用，除非此时关闭模板
 *
 * 改成以下模式：
 *
 * 当组件可见或者被添加到某个大组件 注册相关监听
 * 当组件不可见或者被移除时 立即移除相关监听
 * 及时清理无效监听，减少实时内存占用
 *
 *
 * @author hades
 * @version 11.0
 * Created by hades on 2022/2/14
 */
public class ManageDsListenerRegisterListener implements AncestorListener {

    private ChangeListener changeListener;

    public ManageDsListenerRegisterListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    @Override
    public void ancestorAdded(AncestorEvent event) {
        DesignTableDataManager.addDsChangeListener(changeListener);
        // 添加后 fire一下 更新数据
        changeListener.stateChanged(null);
    }

    @Override
    public void ancestorRemoved(AncestorEvent event) {
        DesignTableDataManager.removeDsChangeLister(changeListener);
    }

    @Override
    public void ancestorMoved(AncestorEvent event) {
        // do nothing
    }
}
