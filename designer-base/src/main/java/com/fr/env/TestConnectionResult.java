package com.fr.env;

import com.fr.decision.webservice.exception.login.UserLoginLockException;
import com.fr.decision.webservice.exception.login.UserPasswordNeedUpdateException;
import com.fr.decision.webservice.exception.user.UserPasswordStrengthLimitException;
import com.fr.design.i18n.Toolkit;
import com.fr.exception.RemoteDesignPermissionDeniedException;
import com.fr.general.ComparatorUtils;
import com.fr.stable.StringUtils;
import com.fr.workspace.connect.WorkspaceConnectionInfo;

import com.fr.workspace.engine.exception.WorkspaceCheckException;
import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * 测试连接的结果。
 * 不改变原有逻辑的情况下，加入一层转化。
 * 根据这里的转化结果，判断需要提示哪些内容。
 * <p>
 * created by Harrison on 2018/12/20
 **/
public enum TestConnectionResult {
    /**
     * 完全成功, 版本匹配，测试连接成功。
     */
    FULLY_SUCCESS {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.informationIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Connect_Successful");
        }
    },

    /**
     * 不完全成功，版本不匹配，但测试连接成功。该状态先保留
     */
    PARTLY_SUCCESS {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.warningIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Version_Inconsistence_Test");
        }
    },

    /**
     * 完全失败，直接没连上
     */
    FULLY_FAILED {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Connect_Failed");
        }
    },

    /**
     * 验证 Token 失败
     */
    AUTH_FAILED {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Connect_Auth_Failed");
        }
    },

    /**
     * 登录锁定
     */
    LOGIN_LOCK {

        private String value;

        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Login_Lock", value);
        }

        @Override
        public String errorCode() {
            return UserLoginLockException.ERROR_CODE;
        }

        @Override
        public void setValue(String lockMin) {
            this.value = lockMin;
        }

        @Override
        public boolean isVerifyResult() {
            return true;
        }
    },

    /**
     * 没有远程设计权限
     */
    PERMISSION_DENIED {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Permission_Denied");
        }

        @Override
        public String errorCode() {
            return RemoteDesignPermissionDeniedException.ERROR_CODE;
        }
    },

    /**
     * 弱密码
     */
    PASSWORD_STRENGTH_LIMIT () {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Weak_Password");
        }

        @Override
        public String errorCode() {
            return UserPasswordStrengthLimitException.ERROR_CODE;
        }

        @Override
        public boolean isVerifyResult() {
            return true;
        }
    },

    /**
     * 密码更新
     */
    PASSWORD_NEED_UPDATE {
        @Override
        public Icon getIcon() {
            return UIManager.getIcon("OptionPane.errorIcon");
        }

        @Override
        public String getText() {
            return Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Update_Password");
        }

        @Override
        public String errorCode() {
            return UserPasswordNeedUpdateException.ERROR_CODE;
        }

        @Override
        public boolean isVerifyResult() {
            return true;
        }
    };

    public abstract Icon getIcon();

    public abstract String getText();

    public void setValue(String value) {
        // do noting
    }

    public String errorCode() {
        return StringUtils.EMPTY;
    }

    public boolean isVerifyResult() {
        return false;
    }

    public static final String WRAP = "<br/>";

    public static TestConnectionResult parse(Boolean value, WorkspaceConnectionInfo info) {
        if (value == null) {
            return AUTH_FAILED;
        }
        if (!value) {
            return FULLY_FAILED;
        }
        //去掉测试连接时所做的检测
        return FULLY_SUCCESS;
    }

    public static TestConnectionResult parseByException(WorkspaceCheckException e) {
        for (TestConnectionResult result : values()) {
            if (ComparatorUtils.equals(e.errorCode(), result.errorCode())) {
                // 登录锁定 获取下锁定的时间
                if (ComparatorUtils.equals(result, LOGIN_LOCK)) {
                    result.setValue(e.getMessage().replaceAll("\\D+", StringUtils.EMPTY));
                }
                return result;
            }
        }
        return AUTH_FAILED;
    }
}
