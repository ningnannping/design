package com.fr.env.handler.impl;

import com.fr.base.exception.ExceptionDescriptor;
import com.fr.design.EnvChangeEntrance;
import com.fr.design.dialog.UIDetailErrorLinkDialog;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.env.HelpLink;
import com.fr.env.handler.Handler;
import com.fr.env.handler.RefWrapper;
import com.fr.env.handler.ResultWrapper;
import com.fr.stable.StringUtils;
import javax.swing.SwingUtilities;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/5
 */
public class CommonHandler implements Handler<RefWrapper, ResultWrapper> {

    private final boolean onSwitch;

    public CommonHandler(boolean onSwitch) {
        this.onSwitch = onSwitch;
    }

    @Override
    public ResultWrapper handle(RefWrapper wrapper) {
        Throwable e = wrapper.getThrowable();
        if (e instanceof ExceptionDescriptor) {
            ExceptionDescriptor exceptionDescriptor = (ExceptionDescriptor) e;
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String link = HelpLink.getLink(exceptionDescriptor.solution());
                    UIDetailErrorLinkDialog detailErrorLinkDialog =  UIDetailErrorLinkDialog.newBuilder().
                            setWindow(onSwitch ? DesignerContext.getDesignerFrame() : EnvChangeEntrance.getInstance().getDialog()).
                            setErrorCode(exceptionDescriptor.errorCode()).
                            setReason(exceptionDescriptor.reason()).
                            setSolution(exceptionDescriptor.solution()).
                            setDetailReason(exceptionDescriptor.detailReason()).
                            setTitle(Toolkit.i18nText("Fine-Design_Basic_Connection_Failed")).
                            setLink(StringUtils.isEmpty(link) ? wrapper.getLink() : link).
                            setThrowable(e).build();
                    detailErrorLinkDialog.setVisible(true);
                }
            });
        }
        return new ResultWrapper(e);
    }
}
