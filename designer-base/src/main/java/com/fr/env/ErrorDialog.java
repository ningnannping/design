package com.fr.env;

import com.fr.design.dialog.link.MessageWithLink;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.IOUtils;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * 主jar下载失败的弹出框
 *
 * */

public class ErrorDialog extends JDialog implements ActionListener {
    public ErrorDialog(Frame parent, String message) {
        super(parent, true);
        init(message);
    }

    private void init(String message){
        //主体内容
        JPanel centerPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();

        //带超链的提示信息
        JPanel messagePanel = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
        messagePanel.setBorder(BorderFactory.createEmptyBorder(5,0,0,0));

        MessageWithLink messageWithLink = new MessageWithLink(message + ",", Toolkit.i18nText("Fine-Design_Basic_Sync_Help"), CloudCenter.getInstance().acquireUrlByKind("help.replacejars", "https://help.fanruan.com/finereport/doc-view-3268.html"));
        messageWithLink.setPreferredSize(new Dimension(150, 20));
        JPanel messageLinkPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        messageLinkPane.setBorder(BorderFactory.createEmptyBorder(5, 8, 5, 0));
        messageLinkPane.add(messageWithLink);

        //错误提示图标
        UILabel imageLabel = new UILabel(IOUtils.readIcon("com/fr/design/icon/versioncheck/bigfail.png"));
        messagePanel.add(imageLabel);
        messagePanel.add(messageLinkPane);

        //确定按钮
        UIButton ok = new UIButton(Toolkit.i18nText("Fine-Design_Basic_Button_OK"));
        ok.addActionListener(this);
        ok.setSize(new Dimension(44, 20));
        JPanel okPanel = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        okPanel.add(ok);

        centerPanel.add(messagePanel, BorderLayout.CENTER);
        centerPanel.add(okPanel, BorderLayout.SOUTH);

        this.setTitle(Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"));
        this.setResizable(false);
        this.add(centerPanel, BorderLayout.CENTER);
        this.setSize(new Dimension(GeneralContext.getLocale().equals(Locale.US) ? 282 : 262, 118));
        GUICoreUtils.centerWindow(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        this.dispose();
    }
}
