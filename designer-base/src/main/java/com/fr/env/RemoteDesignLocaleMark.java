package com.fr.env;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/8/9
 */
public class RemoteDesignLocaleMark implements LocaleMark<String> {

    private Map<Locale, String> map = new HashMap<>();
    private static final String REMOTE_DESIGN_CN = CloudCenter.getInstance().acquireUrlByKind("help.remote.design.zh_CN", "https://help.fanruan.com/finereport/doc-view-3925.html");
    private static final String REMOTE_DESIGN_EN = CloudCenter.getInstance().acquireUrlByKind("help.remote.design.en_US", "https://help.fanruan.com/finereport-en/doc-view-3862.html");


    public RemoteDesignLocaleMark() {
        map.put(Locale.CHINA, REMOTE_DESIGN_CN);
        map.put(Locale.KOREA, REMOTE_DESIGN_EN);
        map.put(Locale.JAPAN, REMOTE_DESIGN_EN);
        map.put(Locale.US, REMOTE_DESIGN_EN);
        map.put(Locale.TAIWAN, REMOTE_DESIGN_CN);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? REMOTE_DESIGN_CN : result;
    }

}
