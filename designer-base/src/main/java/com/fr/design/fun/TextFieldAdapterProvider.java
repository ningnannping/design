package com.fr.design.fun;

import com.fr.design.beans.ErrorMsgTextFieldAdapter;
import com.fr.stable.fun.mark.Immutable;

public interface TextFieldAdapterProvider extends Immutable {
    String XML_TAG = "TextFieldAdapterProvider";
    int CURRENT_LEVEL = 1;

    ErrorMsgTextFieldAdapter createTextFieldAdapter();
}
