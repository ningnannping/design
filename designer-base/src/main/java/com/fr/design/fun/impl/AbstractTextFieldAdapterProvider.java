package com.fr.design.fun.impl;

import com.fr.design.fun.TextFieldAdapterProvider;
import com.fr.stable.fun.mark.API;

/**
 * @author Joe
 * 2021/10/8 15:17
 */
@API(level = TextFieldAdapterProvider.CURRENT_LEVEL)
public abstract class AbstractTextFieldAdapterProvider implements TextFieldAdapterProvider {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public int layerIndex() {
        return DEFAULT_LAYER_INDEX;
    }
}
