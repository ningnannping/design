package com.fr.design.fun;

import com.fr.stable.fun.mark.Mutable;
import com.fr.start.BaseDesigner;

/**
 * 设计器启动类替换接口
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/5/7
 */

public interface DesignerStartClassProcessor extends Mutable {

    String MARK_STRING = "DesignerStartClassProcessor";

    int CURRENT_LEVEL = 1;


    Class<? extends BaseDesigner> transform();

}
