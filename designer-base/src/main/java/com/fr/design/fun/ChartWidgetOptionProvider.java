package com.fr.design.fun;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-06-12
 */
public interface ChartWidgetOptionProvider extends ParameterWidgetOptionProvider {

    String XML_TAG = "ChartWidgetOptionProvider";

    //在图表区域的开头还是结尾插入
    boolean isBefore();
}
