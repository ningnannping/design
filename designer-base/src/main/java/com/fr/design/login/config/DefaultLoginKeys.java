package com.fr.design.login.config;

import com.fr.log.FineLoggerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2022/6/20
 */
public class DefaultLoginKeys {

    private static final String FILENAME = "com/fr/design/config/default";

    private static final DefaultLoginKeys INSTANCE = new DefaultLoginKeys();

    public static DefaultLoginKeys getInstance() {
        return INSTANCE;
    }

    private final Map<String, String> keys = new HashMap<>();

    private DefaultLoginKeys() {
        Properties properties = load();
        for (Map.Entry<Object, Object> entry : properties.entrySet()) {
            String name = entry.getKey().toString();
            keys.put(name, entry.getValue().toString());
        }
    }

    public String getKey(String name) {
        return keys.get(name);
    }

    private Properties load() {
        Properties properties = new Properties();
        try (InputStream inputStream = DefaultLoginKeys.class.getClassLoader().getResourceAsStream(FILENAME)) {
            properties.load(inputStream);
        } catch (IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return properties;
    }
}
