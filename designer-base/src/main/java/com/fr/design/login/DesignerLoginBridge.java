package com.fr.design.login;

import com.fr.design.bridge.exec.JSBridge;
import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ilable.ActionLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.locale.impl.BbsResetMark;
import com.fr.design.login.executor.DesignerLoginBrowserExecutor;
import com.fr.design.login.executor.DesignerLoginExecutor;
import com.fr.design.login.executor.DesignerSendCaptchaExecutor;
import com.fr.design.login.executor.DesignerSmsLoginExecutor;
import com.fr.design.login.executor.DesignerSmsRegisterExecutor;
import com.fr.design.login.task.DesignerLoginTaskWorker;
import com.fr.design.mainframe.toast.DesignerToastMsgUtil;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.CloudCenter;
import com.fr.general.locale.LocaleCenter;
import com.fr.general.locale.LocaleMark;
import com.fr.log.FineLoggerFactory;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSFunction;
import com.teamdev.jxbrowser.chromium.JSObject;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URI;
import java.util.Map;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerLoginBridge {

    private Map<String, String> params;

    public static DesignerLoginBridge getBridge(Browser browser, Map<String, String> params) {
        return new DesignerLoginBridge(browser, params);
    }

    private JSObject window;

    private DesignerLoginBridge(Browser browser, Map<String, String> params) {
        this.params = params;
        this.window = browser.executeJavaScriptAndReturnValue("window").asObject();
        Set<Map.Entry<String, String>> entries = params.entrySet();
        for (Map.Entry<String, String> entry : entries) {
            this.window.setProperty(entry.getKey(), entry.getValue());
        }
    }

    @JSBridge
    public String i18nText(String key) {
        return Toolkit.i18nText(key);
    }

    @JSBridge
    public void closeWindow(boolean loginSuccess) {
        DesignerLoginSource source = DesignerLoginSource.valueOf(Integer.parseInt(params.get("designerLoginSource")));
        if (loginSuccess) {
            if (source == DesignerLoginSource.GUIDE) {
                DesignerToastMsgUtil.toastPrompt(
                        getHyperlinkPane(
                                com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Guide_Login_Success_Title"),
                                com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Guide_Login_Success_Hyperlink_Text"),
                                CloudCenter.getInstance().acquireUrlByKind("designer.premium.template", "https://market.fanruan.com/template")
                        )
                );
            } else if (source == DesignerLoginSource.BBS_JUMP) {
                String bbsJumpUrl = params.get("bbsJumpUrl");
                BrowseUtils.browser(bbsJumpUrl);
            }
            DesignerLoginHelper.closeWindow();
            return;
        }
        if (source == DesignerLoginSource.SWITCH_ACCOUNT) {
            DesignerLoginHelper.closeWindow();
            return;
        }
        if (source == DesignerLoginSource.BBS_JUMP) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String bbsJumpUrl = params.get("bbsJumpUrl");
                    String[] options = new String[]{
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_BBS_Go_Directly"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Return_Login")
                    };
                    int rv = FineJOptionPane.showConfirmDialog(
                            DesignerLoginHelper.getDialog(),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_BBS_Quit_Tip"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[1]
                    );
                    if (rv == JOptionPane.YES_OPTION) {
                        BrowseUtils.browser(bbsJumpUrl);
                        DesignerLoginHelper.closeWindow();
                    }
                }
            });
        } else {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String[] options = new String[]{
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Quit"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Return_Login")
                    };
                    int rv = FineJOptionPane.showConfirmDialog(
                            DesignerLoginHelper.getDialog(),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Designer_Login_Quit_Tip"),
                            com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Tool_Tips"),
                            JOptionPane.OK_CANCEL_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[1]
                    );
                    if (rv == JOptionPane.OK_OPTION) {
                        DesignerLoginHelper.closeWindow();
                    }
                }
            });
        }
    }

    /**
     * 服务条款
     */
    @JSBridge
    public void serviceHref() {
        try {
            String url = CloudCenter.getInstance().acquireUrlByKind("designer.bbs.service.terms", "https://bbs.fanruan.com/thread-102821-1-1.html");
            Desktop.getDesktop().browse(new URI(url));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().info(e.getMessage());
        }
    }

    /**
     * 忘记密码
     */
    @JSBridge
    public void forgetHref() {
        try {
            LocaleMark<String> resetMark = LocaleCenter.getMark(BbsResetMark.class);
            Desktop.getDesktop().browse(new URI(resetMark.getValue()));
        } catch (Exception e) {
            FineLoggerFactory.getLogger().info(e.getMessage());
        }
    }


    /**
     * 设计器端的用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @param callback 回调函数
     */
    @JSBridge
    public void normalLogin(String username, String password, final JSFunction callback) {
        DesignerLoginTaskWorker<Void> worker = new DesignerLoginTaskWorker<>(
                new JSCallback(DesignerLoginBrowserExecutor.create(window, callback)),
                new DesignerLoginExecutor(username, password));
        worker.execute();
    }

    /**
     * 发送短信验证码
     *
     * @param regionCode 区号
     * @param phone      手机
     * @param callback   回调函数
     */
    @JSBridge
    public void sendCaptcha(String regionCode, String phone, final JSFunction callback) {
        DesignerLoginTaskWorker<Void> worker = new DesignerLoginTaskWorker<>(
                new JSCallback(DesignerLoginBrowserExecutor.create(window, callback)),
                new DesignerSendCaptchaExecutor(regionCode, phone));
        worker.execute();
    }

    /**
     * 设计器端的短信登录
     *
     * @param regionCode 区号
     * @param phone      手机
     * @param code       验证码
     * @param callback   回调函数
     */
    @JSBridge
    public void smsLogin(String regionCode, String phone, String code, final JSFunction callback) {
        DesignerLoginTaskWorker<Void> worker = new DesignerLoginTaskWorker<>(
                new JSCallback(DesignerLoginBrowserExecutor.create(window, callback)),
                new DesignerSmsLoginExecutor(regionCode, phone, code));
        worker.execute();
    }

    /**
     * 设计器端的用户注册
     *
     * @param regionCode 区号
     * @param phone      手机
     * @param password   密码
     * @param regToken   注册令牌
     * @param callback   回调函数
     */
    @JSBridge
    public void smsRegister(String regionCode, String phone, String password, String regToken, final JSFunction callback) {
        DesignerLoginTaskWorker<Void> worker = new DesignerLoginTaskWorker<>(
                new JSCallback(DesignerLoginBrowserExecutor.create(window, callback)),
                new DesignerSmsRegisterExecutor(regionCode, phone, password, regToken));
        worker.execute();
    }

    /**
     * 使用系统浏览器打开网页
     *
     * @param url 要打开的网页
     */
    @JSBridge
    public void openShopUrlAtWebBrowser(String url) {
        if (Desktop.isDesktopSupported()) {
            try {
                //创建一个URI实例,注意不是URL
                URI uri = URI.create(url);
                //获取当前系统桌面扩展
                Desktop desktop = Desktop.getDesktop();
                //判断系统桌面是否支持要执行的功能
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    //获取系统默认浏览器打开链接
                    desktop.browse(uri);
                }
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
    }

    /**
     * 调整面板大小
     *
     * @param width  宽
     * @param height 高
     */
    @JSBridge
    public void resize(int width, int height) {
        DesignerLoginHelper.getDialog().setSize(width, height);
    }

    private JPanel getHyperlinkPane(String title, String hyperlinkText, String hyperlink) {
        ActionLabel actionLabel = new ActionLabel(hyperlinkText);
        actionLabel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI(hyperlink));
                } catch (Exception ignore) {
                }
            }
        });
        JPanel panel = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
        panel.add(new UILabel(title));
        panel.add(actionLabel);
        return panel;
    }
}
