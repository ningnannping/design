package com.fr.design.login.service;

import com.fr.design.DesignerEnvManager;
import com.fr.design.login.DesignerLoginType;
import com.fr.design.upm.event.CertificateEvent;
import com.fr.event.EventDispatcher;
import com.fr.json.JSONObject;
import com.fr.stable.StringUtils;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/28
 */
public class DesignerPassportManager {

    private static final String STATUS = "status";
    private static final String REGISTER = "register";
    private static final String REG_TOKEN = "regtoken";

    private static volatile DesignerPassportManager instance = null;

    public static DesignerPassportManager getInstance() {
        if (instance == null) {
            synchronized (DesignerPassportManager.class) {
                if (instance == null) {
                    instance = new DesignerPassportManager();
                }
            }
        }
        return instance;
    }

    /**
     * 账号密码登录帆软通行证
     *
     * @param username 论坛账号
     * @param password 密码
     */
    public int login(String username, String password) {
        DesignerLoginClient client = new DesignerLoginClient();
        DesignerLoginResult result = client.login(username, password);
        int uid = result.getUid();
        if (uid > 0) {
            saveUserInfo(uid, result.getUsername(), result.getAppId(), result.getRefreshToken(), DesignerLoginType.NORMAL_LOGIN, username);
        }
        return uid;
    }

    /**
     * 发送短信验证码
     *
     * @param regionCode 区号
     * @param phone 手机
     */
    public int sendCaptcha(String regionCode, String phone) {
        DesignerLoginClient client = new DesignerLoginClient();
        return client.sendCaptcha(regionCode, phone);
    }

    /**
     * 短信登录帆软通行证
     *
     * @param regionCode 区号
     * @param phone 手机
     * @param code 验证码
     */
    public String smsLogin(String regionCode, String phone, String code) {
        DesignerLoginClient client = new DesignerLoginClient();
        DesignerLoginResult result = client.smsLogin(regionCode, phone, code);
        int uid = result.getUid();
        if (uid > 0) {
            saveUserInfo(uid, result.getUsername(), result.getAppId(), result.getRefreshToken(), DesignerLoginType.SMS_LOGIN, regionCode + "-" + phone);
        }
        JSONObject jo = new JSONObject();
        jo.put(STATUS, result.getUid());
        jo.put(REGISTER, result.isRegister());
        jo.put(REG_TOKEN, result.getRegToken());
        return jo.toString();
    }

    /**
     * 注册帆软通行证
     *
     * @param regionCode 区号
     * @param phone 手机
     * @param password 密码
     * @param regToken 注册令牌
     */
    public int smsRegister(String regionCode, String phone, String password, String regToken) {
        DesignerLoginClient client = new DesignerLoginClient();
        DesignerLoginResult result = client.smsRegister(regionCode, phone, password, regToken);
        int uid = result.getUid();
        if (uid > 0) {
            saveUserInfo(uid, result.getUsername(), result.getAppId(), result.getRefreshToken(), DesignerLoginType.SMS_LOGIN, regionCode + "-" + phone);
        }
        return uid;
    }

    /**
     * 登出帆软通行证
     */
    public void logout() {
        saveUserInfo(-1, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, DesignerLoginType.UNKNOWN, StringUtils.EMPTY);
        EventDispatcher.fire(CertificateEvent.LOGOUT, StringUtils.EMPTY);
    }

    /**
     * 保存登录信息
     */
    private void saveUserInfo(int uid, String username, String appId, String refreshToken, DesignerLoginType type, String account) {
        DesignerEnvManager manager = DesignerEnvManager.getEnvManager();
        manager.setDesignerLoginUid(uid);
        manager.setDesignerLoginUsername(username);
        manager.setDesignerLoginAppId(appId);
        manager.setDesignerLoginRefreshToken(refreshToken);
        manager.setDesignerLastLoginTime(System.currentTimeMillis());
        manager.setLastLoginType(type);
        manager.setLastLoginAccount(account);
        DesignerEnvManager.getEnvManager().saveXMLFile();
        EventDispatcher.fire(CertificateEvent.LOGIN, username);
    }
}
