package com.fr.design.login;

import com.fr.design.DesignerEnvManager;
import com.fr.design.dialog.UIDialog;
import com.fr.design.extra.WebViewDlgHelper;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.os.impl.SupportOSImpl;
import com.fr.design.plugin.DesignerPluginContext;
import com.fr.design.update.ui.dialog.UpdateMainDialog;
import com.fr.general.GeneralContext;
import java.awt.Dialog;
import java.awt.Frame;
import java.awt.Window;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerLoginHelper {

    private static final String MAIN_RESOURCE_PATH = "/com/fr/design/login/login.html";
    private static final String JXBROWSER = "com.teamdev.jxbrowser.browser.Browser";

    private static UIDialog dialog = null;

    public static String getMainResourcePath() {
        return MAIN_RESOURCE_PATH;
    }

    public static UIDialog getDialog() {
        return dialog;
    }

    public static void showLoginDialog(DesignerLoginSource source) {
        showLoginDialog(source, new HashMap<>());
    }

    public static void showLoginDialog(DesignerLoginSource source, Map<String, String> params) {
        showLoginDialog(source, params, DesignerContext.getDesignerFrame());
    }

    public static void showLoginDialog(DesignerLoginSource source, Map<String, String> params, Window window) {
        if (!SupportOSImpl.DESIGNER_LOGIN.support() || DesignerEnvManager.getEnvManager().isUseOldVersionLogin()) {
            WebViewDlgHelper.createLoginDialog(window);
            return;
        }
        boolean hasJxBrowser = true;
        try {
            Class.forName(JXBROWSER);
        } catch (ClassNotFoundException e) {
            hasJxBrowser = false;
        }
        if (hasJxBrowser) {
            showLoginPane(source, params, window);
        } else {
            showUpdatePane();
        }
    }

    private static void showLoginPane(DesignerLoginSource source, Map<String, String> params, Window window) {
        DesignerLoginPane designerLoginPane = new DesignerLoginPane(source, params);
        if (dialog == null) {
            if (window instanceof Dialog) {
                dialog = new DesignerLoginShowDialog((Dialog) window, designerLoginPane);
            } else {
                dialog = new DesignerLoginShowDialog((Frame) window, designerLoginPane);
            }
        }
        dialog.setVisible(true);
    }


    private static void showUpdatePane() {
        JOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), Toolkit.i18nText("Fine-Design_Update_Info_Login_Message"));
        if (!GeneralContext.getLocale().equals(Locale.JAPANESE) && !GeneralContext.getLocale().equals(Locale.JAPAN)
                && !Locale.getDefault().equals(Locale.JAPAN) && !Locale.getDefault().equals(Locale.JAPANESE)) {
            UpdateMainDialog dialog = new UpdateMainDialog(DesignerContext.getDesignerFrame());
            dialog.setAutoUpdateAfterInit();
            dialog.showDialog();
        }
    }

    public static void closeWindow() {
        if (dialog != null) {
            dialog.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            dialog.setVisible(false);
            dialog = null;
            DesignerPluginContext.setPluginDialog(null);
        }
    }

    public static void main(String[] args) {
        DesignerEnvManager.getEnvManager().setOpenDebug(true);
        showLoginDialog(DesignerLoginSource.NORMAL);
    }
}
