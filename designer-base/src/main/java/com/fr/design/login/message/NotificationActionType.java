package com.fr.design.login.message;

import com.fr.config.ServerPreferenceConfig;
import com.fr.design.dialog.NotificationDialogAction;
import com.fr.design.extra.WebViewDlgHelper;
import com.fr.design.mainframe.BaseJForm;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.EastRegionContainerPane;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.os.impl.SupportOSImpl;
import com.fr.design.upm.UpmFinder;
import com.fr.design.utils.DesignUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import com.fr.stable.bridge.StableFactory;
import com.fr.stable.os.Arch;
import com.fr.stable.os.OperatingSystem;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/6/11
 */
public enum NotificationActionType {
    PLUGIN("PLUGIN", new NotificationDialogAction() {
        @Override
        public String name() {
            return "PLUGIN";
        }

        @Override
        public void doClick() {
            try {
                if (Arch.getArch() == Arch.ARM || OperatingSystem.isLinux() || SupportOSImpl.MACOS_WEB_PLUGIN_MANAGEMENT.support()) {
                    DesignUtils.visitEnvServerByParameters("#management/plugin", null, null);
                    return;
                }
                if (ServerPreferenceConfig.getInstance().isUseOptimizedUPM() || SupportOSImpl.MACOS_NEW_PLUGIN_MANAGEMENT.support()) {
                    UpmFinder.showUPMDialog();
                } else {
                    WebViewDlgHelper.createPluginDialog();
                }
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
    }),
    REUSE("REUSE", new NotificationDialogAction() {
        @Override
        public String name() {
            return "REUSE";
        }

        @Override
        public void doClick() {
            try {
                BaseJForm jform = StableFactory.getMarkedInstanceObjectFromClass(BaseJForm.XML_TAG, BaseJForm.class);
                DesignerContext.getDesignerFrame().addAndActivateJTemplate((JTemplate<?, ?>) jform);
                EastRegionContainerPane.getInstance().showContainer();
                EastRegionContainerPane.getInstance().switchTabTo(EastRegionContainerPane.KEY_WIDGET_LIB);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
    }),
    UNKNOWN(StringUtils.EMPTY, new NotificationDialogAction() {
        @Override
        public String name() {
            return "UNKNOWN";
        }

        @Override
        public void doClick() {
        }
    });

    private String jumpTo;
    private NotificationDialogAction action;

    NotificationActionType(String jumpTo, NotificationDialogAction action) {
        this.jumpTo = jumpTo;
        this.action = action;
    }

    public String getJumpTo() {
        return jumpTo;
    }

    public NotificationDialogAction getAction() {
        return action;
    }
}
