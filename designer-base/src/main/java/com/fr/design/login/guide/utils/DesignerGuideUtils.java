package com.fr.design.login.guide.utils;

import com.fr.general.GeneralContext;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Lanlan
 * @version 10.0
 * Created by Lanlan on 2021/5/21
 */
public class DesignerGuideUtils {

    public static Map<String, String> renderMap() {
        Map<String, String> map4Tpl = new HashMap<>();
        map4Tpl.put("language", GeneralContext.getLocale().toString());
        return map4Tpl;
    }
}
