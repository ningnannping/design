/*
 * Copyright(c) 2001-2010, FineReport Inc, All Rights Reserved.
 */
package com.fr.design.style.background.image;

import com.fr.design.DesignerEnvManager;
import com.fr.design.gui.ifilechooser.FileChooserFactory;
import com.fr.design.gui.ifilechooser.FileChooserProvider;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.toast.DesignerToastMsgUtil;


import java.awt.Component;
import java.io.File;
import javax.swing.SwingUtilities;


/**
 * This class used to choose image files.
 */
public class ImageFileChooser {
    private final FileChooserProvider fileChooserProvider;

    public ImageFileChooser() {
        fileChooserProvider = FileChooserFactory.createImageFileChooser();
    }


    public int showOpenDialog(Component parent, String approveButtonText) {
        return fileChooserProvider.showOpenDialog(parent, approveButtonText);
    }

    public int showOpenDialog(Component parent) {
        showImageCompressMoveTip();
        return fileChooserProvider.showDialog(parent);
    }

    public void setCurrentDirectory(File file) {
        fileChooserProvider.setCurrentDirectory(file);
    }

    public void setMultiSelectionEnabled(boolean multiple) {
        fileChooserProvider.setMultiSelectionEnabled(multiple);
    }

    public File getSelectedFile() {
        return fileChooserProvider.getSelectedFile();
    }

    public boolean isCheckSelected() {
        return DesignerEnvManager.getEnvManager().isImageCompress();
    }

    private void showImageCompressMoveTip() {
        if (DesignerEnvManager.getEnvManager().isShowImageCompressMoveTip()) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    DesignerToastMsgUtil.toastWarning(Toolkit.i18nText("Fine-Design_Image_Compress_Move_Tip"));
                    DesignerEnvManager.getEnvManager().setShowImageCompressMoveTip(false);
                }
            });
        }
    }
}
