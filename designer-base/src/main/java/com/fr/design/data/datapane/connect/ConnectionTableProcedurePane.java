package com.fr.design.data.datapane.connect;

import com.fr.base.BaseUtils;
import com.fr.base.svg.IconUtils;
import com.fr.data.core.db.TableProcedure;
import com.fr.data.impl.AbstractDatabaseConnection;
import com.fr.data.impl.Connection;
import com.fr.design.border.UIRoundedBorder;
import com.fr.design.constants.UIConstants;
import com.fr.design.data.tabledata.tabledatapane.DBTableDataPane;
import com.fr.design.data.tabledata.tabledatapane.loading.SwitchableTableDataPane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ilist.TableViewList;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.general.GeneralContext;
import com.fr.stable.ArrayUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.ToolTipManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

/**
 * 数据集编辑面板左边的部分
 *
 * @editor zhou
 * @since 2012-3-28下午10:14:59
 */
public class ConnectionTableProcedurePane extends BasicPane {
	private static int WIDTH = 155;
	private ConnectionComboBoxPanel connectionComboBox;
	private UICheckBox tableCheckBox;
	private UICheckBox viewCheckBox;
	protected UITextField searchField;
	private TableViewList tableViewList;
	private java.util.List<DoubleClickSelectedNodeOnTreeListener> listeners = new java.util.ArrayList<DoubleClickSelectedNodeOnTreeListener>();

	public ConnectionTableProcedurePane() {
		init(null);
	}

	/**
	 * 传入父容器
	 * @param parent
	 */
	public ConnectionTableProcedurePane(SwitchableTableDataPane parent) {
		init(parent);
	}

	private void init(SwitchableTableDataPane parent) {
		this.setLayout(new BorderLayout(4, 4));
		// 初始化数据连接下拉框
		initConnectionComboBox(parent);
		// 初始化中间的面板
		JPanel centerPane = initCenterPane();
		this.add(connectionComboBox, BorderLayout.NORTH);
		this.add(centerPane, BorderLayout.CENTER);
		this.setPreferredSize(new Dimension(WIDTH, getPreferredSize().height));
		addKeyMonitor();
	}

	private JPanel initCenterPane() {
		JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
		// 搜索面板
		centerPane.add(createSearchPane(), BorderLayout.NORTH);
		// 数据库表视图面板
		centerPane.add(createTableViewBorderPane(), BorderLayout.CENTER);
		return centerPane;
	}

	private void initConnectionComboBox(SwitchableTableDataPane parent) {
		connectionComboBox = new ConnectionComboBoxPanel(com.fr.data.impl.Connection.class) {

			@Override
			protected void filterConnection(Connection connection, String conName, List<String> nameList) {
				filter(connection, conName, nameList);
			}

			@Override
			protected void refreshItems() {
				super.refreshItems();
				if (tableViewList != null) {
					search(true);
				}
			}

			@Override
			protected void afterRefreshItems() {
				// 刷新完成后，如果未选中（在nameList初始化完成之前可能会出现），则尝试再次设置
				if (isSelectedItemEmpty()) {
					setRecentConnection();
				}
				// 获取数据连接之后，让父容器切换面板
				if (parent != null) {
					parent.switchTo(SwitchableTableDataPane.CONTENT_PANE_NAME);
				}
			}

			@Override
			protected void refreshItemsError() {
				// 获取数据连接出现错误时，也让父容器从Loading面板切换至内容面板
				if (parent != null) {
					parent.switchTo(SwitchableTableDataPane.CONTENT_PANE_NAME);
				}
			}
		};
		connectionComboBox.addComboBoxActionListener(filter);
	}

	private JPanel createTableViewBorderPane() {
		tableViewList = new TableViewList();
		ToolTipManager.sharedInstance().registerComponent(tableViewList);

		tableViewList.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				if (evt.getClickCount() >= 2) {
					Object obj = tableViewList.getSelectedValue();
					TableProcedure tableProcedure = null;
					if (obj instanceof TableProcedure) {
						tableProcedure = (TableProcedure) obj;
					} else {
						return;
					}
					for (int i = 0; i < ConnectionTableProcedurePane.this.listeners.size(); i++) {
						ConnectionTableProcedurePane.this.listeners.get(i).actionPerformed(tableProcedure);
					}
				}
			}
		});
		UIScrollPane tableViewListPane = new UIScrollPane(tableViewList);
		tableViewListPane.setBorder(new UIRoundedBorder(UIConstants.LINE_COLOR, 1, UIConstants.ARC));
		JPanel tableViewBorderPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
		tableViewBorderPane.add(tableViewListPane, BorderLayout.CENTER);
		JPanel checkBoxgroupPane = createCheckBoxgroupPane();
		if (checkBoxgroupPane != null) {
			tableViewBorderPane.add(createCheckBoxgroupPane(), BorderLayout.SOUTH);
		}
		return tableViewBorderPane;
	}

	/**
	 * 创建搜索Panel，用于搜索表或视图
	 * @return
	 */
	private JPanel createSearchPane() {
		JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
		JPanel searchPane = new JPanel(new BorderLayout(10, 0));
		searchPane.setBorder(BorderFactory.createLineBorder(UIConstants.TOOLBAR_BORDER_COLOR));
		searchPane.setBackground(Color.WHITE);
		searchField = new UITextField();
		searchField.setBorderPainted(false);
		searchField.setPlaceholder(Toolkit.i18nText("Fine-Design_Basic_Table_Search"));
		searchField.getDocument().addDocumentListener(searchListener);
		searchField.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				super.mouseEntered(e);
				searchPane.setBorder(BorderFactory.createLineBorder(UIConstants.CHECKBOX_HOVER_SELECTED));
			}

			@Override
			public void mouseExited(MouseEvent e) {
				super.mouseExited(e);
				searchPane.setBorder(BorderFactory.createLineBorder(UIConstants.TOOLBAR_BORDER_COLOR));
			}
		});
		// 搜索图标
		UILabel searchLabel = new UILabel(IconUtils.readIcon("/com/fr/design/images/data/search"));
		searchLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));
		searchPane.add(searchField, BorderLayout.CENTER);
		searchPane.add(searchLabel, BorderLayout.EAST);
		panel.add(searchPane, BorderLayout.CENTER);
		return panel;
	}

	protected void filter(Connection connection, String conName, List<String> nameList) {
		connection.addConnection(nameList, conName, new Class[]{AbstractDatabaseConnection.class});
	}

	protected void addKeyMonitor() {
		//do nothing
	}

	protected JPanel createCheckBoxgroupPane() {
		JPanel checkBoxgroupPane = FRGUIPaneFactory.createNColumnGridInnerContainer_S_Pane(2);
		JPanel first = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
		tableCheckBox = new UICheckBox();
		tableCheckBox.setSelected(true);
		tableCheckBox.addActionListener(filter);
		first.add(tableCheckBox);

		JPanel second = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
		viewCheckBox = new UICheckBox();
		viewCheckBox.setSelected(true);
		viewCheckBox.addActionListener(filter);
		second.add(viewCheckBox);

		// 根据环境是否为中文设置不同的显示
		if (GeneralContext.isChineseEnv()) {
			first.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_SQL_Table"),
					BaseUtils.readIcon("/com/fr/design/images/data/tables.png"), UILabel.LEADING));
			second.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_SQL_View"),
					BaseUtils.readIcon("/com/fr/design/images/data/views.png"), UILabel.LEADING));
		} else {
			UILabel ui1 = new UILabel(BaseUtils.readIcon("/com/fr/design/images/data/tables.png"), UILabel.LEADING);
			UILabel ui2 = new UILabel(BaseUtils.readIcon("/com/fr/design/images/data/views.png"), UILabel.LEADING);
			ui1.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_SQL_Table"));
			ui2.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_SQL_View"));
			first.add(ui1);
			second.add(ui2);
		}
		checkBoxgroupPane.add(first);
		checkBoxgroupPane.add(second);

		return checkBoxgroupPane;
	}

	/**
	 * 给 itemComboBox 加上 itemListener
	 *
	 * @param itemListener
	 */
	public void addItemListener(ItemListener itemListener) {
		connectionComboBox.itemComboBox.addItemListener(itemListener);
	}

	private DocumentListener searchListener = new DocumentListener() {

		@Override
		public void removeUpdate(DocumentEvent e) {
			search(false);
		}

		@Override
		public void insertUpdate(DocumentEvent e) {
			search(false);
		}

		@Override
		public void changedUpdate(DocumentEvent e) {
			search(false);
		}
	};

	private ActionListener filter = new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
			search(false);
		}
	};

	/**
	 * 选项改变，需要重新刷新下拉列表里面的项
	 */
	protected void search(boolean refresh) {
		String selectedObj = connectionComboBox.getSelectedItem();

		String[] types = ArrayUtils.EMPTY_STRING_ARRAY;
		if (tableCheckBox != null) {
			if (tableCheckBox.isSelected()) {
				types = (String[]) ArrayUtils.add(types, TableProcedure.TABLE);
			}
			if (viewCheckBox.isSelected()) {
				types = (String[]) ArrayUtils.add(types, TableProcedure.VIEW);
			}
		} else {
			types = (String[]) ArrayUtils.add(types, TableProcedure.PROCEDURE);
		}
		tableViewList.populate(selectedObj, searchField.getText().trim(), refresh, types);
	}

	@Override
	protected String title4PopupWindow() {
		return "Connection";
	}

	/**
	 * @param l
	 */
	public void addDoubleClickListener(DoubleClickSelectedNodeOnTreeListener l) {
		this.listeners.add(l);
	}

	public void setSelectedDatabaseConnection(com.fr.data.impl.Connection db) {
		connectionComboBox.populate(db);
	}

	public String getSelectedDatabaseConnnectonName() {
		return connectionComboBox.getSelectedItem();
	}

	public static interface DoubleClickSelectedNodeOnTreeListener {
		/**
		 * 处理双击事件
		 *
		 * @param target
		 */
		public void actionPerformed(TableProcedure target);
	}
}
