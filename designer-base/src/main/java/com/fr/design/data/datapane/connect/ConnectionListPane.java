package com.fr.design.data.datapane.connect;

import com.fr.data.driver.DriverClassNotFoundException;
import com.fr.data.impl.Connection;
import com.fr.data.impl.JDBCDatabaseConnection;
import com.fr.data.impl.JNDIDatabaseConnection;
import com.fr.data.operator.DataOperator;
import com.fr.design.ExtraDesignClassManager;
import com.fr.design.data.MapCompareUtils;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.fun.ConnectionProvider;
import com.fr.design.gui.controlpane.JListControlPane;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.gui.ilist.ListModelElement;
import com.fr.design.i18n.Toolkit;
import com.fr.file.ConnectionConfig;
import com.fr.general.ComparatorUtils;
import com.fr.general.NameObject;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ArrayUtils;
import com.fr.stable.Nameable;
import com.fr.stable.StringUtils;
import com.fr.stable.core.PropertyChangeAdapter;

import javax.swing.SwingUtilities;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Connection List Pane.
 */
public class ConnectionListPane extends JListControlPane implements ConnectionShowPane {
    public static final String TITLE_NAME = Toolkit.i18nText("Fine-Design_Basic_Server_Define_Data_Connection");
    private boolean isNamePermitted = true;
    private final HashMap<String, String> renameMap = new HashMap<>();
    private final Map<String, Connection> populatedConnectionsSnapshot = new LinkedHashMap<>();

    public ConnectionListPane() {
        renameMap.clear();
        this.addEditingListener(new PropertyChangeAdapter() {
            public void propertyChange() {
                isNamePermitted = true;
                String[] allListNames = nameableList.getAllNames();
                allListNames[nameableList.getSelectedIndex()] = StringUtils.EMPTY;
                String tempName = getEditingName();
                if (StringUtils.isEmpty(tempName)) {
                    nameableList.stopEditing();
                    FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(ConnectionListPane.this), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Connection_Empty_Name"));
                    setIllegalIndex(editingIndex);
                    isNamePermitted = false;
                    return;
                }
                if (!ComparatorUtils.equals(tempName, selectedName)
                        && isNameRepeated(new List[]{Arrays.asList(allListNames)}, tempName)) {
                    isNamePermitted = false;
                    nameableList.stopEditing();
                    String message = Toolkit.i18nText("Fine-Design_Basic_Connection_Duplicate_Name", tempName);
                    FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(ConnectionListPane.this), message);
                    setIllegalIndex(editingIndex);
                }
                if (isNamePermitted && !ComparatorUtils.equals(tempName, selectedName)) {
                    rename(selectedName, tempName);
                }

            }
        });
    }


    protected void rename(String oldName, String newName) {
        renameMap.remove(selectedName);
        renameMap.put(selectedName, newName);
    }

    /**
     * 名字是否允许
     *
     * @return 是/否
     */
    public boolean isNamePermitted() {
        return isNamePermitted;
    }

    /**
     * 检查按钮可用状态 Check button enabled.
     */
    public void checkButtonEnabled() {
        super.checkButtonEnabled();
        isNamePermitted = !isContainsRename();
    }


    public HashMap<String, String> getRenameMap() {
        return renameMap;
    }

    /**
     * 创建菜单项
     *
     * @return 菜单项
     */
    public NameableCreator[] createNameableCreators() {
        NameableCreator[] creators = new NameableCreator[]{new NameObjectCreator(
                "JDBC",
                "/com/fr/design/images/data/source/jdbcTableData.png",
                JDBCDatabaseConnection.class,
                DatabaseConnectionPane.JDBC.class
        ), new NameObjectCreator(
                "JNDI",
                "/com/fr/design/images/data/source/jdbcTableData.png",
                JNDIDatabaseConnection.class,
                DatabaseConnectionPane.JNDI.class
        )};
        Set<ConnectionProvider> pluginCreators = ExtraDesignClassManager.getInstance().getArray(ConnectionProvider.XML_TAG);
        for (ConnectionProvider provider : pluginCreators) {
            NameObjectCreator creator = new NameObjectCreator(
                    provider.nameForConnection(),
                    provider.iconPathForConnection(),
                    provider.classForConnection(),
                    provider.appearanceForConnection()
            );
            creators = ArrayUtils.add(creators, creator);
        }

        return creators;
    }

    @Override
    protected String title4PopupWindow() {
        return TITLE_NAME;
    }

    /**
     * Populate.
     *
     * @param connectionConfig the new datasourceManager.
     */
    public void populate(ConnectionConfig connectionConfig) {
        List<NameObject> nameObjectList = new ArrayList<NameObject>();
        populatedConnectionsSnapshot.clear();
        for (Map.Entry<String, Connection> entry : connectionConfig.getConnections().entrySet()) {
            nameObjectList.add(new NameObject(entry.getKey(), entry.getValue()));
            try {
                populatedConnectionsSnapshot.put(entry.getKey(), (Connection) entry.getValue().clone());
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        }
        this.populate(nameObjectList.toArray(new NameObject[nameObjectList.size()]));
    }

    /**
     * Update.
     */
    public void update(ConnectionConfig connectionConfig) throws Exception {
        // Nameable[]居然不能强转成NameObject[],一定要这么写...
        Nameable[] res = this.update();
        Map<String, Connection> updatedMap = new LinkedHashMap<>();
        Arrays.stream(res).map(n -> (NameObject) n).forEach(no -> updatedMap.put(no.getName(), (Connection) no.getObject()));

        List<String> removedConnNames = new ArrayList<>();
        Map<String, Connection> addedOrUpdatedConnections = new LinkedHashMap<>();
        MapCompareUtils.contrastMapEntries(populatedConnectionsSnapshot, updatedMap, (entryEventKind, s, connection) -> {
            switch (entryEventKind) {
                case REMOVED:
                    removedConnNames.add(s);
                    break;
                case ADDED:
                case UPDATED:
                    addedOrUpdatedConnections.put(s, connection);
                default:
                    break;
            }
        }, new MapCompareUtils.UpdateRule<String, Connection>() {
            @Override
            public boolean needUpdate(Connection origin, Connection connection) {
                return needUpdate0(origin, connection);
            }

            /**
             * 是否需要更新处理
             * 1. Connection本身equals为false，代表字段修改
             * 2. 非内置的Connection，即插件提供的Connection
             * todo 原本一个equals方法就可以搞定，但是插件里面没有实现equals，结果导致不能正确判断，只能主代码里做兼容，很恶心，先记个todo，以后看有没有办法改掉
             * @param origin
             * @param connection
             * @return
             */
            private boolean needUpdate0(Connection origin, Connection connection) {
                return  !connection.equals(origin) || !isEmbedConnection(connection);
            }

            /**
             * 是否是主工程里内置的Connection
             * @return
             */
            private boolean isEmbedConnection(Connection connection) {
                return connection instanceof JDBCDatabaseConnection || connection instanceof JNDIDatabaseConnection;
            }
        });

        this.validateConnections(addedOrUpdatedConnections);
        alterConnections(removedConnNames, addedOrUpdatedConnections);
    }

    private void alterConnections(List<String> removedConnNames, Map<String, Connection> addedOrUpdatedConnections) {
        removedConnNames.forEach(n -> ConnectionConfig.getInstance().removeConnection(n));
        addedOrUpdatedConnections.forEach((name, conn) -> ConnectionConfig.getInstance().addConnection(name, conn));
    }

    private void validateConnections(Map<String, Connection> addedOrUpdatedConnections) throws Exception {

        for (Map.Entry<String, Connection> entry : addedOrUpdatedConnections.entrySet()) {
            Connection connection = entry.getValue();
            try {
                DataOperator.getInstance().validateConnectionSettings(connection);
            } catch (DriverClassNotFoundException e) {
                FineLoggerFactory.getLogger().info(e.getMessage());
            } catch (SQLException e) {
                throw new SQLException(Toolkit.i18nText("Fine-Design_Basic_Database_Connection_Invalid_Config", entry.getKey()) + ", " + e.getMessage(), e.getCause());
            }
        }
    }

    @Override
    public void onCopyItem() {
        super.onCopyItem();
        ListModelElement selectedValue = getSelectedValue();
        // identity 需要重置
        if (selectedValue != null && selectedValue.wrapper != null) {
            Object temp = ((NameObject) selectedValue.wrapper).getObject();
            if (temp instanceof JDBCDatabaseConnection) {
                JDBCDatabaseConnection object = (JDBCDatabaseConnection) temp;
                object.setIdentity(UUID.randomUUID().toString());
            }
        }
    }
}
