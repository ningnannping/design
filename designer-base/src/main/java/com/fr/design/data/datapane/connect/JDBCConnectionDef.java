package com.fr.design.data.datapane.connect;

import com.fr.data.impl.JDBCDatabaseConnection;

/**
 *  JDBCDefPane和DBCPAttrPane沟通的桥梁
 *
 */
public class JDBCConnectionDef {

    private static volatile JDBCConnectionDef instance;

    private String databaseName;
    private JDBCDatabaseConnection connection;

    public static JDBCConnectionDef getInstance() {
        if (instance == null) {
            synchronized (JDBCConnectionDef.class) {
                if (instance == null) {
                    instance = new JDBCConnectionDef();
                }
            }
        }
        return instance;
    }

    private JDBCConnectionDef() {

    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public JDBCDatabaseConnection getConnection() {
        return connection;
    }

    public void setConnection(String databaseName, JDBCDatabaseConnection connection) {
        this.databaseName = databaseName;
        this.connection = connection;
    }
}
