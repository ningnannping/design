package com.fr.design.data.datapane.preview;

import com.fr.design.base.clipboard.ClipboardHelper;
import com.fr.design.gui.itable.SortableJTable;
import com.fr.design.gui.itable.TableSorter;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.os.OperatingSystem;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CopyableJTable extends SortableJTable {

    //区域选中用到的定位数据
    public int startRow = -1;
    public int startCol = -1;
    public int endRow = -1;
    public int endCol = -1;
    //单元格不连续多选用到的定位数据
    java.util.List<Point> pointList = new ArrayList<>();
    //shift键是否被按下
    public boolean isShiftDown = false;
    //control\command键是否被按下
    public boolean isControlDown = false;
    //是否可以复制
    public boolean isCopy = true;
    int ctrlKeyCode = 17;
    int cKeyCode = 67;
    int shiftKeyCode = 16;
    int commandKeyCode = 157;
    //选中单元格的背景色
    Color selectBackGround = new Color(54, 133, 242, 63);
    Color headerBackGround = new Color(229, 229, 229);
    boolean mouseDrag = false;
    boolean headerSelect = false;


    class CopyableTableHeaderCellRenderer implements TableCellRenderer {
        TableCellRenderer tableCellRenderer;

        CopyableTableHeaderCellRenderer(TableCellRenderer tableCellRenderer) {
            this.tableCellRenderer = tableCellRenderer;
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JComponent comp = (JComponent) this.tableCellRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
            if (isChoose(row, column)) {
                comp.setBackground(selectBackGround);
            } else {
                comp.setBackground(headerBackGround);
            }
            return comp;
        }
    }


    public CopyableJTable(TableSorter tableModel) {
        super(tableModel);
        initListener();
        this.getTableHeader().setDefaultRenderer(new CopyableTableHeaderCellRenderer(this.getTableHeader().getDefaultRenderer()));
    }

    private void initListener() {
        CopyableJTable self = this;
        this.getTableHeader().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                if (mouseDrag) {
                    headerSelect = true;
                    int column = getColumn(e);
                    self.updateEndPoint(-1, column);
                    self.getTableHeader().repaint();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (mouseDrag) {
                    headerSelect = false;
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                headerSelect = true;
                int column = getColumn(e);
                if (column != -1) {
                    self.clearPoint();
                    self.addPoint(-1, column);
                    self.updateStartPoint(-1, column);
                    self.updateEndPoint(-1, column);
                    self.refreshTable();
                }
                self.requestFocusInWindow();
            }

            private int getColumn(MouseEvent e) {
                JTableHeader h = (JTableHeader) e.getSource();
                TableColumnModel columnModel = h.getColumnModel();
                int viewColumn = columnModel.getColumnIndexAtX(e.getX());
                return viewColumn;
            }
        });
        this.getTableHeader().addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                mouseDrag = false;
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                self.clearPoint();
                self.updateStartPoint(-1, -1);
                self.updateEndPoint(-1, -1);
                self.refreshTable();
            }

        });


        this.addMouseMotionListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseDragged(MouseEvent evt) {
                mouseDrag = true;
                int row = self.rowAtPoint(evt.getPoint());
                int col = self.columnAtPoint(evt.getPoint());
                if (self.updateEndPoint(row, col)) {
                    self.refreshTable();
                }
            }

            public void mouseMoved(MouseEvent e) {
                mouseDrag = false;
            }
        });
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                headerSelect = false;
                int row = self.rowAtPoint(e.getPoint());
                int col = self.columnAtPoint(e.getPoint());
                if (!self.isControlDown) {
                    self.clearPoint();
                }
                if (self.isShiftDown) {
                    self.clearPoint();
                } else {
                    self.updateStartPoint(row, col);
                }
                self.addPoint(row, col);
                self.updateEndPoint(row, col);

                self.refreshTable();
            }

        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (isControlKey(e)) {
                    isControlDown = true;
                } else if (e.getKeyCode() == shiftKeyCode) {
                    isShiftDown = true;
                } else if (e.getKeyCode() == cKeyCode) {
                    if (isControlDown && isCopy) {
                        self.copy();
                        isCopy = false;
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                if (isControlKey(e)) {
                    isControlDown = false;
                    isCopy = true;
                } else if (e.getKeyCode() == shiftKeyCode) {
                    isShiftDown = false;
                }
            }

            private boolean isControlKey(KeyEvent e) {
                if (e.getKeyCode() == ctrlKeyCode) {
                    return true;
                }
                if (e.getKeyCode() == commandKeyCode && OperatingSystem.isMacos()) {
                    return true;
                }
                return false;
            }
        });
    }


    @Override
    public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
        Component comp = super.prepareRenderer(renderer, row, column);
        if (isChoose(row, column)) {
            comp.setBackground(selectBackGround);
        } else {
            comp.setBackground(this.getBackground());
        }
        return comp;
    }


    private boolean updateEndPoint(int row, int col) {
        if (headerSelect && row != -1)
            return false;
        if (endRow != row || endCol != col) {
            endRow = row;
            endCol = col;
            return true;
        }
        return false;
    }

    private boolean updateStartPoint(int row, int col) {
        if (startRow != row || startCol != col) {
            startRow = row;
            startCol = col;
            return true;
        }
        return false;
    }

    private void addPoint(int row, int col) {
        pointList.add(new Point(row, col));
    }

    private void clearPoint() {
        pointList = new ArrayList<>();
    }

    private void copy() {
        FineLoggerFactory.getLogger().info("copy cell value");
        java.util.List<java.util.List<Object>> table = new ArrayList<>();
        if ((startRow != endRow || startCol != endCol) && Math.min(startCol, endCol) > -1) {
            copyAreaData(table);
        } else if (pointList.size() > 0) {
            copyPointsData(table);
        }

        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        Transferable tText = new StringSelection(ClipboardHelper.formatExcelString(table));
        clip.setContents(tText, null);
    }

    private void copyAreaData(java.util.List<java.util.List<Object>> table) {
        for (int i = Math.min(startRow, endRow); i <= Math.max(startRow, endRow); i++) {
            table.add(new ArrayList<>());
            for (int j = Math.min(startCol, endCol); j <= Math.max(startCol, endCol); j++) {
                Object text = this.getTableValue(i, j);
                if (text != null) {
                    table.get(table.size() - 1).add(text);
                }
            }
        }
    }

    private void copyPointsData(java.util.List<java.util.List<Object>> table) {
        Collections.sort(pointList, Comparator.comparing(Point::getX).thenComparing(Point::getY));
        int startRow = pointList.get(0).x;
        int currentRow = startRow;
        table.add(new ArrayList<>());
        for (Point point : pointList) {
            while (currentRow < point.x) {
                table.add(new ArrayList<>());
                currentRow++;
            }
            Object text = this.getTableValue(point.x, point.y);
            if (text != null) {
                table.get(table.size() - 1).add(text);
            }
        }
    }

    private Object getTableValue(int row, int col) {
        Object value = null;
        if (col > -1) {
            if (row > -1) {
                value = this.getValueAt(row, col);
            } else if (row == -1) {
                col = columnModel.getColumn(col).getModelIndex();
                value = this.getModel().getColumnName(col);
            }
        }
        return value;
    }

    private void refreshTable() {
        this.repaint();
        this.getTableHeader().repaint();
    }

    private boolean isChoose(int row, int col) {
        if (row >= Math.min(startRow, endRow) && row <= Math.max(startRow, endRow)) {
            if (col >= Math.min(startCol, endCol) && col <= Math.max(startCol, endCol)) {
                return true;
            }
        }
        for (Point point : pointList) {
            if (point.x == row && point.y == col) {
                return true;
            }
        }
        return false;
    }

}
