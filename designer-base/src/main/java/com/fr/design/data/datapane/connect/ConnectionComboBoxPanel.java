package com.fr.design.data.datapane.connect;

import com.fr.base.svg.IconUtils;
import com.fr.data.impl.AbstractDatabaseConnection;
import com.fr.data.impl.Connection;
import com.fr.data.impl.NameDatabaseConnection;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.server.ConnectionListAction;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.editlock.ConnectionLockChangeChecker;
import com.fr.design.editlock.EditLockUtils;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UILockButton;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.file.ConnectionConfig;
import com.fr.report.LockItem;
import com.fr.stable.StringUtils;
import com.fr.transaction.CallBackAdaptor;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.connection.DBConnectAuth;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * 选择数据连接的下拉框
 *
 * @editor zhou
 * @since 2012-3-28下午3:02:30
 */
public class ConnectionComboBoxPanel extends ItemEditableComboBoxPanel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Class<? extends Connection> cls; // 所取的Connection都是cls及其子类
    private List<String> nameList = new ArrayList<String>();

    public ConnectionComboBoxPanel(Class<? extends Connection> cls) {
        super();

        this.cls = cls;

        // alex:添加item change监听,当改变时改变DesignerEnvManager中的最近选中的数据连接
        this.itemComboBox.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                String selected = ConnectionComboBoxPanel.this.getSelectedItem();
                if (StringUtils.isNotBlank(selected)) {
                    DesignerEnvManager.getEnvManager().setRecentSelectedConnection(selected);
                }
            }
        });
        refreshItems();
    }

    @Override
    protected UIButton initEditButton(UIButton editButton, Dimension buttonSize) {
        editButton = new UILockButton(
                EditLockUtils.CONNECTION_LOCKED_ICON,
                IconUtils.readIcon("/com/fr/design/images/m_web/connection"),
                EditLockUtils.CONNECTION_LOCKED_TOOLTIPS,
                null
        );
        editButton.setPreferredSize(buttonSize);
        editButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                editItems();
            }
        });
        ConnectionLockChangeChecker.getInstance().addEditLockChangeListener((UILockButton) editButton);
        return editButton;
    }

    /*
     * 刷新ComboBox.items
     */
    protected Iterator<String> items() {
        ConnectionConfig mgr = ConnectionConfig.getInstance();
        Iterator<String> nameIt = mgr.getConnections().keySet().iterator();

        Collection<String> noAuthConnections = WorkContext.getCurrent().get(DBConnectAuth.class).getNoAuthConnections();

        nameList = new ArrayList<>();

        if (noAuthConnections == null) {
            return nameList.iterator();
        }
        while (nameIt.hasNext()) {
            String conName = nameIt.next();
            if (noAuthConnections.contains(conName)) {
                continue;
            }
            Connection connection = mgr.getConnection(conName);
            // nameList依赖items方法初始化，父类ItemEditableComboBoxPanel里异步执行item方法
            filterConnection(connection, conName, nameList);
        }

        return nameList.iterator();
    }

    protected void filterConnection(Connection connection, String conName, List<String> nameList) {
        connection.addConnection(nameList, conName, new Class[]{AbstractDatabaseConnection.class});
    }

    public int getConnectionSize() {
        return nameList.size();
    }

    public String getConnection(int i) {
        return nameList.get(i);
    }

    /*
     * 弹出对话框编辑Items
     */
    protected void editItems() {
        // 尝试为数据连接加锁
        boolean actionLock = EditLockUtils.lock(LockItem.CONNECTION);
        if (!actionLock) {
            // 锁定失败，代表已经被其他用户锁定，跳出弹窗提示
            EditLockUtils.showLockMessage(this);
            return;
        }
        // 锁定成功，执行后续操作
        final ConnectionListPane connectionListPane = new ConnectionListPane();
        final ConnectionConfig connectionConfig = ConnectionConfig.getInstance();
        ConnectionConfig cloned = connectionConfig.mirror();
        connectionListPane.populate(cloned);
        final BasicDialog connectionListDialog = connectionListPane.showLargeWindow(
                SwingUtilities.getWindowAncestor(ConnectionComboBoxPanel.this), null);
        connectionListDialog.addDialogActionListener(new DialogActionAdapter() {
            public void doOk() {
                if (!connectionListPane.isNamePermitted()) {
                    connectionListDialog.setDoOKSucceed(false);
                    return;
                }
                AtomicBoolean saved = new AtomicBoolean(true);
                Configurations.modify(new WorkerFacade(ConnectionConfig.class) {
                    @Override
                    public void run() {
                        try {
                            connectionListPane.update(connectionConfig);
                        } catch (Exception e) {
                            saved.set(false);
                            FineJOptionPane.showMessageDialog(connectionListPane, e.getMessage(), Toolkit.i18nText("Fine-Design_Basic_Error"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.addCallBack(new CallBackAdaptor() {
                    @Override
                    public boolean beforeCommit() {
                        //如果更新失败，则不关闭对话框，也不写xml文件，并且将对话框定位在请重命名的那个对象页面
                        return ConnectionListAction.doWithDatasourceManager(connectionConfig, connectionListPane, connectionListDialog);
                    }

                    @Override
                    public void afterCommit() {
                        if (saved.get()) {
                            DesignerContext.getDesignerBean("databasename").refreshBeanElement();
                            // 定义数据连接弹窗关闭后，解锁
                            EditLockUtils.unlock(LockItem.CONNECTION);
                        } else {
                            connectionListDialog.setDoOKSucceed(false);
                        }
                    }
                }));

            }

            @Override
            public void doCancel() {
                // 关闭定义数据连接页面，为其解锁
                super.doCancel();
                EditLockUtils.unlock(LockItem.CONNECTION);
            }
        });
        connectionListDialog.setVisible(true);
        refreshItems();
    }

    /**
     * @param connection 数据库链接
     */
    public void populate(Connection connection) {
        editButton.setEnabled(WorkContext.getCurrent().isRoot());
        if (connection instanceof NameDatabaseConnection) {
            this.setSelectedItem(((NameDatabaseConnection) connection).getName());
        } else {
            setRecentConnection();
        }
    }

    /**
     * 下拉框选项设置成最近选择的connection，如果最近选择不存在，则选择列表中的第一个
     */
    protected void setRecentConnection() {
        String s = DesignerEnvManager.getEnvManager().getRecentSelectedConnection();
        if (StringUtils.isNotBlank(s)) {
            // 之前的写法有多线程问题，nameList异步尚未初始化完成的时候，这里可能无法匹配设置数据连接名称，导致DBTableDataPane打开后连接面板空白
            // 这里的需求无非是设置上一次使用的数据连接，做个简单检查这个连接是否存在即可，存在就设置
            if (nameList.contains(s)) {
                this.setSelectedItem(s);
            }
        }
        // alex:如果这个ComboBox还是没有选中,那么选中第一个
        if (StringUtils.isBlank(this.getSelectedItem()) && this.getConnectionSize() > 0) {
            this.setSelectedItem(this.getConnection(0));
        }
    }

    /**
     * 是否无选中状态（空白item也视为无选中）
     * @return
     */
    protected boolean isSelectedItemEmpty() {
        String selectedItem = this.getSelectedItem();
        return selectedItem == null || StringUtils.equals(selectedItem, EMPTY.toString());
    }
}
