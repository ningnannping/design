package com.fr.design.data.tabledata.tabledatapane.loading;



/**
 * 可切换的DBTableData对应的数据集面板，需要使用CardLayout布局
 * 主要是给插件适配用的
 * @author Yvan
 */
public interface SwitchableTableDataPane {

    /** Loading面板 */
    String LOADING_PANE_NAME = "Loading";
    /** 内容面板 */
    String CONTENT_PANE_NAME = "Content";

    /**
     * 根据面板名称切换面板
     * @param paneName 面板名称
     */
    void switchTo(String paneName);

}
