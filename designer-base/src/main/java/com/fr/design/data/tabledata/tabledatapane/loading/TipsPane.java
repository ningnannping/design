package com.fr.design.data.tabledata.tabledatapane.loading;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;

import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;

/**
 * 提示面板，支持自定义提示，支持进度条配置可选
 * @author Yvan
 */
public class TipsPane extends JPanel {

    /**
     * 默认提示
     */
    private static final String LOADING = Toolkit.i18nText("Fine-Design_Basic_Loading_And_Waiting");

    public TipsPane () {
        this(LOADING, false);
    }

    public TipsPane (String tip) {
        this(tip, false);
    }

    public TipsPane (boolean needProgressBar) {
        this(LOADING, needProgressBar);
    }

    public TipsPane (String tips, boolean needProgressBar) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        UILabel tipsLabel = new UILabel(tips, SwingConstants.CENTER);
        this.add(tipsLabel, BorderLayout.CENTER);
        if (needProgressBar) {
            JProgressBar progressBar = new JProgressBar();
            progressBar.setIndeterminate(true);
            this.add(progressBar, BorderLayout.SOUTH);
        }
    }
}
