package com.fr.design.locale.impl;

import com.fr.general.CloudCenter;
import com.fr.general.GeneralContext;
import com.fr.general.locale.LocaleMark;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/7/29
 */
public class TechSupportMark implements LocaleMark<String> {
    private Map<Locale, String> map = new HashMap<>();
    private static final String EN_TECH_SUPPORT = CloudCenter.getInstance().acquireUrlByKind("bbs.tech.support.en_US", "https://fanruanhelp.zendesk.com/hc/en-us");
    private static final String TW_TECH_SUPPORT = CloudCenter.getInstance().acquireUrlByKind("bbs.tech.support.zh_TW", "https://fanruanhelp.zendesk.com/hc/zh-tw");

    public TechSupportMark() {
        map.put(Locale.US, EN_TECH_SUPPORT);
        map.put(Locale.TAIWAN, TW_TECH_SUPPORT);
    }

    @Override
    public String getValue() {
        String result = map.get(GeneralContext.getLocale());
        return result == null ? EN_TECH_SUPPORT : result;
    }
}
