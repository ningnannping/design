package com.fr.design.locale.impl;

import com.fr.general.locale.SupportLocale;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * 某些国际化环境支持的操作
 * 需要增加/删除支持的语言 统一在这里修改 无须改动业务代码
 * 后续有新的不同语言下的差异操作 添加新的枚举
 *
 * @author Hades
 * @date 2019/6/24
 */
public enum SupportLocaleImpl implements SupportLocale {

    /**
     * 社区菜单支持的国际化环境
     */
    COMMUNITY {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.CHINA);
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * Facebook支持的国际化环境
     */
    FACEBOOK {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * 支持韩文
     */
    SUPPORT_KOREA {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.KOREA);
            return set;
        }
    },

    /**
     * BUG反馈
     */
    BUG {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.CHINA);
            set.add(Locale.US);
            set.add(Locale.JAPAN);
            set.add(Locale.KOREA);
            return set;
        }
    },

    /**
     * 需求反馈
     */
    NEED {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.CHINA);
            set.add(Locale.US);
            set.add(Locale.JAPAN);
            set.add(Locale.KOREA);
            return set;
        }
    },

    /**
     * BUG需求反馈
     */
    BUG_AND_NEED {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * 视频
     */
    VIDEO {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.US);
            return set;
        }
    },

    /**
     * 技术支持-帮助菜单下
     */
    TECH_SUPPORT_HELP {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.US);
            return set;
        }
    },

    /**
     * 技术支持-社区菜单下
     */
    TECH_SUPPORT_COMMUNITY {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * 帮助文档-帮助菜单下
     */
    TUTORIAL_HELP {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.US);
            set.add(Locale.KOREA);
            set.add(Locale.JAPAN);
            return set;
        }
    },

    /**
     * 帮助文档-社区菜单下
     */
    TUTORIAL_COMMUNITY {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<Locale>();
            set.add(Locale.CHINA);
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * 更新升级-帮助菜单下
     */
    UPDATE_HELP {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<>();
            set.add(Locale.US);
            set.add(Locale.KOREA);
            set.add(Locale.CHINA);
            set.add(Locale.TAIWAN);
            return set;
        }
    },

    /**
     * 帮助-服务平台（只针对中国大陆）
     */
    SERVICE_PLATFORM {
        @Override
        public Set<Locale> support() {
            Set<Locale> set = new HashSet<>();
            set.add(Locale.CHINA);
            return set;
        }
    }
}
