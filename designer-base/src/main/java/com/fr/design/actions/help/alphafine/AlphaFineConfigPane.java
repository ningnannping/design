package com.fr.design.actions.help.alphafine;

import com.fr.base.FRContext;
import com.fr.base.svg.IconUtils;
import com.fr.design.DesignerEnvManager;
import com.fr.design.actions.help.alphafine.component.CustomSortPane;
import com.fr.design.constants.UIConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.ActionLabel;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.imenu.UIPopupMenu;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.KeyStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by XiaXiang on 2017/4/6.
 */
public class AlphaFineConfigPane extends BasicPane {
    private static final String TYPE = "pressed";
    private static final String DISPLAY_TYPE = "+";
    private static final Color LABEL_TEXT = new Color(0x919193);

    private static final int SEARCH_CONFIG_PANE_HEIGHT = 70;
    private static final int SEARCH_CONFIG_PANE_WIDTH = 87;
    private static final double COLUMN_WIDTH = 150;
    private static final double ROW_HEIGHT = 25;
    private KeyStroke shortCutKeyStore = null;
    private UICheckBox enabledCheckbox, searchOnlineCheckbox, needSegmentationCheckbox;
    private UICheckBox productDynamicsCheckbox, containTemplateShopCheckbox, containDocumentCheckbox,
            containPluginCheckbox, containActionCheckbox, containMyTemplateCheckbox;
    // 自定义排序按钮
    private ActionLabel customSortLabel;
    private UITextField shortcutsField;



    // 当前tab排序。点击确定后会保存到配置文件中
    private String[] currentOrder;


    // 搜索范围-我的模板，相关组件
    private JPanel containMyTemplatePane;
    private JButton myTemplateSearchConfigButton;
    private UIPopupMenu myTemplateSearchMenu;
    private UICheckBox containTemplateNameSearchCheckbox, containFileContentSearchCheckbox;

    public AlphaFineConfigPane() {
        this.initComponents();
    }

    private void initComponents() {
        JPanel contentPane = FRGUIPaneFactory.createY_AXISBoxInnerContainer_L_Pane();
        createOpenPane(contentPane);
        createOnlinePane(contentPane);
        createShortcutsPane(contentPane);
        createSearchConfigPane(contentPane);
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.add(contentPane, BorderLayout.NORTH);
    }

    private Component[][] initOnlineComponents() {
        Component[][] components = new Component[][]{
                new Component[]{searchOnlineCheckbox, needSegmentationCheckbox, null}
        };
        return components;
    }

    /**
     * 搜索范围面板
     */
    private void createSearchConfigPane(JPanel contentPane) {
        double[] rowSize = {ROW_HEIGHT, ROW_HEIGHT, ROW_HEIGHT};
        double[] columnSize = {COLUMN_WIDTH, COLUMN_WIDTH, COLUMN_WIDTH};

        JPanel searchConfigWrapperPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Search_Range"));
        // 搜索选项
        productDynamicsCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Product_News"));
        containActionCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Function"));
        containPluginCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Plugin_Addon"));
        containDocumentCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Community_Help"));
        containMyTemplateCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_My_Templates"));
        containFileContentSearchCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Templates_Content"));
        containTemplateShopCheckbox = new UICheckBox(Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Shop"));
        containMyTemplateCheckbox = new UICheckBox(Toolkit.i18nText("Fine-Design_Report_My_Templates"));
        JPanel searchConfigPane = TableLayoutHelper.createTableLayoutPane(initSearchRangeComponents(), rowSize, columnSize);

        // 自定义排序
        JPanel customSortWrapperPane = new JPanel();
        customSortWrapperPane.setPreferredSize(new Dimension(SEARCH_CONFIG_PANE_HEIGHT, SEARCH_CONFIG_PANE_WIDTH));
        customSortWrapperPane.setAlignmentY(JPanel.TOP_ALIGNMENT);
        customSortLabel = new ActionLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Config_Custom_Sort"), false);
        customSortLabel.setForeground(UIConstants.NORMAL_BLUE);
        customSortLabel.addActionListener((event)->{
            if (customSortLabel.isEnabled()) {
                openCustomSortMenu();
            }
        });
        customSortWrapperPane.add(customSortLabel);
        if (!hasSelectedSearchRangeCheckBox()) {
            customSortLabel.setEnabled(false);
        }

        searchConfigWrapperPane.add(searchConfigPane);
        searchConfigWrapperPane.add(customSortWrapperPane);
        contentPane.add(searchConfigWrapperPane);
    }

    /**
     * 打开自定义排序面板
     * */
    private void openCustomSortMenu() {
        CustomSortPane customSortPane = new CustomSortPane(getSelectedSearchRangeCheckBox(), this);
        JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(customSortPane);
        GUICoreUtils.showPopupMenu(popupMenu, customSortLabel, 0, customSortLabel.getHeight());
    }


    private Component[][] initSearchRangeComponents() {
        // 我的模板checkbox设置，点击后
        initMyTemplateSearchPane();

        Component[][] components = new Component[][]{
                new Component[]{productDynamicsCheckbox, containTemplateShopCheckbox, containDocumentCheckbox},
                new Component[]{containPluginCheckbox, containActionCheckbox, containMyTemplatePane},
        };

        // 添加选项点事件，无选中选项时自定排序按钮置灰
        UICheckBox[] checkBoxes = new UICheckBox[]{productDynamicsCheckbox, containTemplateShopCheckbox, containDocumentCheckbox, containPluginCheckbox, containActionCheckbox, containMyTemplateCheckbox};
        for (UICheckBox box : checkBoxes) {
            box.addActionListener((e)->{
                customSortLabel.setEnabled(hasSelectedSearchRangeCheckBox());
            });
        }
        return components;
    }

    /**
     * 【搜索范围】中的复选框有无选中的
     * */
    private boolean hasSelectedSearchRangeCheckBox() {
        return productDynamicsCheckbox.isSelected() || containTemplateShopCheckbox.isSelected() || containDocumentCheckbox.isSelected()
                || containPluginCheckbox.isSelected() || containActionCheckbox.isSelected() || containMyTemplateCheckbox.isSelected();
    }

    /**
     * 获取当前选中的搜索范围选项
     * */
    private List<UICheckBox> getSelectedSearchRangeCheckBox() {
        List<UICheckBox> res = new ArrayList<>();
        UICheckBox[] checkBoxes = new UICheckBox[]{productDynamicsCheckbox, containTemplateShopCheckbox, containDocumentCheckbox, containPluginCheckbox, containActionCheckbox, containMyTemplateCheckbox};
        for (UICheckBox c : checkBoxes) {
            if (c.isSelected()) {
                res.add(c);
            }
        }
        return res;
    }

    /**
     * 搜索范围-我的模板
     */
    private void initMyTemplateSearchPane() {
        containMyTemplatePane = new JPanel(new FlowLayout(FlowLayout.LEFT, 4, 5));
        containMyTemplateCheckbox.setBorder(BorderFactory.createEmptyBorder());
        containMyTemplateCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (containMyTemplateCheckbox.isSelected()) {
                    myTemplateSearchConfigButton.setVisible(true);
                } else {
                    myTemplateSearchConfigButton.setVisible(false);
                }
            }
        });
        myTemplateSearchConfigButton = new JButton();
        myTemplateSearchConfigButton.setBorder(BorderFactory.createEmptyBorder());
        myTemplateSearchConfigButton.setMargin(new Insets(0, 0, 0, 0));
        myTemplateSearchConfigButton.setIcon(IconUtils.readIcon("/com/fr/design/mainframe/alphafine/images/config.svg"));
        myTemplateSearchMenu = new UIPopupMenu();
        containTemplateNameSearchCheckbox = new UICheckBox(Toolkit.i18nText("Fine-Design_AlphaFine_Config_Name_Search"));
        containFileContentSearchCheckbox = new UICheckBox(Toolkit.i18nText("Fine-Design_AlphaFine_Config_Content_Search"));
        containTemplateNameSearchCheckbox.setSelected(true);
        containTemplateNameSearchCheckbox.setEnabled(false);
        containTemplateNameSearchCheckbox.setBackground(null);
        containFileContentSearchCheckbox.setBackground(null);
        myTemplateSearchMenu.add(containTemplateNameSearchCheckbox);
        myTemplateSearchMenu.add(containFileContentSearchCheckbox);
        myTemplateSearchConfigButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                GUICoreUtils.showPopupMenu(myTemplateSearchMenu, containMyTemplatePane, containMyTemplateCheckbox.getWidth(), containMyTemplatePane.getY());
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                super.mouseMoved(e);
                myTemplateSearchMenu.setVisible(false);
            }
        });
        containMyTemplatePane.add("containMyTemplateCheckbox", containMyTemplateCheckbox);
        containMyTemplatePane.add("myTemplateSearchConfigButton", myTemplateSearchConfigButton);
    }


    private void createShortcutsPane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Shortcut_Config"));
        shortcutsField = new UITextField();
        shortcutsField.setEditable(false);
        shortcutsField.selectAll();
        shortcutsField.setPreferredSize(new Dimension(100, 20));
        initFieldListener();
        northPane.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Open") + ":"));
        northPane.add(shortcutsField);
        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_SetShortcuts"));
        label.setForeground(LABEL_TEXT);
        northPane.add(label);
        contentPane.add(northPane);
    }

    private void initFieldListener() {
        shortcutsField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                shortcutsField.selectAll();
            }
        });
        shortcutsField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int modifier = e.getModifiers();
                if (modifier == 0) {
                    return;
                }
                int keyCode = e.getKeyCode();
                shortCutKeyStore = KeyStroke.getKeyStroke(keyCode, modifier);
                String str = shortCutKeyStore.toString();
                shortcutsField.setText(AlphaFineShortCutUtil.getDisplayShortCut(str));
                shortcutsField.selectAll();
            }
        });
    }

    private void createOnlinePane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Search_Type"));
        searchOnlineCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_Internet_Search"));
        needSegmentationCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_Segmentation"));
        searchOnlineCheckbox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!searchOnlineCheckbox.isSelected()) {
                    productDynamicsCheckbox.setEnabled(false);
                    containPluginCheckbox.setEnabled(false);
                    containDocumentCheckbox.setEnabled(false);
                    containTemplateShopCheckbox.setEnabled(false);
                    productDynamicsCheckbox.setSelected(false);
                    containPluginCheckbox.setSelected(false);
                    containDocumentCheckbox.setSelected(false);
                    containTemplateShopCheckbox.setSelected(false);
                } else {
                    productDynamicsCheckbox.setEnabled(true);
                    containPluginCheckbox.setEnabled(true);
                    containDocumentCheckbox.setEnabled(true);
                    containTemplateShopCheckbox.setEnabled(true);
                }
            }
        });
        double[] rowSize = {ROW_HEIGHT};
        double[] columnSize = {COLUMN_WIDTH, COLUMN_WIDTH, COLUMN_WIDTH};
        JPanel onlinePane = TableLayoutHelper.createTableLayoutPane(initOnlineComponents(), rowSize, columnSize);
        northPane.add(onlinePane);
        contentPane.add(northPane);
    }

    private void createOpenPane(JPanel contentPane) {
        JPanel northPane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable"));
        enabledCheckbox = new UICheckBox(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_AlphaFine_Enable_AlphaFine"));
        northPane.add(enabledCheckbox);
        contentPane.add(northPane);
    }

    @Override
    protected String title4PopupWindow() {
        return "AlphaFine";
    }

    public void populate(AlphaFineConfigManager alphaFineConfigManager) {

        this.enabledCheckbox.setSelected(alphaFineConfigManager.isEnabled());

        boolean enabled4Locale = FRContext.isChineseEnv() && alphaFineConfigManager.isSearchOnLine();

        this.searchOnlineCheckbox.setEnabled(FRContext.isChineseEnv());
        this.searchOnlineCheckbox.setSelected(enabled4Locale);

        this.containActionCheckbox.setSelected(alphaFineConfigManager.isContainAction());

        this.containMyTemplateCheckbox.setSelected(alphaFineConfigManager.isContainMyTemplate());
        this.myTemplateSearchConfigButton.setVisible(alphaFineConfigManager.isContainMyTemplate());
        this.containFileContentSearchCheckbox.setSelected(alphaFineConfigManager.isContainFileContent());

        this.containDocumentCheckbox.setSelected(alphaFineConfigManager.isContainDocument() && enabled4Locale);
        this.containDocumentCheckbox.setEnabled(enabled4Locale);

        this.containPluginCheckbox.setSelected(alphaFineConfigManager.isContainPlugin() && enabled4Locale);
        this.containPluginCheckbox.setEnabled(enabled4Locale);

        this.productDynamicsCheckbox.setSelected(alphaFineConfigManager.isProductDynamics() && enabled4Locale);
        this.productDynamicsCheckbox.setEnabled(enabled4Locale);

        this.containTemplateShopCheckbox.setSelected(alphaFineConfigManager.hasTemplateShop() && enabled4Locale);
        this.containTemplateShopCheckbox.setEnabled(enabled4Locale);

        this.shortcutsField.setText(AlphaFineShortCutUtil.getDisplayShortCut(alphaFineConfigManager.getShortcuts()));

        this.needSegmentationCheckbox.setSelected(alphaFineConfigManager.isNeedSegmentationCheckbox());

        shortCutKeyStore = convert2KeyStroke(alphaFineConfigManager.getShortcuts());

        this.currentOrder = alphaFineConfigManager.getTabOrder().clone();

        if (!hasSelectedSearchRangeCheckBox()) {
            customSortLabel.setEnabled(false);
        } else {
            customSortLabel.setEnabled(true);
        }
    }

    public void update() {
        DesignerEnvManager designerEnvManager = DesignerEnvManager.getEnvManager();
        AlphaFineConfigManager alphaFineConfigManager = designerEnvManager.getAlphaFineConfigManager();
        alphaFineConfigManager.setContainPlugin(this.containPluginCheckbox.isSelected());
        alphaFineConfigManager.setContainAction(this.containActionCheckbox.isSelected());
        alphaFineConfigManager.setContainDocument(this.containDocumentCheckbox.isSelected());
        alphaFineConfigManager.setProductDynamics(this.productDynamicsCheckbox.isSelected());
        alphaFineConfigManager.setShowTemplateShop(this.containTemplateShopCheckbox.isSelected());
        alphaFineConfigManager.setEnabled(this.enabledCheckbox.isSelected());
        alphaFineConfigManager.setSearchOnLine(this.searchOnlineCheckbox.isSelected());
        alphaFineConfigManager.setContainMyTemplate(this.containMyTemplateCheckbox.isSelected());
        alphaFineConfigManager.setContainFileContent(this.containFileContentSearchCheckbox.isSelected());
        alphaFineConfigManager.setNeedSegmentationCheckbox(this.needSegmentationCheckbox.isSelected());
        alphaFineConfigManager.setShortcuts(shortCutKeyStore != null ? shortCutKeyStore.toString().replace(TYPE, DISPLAY_TYPE) : this.shortcutsField.getText());
        alphaFineConfigManager.setTabOrder(currentOrder);

        designerEnvManager.setAlphaFineConfigManager(alphaFineConfigManager);
        try {
            DesignerEnvManager.loadLogSetting();
            DesignerEnvManager.getEnvManager().saveXMLFile();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }


    }


    private KeyStroke convert2KeyStroke(String ks) {
        return KeyStroke.getKeyStroke(ks.replace(DISPLAY_TYPE, TYPE));
    }

    public KeyStroke getShortCutKeyStore() {
        return shortCutKeyStore;
    }

    public void setShortCutKeyStore(KeyStroke shortCutKeyStore) {
        this.shortCutKeyStore = shortCutKeyStore;
    }

    public UICheckBox getIsContainFileContentCheckbox() {
        return containFileContentSearchCheckbox;
    }

    public void setIsContainFileContentCheckbox(UICheckBox isContainFileContentCheckbox) {
        this.containFileContentSearchCheckbox = isContainFileContentCheckbox;
    }

    public String[] getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(String[] currentOrder) {
        this.currentOrder = currentOrder;
    }
}
