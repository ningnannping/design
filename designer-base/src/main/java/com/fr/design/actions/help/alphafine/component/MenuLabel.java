package com.fr.design.actions.help.alphafine.component;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.utils.DesignUtils;

import javax.swing.BorderFactory;
import javax.swing.plaf.LabelUI;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.function.Function;

/**
 * 菜单选项label
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class MenuLabel extends UILabel {

    private static final Color BACKGROUND_COLOR = Color.white;
    private static final Color SELECTED_COLOR = new Color(0x419BF9);
    private static final Color HOVERED_COLOR = new Color(0xd9ebfe);
    private static final int HEIGHT = 23;
    private static final int WIDTH = 147;

    private MenuLabelPane parentMenu;
    private final Function function;
    private boolean selected;

    public MenuLabel(String text, Function function) {
        super(text);
        this.function = function;
        setOpaque(true);
        addMouseListener(createMouseListener());
    }

    public void setParentMenu(MenuLabelPane menu) {
        this.parentMenu = menu;
    }


    @Override
    public void setUI(LabelUI ui) {
        super.setUI(ui);
        this.setBackground(BACKGROUND_COLOR);
        this.setBorder(BorderFactory.createEmptyBorder(2, 10, 1, 10));
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setFont(DesignUtils.getDefaultGUIFont().applySize(12));
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        if (selected) {
            parentMenu.setNoneSelected();
            setBackground(SELECTED_COLOR);
            function.apply(this);
            this.selected = true;
        } else {
            setBackground(BACKGROUND_COLOR);
            this.selected = false;
        }
    }

    MouseListener createMouseListener() {
        return new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                setSelected(true);
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                super.mouseEntered(e);
                if (!selected) {
                    setBackground(HOVERED_COLOR);
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                super.mouseExited(e);
                if (!selected) {
                    setBackground(BACKGROUND_COLOR);
                }
            }
        };
    }
}
