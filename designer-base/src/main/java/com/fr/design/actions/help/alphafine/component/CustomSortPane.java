package com.fr.design.actions.help.alphafine.component;

import com.fr.base.svg.IconUtils;
import com.fr.design.actions.help.alphafine.AlphaFineConfigPane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.plaf.PanelUI;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * alphafine设置 - 搜索范围 - 自定义排序 - 弹出面板
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class CustomSortPane extends JPanel {


    private static final int WIDTH = 147;
    private static final int ITEM_HEIGHT = 23;
    private static final int GAP = 1;
    private static final Color BACKGROUND_COLOR = new Color(0xdadadd);

    private UIButton top;
    private UIButton bottom;
    private UIButton up;
    private UIButton down;
    private JPanel toolbarPane;
    private MenuLabelPane sortItemPane;
    private List<UICheckBox> sortItems;
    private MenuLabel selectedLabel;
    private AlphaFineConfigPane parentPane;

    public CustomSortPane(List<UICheckBox> items, AlphaFineConfigPane parentPane) {
        this.sortItems = items;
        this.parentPane = parentPane;
        setLayout(new BorderLayout(GAP, GAP));
        int height = (sortItems.size() + 1) * (ITEM_HEIGHT + GAP) + GAP;
        setPreferredSize(new Dimension(WIDTH, height));
        initComponent();
        add(toolbarPane, BorderLayout.NORTH);
        add(sortItemPane, BorderLayout.CENTER);
        revalidate();
        this.setVisible(true);
    }

    @Override
    public void setUI(PanelUI ui) {
        super.setUI(ui);
        setBackground(BACKGROUND_COLOR);

    }

    private void initComponent() {
        createToolbarPane();
        createSortItemPane();
    }

    private void createToolbarPane() {
        top = new UIButton(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/top.svg"), false);
        bottom = new UIButton(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/bottom.svg"), false);
        up = new UIButton(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/up.svg"), false);
        down = new UIButton(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/down.svg"), false);
        top.setDisabledIcon(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/top_disable.svg"));
        bottom.setDisabledIcon(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/bottom_disable.svg"));
        up.setDisabledIcon(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/up_disable.svg"));
        down.setDisabledIcon(IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/down_disable.svg"));
        top.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                sortItemPane.setComponentZOrder(selectedLabel, 0);
                setToolbarEnable(sortItemPane.getComponentZOrder(selectedLabel), sortItemPane.getComponentCount());
                CustomSortPane.this.revalidate();
                CustomSortPane.this.repaint();
                refreshCurrentOrder();
            });

        });
        bottom.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                sortItemPane.setComponentZOrder(selectedLabel, sortItemPane.getComponentCount() - 1);
                setToolbarEnable(sortItemPane.getComponentZOrder(selectedLabel), sortItemPane.getComponentCount());
                CustomSortPane.this.revalidate();
                CustomSortPane.this.repaint();
                refreshCurrentOrder();
            });

        });
        up.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                sortItemPane.setComponentZOrder(selectedLabel, sortItemPane.getComponentZOrder(selectedLabel) - 1);
                setToolbarEnable(sortItemPane.getComponentZOrder(selectedLabel), sortItemPane.getComponentCount());
                CustomSortPane.this.revalidate();
                CustomSortPane.this.repaint();
                refreshCurrentOrder();
            });

        });
        down.addActionListener(e -> {
            SwingUtilities.invokeLater(() -> {
                sortItemPane.setComponentZOrder(selectedLabel, sortItemPane.getComponentZOrder(selectedLabel) + 1);
                setToolbarEnable(sortItemPane.getComponentZOrder(selectedLabel), sortItemPane.getComponentCount());
                CustomSortPane.this.revalidate();
                CustomSortPane.this.repaint();
                refreshCurrentOrder();
            });

        });
        toolbarPane = new JPanel(new FlowLayout(FlowLayout.TRAILING, GAP, GAP));
        toolbarPane.setBorder(BorderFactory.createEmptyBorder());
        toolbarPane.add(top);
        toolbarPane.add(bottom);
        toolbarPane.add(up);
        toolbarPane.add(down);
    }

    private void createSortItemPane() {
        String[] currentTabOrder = parentPane.getCurrentOrder();
        Map<String, Integer> sortMap = new HashMap<>();
        for (int i = 0; i < currentTabOrder.length; i++) {
            sortMap.put(currentTabOrder[i], i);
        }
        List<MenuLabel> sortLabels = new ArrayList<>();
        for (UICheckBox item : sortItems) {
            MenuLabel label = new MenuLabel(item.getText(), (Function<MenuLabel, Object>) o -> {
                selectedLabel = o;
                disableButton();
                return null;
            });
            sortLabels.add(label);
        }
        sortLabels.sort(Comparator.comparingInt(tab -> sortMap.get(tab.getText())));
        sortItemPane = new MenuLabelPane(sortLabels);
    }

    /**
     * 如果选中第一个和最后一个，则置灰向上和向下的按钮
     */
    private void disableButton() {
        int order = sortItemPane.getComponentZOrder(selectedLabel);
        if (order == 0) {
            setToolbarEnable(false, false, true, true);
        } else if (order == sortItemPane.getComponentCount() - 1) {
            setToolbarEnable(true, true, false, false);
        } else {
            setToolbarEnable(true, true, true, true);
        }
    }

    /**
     * 设置 置顶，上移，下移，置底 按钮的状态
     * true：启用
     * false：关闭
     */
    private void setToolbarEnable(boolean top, boolean up, boolean down, boolean bottom) {
        this.top.setEnabled(top);
        this.up.setEnabled(up);
        this.down.setEnabled(down);
        this.bottom.setEnabled(bottom);
    }

    /**
     * 根据选项当前位置以及菜单大小设置 置顶，上移，下移，置底 按钮的状态
     */
    private void setToolbarEnable(int order, int maxOrder) {
        this.top.setEnabled(true);
        this.up.setEnabled(true);
        this.down.setEnabled(true);
        this.bottom.setEnabled(true);
        // 选项处于顶端，则置灰上移和置顶按钮
        if (order == 0) {
            this.top.setEnabled(false);
            this.up.setEnabled(false);
        }
        // 选项处于底端，则置灰下移和置底按钮
        if (order == maxOrder - 1) {
            this.down.setEnabled(false);
            this.bottom.setEnabled(false);
        }
    }

    private void refreshCurrentOrder() {
        String[] currentTabOrder = parentPane.getCurrentOrder();
        HashSet<String> selectedTab = new HashSet<>();
        for (UICheckBox item : sortItems) {
            selectedTab.add(item.getText());
        }

        // 未选中的tab，保持原排序不变
        Map<String, Integer> exTab = new HashMap<>();
        for (int i = 0; i < currentTabOrder.length; i++) {
            if (!selectedTab.contains(currentTabOrder[i])) {
                exTab.put(currentTabOrder[i], i);
            }
        }

        // 计算当前排序
        String[] newOrder = new String[currentTabOrder.length];
        Component[] components = sortItemPane.getComponents();
        for (String s : exTab.keySet()) {
            newOrder[exTab.get(s)] = s;
        }

        int t = 0;
        for (int i = 0; i < newOrder.length; i++) {
            if (StringUtils.isEmpty(newOrder[i])) {
                newOrder[i] = ((MenuLabel) components[t++]).getText();
            }
        }
        parentPane.setCurrentOrder(newOrder);
    }

}
