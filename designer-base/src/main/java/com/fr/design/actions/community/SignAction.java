package com.fr.design.actions.community;

import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;

import com.fr.general.CloudCenter;

import javax.swing.*;

public class SignAction extends AbstractDesignerSSO {

    public SignAction() {
        this.setMenuKeySet(SIGN);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/sign");
    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.aut", "https://bbs.fanruan.com/certification/");
    }

    public static final MenuKeySet SIGN = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'S';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_sign");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
