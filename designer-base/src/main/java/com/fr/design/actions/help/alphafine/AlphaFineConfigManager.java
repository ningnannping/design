package com.fr.design.actions.help.alphafine;

import com.fr.base.FRContext;
import com.fr.design.DesignerEnvManager;
import com.fr.general.ComparatorUtils;
import com.fr.license.function.VT4FR;
import com.fr.stable.OperatingSystem;
import com.fr.stable.StringUtils;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLReadable;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;
import org.jetbrains.annotations.NotNull;

import javax.swing.KeyStroke;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

/**
 * AlphaFine配置类
 *
 * @author XiaXiang
 * @date 2017/4/5
 */
public class AlphaFineConfigManager implements XMLable {

    public static final String COMMA = ",";
    private static final long serialVersionUID = -8170289826729582122L;
    private static AlphaFineConfigManager alphaFineConfigManager = new AlphaFineConfigManager();
    /**
     * 是否开启alphafine
     */
    private boolean enabled = true;
    /**
     * 是否联网搜索
     */
    private boolean searchOnLine = true;

    /**
     * 搜索范围
     */
    /**
     * 快捷键设置
     */
    private String shortcuts;
    /**
     * 猜您需要
     */
    private boolean containRecommend = true;
    /**
     * 功能
     */
    private boolean containAction = true;
    /**
     * 帮助文档
     */
    private boolean containDocument = true;
    /**
     * 我的模板
     * */
    private boolean containMyTemplate = true;
    /**
     * 模板
     */
    private boolean containTemplate = true;
    /**
     * 模板内容
     */
    private boolean containFileContent;
    /**
     * 插件中心
     */
    private boolean containPlugin = true;
    /**
     * 分词搜索
     */
    private boolean needSegmentationCheckbox = true;
    /**
     * 智能客服
     */
    private boolean needIntelligentCustomerService = true;
    /**
     * 快捷键
     */
    private KeyStroke shortCutKeyStore;
    /**
     * 是否提醒
     */
    private boolean needRemind = true;
    /**
     * 产品动态
     */
    private boolean productDynamics = true;
    /**
     * 模板商城是否展示
     */
    private boolean showTemplateShop = true;
    /**
     * tab页排序
     * 默认排序：动态，模板商城，帮助文档，插件中心，功能，我的模板
     */
    private String[] tabOrder;
    private Map<String, String> actionSearchTextCache = new HashMap<>(8);
    private String cacheBuildNO;

    private static final String CONTAIN_TEMPLATE_SHOP = "isContainTemplateShop";
    private static final String TAB_ORDER = "tabOrder";

    /**
     * key: 登录的bbs用户
     * value: alphaFine历史搜索记录
     */
    private Map<String, Stack<String>> historySearchMap = new LinkedHashMap<>();

    /**
     * key: 登录的bbs用户
     * value: 已读的cid的集合
     */
    private Map<String, Set<Long>> readSetMap = new LinkedHashMap<>();

    /**
     * 直接操作菜单次数
     */
    private int operateCount;

    private AlphaFineConfigManager() {
    }

    public static AlphaFineConfigManager getInstance() {
        return alphaFineConfigManager;
    }

    public static boolean isALPHALicAvailable() {

        return VT4FR.AlphaFine.isSupport();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public void readXML(XMLableReader reader) {
        if (reader.isAttr()) {
            this.setEnabled(reader.getAttrAsBoolean("isEnabled", true));
            this.setSearchOnLine(reader.getAttrAsBoolean("isSearchOnline", true));
            this.setContainPlugin(reader.getAttrAsBoolean("isContainDocument", true));
            this.setContainDocument(reader.getAttrAsBoolean("isContainDocument", true));
            this.setContainRecommend(reader.getAttrAsBoolean("isContainRecommend", true));
            this.setShowTemplateShop(reader.getAttrAsBoolean(CONTAIN_TEMPLATE_SHOP, true));
            this.setContainAction(reader.getAttrAsBoolean("isContainAction", true));
            this.setContainTemplate(reader.getAttrAsBoolean("isContainTemplate", true));
            this.setContainFileContent(reader.getAttrAsBoolean("isContainFileContent", false));
            this.setNeedSegmentationCheckbox(reader.getAttrAsBoolean("needSegmentationCheckbox", true));
            this.setNeedIntelligentCustomerService(reader.getAttrAsBoolean("needIntelligentCustomerService", true));
            this.setProductDynamics(reader.getAttrAsBoolean("productDynamics", true));
            this.setShortcuts(reader.getAttrAsString("shortcuts", getDefaultShortCuts()));
            this.setNeedRemind(reader.getAttrAsBoolean("isNeedRemind", true));
            this.setOperateCount(reader.getAttrAsInt("operateCount", 0));
            this.setTabOrder(reader.getAttrAsString(TAB_ORDER, getDefaultTabOrder()).split(COMMA));
        } else if (reader.isChildNode()) {
            if (ComparatorUtils.equals(reader.getTagName(), "ActionSearchTextCache")) {
                readActionSearchTextCacheXML(reader);
            } else if ("SearchHistory".equals(reader.getTagName())) {
                readHistorySearch(reader);
            } else if ("ReadSet".equals(reader.getTagName())) {
                readReadSet(reader);
            }
        }
    }

    /**
     * 读出搜索缓存
     */
    private void readActionSearchTextCacheXML(XMLableReader reader) {
        reader.readXMLObject(new XMLReadable() {
            @Override
            public void readXML(XMLableReader reader) {
                if (ComparatorUtils.equals(reader.getTagName(), "ActionSearchTextCache")) {
                    setCacheBuildNO(reader.getAttrAsString("buildNO", ""));
                } else if (ComparatorUtils.equals(reader.getTagName(), "item")) {
                    String tmpVal = reader.getElementValue();
                    if (tmpVal != null) {
                        actionSearchTextCache.put(reader.getAttrAsString("key", ""), tmpVal);
                    } else {
                        actionSearchTextCache.put(reader.getAttrAsString("key", ""), StringUtils.EMPTY);
                    }

                }
            }
        });
    }

    private void readHistorySearch(XMLableReader reader) {
        reader.readXMLObject(new XMLReadable() {
            @Override
            public void readXML(XMLableReader xmLableReader) {
                if (ComparatorUtils.equals(reader.getTagName(), "history")) {
                    String tmpVal = reader.getElementValue();
                    if (tmpVal != null) {
                        tmpVal = tmpVal.replace("[", StringUtils.EMPTY).replace("]", StringUtils.EMPTY);
                        Stack<String> stack = new SizedStack<>(3);
                        String[] historyList = tmpVal.split(",");
                        for (String history : historyList) {
                            String value = history.trim();
                            if (StringUtils.isNotEmpty(value)) {
                                stack.add(value);
                            }
                        }
                        historySearchMap.put(reader.getAttrAsString("user", StringUtils.EMPTY), stack);
                    }
                }
            }
        });
    }


    private void readReadSet(XMLableReader reader) {
        reader.readXMLObject(new XMLReadable() {
            @Override
            public void readXML(XMLableReader xmLableReader) {
                if (ComparatorUtils.equals(reader.getTagName(), "readId")) {
                    String tmpVal = reader.getElementValue();
                    if (tmpVal != null) {
                        tmpVal = tmpVal.replace("[", StringUtils.EMPTY).replace("]", StringUtils.EMPTY);
                        String[] idArr = tmpVal.split(",");
                        Set<Long> setId = new HashSet<>();
                        for (String id : idArr) {
                            String value = id.trim();
                            if (StringUtils.isNotEmpty(value)) {
                                setId.add(Long.parseLong(value));
                            }
                        }
                        readSetMap.put(reader.getAttrAsString("user", StringUtils.EMPTY), setId);
                    }
                }
            }
        });
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG("AlphaFineConfigManager");
        writer.attr("isEnabled", this.isEnabled())
                .attr("isSearchOnline", this.isSearchOnLine())
                .attr("shortcuts", this.getShortcuts())
                .attr("isContainRecommend", this.isContainRecommend())
                .attr("isContainAction", this.isContainAction())
                .attr("isContainDocument", this.isContainDocument())
                .attr("isContainTemplate", this.isContainTemplate())
                .attr("isContainPlugin", this.isContainPlugin())
                .attr("isContainFileContent", this.isContainFileContent())
                .attr("isNeedRemind", this.isNeedRemind())
                .attr("operateCount", this.getOperateCount())
                .attr("needSegmentationCheckbox", this.isNeedSegmentationCheckbox())
                .attr("needIntelligentCustomerService", this.isNeedIntelligentCustomerService())
                .attr("productDynamics", this.isProductDynamics())
                .attr(CONTAIN_TEMPLATE_SHOP, this.showTemplateShop)
                .attr(TAB_ORDER, this.getTabOrderString());
        writeActionSearchTextCacheXML(writer);
        writeSearchHistory(writer);
        writeReadSet(writer);
        writer.end();
    }

    /**
     * 写入搜索缓存
     */
    private void writeActionSearchTextCacheXML(XMLPrintWriter writer) {
        writer.startTAG("ActionSearchTextCache").attr("buildNO", cacheBuildNO);
        for (Map.Entry<String, String> item : actionSearchTextCache.entrySet()) {
            writer.startTAG("item").attr("key", item.getKey()).textNode(item.getValue()).end();
        }
        writer.end();
    }

    private void writeSearchHistory(XMLPrintWriter writer) {
        writer.startTAG("SearchHistory");
        for (Map.Entry<String, Stack<String>> entry : historySearchMap.entrySet()) {
            writer.startTAG("history").attr("user", entry.getKey()).textNode(entry.getValue().toString()).end();
        }
        writer.end();
    }

    private void writeReadSet(XMLPrintWriter writer) {
        writer.startTAG("ReadSet");
        for (Map.Entry<String, Set<Long>> entry : readSetMap.entrySet()) {
            writer.startTAG("readId").attr("user", entry.getKey()).textNode(entry.getValue().toString()).end();
        }
        writer.end();
    }

    public boolean isSearchOnLine() {
        return searchOnLine;
    }

    public void setSearchOnLine(boolean searchOnLine) {
        this.searchOnLine = searchOnLine;
    }

    public String getShortcuts() {
        if (StringUtils.isBlank(shortcuts)) {
            return getDefaultShortCuts();
        }
        return shortcuts;
    }

    public void setShortcuts(String shortcuts) {
        this.shortcuts = shortcuts;
        this.shortCutKeyStore = convert2KeyStroke(this.shortcuts);
    }

    /**
     * 返回默认快捷键
     *
     * @return
     */
    private String getDefaultShortCuts() {
        return OperatingSystem.isMacOS() ? "meta + D" : "ctrl + D";
    }

    /**
     * 返回默认排序
     */
    private String getDefaultTabOrder() {
        return AlphaFineConstants.PRODUCT_NEWS + COMMA + AlphaFineConstants.TEMPLATE_SHOP + COMMA + AlphaFineConstants.HELP + COMMA + AlphaFineConstants.PLUGIN + COMMA + AlphaFineConstants.FUNCTION + COMMA + AlphaFineConstants.MY_TEMPLATES;
    }

    public boolean isContainAction() {
        return containAction;
    }

    public void setContainAction(boolean containAction) {
        this.containAction = containAction;
    }

    public boolean isContainDocument() {
        return containDocument;
    }

    public void setContainDocument(boolean containDocument) {
        this.containDocument = containDocument;
    }

    public boolean isContainMyTemplate() {
        return containMyTemplate;
    }

    public void setContainMyTemplate(boolean containMyTemplate) {
        this.containMyTemplate = containMyTemplate;
    }

    public boolean isContainTemplate() {
        return containTemplate;
    }

    public void setContainTemplate(boolean containTemplate) {
        this.containTemplate = containTemplate;
    }

    public boolean isContainPlugin() {
        return containPlugin;
    }

    public void setContainPlugin(boolean containPlugin) {
        this.containPlugin = containPlugin;
    }

    public boolean isContainRecommend() {
        return containRecommend;
    }

    public void setContainRecommend(boolean containConclude) {
        this.containRecommend = containConclude;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public KeyStroke getShortCutKeyStore() {
        if (shortCutKeyStore == null) {
            shortCutKeyStore = convert2KeyStroke(this.getShortcuts());
        }
        return shortCutKeyStore;
    }

    public void setShortCutKeyStore(KeyStroke shortCutKeyStore) {
        this.shortCutKeyStore = shortCutKeyStore;
    }

    private KeyStroke convert2KeyStroke(String ks) {
        return KeyStroke.getKeyStroke(ks.replace("+", "pressed"));
    }

    public boolean isContainFileContent() {
        return containFileContent;
    }

    public void setContainFileContent(boolean containFileContent) {
        this.containFileContent = containFileContent;
    }

    public boolean isNeedRemind() {
        return needRemind;
    }

    public void setNeedRemind(boolean needRemind) {
        this.needRemind = needRemind;
    }

    public boolean isNeedSegmentationCheckbox() {
        return needSegmentationCheckbox;
    }

    public void setNeedSegmentationCheckbox(boolean needSegmentationCheckbox) {
        this.needSegmentationCheckbox = needSegmentationCheckbox;
    }

    public boolean isNeedIntelligentCustomerService() {
        return needIntelligentCustomerService;
    }

    public void setNeedIntelligentCustomerService(boolean needIntelligentCustomerService) {
        this.needIntelligentCustomerService = needIntelligentCustomerService;
    }

    public int getOperateCount() {
        return operateCount;
    }

    public void setOperateCount(int operateCount) {
        this.operateCount = operateCount;
    }

    @NotNull
    public Map<String, String> getActionSearchTextCache() {
        return Collections.unmodifiableMap(actionSearchTextCache);
    }

    public void setActionSearchTextCache(@NotNull String key, @NotNull String value) {
        this.actionSearchTextCache.put(key, value);
    }

    @NotNull
    public String getCacheBuildNO() {
        if (cacheBuildNO == null) {
            return StringUtils.EMPTY;
        }
        return cacheBuildNO;
    }

    public void setCacheBuildNO(@NotNull String cacheBuildNO) {
        this.cacheBuildNO = cacheBuildNO;
    }

    public Stack<String> getHistorySearch() {
        return historySearchMap.computeIfAbsent(DesignerEnvManager.getEnvManager().getDesignerLoginUsername(), k -> new SizedStack<>(3));
    }

    public Set<Long> getReadSet() {
        return readSetMap.computeIfAbsent(DesignerEnvManager.getEnvManager().getDesignerLoginUsername(), k -> new HashSet<>());
    }

    public boolean isProductDynamics() {
        return productDynamics && FRContext.isChineseEnv();
    }

    public void setProductDynamics(boolean productDynamics) {
        this.productDynamics = productDynamics;
    }

    public boolean hasTemplateShop() {
        return showTemplateShop && FRContext.isChineseEnv();
    }

    public void setShowTemplateShop(boolean showTemplateShop) {
        this.showTemplateShop = showTemplateShop;
    }

    /**
     * 是否展示alphafine窗口，设置-搜索范围 0勾选，则不显示
     */
    public boolean needShowAlphaFineDialog() {
        return hasTemplateShop() || isContainDocument() || isContainPlugin() ||
                isContainAction() || isProductDynamics() || isContainMyTemplate();
    }

    /**
     * 返回tab显示顺序
     */
    public String[] getTabOrder() {
        if (tabOrder == null) {
            tabOrder = getDefaultTabOrder().split(COMMA);
        }
        return tabOrder;
    }

    public void setTabOrder(String[] tabOrder) {
        this.tabOrder = tabOrder;
    }

    /**
     * getTabOrder的tostring
     */
    public String getTabOrderString() {
        StringBuilder sb = new StringBuilder();
        for (String s : getTabOrder()) {
            sb.append(s + COMMA);
        }
        return sb.toString();
    }
}
