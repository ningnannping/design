package com.fr.design.actions.help.alphafine;

import java.util.Stack;

/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/4/23
 */
public class SizedStack<T> extends Stack<T> {

    private final int maxSize;

    public SizedStack(int size) {
        super();
        this.maxSize = size;
    }

    @Override
    public T push(T object) {
        while (this.size() >= maxSize) {
            this.remove(0);
        }
        // 不重复
        if (this.contains(object)) {
            return object;
        }
        return super.push(object);
    }
}