package com.fr.design.actions;

import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ibutton.UIForbiddenButton;
import javax.swing.AbstractButton;
import javax.swing.JComponent;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/12
 */
public abstract class ForbiddenUpdateAction extends UpdateAction {

    @Override
    public JComponent createToolBarComponent() {
        Object object = this.getValue(UIForbiddenButton.class.getName());
        if (!(object instanceof AbstractButton)) {
            UIButton button = null;
            button = new UIForbiddenButton();
            object = initButton(button, UIForbiddenButton.class.getName());
        }

        return (JComponent) object;
    }
}
