package com.fr.design.actions.community;

import com.fr.design.locale.impl.VideoMark;
import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.locale.LocaleCenter;
import com.fr.general.locale.LocaleMark;

import javax.swing.*;

public class VideoAction extends AbstractDesignerSSO {

    public VideoAction() {
        this.setMenuKeySet(VIDEO);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/video");
    }

	@Override
	public String getJumpUrl() {
		LocaleMark<String> localeMark = LocaleCenter.getMark(VideoMark.class);
		return localeMark.getValue();
	}

    public static final MenuKeySet VIDEO = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'V';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_Video");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
