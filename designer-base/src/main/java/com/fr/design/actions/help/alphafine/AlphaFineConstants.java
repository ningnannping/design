package com.fr.design.actions.help.alphafine;

import com.fr.base.extension.FileExtension;
import com.fr.base.svg.IconUtils;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.DesignUtils;
import com.fr.general.IOUtils;

import javax.swing.Icon;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Created by XiaXiang on 2017/5/10.
 */
public class AlphaFineConstants {


    public static final String FUNCTION = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Function");

    public static final String MY_TEMPLATES = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_My_Templates");

    public static final String PRODUCT_NEWS = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Product_News");

    public static final String HELP = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Community_Help");

    public static final String PLUGIN = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Plugin_Addon");

    public static final String TEMPLATE_SHOP = com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Template_Shop");

    public static final int SHOW_SIZE = 5;

    public static final int MAX_FILE_SIZE = 1000;

    public static final int LATEST_SHOW_SIZE = 3;

    public static final int HEIGHT = 680;

    public static final int WIDTH = 460;

    public static final int LEFT_WIDTH = 300;

    public static final int RIGHT_WIDTH = 380;

    public static final int FIELD_HEIGHT = 55;

    public static final int CONTENT_HEIGHT = 405;

    public static final int CELL_HEIGHT = 29;

    public static final int CELL_TITLE_HEIGHT = 24;

    public static final int HOT_ICON_LABEL_HEIGHT = 36;

    public static final int HOT_ITEMS = 6;

    /**
     * 限制搜索字符长度
     */
    public static final int LEN_LIMIT = 50;

    /**
     * 帮助文档搜索间隔（ms） api限制了1s之内只能搜索一次
     */
    public static final long DOCUMENT_SEARCH_GAP = 1000;

    public static final Dimension FULL_SIZE = new Dimension(680, 460);

    public static final Dimension CONTENT_SIZE = new Dimension(680, 405);

    public static final Dimension FIELD_SIZE = new Dimension(680, 55);

    public static final Dimension ICON_LABEL_SIZE = new Dimension(64, 64);

    public static final Dimension HOT_ICON_LABEL_SIZE = new Dimension(36, 36);

    public static final Dimension HOT_ISSUES_JAPNEL_SIZE = new Dimension(213, 182);

    /**
     * 展示面板的尺寸
     */
    public static final Dimension PREVIEW_SIZE = new Dimension(680, 305);

    public static final Dimension CLOSE_BUTTON_SIZE = new Dimension(40, 40);

    public static final Color WHITE = new Color(0xf9f9f9);

    public static final Color LABEL_SELECTED = new Color(0x419bf9);

    public static final Color GRAY = new Color(0xd2d2d2);

    public static final Color LIGHT_GRAY = new Color(0xcccccc);

    public static final Color MEDIUM_GRAY = new Color(0x999999);

    public static final Color BLUE = new Color(0x3394f0);

    public static final Color BLACK = new Color(0x222222);

    public static final Color DARK_GRAY = new Color(0x666666);

    public static final Color RED = new Color(0xf46c4c);

    public static final String HIGH_LIGHT_COLOR = "rgb(51,148,240)";

    public static final Font SMALL_FONT = DesignUtils.getDefaultGUIFont().applySize(10);

    public static final Font MEDIUM_FONT = DesignUtils.getDefaultGUIFont().applySize(12);

    public static final Font MEDIUM_FONT_ANOTHER = new Font("HiraginoSansGB-W3", 0, 12);

    public static final Font LARGE_FONT = DesignUtils.getDefaultGUIFont().applySize(18);

    public static final Font GREATER_FONT = DesignUtils.getDefaultGUIFont().applySize(20);

    public static final String IMAGE_URL = "/com/fr/design/mainframe/alphafine/images/";

    public static final String ALPHA_HOT_IMAGE_NAME = "alphafine_hot";

    public static final String SPECIAL_CHARACTER_REGEX = "[`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】'；：”“’。，、？]";

    public static final String BOTTOM_REGEX_FIRST = "<div class=\"bang\">([\\s\\S]*?)class=\"jiaoyes\">YES</a><br/>";

    public static final String BOTTOM_REGEX_SECOND = "<div class=\"yes_([\\s\\S]*?)帮助</a></div></div>";

    public static final String LINK_REGEX = "javascript:;\"([\\s\\S]*?)','";

    public static final String LINK_REGEX_ANOTHER = "javascript:([\\s\\S]*?)url=\"";

    public static final String ALPHA_ROBOT_SEARCH_TOKEN = "K8dl0Np6l0gs";

    public static final String SEARCH_BY_ID = "?id=";

    public static final String JAVASCRIPT_PREFIX = "javascript:SendJava";

    public static final String CHINESE_CHARACTERS = "[\\u4e00-\\u9fa5]";

    public static final String FIRST_PAGE = "-1";

    public static final FileExtension[] FILE_EXTENSIONS = new FileExtension[]{FileExtension.CPT, FileExtension.FRM};

    public static final int RECOMMEND_MAX_ITEM_NUM = 3;

    public static final String BACK_ICON_NAME = "back@1x.png";

    public static final Icon NO_RESULT_ICON = IOUtils.readIcon(AlphaFineConstants.IMAGE_URL + "noresult.png");

    public static final Color SUSPENDED_COLOR = new Color(84, 165, 249);

    public static final Color FOREGROUND_COLOR = new Color(51, 51, 52);

    /**
     * 后面数字代表透明度 80%
     */
    public static final Color FOREGROUND_COLOR_8 = new Color(51, 51, 52, 204);

    public static final Color FOREGROUND_COLOR_6 = new Color(51, 51, 52, 153);

    public static final Color FOREGROUND_COLOR_5 = new Color(51, 51, 52, 128);

    public static final Color BACKGROUND_COLOR = new Color(245, 245, 247);

    public static final Icon BULB_ICON = IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/bulb.svg");

    public static final Icon YELLOW_BULB_ICON = IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/yellow_bulb.svg");

    public static final Icon LIGHT_YELLOW_BULB_ICON = IconUtils.readIcon("com/fr/design/mainframe/alphafine/images/light_yellow_bulb.svg");

    public static final String HOT_SEARCH = Toolkit.i18nText("Fine-Design_Report_AlphaFine_Hot_Search");

    public static final Set<String> HOT_SEARCH_SET = new LinkedHashSet<>(
            Arrays.asList(
                    Toolkit.i18nText("Fine-Design_Report_AlphaFine_Hot_Search_TOP_ONE"),
                    Toolkit.i18nText("Fine-Design_Report_AlphaFine_Hot_Search_TOP_TWO"),
                    Toolkit.i18nText("Fine-Design_Report_AlphaFine_Hot_Search_TOP_THREE")
            )
    );


    public static final ArrayList<String> CONJUNCTION = new ArrayList<String>() {{
        add(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Conjunction_HE"));
        add(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Conjunction_YU"));
        add(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_AlphaFine_Conjunction_DE"));
    }};

    public static final String LOADING = "loading";

    public static final String NETWORK_ERROR = "network error";

    public static final String TITLE = "AlphaFine";

    public static final int DEFAULT_CLICK_COUNT = 1;
}
