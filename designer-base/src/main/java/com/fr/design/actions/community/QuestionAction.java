package com.fr.design.actions.community;

import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;
import com.fr.general.CloudCenter;

import javax.swing.*;

public class QuestionAction extends AbstractDesignerSSO {

    public QuestionAction() {
        this.setMenuKeySet(QUESTIONS);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/question");

    }

    @Override
    public String getJumpUrl() {
        return CloudCenter.getInstance().acquireUrlByKind("bbs.questions", "http://bbs.fanruan.com/wenda");
    }

    public static final MenuKeySet QUESTIONS = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'Q';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_Questions");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
