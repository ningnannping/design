package com.fr.design.actions.community;

import com.fr.design.login.AbstractDesignerSSO;
import com.fr.design.menu.MenuKeySet;

import com.fr.general.CloudCenter;

import javax.swing.*;

public class BBSAction extends AbstractDesignerSSO {


    public BBSAction() {
        this.setMenuKeySet(BBS);
        this.setName(getMenuKeySet().getMenuName());
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/bbs/bbs");

    }

	@Override
	public String getJumpUrl() {
		return CloudCenter.getInstance().acquireUrlByKind("bbs", "http://bbs.fanruan.com/");
	}

    public static final MenuKeySet BBS = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'B';
        }

        @Override
        public String getMenuName() {
            return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Community_Bbs");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

}
