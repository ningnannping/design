package com.fr.design.actions.help.alphafine.component;

import javax.swing.JPanel;
import java.awt.FlowLayout;
import java.util.List;

/**
 * 简单菜单面板
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/18
 */
public class MenuLabelPane extends JPanel {

    private static final int GAP = 1;

    private List<MenuLabel> labels;

    public MenuLabelPane(List<MenuLabel> labels) {
        this.labels = labels;
        setLayout(new FlowLayout(FlowLayout.CENTER, GAP, GAP));
        for (MenuLabel label : labels) {
            label.setParentMenu(this);
            add(label);
        }
    }

    public void setNoneSelected() {
        for (MenuLabel label : labels) {
            label.setSelected(false);
        }
    }
}
