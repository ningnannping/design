package com.fr.design.actions.help.alphafine;

import com.fr.design.i18n.Toolkit;
import com.fr.general.CloudCenter;
import com.fr.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

/**
 * 云端变量统一管理
 *
 * @author Link
 * @version 11.0
 * Created by Link on 2022/9/28
 */
public class AlphaFineCloudConstants {

    private static final String PLUGIN_SEARCH_API = "plugin.searchAPI";
    private static final String PLUGIN_ALL_SEARCH_API = "plugin.all.searchAPI";
    private static final String AF_PLUGIN_INFO = "af.pluginInfo";
    private static final String AF_REUSE_INFO = "af.reuseInfo";
    private static final String AF_DOC_VIEW = "af.doc_view";
    private static final String AF_DOC_SEARCH = "af.doc_search";
    private static final String AF_DOC_INFO = "af.doc_info";
    private static final String AF_PLUGIN_IMAGE = "af.plugin_image";
    private static final String AF_RECORD = "af.record";
    private static final String AF_CLOUD_SEARCH = "af.cloud_search";
    private static final String AF_SIMILAR_SEARCH = "af.similar_search";
    private static final String AF_ADVICE_SEARCH = "af.advice_search";
    private static final String AF_HOT_SEARCH = "af.hot_search";
    private static final String AF_GO_FORUM = "af.go_fourm";
    private static final String AF_GO_WEB = "af.go_web";
    private static final String AF_PREVIEW = "af.preview";
    private static final String AF_CID_NEW = "af.cid.new";
    private static final String AF_CID_USER_GROUP_INFO = "af.cid.user.group.info";
    private static final String AF_HELP_QUICK_START = "af.help.quick.start";
    private static final String AF_HELP_REPORT_LEARNING_PATH = "af.help.report.learning.path";
    private static final String AF_HELP_PARAM_LEARNING_PATH = "af.help.param.learning.path";
    private static final String AF_HELP_FILL_LEARNING_PATH = "af.help.fill.learning.path";
    private static final String AF_HELP_API_SUMMARY = "af.help.api.summary";
    private static final String AF_HELP_MONTHLY_DOCUMENT = "af.help.monthly.document";
    private static final String AF_RECOMMEND = "af.recommend";

    private static final String LINK_NAME = "name";
    private static final String LINK_URL = "link";

    /**
     * 获取插件搜索api
     */
    public static String getPluginSearchUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(PLUGIN_SEARCH_API);
    };

    /**
     * 帆软市场里全部插件api
     */
    public static String getSearchAllPluginUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(PLUGIN_ALL_SEARCH_API);
    }

    /**
     * 获取插件信息api
     */
    public static String getPluginUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_PLUGIN_INFO);
    }

    /**
     * 获取组件信息api
     */
    public static String getReuseUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_REUSE_INFO);
    }

    /**
     * 获取帮助文档url
     */
    public static String getDocumentDocUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_DOC_VIEW);
    }

    /**
     * 帮助文档搜索api
     */
    public static String getDocumentSearchUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_DOC_SEARCH);
    }

    /**
     * 帮助文档信息api
     */
    public static String getDocumentInformationUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_DOC_INFO);
    }

    /**
     * 插件图片api
     */
    public static String getPluginImageUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_PLUGIN_IMAGE);
    }

    /**
     * 获取云端接口，用于上传alphafine搜索记录
     */
    public static String getCloudServerUrl() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_RECORD);
    }

    /**
     * 获取搜索api，输入搜索词，返回fr的相关功能
     */
    public static String getSearchApi() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_CLOUD_SEARCH);
    }

    /**
     * 获取模糊搜索api前缀，输入搜索词，返回alphaFine相关内容，插件，文档，功能等
     */
    public static String getSimilarSearchUrlPrefix() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_SIMILAR_SEARCH);
    }

    /**
     * 补全建议搜索结果 api，与AF_SIMILAR_SEARCH接口类似，但是返回的信息更全
     */
    public static String getComplementAdviceSearchUrlPrefix() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_ADVICE_SEARCH);
    }

    /**
     * 获取热门问题
     */
    public static String getAlphaHotSearch() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_HOT_SEARCH);
    }

    /**
     * 跳转论坛url
     */
    public static String getAlphaGoToForum() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_GO_FORUM);
    }

    /**
     * 推荐搜索api，输入搜索词，返回猜你想搜的内容（html格式）
     */
    public static String getAlphaGoToWeb() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_GO_WEB);
    }

    /**
     * 帆软智能客服页面url
     */
    public static String getAlphaPreview() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_PREVIEW);
    }

    /**
     * cid系统的产品动态api
     */
    public static String getAlphaCid() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_CID_NEW);
    }

    /**
     * cid系统的 用户组信息api
     */
    public static String getAlphaCidUserGroupInfo() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_CID_USER_GROUP_INFO);
    }

    private static String getDefaultRecommend() {
        String[][] links = new String[][]{
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Quick_Start"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_QUICK_START)},
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Report_Learning"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_REPORT_LEARNING_PATH)},
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Parameter_Learning"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_PARAM_LEARNING_PATH)},
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Fill_Learning"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_FILL_LEARNING_PATH)},
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Api_Summary"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_API_SUMMARY)},
                {Toolkit.i18nText("Fine-Design_Report_AlphaFine_Doc_Monthly_Document"), CloudCenter.getInstance().acquireUrlByKind(AF_HELP_MONTHLY_DOCUMENT)}
        };
        JSONArray jsonArray = new JSONArray();
        for (String[] link : links) {
            Map<String, String> map = new HashMap<>();
            map.put(LINK_NAME, link[0]);
            map.put(LINK_URL, link[1]);
            jsonArray.put(map);
        }

        return jsonArray.toString();
    }

    /**
     * 获取默认推荐帮助文档url
     */
    public static String getAlphaHelpRecommend() {
        return CloudCenter.getInstance().acquireUrlByKind(AF_RECOMMEND, getDefaultRecommend());
    }
}
