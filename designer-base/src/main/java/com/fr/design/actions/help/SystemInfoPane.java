package com.fr.design.actions.help;

import java.awt.BorderLayout;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import com.fr.design.layout.FRGUIPaneFactory;

import com.fr.stable.ProductConstants;

public class SystemInfoPane extends JPanel {
    public SystemInfoPane() {
        super(FRGUIPaneFactory.createBorderLayout());

        DefaultTableModel tableModel = new DefaultTableModel();
        JTable table = new JTable(tableModel) {
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tableModel.addColumn(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Property"));
        tableModel.addColumn(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Value"));

        Properties properties = System.getProperties();
        Object[] keys = new Object[properties.size()];

        int index = 0;
        Enumeration enumeration = properties.propertyNames();
        while (enumeration.hasMoreElements()) {
            keys[index++] = enumeration.nextElement();
        }
        Arrays.sort(keys);

        for (int i = 0; i < keys.length; i++) {
            Object[] tableRowData = new Object[2];
            String keyValue = keys[i].toString();

            if (needToShield(keyValue)) {
                continue;
            }

            if(keyValue.contains("FineReport")){
                keys[i] = keyValue.replaceAll("FineReport", ProductConstants.APP_NAME);
            }

            tableRowData[0] = keys[i];
            tableRowData[1] = properties.getProperty((String) keys[i]);
            tableModel.addRow(tableRowData);
        }

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        TableColumnModel columnModel = table.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(160);
        columnModel.getColumn(1).setPreferredWidth(240);

        add(new JScrollPane(table), BorderLayout.CENTER);
    }

    /**
     * 是否属于需要屏蔽的内容(当前屏蔽掉exe4j与jxbrowser的内容)
     *
     * @param keyValue 对应的key值
     * @return 需要屏蔽则返回true
     */
    private boolean needToShield(String keyValue) {
        return keyValue.contains("exe4j") || keyValue.contains("jxbrowser") || "install4j.exeDir".equals(keyValue);
    }
}