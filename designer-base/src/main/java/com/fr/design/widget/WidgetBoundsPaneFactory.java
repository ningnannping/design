package com.fr.design.widget;

import com.fr.design.designer.IntervalConstants;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;

/**
 * Created by plough on 2017/8/7.
 */
public class WidgetBoundsPaneFactory {

    public enum NameAttribute {
        //默认的名称
        DEFAULT(Toolkit.i18nText("Fine-Design_Basic_Component_Position"), Toolkit.i18nText("Fine-Design_Basic_Component_Size")),
        //控件对应的名称
        WIDGET(Toolkit.i18nText("Fine-Design_Basic_Widget_Position"), Toolkit.i18nText("Fine-Design_Basic_Widget_Size"));

        private String positionName;
        private String sizeName;

        NameAttribute(String positionName, String sizeName) {
            this.positionName = positionName;
            this.sizeName = sizeName;
        }

        public String getPositionName() {
            return positionName;
        }

        public String getSizeName() {
            return sizeName;
        }
    }

    private static final int RIGHT_PANE_WIDTH = 145;

    public static UIExpandablePane createBoundsPane(UISpinner width, UISpinner height, JComponent ratioLocked, NameAttribute nameAttribute) {
        JPanel boundsPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;
        Component[][] components = new Component[][]{
                new Component[]{FRWidgetFactory.createLineWrapLabel(nameAttribute.getSizeName()),
                        ratioLocked != null ? createRightPane(width, ratioLocked, height) : createRightPane(width, height)},
                new Component[]{null, createRightPane(new UILabel(Toolkit.i18nText("Fine-Design_Basic_Tree_Width"), SwingConstants.CENTER), new UILabel(Toolkit.i18nText("Fine-Design_Basic_Tree_Height"), SwingConstants.CENTER))},
        };
        double[] rowSize = {p, p};
        double[] columnSize = {f, RIGHT_PANE_WIDTH};
        int[][] rowCount = ratioLocked != null ? new int[][]{{1, 1}, {1, 1, 1}} : new int[][]{{1, 1}, {1, 1}};
        final JPanel panel = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, IntervalConstants.INTERVAL_W5, IntervalConstants.INTERVAL_L6);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        boundsPane.add(panel);
        return new UIExpandablePane(Toolkit.i18nText("Fine-Design_Form_Coords_And_Size"), 280, 24, boundsPane);
    }

    public static UIExpandablePane createBoundsPane(UISpinner width, UISpinner height) {
        return createBoundsPane(width, height, null, NameAttribute.DEFAULT);
    }

    public static JPanel createRightPane(Component com1, Component com2) {
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;
        double[] rowSize = {p};
        double[] columnSize = {f, f};
        int[][] rowCount = {{1, 1}};
        Component[][] components = new Component[][]{
                new Component[]{com1, com2}
        };
        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, IntervalConstants.INTERVAL_L6, IntervalConstants.INTERVAL_L1);
    }

    public static JPanel createRightPane(Component com1, Component com2, Component com3) {
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;
        double[] rowSize = {p};
        double[] columnSize = {f, 24, f};
        int[][] rowCount = {{1, 1, 1}};
        Component[][] components = new Component[][]{
                new Component[]{com1, com2, com3}
        };
        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, 0, IntervalConstants.INTERVAL_L1);
    }

    public static UIExpandablePane createAbsoluteBoundsPane(UISpinner x, UISpinner y, UISpinner width, UISpinner height, JComponent ratioLocked, NameAttribute nameAttribute) {
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;

        UILabel positionLabel = FRWidgetFactory.createLineWrapLabel(nameAttribute.getPositionName());
        UILabel xLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_X_Coordinate"), SwingConstants.CENTER);
        UILabel yLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Y_Coordinate"), SwingConstants.CENTER);

        UILabel sizeLabel = FRWidgetFactory.createLineWrapLabel(nameAttribute.getSizeName());
        UILabel widthLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Tree_Width"), SwingConstants.CENTER);
        UILabel heightLabel = new UILabel(Toolkit.i18nText("Fine-Design_Basic_Tree_Height"), SwingConstants.CENTER);

        Component[][] northComponents = new Component[][]{
                new Component[]{positionLabel, ratioLocked != null ? createRightPane(x, null, y) :  createRightPane(x, y)},
                new Component[]{null,          ratioLocked != null ? createRightPane(xLabel, null, yLabel) : createRightPane(xLabel, yLabel)},
        };
        Component[][] centerComponents = new Component[][]{
                new Component[]{sizeLabel, ratioLocked != null ? createRightPane(width, ratioLocked, height) : createRightPane(width, height)},
                new Component[]{null,      ratioLocked != null ? createRightPane(widthLabel, null, heightLabel) :  createRightPane(widthLabel, heightLabel)},
        };
        double[] rowSize = {p, p};
        double[] columnSize = {f, RIGHT_PANE_WIDTH};
        int[][] rowCount = ratioLocked != null ? new int[][]{{1, 1, 1}, {1, 1, 1}} : new int[][]{{1, 1}, {1, 1}};
        final JPanel northPanel = TableLayoutHelper.createGapTableLayoutPane(northComponents, rowSize, columnSize, rowCount, IntervalConstants.INTERVAL_W5, IntervalConstants.INTERVAL_L6);
        final JPanel centerPanel = TableLayoutHelper.createGapTableLayoutPane(centerComponents, rowSize, columnSize, rowCount, IntervalConstants.INTERVAL_W5, IntervalConstants.INTERVAL_L6);
        JPanel boundsPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        northPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
        boundsPane.add(northPanel, BorderLayout.NORTH);
        boundsPane.add(centerPanel, BorderLayout.CENTER);
        return new UIExpandablePane(Toolkit.i18nText("Fine-Design_Form_Coords_And_Size"), 230, 24, boundsPane);
    }

    public static UIExpandablePane createAbsoluteBoundsPane(UISpinner x, UISpinner y, UISpinner width, UISpinner height, NameAttribute nameAttribute) {
        return createAbsoluteBoundsPane(x, y, width, height, null, nameAttribute);
    }

    public static UIExpandablePane createAbsoluteBoundsPane(UISpinner x, UISpinner y, UISpinner width, UISpinner height) {
        return createAbsoluteBoundsPane(x, y, width, height, NameAttribute.DEFAULT);
    }


    public static UIExpandablePane createCardTagBoundPane(UISpinner width) {
        JPanel boundsPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        double f = TableLayout.FILL;
        double p = TableLayout.PREFERRED;
        Component[][] components = new Component[][]{
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Basic_Component_Size")), width},
        };
        double[] rowSize = {p};
        double[] columnSize = {p, f};
        int[][] rowCount = {{1, 1}};
        final JPanel panel = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, rowCount, IntervalConstants.INTERVAL_W1, IntervalConstants.INTERVAL_L6);
        panel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        boundsPane.add(panel);
        return new UIExpandablePane(Toolkit.i18nText("Fine-Design_Form_Coords_And_Size"), 280, 24, boundsPane);
    }
}
