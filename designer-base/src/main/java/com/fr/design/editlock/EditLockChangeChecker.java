package com.fr.design.editlock;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.ui.util.UIUtil;
import com.fr.log.FineLoggerFactory;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.lock.editlock.EditLockOperator;
import com.fr.report.LockItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/19
 * 判断当前设计器在远程设计服务器中的锁状态是否发生了改变
 */
public abstract class EditLockChangeChecker {

    private static final int INTERVAL = 30000;
    private ScheduledExecutorService scheduler;
    protected LockItem lockItem;
    private boolean isLocked = false;
    private List<EditLockChangeListener> listeners = new ArrayList<>();

    /**
     * 轮询任务，如果是远程设计状态，每隔30s查询一次相应lockItem的锁状态是否改变
     */
    public void start() {
        this.scheduler = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("EditLockChangeChecker"));
        this.scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                // 判断是否为远程设计环境
                if (!WorkContext.getCurrent().isLocal()) {
                    try {
                        EditLockOperator operator = WorkContext.getCurrent().get(EditLockOperator.class);
                        boolean locked = operator.isLocked(lockItem);
                        if (isLocked() != locked) {
                            setLocked(locked);
                            fireChange();
                        }
                    } catch (Exception e) {
                        FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    }
                } else if (isLocked()){
                    // 如果不是远程环境，且此前的远程状态下为锁定的话，切换回来后需要将其修改为不锁定
                    setLocked(false);
                    fireChange();
                }
            }
        }, 0, INTERVAL, TimeUnit.MILLISECONDS);
    }

    public void stop() {
        this.scheduler.shutdown();
    }

    public void addEditLockChangeListener(EditLockChangeListener listener) {
        this.listeners.add(listener);
    }

    private void fireChange() {
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                for (EditLockChangeListener listener : EditLockChangeChecker.this.listeners) {
                    listener.updateLockedState(new EditLockChangeEvent(isLocked()));
                }
            }
        });
    }

    public boolean isLocked() {
        return this.isLocked;
    }

    public void setLocked(boolean locked) {
        this.isLocked = locked;
    }
}
