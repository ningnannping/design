package com.fr.design.editlock;

import com.fr.base.svg.IconUtils;
import com.fr.base.svg.SVGLoader;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.general.IOUtils;
import com.fr.workspace.WorkContext;
import com.fr.workspace.server.lock.editlock.EditLockOperator;
import com.fr.report.LockItem;
import org.jetbrains.annotations.Nullable;

import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Image;

/**
 * @author Yvan
 * @version 10.0
 * Created by Yvan on 2021/1/20
 * 关于编辑锁定的一些常量和静态方法
 */
public class EditLockUtils {

    /**
     * 数据连接锁定标志
     */
    public static final Icon CONNECTION_LOCKED_ICON = IconUtils.readIcon("/com/fr/design/images/m_web/connection_locked");

    /**
     * 小锁图片
     */
    public static final @Nullable Image LOCKED_IMAGE = SVGLoader.load("/com/fr/design/images/m_web/locked_normal.svg");

    /**
     * 提示弹窗中的提示标志
     */
    public static final Icon TOOLTIPS_ICON = IOUtils.readIcon("/com/fr/design/images/m_web/warningIcon.png");

    /**
     * 数据连接锁定中
     */
    public static final String CONNECTION_LOCKED_TOOLTIPS = Toolkit.i18nText("Fine_Designer_Remote_Design_Data_Connection_Locked");

    /**
     * 服务器数据集锁定中
     */
    public static final String SERVER_TABLEDATA_LOCKED_TOOLTIPS = Toolkit.i18nText("Fine_Designer_Remote_Design_Server_TableData_Locked");

    /**
     * 提示弹窗中的提示信息
     */
    public static final String LOCKED_MESSAGE = Toolkit.i18nText("Fine_Designer_Remote_Design_Locked_Message");

    /**
     * 提示弹窗中的标题
     */
    public static final String TOOLTIPS = Toolkit.i18nText("Fine-Design_Basic_Remote_Design_Title_Hint");

    /**
     * 已经被锁，跳出弹窗提示
     */
    public static void showLockMessage() {
        FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(), EditLockUtils.LOCKED_MESSAGE, EditLockUtils.TOOLTIPS, JOptionPane.INFORMATION_MESSAGE, EditLockUtils.TOOLTIPS_ICON);
    }

    public static void showLockMessage(Component parentComponent) {
        FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(parentComponent), EditLockUtils.LOCKED_MESSAGE, EditLockUtils.TOOLTIPS, JOptionPane.INFORMATION_MESSAGE, EditLockUtils.TOOLTIPS_ICON);
    }

    public static boolean lock(LockItem lockItem) {
        return WorkContext.getCurrent().get(EditLockOperator.class).lock(lockItem);
    }

    public static boolean unlock(LockItem lockItem) {
        return WorkContext.getCurrent().get(EditLockOperator.class).unlock(lockItem);
    }

    public static boolean isLocked(LockItem lockItem) {
        EditLockOperator operator = WorkContext.getCurrent().get(EditLockOperator.class);
        // 启动过程中UILockButton初始化的时候会调用这个方法，但是此时workObjectPool中还没有对象，会报npe
        return operator != null && operator.isLocked(lockItem);
    }
}
