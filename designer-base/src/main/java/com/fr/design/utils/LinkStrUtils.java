package com.fr.design.utils;

import com.fr.design.gui.ilable.UILabel;
import com.fr.stable.StringUtils;

import javax.swing.JComponent;
import java.awt.Color;
import java.awt.Font;

/**
 *
 * 链接字符串工具类
 *
 * @author Harrison
 * @version 10.0
 * created by Harrison on 2022/05/24
 **/
public class LinkStrUtils {
    
    public static final UILabel LABEL = new UILabel();

    /**
     * 创建链接label
     * */
    public static UILabel generateLabel(String html, JComponent templateLabel) {
        
        String style = generateStyle(templateLabel.getBackground(), templateLabel.getFont(), templateLabel.getForeground());
        String fullHtml = generateHtmlTag(style, html);
        return new UILabel(fullHtml);
    }

    /**
     * 创建链接字符串，html格式
     * */
    public static String generateHtmlTag(String html) {
    
        String defaultStyle = generateDefaultStyle();
        return generateHtmlTag(defaultStyle, html);
    }

    /**
     * 创建链接字符串，html格式
     * */
    public static String generateHtmlTag(String style, String html) {
    
        if (StringUtils.isEmpty(style)) {
            throw new NullPointerException("style");
        }
        if (StringUtils.isEmpty(html)) {
            throw new NullPointerException("html");
        }
        return "<html><body style=\"" + style + "\">" + html + "</body></html>";
    }

    /**
     * 创建链接字符串，html格式
     * */
    public static String generateLinkTag(String link, String text) {
    
        return "<a href=\"" + link + "\">" + text + "</a>";
    }

    /**
     * 创建链接字符串，无下划线，html格式
     * */
    public static String generateLinkTagWithoutUnderLine(String link, String text) {
        return "<a style=\"text-decoration:none;\" href=\"" + link + "\">" + text + "</a>";
    }

    /**
     * 创建链接字符串的html style
     * */
    public static String generateStyle(Color backgroundColor, Font font, Color fontColor) {
        
        // 构建相同风格样式
        StringBuilder style = new StringBuilder("font-family:" + font.getFamily() + ";");
        
        style.append("font-weight:").append(font.isBold() ? "bold" : "normal").append(";");
        style.append("font-size:").append(font.getSize()).append("pt;");
        style.append("color:rgb(").append(fontColor.getRed()).append(",").append(fontColor.getGreen()).append(",").append(fontColor.getBlue()).append(");");
        style.append("background-color: rgb(").append(backgroundColor.getRed()).append(",").append(backgroundColor.getGreen()).append(",").append(backgroundColor.getBlue()).append(");");

        return style.toString();
    }

    /**
     * 创建链接字符串的html style
     * */
    public static String generateStyle(Font font, Color fontColor) {

        // 构建相同风格样式
        StringBuilder style = new StringBuilder("font-family:" + font.getFamily() + ";");

        style.append("font-weight:").append(font.isBold() ? "bold" : "normal").append(";");
        style.append("font-size:").append(font.getSize()).append("pt;");
        style.append("color:rgb(").append(fontColor.getRed()).append(",").append(fontColor.getGreen()).append(",").append(fontColor.getBlue()).append(");");
        return style.toString();
    }


    /**
     * 创建默认style
     * */
    public static String generateDefaultStyle() {
    
        return generateStyle(LABEL.getBackground(), LABEL.getFont(), LABEL.getForeground());
    }
}
