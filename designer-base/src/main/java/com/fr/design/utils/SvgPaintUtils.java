package com.fr.design.utils;

import com.fr.base.svg.SVGIcon;
import com.fr.base.svg.SystemScaleUtils;
import java.awt.Graphics2D;

/**
 * 用于绘制svg图片缩放(高分屏下)
 *
 * @author hades
 * @version 11.0
 * Created by hades on 2022/5/6
 */
public class SvgPaintUtils {

    public static void beforePaint(Graphics2D g2) {
        if (SystemScaleUtils.isJreHiDPIEnabled()) {
            g2.scale(1 / SVGIcon.SYSTEM_SCALE, 1 / SVGIcon.SYSTEM_SCALE);
        }
    }

    public static void afterPaint(Graphics2D g2) {
        if (SystemScaleUtils.isJreHiDPIEnabled()) {
            g2.scale(SVGIcon.SYSTEM_SCALE, SVGIcon.SYSTEM_SCALE);
        }
    }

}
