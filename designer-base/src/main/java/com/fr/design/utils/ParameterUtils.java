package com.fr.design.utils;

import com.fr.base.Parameter;
import com.fr.base.ParameterHelper;
import com.fr.general.ComparatorUtils;
import com.fr.stable.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/8/11
 */
public class ParameterUtils {
    /**
     * 获得新的参数集合,返回的集合中的参数的顺序为：新增参数全部放在后面，以保证原有参数的相对顺序
     *
     * @param paramTexts    sql语句
     * @param oldParameters 旧的参数集合
     * @return 新参数集合
     */
    public static Parameter[] analyzeAndUnionParameters(String[] paramTexts, Parameter[] oldParameters) {
        Parameter[] newParameters = ParameterHelper.analyze4Parameters(paramTexts, false);
        return unionParametersInRelativeOrder(oldParameters, newParameters);
    }

    /**
     * 合并新旧参数集合，新增参数全部放在后面，以保证原有参数的相对顺序
     *
     * @param oldParameters 旧的参数集合
     * @param newParameters 新的参数集合
     * @return 新参数集合
     */
    private static Parameter[] unionParametersInRelativeOrder(Parameter[] oldParameters, Parameter[] newParameters) {
        if (ArrayUtils.isEmpty(newParameters) || ArrayUtils.isEmpty(oldParameters)) {
            return newParameters;
        }

        Parameter[] result = new Parameter[newParameters.length];
        List<Parameter> newParameterList = new ArrayList<>(Arrays.asList(newParameters));
        int i = 0;
        //遍历旧参数数组中的参数，如果新参数list中存在同名参数，将该参数加入到result里，同时删除list中的同名参数
        for (Parameter oldParameter : oldParameters) {
            Iterator<Parameter> iterator = newParameterList.listIterator();
            while (iterator.hasNext()) {
                Parameter newParameter = iterator.next();
                if (ComparatorUtils.equals(oldParameter.getName(), newParameter.getName())) {
                    result[i++] = oldParameter;
                    iterator.remove();
                    break;
                }
            }
        }
        //将新参数list中的剩余参数添加到result中
        System.arraycopy(newParameterList.toArray(new Parameter[0]), 0, result, i, newParameterList.size());
        return result;
    }
}
