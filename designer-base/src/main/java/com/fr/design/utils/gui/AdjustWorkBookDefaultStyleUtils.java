package com.fr.design.utils.gui;

import com.fr.base.Style;
import com.fr.base.background.ColorBackground;
import com.fr.design.base.mode.DesignModeContext;
import com.fr.report.cell.CellElement;
import com.fr.report.cell.FloatElement;

import java.awt.Color;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/9/6
 */
public class AdjustWorkBookDefaultStyleUtils {

    private static final Color TEMPLATE_BACKGROUND = new Color(16, 11, 43);
    private static final Color CELL_ELEMENT_BORDER = new Color(110, 110, 110);
    private static final Color CELL_ELEMENT_FONT_FOREGROUND = Color.WHITE;

    private static Color currentStoryBack = null;

    public static void setCurrentStoryBack(Color color) {
        currentStoryBack = color;
    }

    private static Color getCurrentStoryBack() {
        return currentStoryBack == null ? TEMPLATE_BACKGROUND : currentStoryBack;
    }

    public static void adjustCellElement(CellElement cellElement) {
        if (DesignModeContext.isDuchampMode()) {
            Style style = cellElement.getStyle();
            style = adjustCellElement(style);
            cellElement.setStyle(style);
        }
    }

    public static Style adjustCellElement(Style style) {
        if (DesignModeContext.isDuchampMode()) {
            style = style.deriveFRFont(style.getFRFont().applyForeground(CELL_ELEMENT_FONT_FOREGROUND));
            style = style.deriveBorder(0, CELL_ELEMENT_BORDER,
                    0, CELL_ELEMENT_BORDER,
                    0, CELL_ELEMENT_BORDER,
                    0, CELL_ELEMENT_BORDER);
        }
        return style;
    }

    public static Color adjustCellElementFontForeground(Color color) {
        return DesignModeContext.isDuchampMode() ? CELL_ELEMENT_FONT_FOREGROUND : color;
    }

    public static void adjustFloatElement(FloatElement floatElement) {
        if (DesignModeContext.isDuchampMode()) {
            Style style = floatElement.getStyle();
            style = style.deriveBackground(ColorBackground.getInstance(getCurrentStoryBack()));
            style = style.deriveFRFont(style.getFRFont().applyForeground(Color.WHITE));
            floatElement.setStyle(style);
        }
    }

    public static Color adjustBack(Color color) {
        return DesignModeContext.isDuchampMode() ? getCurrentStoryBack() : color;
    }

}
