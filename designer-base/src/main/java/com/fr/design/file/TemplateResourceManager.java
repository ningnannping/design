package com.fr.design.file;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.file.impl.DefaultTemplateResource;
import com.fr.design.fun.LocalResourceProvider;
import com.fr.design.mainframe.DesignerFrameFileDealerPane;
import com.fr.design.ui.util.UIUtil;
import com.fr.file.filetree.FileNodes;
import com.fr.file.filetree.LocalFileNodes;
import com.fr.plugin.injectable.PluginModule;
import com.fr.plugin.manage.PluginFilter;
import com.fr.plugin.observer.PluginEvent;
import com.fr.plugin.observer.PluginEventListener;
import com.fr.plugin.observer.PluginEventType;
import com.fr.plugin.observer.PluginListenerRegistration;
import com.fr.workspace.WorkContext;
import com.fr.workspace.engine.base.FineObjectPool;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/23
 */
public class TemplateResourceManager {

    private static final TemplateResource DEFAULT_OPERATION = new DefaultTemplateResource();

    private static TemplateResource OPERATION = DEFAULT_OPERATION;

    static  {
        PluginFilter filter = pluginContext -> pluginContext.contain(PluginModule.ExtraDesign, LocalResourceProvider.XML_TAG);

        PluginListenerRegistration.getInstance().listen(PluginEventType.AfterStop, new PluginEventListener() {
            @Override
            public void on(PluginEvent event) {
                registerOperation(new DefaultTemplateResource());
                FineObjectPool.getInstance().getLocalPool().put(FileNodes.class, new LocalFileNodes());
                UIUtil.invokeLaterIfNeeded(() -> DesignerFrameFileDealerPane.getInstance().refresh());
            }
        }, filter);

        PluginListenerRegistration.getInstance().listen(PluginEventType.AfterRun, new PluginEventListener() {
            @Override
            public void on(PluginEvent event) {
                LocalResourceProvider provider = ExtraDesignClassManager.getInstance().getSingle(LocalResourceProvider.XML_TAG);
                if (provider != null && WorkContext.getCurrent().isLocal()) {
                    registerOperation(provider.createResourceOperation());
                    FineObjectPool.getInstance().getLocalPool().put(FileNodes.class, provider.createFileNodes());
                    UIUtil.invokeLaterIfNeeded(() -> DesignerFrameFileDealerPane.getInstance().refresh());
                }
            }
        }, filter);
    }

    private static void registerOperation(TemplateResource operation) {
        OPERATION = operation;
    }

    public static TemplateResource getResource() {
        if (OPERATION == null) {
            return DEFAULT_OPERATION;
        }
        return OPERATION;
    }

}
