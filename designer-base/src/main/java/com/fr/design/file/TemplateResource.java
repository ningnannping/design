package com.fr.design.file;

import com.fr.common.annotations.Open;
import com.fr.file.FILE;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 模板资源操作，可操作模板及模板目录
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/12/23
 */
@Open
public interface TemplateResource {

    /**
     * 读取模板
     * @param path
     * @return
     * @throws Exception
     */
    InputStream readTemplate(String path) throws Exception;

    /**
     * 保存模板
     * @param file
     * @return
     * @throws Exception
     */
    OutputStream saveTemplate(FILE file) throws Exception;

    /**
     * 删除某个目录/某个模板
     * @param  file
     * @return
     */
    boolean delete(FILE file);

    /**
     * 关闭模板
     * @param path
     * @return
     */
    boolean closeTemplate(String path);

    /**
     * 重命名模板/目录
     * @param from
     * @param to
     * @return
     */
    boolean rename(String from, String to);

    /**
     * 模板/目录是否存在
     * @param path
     * @return
     */
    boolean exist(String path);

    /**
     * 创建目录
     * @param path
     * @return
     */
    boolean mkdir(String path);


}
