package com.fr.design.mainframe.mobile.provider;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.fun.impl.AbstractMobileParamUIProvider;
import com.fr.design.mainframe.mobile.ui.EmptyMobileParamDefinePane;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.report.fun.MobileParamStyleProvider;
import com.fr.report.mobile.EmptyMobileParamStyle;

/**
 * 作为MobileParamStyleProvider接口实现兼容转换层
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class EmptyMobileParamUIProvider extends AbstractMobileParamUIProvider {

    private MobileParamStyleProvider styleProvider;

    public EmptyMobileParamUIProvider(MobileParamStyleProvider styleProvider) {
        this.styleProvider = styleProvider;
    }

    @Override
    public Class<? extends MobileParamStyle> classForMobileParamStyle() {
        return EmptyMobileParamStyle.class;
    }

    @Override
    public Class<? extends BasicBeanPane<MobileParamStyle>> classForMobileParamAppearance() {
        return EmptyMobileParamDefinePane.class;
    }

    @Override
    public String displayName() {
        return styleProvider.descriptor();
    }

    public MobileParamStyleProvider getStyleProvider() {
        return styleProvider;
    }
}
