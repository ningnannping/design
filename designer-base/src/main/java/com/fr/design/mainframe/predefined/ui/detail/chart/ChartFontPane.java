package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.base.BaseUtils;
import com.fr.base.FRContext;
import com.fr.base.Utils;
import com.fr.design.constants.LayoutConstants;
import com.fr.design.dialog.BasicPane;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ibutton.UIColorButton;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.FRFont;
import com.fr.general.GeneralUtils;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartFontPane extends BasicPane {

    public static final int FONT_START = 6;
    public static final int FONT_END = 72;
    private UIComboBox fontNameComboBox;
    private UIComboBox fontSizeComboBox;
    private UIToggleButton bold;
    private UIToggleButton italic;
    private UIColorButton fontColor;
    private static Integer[] FONT_SIZES = new Integer[FONT_END - FONT_START + 1];

    static {
        for (int i = FONT_START; i <= FONT_END; i++) {
            FONT_SIZES[i - FONT_START] = i;
        }
    }

    public ChartFontPane() {
        initState();
        initComponents();
    }

    private void initState() {
        fontNameComboBox = new UIComboBox(Utils.getAvailableFontFamilyNames4Report());
        fontSizeComboBox = new UIComboBox(FONT_SIZES);
        bold = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/bold.png"));
        italic = new UIToggleButton(BaseUtils.readIcon("/com/fr/design/images/m_format/cellstyle/italic.png"));
        fontColor = new UIColorButton();
    }

    protected void initComponents() {
        Component[] components = new Component[]{
                fontColor, italic, bold
        };
        JPanel buttonPane = new JPanel(new BorderLayout());
        buttonPane.add(fontSizeComboBox, BorderLayout.CENTER);
        buttonPane.add(GUICoreUtils.createFlowPane(components, FlowLayout.LEFT, LayoutConstants.HGAP_LARGE), BorderLayout.EAST);

        this.setLayout(new BorderLayout());
        this.add(getContentPane(buttonPane), BorderLayout.CENTER);

        populate(FRContext.getDefaultValues().getFRFont());
    }

    protected JPanel getContentPane(JPanel buttonPane) {
        double e = 155;
        double p = TableLayout.PREFERRED;
        double[] rows = {p, p, p};
        double[] columnSize = {p, e};
        UILabel text = new UILabel(getUILabelText(), SwingConstants.LEFT);
        Component[][] components = {
                new Component[]{null, null},
                new Component[]{text, fontNameComboBox},
                new Component[]{null, buttonPane}
        };

        return TableLayoutHelper.createGapTableLayoutPane(components, rows, columnSize, 20, LayoutConstants.VGAP_LARGE);
    }

    public String getUILabelText() {
        return Toolkit.i18nText("Fine-Design_Chart_Character");
    }

    public String title4PopupWindow() {
        return null;
    }

    public void populate(FRFont frFont) {
        UIObserverListener listener = fontNameComboBox == null ? null : fontNameComboBox.getUiObserverListener();
        removeAllComboBoxListener();

        if (frFont != null) {
            fontNameComboBox.setSelectedItem(frFont.getFamily());
            bold.setSelected(frFont.isBold());
            italic.setSelected(frFont.isItalic());
            populateFontSize(frFont);
            if (fontColor != null) {
                fontColor.setColor(frFont.getForeground());
            }
        }

        //更新结束后，注册监听器
        registerAllComboBoxListener(listener);
    }

    private void populateFontSize(FRFont frFont) {
        if (fontSizeComboBox != null) {
            fontSizeComboBox.setSelectedItem(frFont.getSize());
        }
    }

    private void removeAllComboBoxListener() {
        fontNameComboBox.removeChangeListener();
        fontSizeComboBox.removeChangeListener();
    }

    private void registerAllComboBoxListener(UIObserverListener listener) {
        fontNameComboBox.registerChangeListener(listener);
        fontSizeComboBox.registerChangeListener(listener);
    }

    /**
     * 更新字
     *
     * @return 更新字
     */
    public FRFont update() {
        String name = GeneralUtils.objectToString(fontNameComboBox.getSelectedItem());

        return FRFont.getInstance(name, updateFontStyle(), updateFontSize(), fontColor.getColor());
    }

    private int updateFontStyle() {
        int style = Font.PLAIN;
        if (bold.isSelected() && !italic.isSelected()) {
            style = Font.BOLD;
        } else if (!bold.isSelected() && italic.isSelected()) {
            style = Font.ITALIC;
        } else if (bold.isSelected() && italic.isSelected()) {
            style = 3;
        }

        return style;
    }

    private float updateFontSize() {
        return Float.parseFloat(GeneralUtils.objectToString(fontSizeComboBox.getSelectedItem()));
    }
}
