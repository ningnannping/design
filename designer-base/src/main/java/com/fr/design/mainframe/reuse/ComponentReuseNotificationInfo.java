package com.fr.design.mainframe.reuse;

import com.fr.design.DesignerEnvManager;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;

/**
 * Created by kerry on 5/8/21
 */
public class ComponentReuseNotificationInfo implements XMLable {
    public static final String XML_TAG = "ComponentReuseNotificationInfo";

    private static final ComponentReuseNotificationInfo INSTANCE = new ComponentReuseNotificationInfo();

    public static ComponentReuseNotificationInfo getInstance() {
        return INSTANCE;
    }

    private long lastNotifyTime = 0;

    private int notifiedNumber = 0;

    private boolean clickedWidgetLib = false;

    private long lastGuidePopUpTime = 0;

    private String historyCreatedReuses = "[]";

    public long getLastNotifyTime() {
        return lastNotifyTime;
    }

    public void setLastNotifyTime(long lastNotifyTime) {
        this.lastNotifyTime = lastNotifyTime;
    }

    public int getNotifiedNumber() {
        return notifiedNumber;
    }

    public void setNotifiedNumber(int notifiedNumber) {
        this.notifiedNumber = notifiedNumber;
    }

    public boolean isClickedWidgetLib() {
        return clickedWidgetLib;
    }

    public void setClickedWidgetLib(boolean clickedWidgetLib) {
        this.clickedWidgetLib = clickedWidgetLib;
    }

    public long getLastGuidePopUpTime() {
        return lastGuidePopUpTime;
    }

    public void setLastGuidePopUpTime(long lastGuidePopUpTime) {
        this.lastGuidePopUpTime = lastGuidePopUpTime;
    }

    public void updateLastGuidePopUpTime() {
        this.setLastGuidePopUpTime(System.currentTimeMillis());
        DesignerEnvManager.getEnvManager().saveXMLFile();
    }

    public String getHistoryCreatedReuses() {
        return historyCreatedReuses;
    }

    public void setHistoryCreatedReuses(String historyCreatedReuses) {
        this.historyCreatedReuses = historyCreatedReuses;
    }

    @Override
    public void readXML(XMLableReader reader) {
        this.setLastNotifyTime(reader.getAttrAsLong("lastNotifyTime", 0L));
        this.setNotifiedNumber(reader.getAttrAsInt("notifiedNumber", 0));
        this.setClickedWidgetLib(reader.getAttrAsBoolean("clickedWidgetLib", false));
        this.setLastGuidePopUpTime(reader.getAttrAsLong("lastGuidePopUpTime", 0L));
        this.setHistoryCreatedReuses(reader.getAttrAsString("historyCreatedReuses", "[]"));
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {
        writer.startTAG("ComponentReuseNotificationInfo");
        writer.attr("lastNotifyTime", this.lastNotifyTime)
                .attr("notifiedNumber", this.notifiedNumber)
                .attr("clickedWidgetLib", this.clickedWidgetLib)
                .attr("lastGuidePopUpTime", this.lastGuidePopUpTime)
                .attr("historyCreatedReuses", this.historyCreatedReuses);;
        writer.end();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
