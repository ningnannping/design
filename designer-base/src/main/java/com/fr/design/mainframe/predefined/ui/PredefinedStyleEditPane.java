package com.fr.design.mainframe.predefined.ui;

import com.fr.base.ChartColorMatching;
import com.fr.base.ChartPreStyleConfig;
import com.fr.base.Style;
import com.fr.base.background.ColorBackground;
import com.fr.config.predefined.ColorFillStyle;
import com.fr.config.predefined.PredefinedCellStyle;
import com.fr.config.predefined.PredefinedCellStyleConfig;
import com.fr.config.predefined.PredefinedColorStyle;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.config.predefined.PredefinedStyleConfig;
import com.fr.config.ServerPreferenceConfig;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.frpane.UITabbedPane;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.detail.ChartStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.ColorFillStylePane;
import com.fr.design.mainframe.predefined.ui.detail.PredefinedBackgroundSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.ComponentStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.CellStyleListControlPane;
import com.fr.design.mainframe.predefined.ui.preview.PredefinedStylePreviewPane;
import com.fr.design.ui.util.UIUtil;
import com.fr.design.utils.DesignUtils;
import com.fr.general.FRFont;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kerry on 2020-08-26
 */
public class PredefinedStyleEditPane extends AbstractAttrNoScrollPane {
    private static final Color TIP_COLOR = Color.decode("#8F8F92");

    private PredefinedStylePreviewPane previewPane;
    private UITextField styleNameField;
    private PredefinedBackgroundSettingPane backgroundSettingPane;
    private CellStyleListControlPane cellStyleSettingPane;
    private ComponentStyleSettingPane componentStyleSettingPane;
    private ChartStyleSettingPane chartStyleSettingPane;
    private PredefinedStyleSelectPane selectPane;
    private ColorFillStylePane colorFillStylePane;
    private boolean isPopulating = false;
    private UITabbedPane uiTabbedPane;

    private boolean isLightMode = true;

    @Override
    protected JPanel createContentPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.add(createLeftPane(), BorderLayout.WEST);
        jPanel.add(createRightPane(), BorderLayout.CENTER);

        this.addAttributeChangeListener(new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                if (!isPopulating) {
                    valueChangeAction();
                }
            }
        });
        return jPanel;
    }

    public void valueChangeAction() {
        boolean displayFormBackground = backgroundSettingPane.currentFormBackground() || uiTabbedPane.getSelectedIndex() == 3;
        previewPane.refresh(this.update(), displayFormBackground);
    }

    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Edit");
    }

    private PredefinedStyleEditPane(PredefinedStyleSelectPane selectPane, boolean newEditPane) {
        this.selectPane = selectPane;
        this.styleNameField.setEnabled(newEditPane);
    }

    public static PredefinedStyleEditPane createEditPane(PredefinedStyleSelectPane selectPane) {
        return new PredefinedStyleEditPane(selectPane, false);
    }

    public static PredefinedStyleEditPane createNewEditPane(PredefinedStyleSelectPane selectPane) {
        return new PredefinedStyleEditPane(selectPane, true);
    }


    private JPanel createLeftPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel titlePane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Overall_Preview"));
        previewPane = new PredefinedStylePreviewPane();
        previewPane.setPreferredSize(new Dimension(517, 500));

        titlePane.add(previewPane);
        jPanel.add(titlePane, BorderLayout.CENTER);
        return jPanel;
    }

    private JPanel createRightPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel styleNamePane = createStyleNamePane();
        jPanel.add(styleNamePane, BorderLayout.NORTH);

        JPanel basicSettingPane = createBasicSettingPane();
        jPanel.add(basicSettingPane, BorderLayout.CENTER);

        JPanel customDetailPane = createCustomDetailPane();
        jPanel.add(customDetailPane, BorderLayout.SOUTH);
        return jPanel;
    }

    private JPanel createStyleNamePane() {
        JPanel jPanel = FRGUIPaneFactory.createBoxFlowInnerContainer_S_Pane(5, 26, 8);
        jPanel.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Style_Name")));
        this.styleNameField = new UITextField();
        this.styleNameField.setPreferredSize(new Dimension(160, 20));
        jPanel.add(this.styleNameField);
        return jPanel;
    }

    private JPanel createBasicSettingPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel titlePane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Basic_Setting"));
        JPanel contentPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        colorFillStylePane = new ColorFillStylePane();
        contentPane.add(colorFillStylePane);
        titlePane.add(contentPane);
        jPanel.add(titlePane, BorderLayout.CENTER);
        titlePane.setSize(new Dimension(348, 157));
        return jPanel;
    }

    private JPanel createCustomDetailPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel titlePane = FRGUIPaneFactory.createTitledBorderPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Custom_Detail"));
        titlePane.setLayout(FRGUIPaneFactory.createLeftZeroLayout());
        jPanel.add(titlePane, BorderLayout.CENTER);
        uiTabbedPane = new UITabbedPane();
        uiTabbedPane.addTab(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Template_Background"), createTemplateBackgroundSettingPane());
        uiTabbedPane.addTab(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Cell_Style"), createCellStyleSettingPane());
        uiTabbedPane.addTab(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Chart_Style"), createChartStyleSettingPane());
        uiTabbedPane.addTab(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Component_Style"), createComponentStyleSettingPane());
        uiTabbedPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                valueChangeAction();
            }
        });
        titlePane.add(uiTabbedPane);
        uiTabbedPane.setPreferredSize(new Dimension(323, 298));
        titlePane.setPreferredSize(new Dimension(333, 320));
        return jPanel;
    }


    private JPanel createTemplateBackgroundSettingPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
        this.backgroundSettingPane = new PredefinedBackgroundSettingPane();
        jPanel.setPreferredSize(new Dimension(309, 248));
        UIScrollPane scrollPane = new UIScrollPane(this.backgroundSettingPane);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        jPanel.add(new UIScrollPane(this.backgroundSettingPane));
        return jPanel;
    }

    private JPanel createCellStyleSettingPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 5));
        this.cellStyleSettingPane = new CellStyleListControlPane();
        this.cellStyleSettingPane.registerAttrChangeListener(new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                valueChangeAction();
            }
        });
        jPanel.add(this.cellStyleSettingPane);
        return jPanel;
    }


    private JPanel createChartStyleSettingPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
        this.chartStyleSettingPane = new ChartStyleSettingPane();
        jPanel.add(this.chartStyleSettingPane);
        return jPanel;
    }


    private JPanel createComponentStyleSettingPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setLayout(new BorderLayout(0, 5));
        jPanel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
        this.componentStyleSettingPane = new ComponentStyleSettingPane();
        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_ComponentStyle_Setting_Tip"));
        label.setForeground(TIP_COLOR);
        jPanel.add(label, BorderLayout.NORTH);
        jPanel.add(this.componentStyleSettingPane, BorderLayout.CENTER);
        return jPanel;
    }


    public void populate(PredefinedStyle previewObject) {
        isPopulating = true;
        isLightMode = previewObject.isLightMode();
        styleNameField.setText(previewObject.getStyleName());
        this.backgroundSettingPane.populateBean(previewObject.getPredefinedBackground());
        this.cellStyleSettingPane.populateBean(previewObject.getCellStyleConfig());
        this.componentStyleSettingPane.populateBean(previewObject.getComponentStyle());
        this.colorFillStylePane.populateBean(previewObject.getPredefinedColorStyle());
        this.chartStyleSettingPane.populateBean(previewObject.getPredefinedChartStyle());
        previewPane.refresh(previewObject);
        isPopulating = false;
    }

    public PredefinedStyle update() {
        PredefinedStyle predefinedStyle = new PredefinedStyle();
        predefinedStyle.setLightMode(isLightMode);
        predefinedStyle.setStyleName(this.styleNameField.getText());
        PredefinedCellStyleConfig cellStyleConfig = this.cellStyleSettingPane.updateBean();
        predefinedStyle.setCellStyleConfig(cellStyleConfig);

        predefinedStyle.setPredefinedBackground(this.backgroundSettingPane.updateBean());
        predefinedStyle.setComponentStyle(this.componentStyleSettingPane.updateBean());

        PredefinedColorStyle colorStyle = this.colorFillStylePane.update();
        updateCellStyleByColorStyle(colorStyle, cellStyleConfig);
        predefinedStyle.setPredefinedColorStyle(colorStyle);
        predefinedStyle.setPredefinedChartStyle(this.chartStyleSettingPane.updateBean());
        return predefinedStyle;
    }

    private void updateCellStyleByColorStyle(PredefinedColorStyle colorStyle, PredefinedCellStyleConfig cellStyleConfig) {
        PredefinedCellStyle headerStyle = cellStyleConfig.getStyle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Header"));
        PredefinedCellStyle highlightStyle = cellStyleConfig.getStyle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Highlight_Text"));
        ColorFillStyle colorFillStyle = colorStyle.getColorFillStyle();
        List<Color> colorList = new ArrayList<>();
        if (colorFillStyle == null || colorFillStyle.getColorList().size() == 0){
            ChartPreStyleConfig config = ChartPreStyleConfig.getInstance();
            String defaultName = config.getCurrentStyle();
            ChartColorMatching defaultStyle = (ChartColorMatching) config.getPreStyle(defaultName);
            if (defaultStyle != null) {
                colorList = defaultStyle.getColorList();
            }
        } else {
            colorList = colorFillStyle.getColorList();
        }
        if (colorList.size() < 2) {
            return;
        }
        if (headerStyle != null) {
            Style style = headerStyle.getStyle();
            Color color = colorList.get(0);
            headerStyle.setStyle(style.deriveBackground(ColorBackground.getInstance(color)));
        }
        if (highlightStyle != null) {
            Style style = highlightStyle.getStyle();
            Color color = colorList.get(1);
            FRFont font = style.getFRFont();
            font.setForeground(color);
            highlightStyle.setStyle(style.deriveFRFont(font));
        }
    }

    public boolean saveStyle() {
        PredefinedStyle previewObject;
        try {
            previewObject = update();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return false;
        }
        if (this.styleNameField.isEnabled() && !validateRepeat(previewObject.getStyleName())) {
            return false;
        }
        if (!saveStyle(previewObject)) {
            return false;
        }
        HistoryTemplateListCache.getInstance().repaintCurrentEditingTemplate();
        return true;
    }

    private boolean saveStyle(PredefinedStyle previewObject) {
        PredefinedStyleConfig config = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig();
        if (StringUtils.isEmpty(previewObject.getStyleName())) {
            FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(PredefinedStyleEditPane.this),
                    com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Name_Cannot_Empty"));

            return false;
        }
        config.add(previewObject);
        PredefinedStyleConfig sortedConfig = resortConfigStyles(previewObject, config);
//        Configurations.modify(new WorkerFacade(ServerPreferenceConfig.class) {
//            @Override
//            public void run() {
//                ServerPreferenceConfig.getInstance().setPreferenceStyleConfig(sortedConfig);
//            }
//        });
        selectPane.refreshPane();
        return true;
    }

    public void saveAsNewStyle(String styleName) {
        PredefinedStyle previewObject;
        try {
            previewObject = update();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return;
        }
        previewObject.setStyleName(styleName);
        if (validateRepeat(styleName)) {
            saveStyle(previewObject);
        }
    }

    private boolean validateRepeat(String styleName) {
        PredefinedStyleConfig config = ServerPreferenceConfig.getInstance().getPreferenceStyleConfig();
        if (config.getStyle(styleName) != null) {
            FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(PredefinedStyleEditPane.this),
                    com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Name_Repeat"));

            return false;
        }
        return true;
    }

    private PredefinedStyleConfig resortConfigStyles(PredefinedStyle priorityStyle, PredefinedStyleConfig config){
        PredefinedStyleConfig sortedConfig = new PredefinedStyleConfig();
        PredefinedStyle defaultStyle = config.getDefaultPredefinedStyle();
        if (defaultStyle != null) {
            sortedConfig.add(defaultStyle);
            config.removeStyle(defaultStyle.getStyleName());
            sortedConfig.setDefaultPredefinedStyle(defaultStyle.getStyleName());
        }
        if (priorityStyle != null && !priorityStyle.isDefaultStyle()) {
            sortedConfig.add(priorityStyle);
            config.removeStyle(priorityStyle.getStyleName());
        }
        Iterator<PredefinedStyle> iterator = config.getPredefinedStyleIterator();
        while (iterator.hasNext()) {
            PredefinedStyle entry = iterator.next();
            sortedConfig.add(entry);
        }
        sortedConfig.setCompatibleStyleName(config.getCompatibleStyleName());
        return sortedConfig;
    }




}
