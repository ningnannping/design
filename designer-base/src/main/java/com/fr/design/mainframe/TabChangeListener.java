package com.fr.design.mainframe;

/**
 * tab切换时对当前打开的模版处理些事件
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/7/22
 */
public interface TabChangeListener {

    void fireTabChange();
}
