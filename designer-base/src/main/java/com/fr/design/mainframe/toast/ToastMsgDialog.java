package com.fr.design.mainframe.toast;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.dialog.UIDialog;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.module.ModuleContext;

import javax.swing.JPanel;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by kerry on 4/29/21
 */
public class ToastMsgDialog extends UIDialog {
    private static final int MIN_HEIGHT = 36;
    private static final String TOAST_MSG_TIMER = "TOAST_MSG_TIMER";
    private ScheduledExecutorService TIMER;
    private int hide_height = 0;
    private JPanel contentPane;
    private boolean show = false;

    public ToastMsgDialog(Frame parent, JPanel panel) {
        super(parent);
        init(panel);
    }

    public ToastMsgDialog(Dialog parent, JPanel panel) {
        super(parent);
        init(panel);
    }

    private void init(JPanel panel) {
        setFocusable(false);
        setAutoRequestFocus(false);
        setUndecorated(true);
        contentPane = panel;
        initComponent();
    }

    private void initComponent() {
        this.getContentPane().setLayout(null);
        this.getContentPane().add(contentPane);
        Dimension dimension = calculatePreferSize();
        hide_height = dimension.height;
        setSize(new Dimension(dimension.width, 0));
        contentPane.setSize(dimension);
        setRelativeLocation(dimension);
        addMouseEvent(contentPane);
    }

    private void setRelativeLocation(Dimension dimension) {
        DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
        int positionX = designerFrame.getLocationOnScreen().x + (designerFrame.getWidth() - dimension.width) / 2;
        int positionY = designerFrame.getContentFrame().getLocationOnScreen().y + 10;
        this.setLocation(positionX, positionY);
    }

    private Dimension calculatePreferSize() {
        Dimension contentDimension = contentPane.getPreferredSize();
        int height = Math.max(MIN_HEIGHT, contentDimension.height);
        return new Dimension(contentDimension.width, height);
    }


    public void display(JPanel outerJPanel) {
        show = true;
        outerJPanel.setLocation(0, -hide_height);
        ScheduledExecutorService TIP_TOOL_TIMER = createToastScheduleExecutorService();
        TIP_TOOL_TIMER.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                Point point = outerJPanel.getLocation();
                if (point.y >= 0) {
                    TIP_TOOL_TIMER.shutdown();
                    disappear(outerJPanel);
                }
                int showDistance = 5 + point.y < 0 ? 5 : -point.y;
                outerJPanel.setLocation(point.x, point.y + showDistance);
                Dimension dimension = ToastMsgDialog.this.getSize();
                ToastMsgDialog.this.setSize(new Dimension(dimension.width, dimension.height + showDistance));
            }
        }, 0, 50, TimeUnit.MILLISECONDS);

    }

    private void disappear(JPanel outerJPanel) {
        TIMER = createToastScheduleExecutorService();
        TIMER.schedule(new Runnable() {
            @Override
            public void run() {
                ScheduledExecutorService TIP_TOOL_TIMER = createToastScheduleExecutorService();
                TIP_TOOL_TIMER.scheduleAtFixedRate(new Runnable() {
                    @Override
                    public void run() {
                        Point point = outerJPanel.getLocation();
                        if (point.y <= -hide_height) {
                            TIP_TOOL_TIMER.shutdown();
                            ToastMsgDialog.this.setVisible(false);
                            ToastMsgDialog.this.dispose();
                            ToastMsgDialog.this.show = false;
                        }
                        outerJPanel.setLocation(point.x, point.y - 5);
                        Dimension dimension = ToastMsgDialog.this.getSize();
                        ToastMsgDialog.this.setSize(new Dimension(dimension.width, dimension.height - 5));
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);

            }
        }, 5000, TimeUnit.MILLISECONDS);
    }

    private ScheduledExecutorService createToastScheduleExecutorService() {
        return ModuleContext.getExecutor().newSingleThreadScheduledExecutor(new NamedThreadFactory(TOAST_MSG_TIMER));
    }

    private void addMouseEvent(JPanel jPanel) {
        jPanel.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {

            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                TIMER.shutdownNow();
            }

            @Override
            public void mouseExited(MouseEvent e) {
                disappear(jPanel);
            }
        });
    }


    @Override
    public void checkValid() throws Exception {
    }

    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            display(contentPane);
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public boolean isShow() {
        return show;
    }
}