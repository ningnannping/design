package com.fr.design.mainframe;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Composite;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.LayoutManager;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.Timer;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/12
 */
public class TransparentPane extends JComponent implements ActionListener {

    private UILabel label;
    private AlphaComposite composite = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f);
    private volatile boolean mIsRunning;
    private volatile boolean mIsFadingOut;
    private Timer mTimer;
    private int mAngle;
    private int mFadeCount;
    private int mFadeLimit = 15;
    private int lines = 12;
    private int maxAngle = 360;
    private int angleAdd = 30;
    private double prec = 0.56;

    public TransparentPane() {

        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // do nothing
            }
        });

        setLayout(getCoverLayout());
        setBackground(null);
        setOpaque(false);
        label = new UILabel(Toolkit.i18nText("Fine-Design_Saving_Template_Tip"));
        add(label);
    }

    protected LayoutManager getCoverLayout() {
        return new LayoutManager() {

            @Override
            public void removeLayoutComponent(Component comp) {
            }

            @Override
            public Dimension preferredLayoutSize(Container parent) {
                return parent.getPreferredSize();
            }

            @Override
            public Dimension minimumLayoutSize(Container parent) {
                return null;
            }

            @Override
            public void layoutContainer(Container parent) {
                int width = parent.getParent().getWidth();
                int height = parent.getParent().getHeight();
                int labelWidth = label.getPreferredSize().width;
                int labelHeight = label.getPreferredSize().height;
                int labelX = (width - labelWidth) / 2;
                int labelY = (int) ((height - labelHeight) * prec);
                label.setBounds(labelX, labelY, labelWidth, labelHeight);
            }

            @Override
            public void addLayoutComponent(String name, Component comp) {
            }
        };
    }


    @Override
    public void paint(Graphics g) {
        int w = this.getWidth();
        int h = this.getHeight();
        super.paint(g);
        if (!mIsRunning) {
            return;
        }
        Graphics2D g2 = (Graphics2D) g.create();
        float fade = (float) mFadeCount / (float) mFadeLimit;
        Composite urComposite = g2.getComposite();
        g2.setComposite(composite);
        g2.fillRect(0, 0, w, h);
        g2.setComposite(urComposite);
        int s = Math.min(w, h) / 50;
        int cx = w / 2;
        int cy = h / 2;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2.setStroke(new BasicStroke(s / 4, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        g2.setPaint(Color.BLACK);
        g2.rotate(Math.PI * mAngle / 180, cx, cy);
        for (int i = 0; i < lines; i++) {
            float scale = (11.0f - (float) i) / 11.0f;
            g2.drawLine(cx + s, cy, cx + s * 2, cy);
            g2.rotate(-Math.PI / 6, cx, cy);
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, scale * fade));
        }
        g2.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (mIsRunning) {
            repaint();
            mAngle += angleAdd;
            if (mAngle >= maxAngle) {
                mAngle = 0;
            }
            if (mIsFadingOut) {
                if (--mFadeCount == 0) {
                    mIsRunning = false;
                    mTimer.stop();
                }
            } else if (mFadeCount < mFadeLimit) {
                mFadeCount++;
            }
        }
    }

    public void start() {
        if (mIsRunning) {
            return;
        }
        mIsRunning = true;
        mIsFadingOut = false;
        mFadeCount = 0;
        int fps = 24;
        int tick = 1000 / fps;
        mTimer = new Timer(tick, this);
        mTimer.start();
    }

    public void stop() {
        mIsRunning = false;
        mIsFadingOut = true;
    }

}
