package com.fr.design.mainframe.predefined.ui.dialog;

import com.fr.design.mainframe.predefined.ui.ServerPredefinedStylePane;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.gui.GUICoreUtils;

import javax.swing.JDialog;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by kerry on 2020-08-26
 */
public class ServerPredefinedStyleDialog extends JDialog  {


    public ServerPredefinedStyleDialog(Window parent, ServerPredefinedStylePane contentPane) {
        super(parent, ModalityType.APPLICATION_MODAL);
        this.setTitle(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Predefined_Server_Style"));
        this.setResizable(false);
        JPanel defaultPane = FRGUIPaneFactory.createBorderLayout_L_Pane();
        this.setContentPane(defaultPane);

        UIButton settingBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Predefined_Set_Default"));
        settingBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                contentPane.update();
                dialogExit();
            }
        });

        UIButton cancelBtn = new UIButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Cancel"));
        cancelBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialogExit();
            }
        });
        JPanel buttonPanel = FRGUIPaneFactory.createRightFlowInnerContainer_S_Pane();
        buttonPanel.add(settingBtn);
        buttonPanel.add(cancelBtn);

        defaultPane.add(contentPane, BorderLayout.CENTER);
        defaultPane.add(buttonPanel, BorderLayout.SOUTH);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dialogExit();
            }
        });


        this.setSize(new Dimension(660, 600));
        GUICoreUtils.centerWindow(this);
    }

    public void dialogExit(){
        this.dispose();
    }
}
