package com.fr.design.mainframe.predefined.ui.detail;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.MultiTabPane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartAxisStylePane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartBackgroundStylePane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartDataSheetStylePane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartLabelStylePane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartLegendStylePane;
import com.fr.design.mainframe.predefined.ui.detail.chart.ChartTitleStylePane;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartStyleSettingPane extends MultiTabPane<PredefinedChartStyle> {

    private ChartTitleStylePane chartTitleStylePane;
    private ChartLegendStylePane chartLegendStylePane;
    private ChartLabelStylePane chartLabelPane;
    private ChartAxisStylePane chartAxisStylePane;
    private ChartDataSheetStylePane chartDataSheetStylePane;
    private ChartBackgroundStylePane chartBackgroundStylePane;

    public ChartStyleSettingPane() {
    }

    @Override
    protected void initLayout() {
        super.initLayout();
    }

    @Override
    protected List<BasicPane> initPaneList() {
        this.chartTitleStylePane = new ChartTitleStylePane();
        this.chartLegendStylePane = new ChartLegendStylePane();
        this.chartLabelPane = new ChartLabelStylePane();
        this.chartAxisStylePane = new ChartAxisStylePane();
        this.chartDataSheetStylePane = new ChartDataSheetStylePane();
        this.chartBackgroundStylePane = new ChartBackgroundStylePane();
        paneList = new ArrayList<>();
        paneList.add(this.chartTitleStylePane);
        paneList.add(this.chartLegendStylePane);
        paneList.add(this.chartLabelPane);
        paneList.add(this.chartAxisStylePane);
        paneList.add(this.chartDataSheetStylePane);
        paneList.add(this.chartBackgroundStylePane);
        return paneList;
    }

    @Override
    public void populateBean(PredefinedChartStyle ob) {
        chartTitleStylePane.populate(ob);
        chartLegendStylePane.populate(ob);
        chartLabelPane.populate(ob);
        chartAxisStylePane.populate(ob);
        chartDataSheetStylePane.populate(ob);
        chartBackgroundStylePane.populate(ob);
    }

    @Override
    public void updateBean(PredefinedChartStyle ob) {

    }


    @Override
    public PredefinedChartStyle updateBean() {
        PredefinedChartStyle chartStyle = new PredefinedChartStyle();
        chartTitleStylePane.update(chartStyle);
        chartLegendStylePane.update(chartStyle);
        chartLabelPane.update(chartStyle);
        chartAxisStylePane.update(chartStyle);
        chartDataSheetStylePane.update(chartStyle);
        chartBackgroundStylePane.update(chartStyle);
        return chartStyle;
    }


    @Override
    public boolean accept(Object ob) {
        return false;
    }

    @Override
    public void reset() {

    }
}
