/*
 * Copyright(c) 2001-2011, FineReport Inc, All Rights Reserved.
 */
package com.fr.design.mainframe;

import com.fr.base.iofile.attr.DesignBanCopyAttrMark;
import com.fr.design.base.mode.DesignModeContext;
import com.fr.design.base.mode.DesignerMode;
import com.fr.design.data.BasicTableDataTreePane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.event.TargetModifiedEvent;
import com.fr.design.event.TargetModifiedListener;
import com.fr.design.utils.LoadingUtils;

import javax.swing.JComponent;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;


/**
 * Created by IntelliJ IDEA. User : Richer Version: 6.5.5 Date : 11-7-21 Time :
 * 下午5:09
 */
public class DesktopCardPane extends BasicPane implements TargetModifiedListener {

    private static final int CONTENT_LAYER = 0;
    private static final int TRANSPARENT_LAYER = 1;
    private static final int LOADING_LAYER = 2;
    private static final int FAILED_LAYER = 3;

    private static final long serialVersionUID = 1L;
    private JTemplate<?, ?> component;
    private TransparentPane transparentPane = new TransparentPane();
    private JPanel loadingPane = LoadingUtils.createLoadingPane();
    private OpenFailedPane failedPane = new OpenFailedPane();
    private JLayeredPane layeredPane = new JLayeredPane() {
        @Override
        public void doLayout() {
            for (Component comp : getComponents()) {
                comp.setBounds(0, 0, getWidth(), getHeight());
            }
        }
    };

    protected DesktopCardPane() {
        setLayout(new BorderLayout());
        layeredPane.add(transparentPane, TRANSPARENT_LAYER);
        layeredPane.add(failedPane, FAILED_LAYER);
        add(layeredPane, BorderLayout.CENTER);
    }

    protected void showJTemplate(final JTemplate<?, ?> jt) {
        // 判断是否切换设计器状态到禁止拷贝剪切
        if (jt.getTarget().getAttrMark(DesignBanCopyAttrMark.XML_TAG) != null) {
            DesignModeContext.switchTo(DesignerMode.BAN_COPY_AND_CUT);
        } else if (!DesignModeContext.isVcsMode() && !DesignModeContext.isAuthorityEditing() && !DesignModeContext.isDuchampMode()) {
            DesignModeContext.switchTo(DesignerMode.NORMAL);
        }
        // 切换时
        if (component != null) {
            component.fireTabChange();
        }
        DesignerFrameFileDealerPane.getInstance().setCurrentEditingTemplate(jt);

        refresh(jt);
    }

    protected void refresh(final JTemplate<?, ?> jt) {
        if (component != null) {
            layeredPane.remove(component);
        }
        component = jt;
        layeredPane.add(component, CONTENT_LAYER);
        checkSavingAndOpening(jt);
        add(layeredPane, BorderLayout.CENTER);
        validate();
        repaint();
        revalidate();
        component.requestGridFocus();
    }


    private void checkSavingAndOpening(JTemplate<?, ?> jt) {
        if (jt.isSaving()) {
            showCover();
        } else if (jt.isOpening()) {
            showOpenStatus();
        } else if (jt.isOpenFailed()) {
            showOpenFailedCover(jt.getTemplateOpenFailedTip());
        } else {
            hideCover();
        }
    }

    /**
     * 让loadingPane懒加载
     */
    private void checkLoadingPane() {
        if (layeredPane.getComponent(LOADING_LAYER) != loadingPane) {
            layeredPane.add(loadingPane, LOADING_LAYER);
            component.setVisible(false);
        }
    }

    private void showOpenStatus() {
        DesignerContext.getDesignerFrame().refreshUIToolBar();
        DesignerFrameFileDealerPane.getInstance().stateChange();
        EastRegionContainerPane.getInstance().updateAllPropertyPane();
        JComponent downPane = WestRegionContainerPane.getInstance().getDownPane();
        if (downPane instanceof BasicTableDataTreePane) {
            BasicTableDataTreePane dataTreePane = (BasicTableDataTreePane) downPane;
            dataTreePane.refreshToolBar();
        }
        checkLoadingPane();
        layeredPane.moveToFront(loadingPane);
    }

    public void showOpenFailedCover(String text) {
        failedPane.setFailedTip(text);
        layeredPane.moveToFront(failedPane);
    }

    public void showCover() {
        transparentPane.start();
        layeredPane.moveToFront(transparentPane);
        DesignerContext.getDesignerFrame().refreshUIToolBar();
        EastRegionContainerPane.getInstance().updateAllPropertyPane();
        JComponent downPane = WestRegionContainerPane.getInstance().getDownPane();
        if (downPane instanceof BasicTableDataTreePane) {
            BasicTableDataTreePane dataTreePane = (BasicTableDataTreePane) downPane;
            dataTreePane.refreshToolBar();
        }
    }

    public void hideCover() {
        transparentPane.stop();
        layeredPane.moveToFront(component);
        EastRegionContainerPane.getInstance().updateAllPropertyPane();
        JComponent downPane = WestRegionContainerPane.getInstance().getDownPane();
        if (downPane instanceof BasicTableDataTreePane) {
            BasicTableDataTreePane dataTreePane = (BasicTableDataTreePane) downPane;
            dataTreePane.checkEnable();
        }
    }

    protected JTemplate<?, ?> getSelectedJTemplate() {
        return component;
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Desktop");
    }

    @Override
    public void targetModified(TargetModifiedEvent e) {
    }
}