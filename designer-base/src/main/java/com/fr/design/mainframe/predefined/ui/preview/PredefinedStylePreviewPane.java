package com.fr.design.mainframe.predefined.ui.preview;

import com.fr.base.background.ColorBackground;
import com.fr.base.chart.chartdata.CallbackEvent;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.chart.chartattr.Title;
import com.fr.chart.charttypes.ChartTypeManager;
import com.fr.config.predefined.PredefinedStyle;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.utils.ComponentUtils;
import com.fr.general.Background;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.chart.attr.axis.VanChartAxis;
import com.fr.plugin.chart.base.AttrLabel;
import com.fr.plugin.chart.base.VanChartTools;
import com.fr.plugin.chart.column.VanChartColumnPlot;
import com.fr.plugin.chart.vanchart.VanChart;

import javax.swing.JComponent;
import javax.swing.JPanel;
import java.util.ArrayList;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

/**
 * Created by kerry on 2020-09-06
 */
public class PredefinedStylePreviewPane extends StyleSettingPreviewPane implements CallbackEvent {
    private ElementCasePreview elementCasePreview;
    private Background background;
    private double scaleX = 1.0;
    private double scaleY = 1.0;
    private ChartPreStylePreView columnChartPane;

    private int COLUMN_CHART_WIDTH = 517;
    private int COLUMN_CHART_HEIGHT = 290;

    private JPanel parent;

    public PredefinedStylePreviewPane() {
        this(1.0, 1.0);
    }

    public PredefinedStylePreviewPane(double scaleX, double scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.setBackground(Color.WHITE);
        this.elementCasePreview = new ElementCasePreview();
        this.add(initChartPreViewPane());
        this.elementCasePreview.setPreferredSize(new Dimension(517, 200));
        this.add(this.elementCasePreview);
    }

    public void setParent(JPanel parent) {
        this.parent = parent;
    }

    private JPanel initChartPreViewPane() {
        columnChartPane = new ChartPreStylePreView(initVanColumnChart());
        columnChartPane.setPreferredSize(new Dimension(COLUMN_CHART_WIDTH, COLUMN_CHART_HEIGHT));
        columnChartPane.setCallbackEvent(this);

        JPanel panel = FRGUIPaneFactory.createNormalFlowInnerContainer_S_Pane();
        panel.add(columnChartPane);
        return panel;
    }

    //柱形图
    private ChartCollection initVanColumnChart() {
        try {
            VanChart chart = (VanChart) ChartTypeManager.getInstance().getCharts(VanChartColumnPlot.VAN_CHART_COLUMN_PLOT_ID)[0].clone();
            VanChartTools vanChartTools = chart.getVanChartTools();
            vanChartTools.setSort(false);
            vanChartTools.setExport(false);
            vanChartTools.setFullScreen(false);
            VanChartColumnPlot plot = chart.getPlot();
            AttrLabel defaultAttrLabel = plot.getDefaultAttrLabel();
            defaultAttrLabel.setEnable(true);
            defaultAttrLabel.getAttrLabelDetail().getBorder().setBorderStyle(0);
            defaultAttrLabel.getAttrLabelDetail().getBackground().setBackground(null);
            plot.getConditionCollection().getDefaultAttr().addDataSeriesCondition(defaultAttrLabel);
            plot.getLegend().setLegendVisible(false);
            plot.getDataSheet().setVisible(true);

            VanChartAxis defaultYAxis = plot.getDefaultYAxis();
            Title title = new Title();
            title.setTextObject(Toolkit.i18nText("Fine-Design_Chart_Axis_Title"));
            title.getTextAttr().setRotation(-90);
            defaultYAxis.setTitle(title);
            defaultYAxis.setShowAxisTitle(true);

            ChartCollection chartCollection = new ChartCollection(chart);
            return chartCollection;
        } catch (Exception ex) {
            FineLoggerFactory.getLogger().error(ex.getMessage(), ex);
        }
        return null;
    }

    @Override
    public void refresh() {

    }

    @Override
    public void paint(Graphics g) {
        ((Graphics2D) g).scale(scaleX, scaleY);
        // 禁止双缓冲
        ArrayList<JComponent> dbcomponents = new ArrayList<JComponent>();
        ComponentUtils.disableBuffer(this.elementCasePreview, dbcomponents);

        if (background == null) {
            background = ColorBackground.getInstance(Color.WHITE);
        }
        background.paint(g, new Rectangle2D.Double(0, 0, 517, 500));
        this.columnChartPane.paintComponent(g);

        g.translate(0, COLUMN_CHART_HEIGHT);
        this.elementCasePreview.paintComponents(g);
        g.translate(0, -COLUMN_CHART_HEIGHT);
        // 恢复双缓冲
        ComponentUtils.resetBuffer(dbcomponents);

    }

    @Override
    public void paintComponents(Graphics g) {
        super.paintComponents(g);
        if (background != null) {
            background.paint(g, new Rectangle2D.Double(0, 0, this.getWidth(), this.getHeight()));
        }
    }

    public void refresh(PredefinedStyle style) {
        refresh(style, false);
    }

    public void refresh(PredefinedStyle style, boolean displayFormBackground) {
        elementCasePreview.refresh(style);
        columnChartPane.refresh(style);
        background = displayFormBackground ? style.getFormBackground().getBackground() : style.getReportBackground();
        this.repaint();
    }

    @Override
    public void callback() {
        if (parent != null) {
            parent.repaint();
        } else {
            this.repaint();
        }
    }
}
