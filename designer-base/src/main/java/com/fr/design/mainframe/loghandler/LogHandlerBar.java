package com.fr.design.mainframe.loghandler;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.ui.util.UIUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import static com.fr.design.i18n.Toolkit.i18nText;

public class LogHandlerBar extends JPanel implements ItemSelectable {

    // 可以作为常量 提前初始化 不用每次都去实时频繁的读取
    private static final String NORMAL_MARK = i18nText("Fine-Design_Basic_NNormal");
    private static final String ALERT_MARK = i18nText("Fine-Design_Basic_Alert");
    private static final String SERIOUSLY_MARK = i18nText("Fine-Design_Basic_Seriously");

    private static final long serialVersionUID = 1L;
    private ItemListener listeners;
    private UIButton clear;
    private UIButton selectedall;
    private UIButton set;
    private UILabel normalLabel;
    private UILabel alertLabel;
    private UILabel seriouslyLabel;

    private String text;
    private int INFONUM = 0;
    private int ERRORNUM = 0;
    private int SERVERNUM = 0;
    private static final int FLOW_LAYOUT_HGAP = 10;
    private static final int FLOW_LAYOUT_VGAP = 5;

    private boolean isWithSerious;

    public LogHandlerBar() {
        this(null);
    }

    public LogHandlerBar(String text) {
        this.setLayout(new FlowLayout(FlowLayout.RIGHT, FLOW_LAYOUT_HGAP, FLOW_LAYOUT_VGAP));
        this.setUI(new LogHandlerBarUI());
        this.text = text;
        clear = new UIButton(BaseUtils.readIcon("com/fr/design/images/log/clear.png"));
        clear.setMargin(null);
        clear.setOpaque(false);
        clear.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        clear.setToolTipText(i18nText("Fine-Design_Basic_Clear_All"));
        selectedall = new UIButton(BaseUtils.readIcon("com/fr/design/images/log/selectedall.png"));
        selectedall.setMargin(null);
        selectedall.setOpaque(false);
        selectedall.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        selectedall.setToolTipText(i18nText("Fine-Design_Basic_Select_All"));
        set = new UIButton(BaseUtils.readIcon("com/fr/design/images/log/setting.png"));
        set.setMargin(null);
        set.setOpaque(false);
        set.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        set.setToolTipText(i18nText("Fine-Design_Report_Set"));
        normalLabel = new UILabel(i18nText("Fine-Design_Basic_NNormal") + "(0)");
        alertLabel = new UILabel(i18nText("Fine-Design_Basic_Alert") + "(0)");
        seriouslyLabel = new UILabel(i18nText("Fine-Design_Basic_Seriously") + "(0)");

        this.add(normalLabel);
        this.add(alertLabel);
        this.add(seriouslyLabel);
        this.add(clear);
        this.add(selectedall);
        this.add(set);
    }

    /**
     * 更新日志数量统计标签
     */
    private void changeLabel() {
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                // 这块逻辑执行地很频繁
                // 容易造成 AWT 与 FinePluginController 发生死锁
                // in18Text每次读取 资源的时候 都会走下插件引擎逻辑去读取下插件的资源
                // 应该避免每次都去读取 可以作为常量提前初始化好 避免与FinePluginController的竞争
                normalLabel.setText(NORMAL_MARK + '(' + getInfo() + ')');
                alertLabel.setText(ALERT_MARK + '(' + getError() + ')');
                seriouslyLabel.setText(SERIOUSLY_MARK + '(' + getServer() + ')');
            }
        });
    }

    public void clearMessage() {
        INFONUM = ERRORNUM = SERVERNUM = 0;
        LogMessageBar.getInstance().clear();
        changeLabel();
    }

    public boolean IsWithSerious() {
        return isWithSerious;
    }

    public void setWithSerious(boolean b) {
        this.isWithSerious = b;
    }

    public void infoAdd() {
        INFONUM++;
        changeLabel();
    }

    public void errorAdd() {
        ERRORNUM++;
        changeLabel();
    }

    public void serverAdd() {
        SERVERNUM++;
        changeLabel();
    }

    public int getInfo() {
        return INFONUM;
    }

    public int getError() {
        return ERRORNUM;
    }

    public int getServer() {
        return SERVERNUM;
    }

    public void addItemListener(ItemListener l) {
        listeners = l;
    }

    public void removeItemListener(ItemListener l) {
        listeners = null;
    }

    protected void fireItemStateChanged(ItemEvent e) {
        listeners.itemStateChanged(e);
    }

    public Object[] getSelectedObjects() {
        return new Object[]{text};
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
        repaint();
    }

    public void addClearListener(ActionListener l) {
        clear.addActionListener(l);
    }

    public void addSelectedListener(ActionListener l) {
        selectedall.addActionListener(l);
    }

    public void addSetListener(ActionListener l) {
        set.addActionListener(l);
    }
}
