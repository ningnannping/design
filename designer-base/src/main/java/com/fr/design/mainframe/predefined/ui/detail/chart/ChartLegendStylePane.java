package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.i18n.Toolkit;

import java.awt.Component;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-16
 */
public class ChartLegendStylePane extends AbstractChartStylePane {

    //字体样式
    private ChartFontPane chartFontPane;

    protected void initComponents() {
        chartFontPane = new ChartFontPane() {
            public String getUILabelText() {
                return Toolkit.i18nText("Fine-Design_Chart_Legend_Character");
            }
        };
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{chartFontPane, null}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p};
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Legend");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        chartFontPane.populate(chartStyle.getLegendFont());
    }


    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setLegendFont(chartFontPane.update());
    }
}
