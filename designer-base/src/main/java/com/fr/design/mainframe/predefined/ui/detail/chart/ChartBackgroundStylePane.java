package com.fr.design.mainframe.predefined.ui.detail.chart;

import com.fr.base.background.ImageBackground;
import com.fr.config.predefined.PredefinedChartStyle;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.backgroundpane.BackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.ColorBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.ImageBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.NullBackgroundQuickPane;
import com.fr.design.mainframe.backgroundpane.VanChartGradientPane;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.general.Background;
import com.fr.stable.Constants;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.List;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2020-09-17
 */
public class ChartBackgroundStylePane extends AbstractChartStylePane {

    private UIComboBox typeComboBox;
    private List<BackgroundQuickPane> paneList;
    private JPanel centerPane;
    //网格线颜色
    private ColorSelectBox mainGridColor;

    protected void initComponents() {
        mainGridColor = new ColorSelectBox(100);

        typeComboBox = new UIComboBox();
        final CardLayout cardlayout = new CardLayout();
        initList();

        centerPane = new JPanel(cardlayout) {
            @Override
            public Dimension getPreferredSize() {// AUGUST:使用当前面板的的高度
                int index = typeComboBox.getSelectedIndex();
                return new Dimension(super.getPreferredSize().width, paneList.get(index).getPreferredSize().height);
            }
        };
        for (int i = 0; i < paneList.size(); i++) {
            BackgroundQuickPane pane = paneList.get(i);
            typeComboBox.addItem(pane.title4PopupWindow());
            centerPane.add(pane, pane.title4PopupWindow());
        }

        typeComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                cardlayout.show(centerPane, (String) typeComboBox.getSelectedItem());
            }
        });
    }

    protected Component[][] getComponent() {
        return new Component[][]{
                new Component[]{null, null},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Area_Background_Color")), typeComboBox},
                new Component[]{null, centerPane},
                new Component[]{new UILabel(Toolkit.i18nText("Fine-Design_Chart_Grid_Line_Color")), mainGridColor}
        };
    }

    protected double[] getRows(double p) {
        return new double[]{p, p, p, p};
    }

    private void initList() {
        paneList = new ArrayList<>();
        paneList.add(new NullBackgroundQuickPane());
        paneList.add(new ColorBackgroundQuickPane());
        paneList.add(new ImageBackgroundQuickPane(false));
        paneList.add(new VanChartGradientPane());
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Chart_Background");
    }

    public void populate(PredefinedChartStyle chartStyle) {
        Background background = chartStyle.getChartBackground();
        for (int i = 0; i < paneList.size(); i++) {
            BackgroundQuickPane pane = paneList.get(i);
            if (pane.accept(background)) {
                pane.populateBean(background);
                typeComboBox.setSelectedIndex(i);
                break;
            }
        }
        mainGridColor.setSelectObject(chartStyle.getGridMainLineColor());
    }

    public void update(PredefinedChartStyle chartStyle) {
        chartStyle.setChartBackground(paneList.get(typeComboBox.getSelectedIndex()).updateBean());
        if (chartStyle.getChartBackground() instanceof ImageBackground) {
            ((ImageBackground) chartStyle.getChartBackground()).setLayout(Constants.IMAGE_EXTEND);
        }
        chartStyle.setGridMainLineColor(mainGridColor.getSelectObject());
    }
}
