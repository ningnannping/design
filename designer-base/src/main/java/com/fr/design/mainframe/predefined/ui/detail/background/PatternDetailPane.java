package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.base.background.PatternBackground;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.background.impl.PatternBackgroundPane;
import com.fr.design.style.color.ColorSelectBox;
import com.fr.general.Background;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.LayoutManager;

/**
 * Created by kerry on 2020-08-31
 */
public class PatternDetailPane extends AbstractBackgroundDetailPane<PatternBackground> implements UIObserver {

    private UIObserverListener listener;
    private PatternNewBackgroundPane patternNewBackgroundPane;

    public PatternDetailPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        patternNewBackgroundPane = new PatternNewBackgroundPane(6);
        this.add(patternNewBackgroundPane, BorderLayout.CENTER);
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
    }

    @Override
    public void populate(PatternBackground background) {
        this.patternNewBackgroundPane.populate(background);
    }

    @Override
    public PatternBackground update() {
        try {
            return (PatternBackground) this.patternNewBackgroundPane.update();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return null;
    }
    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Pattern");
    }


    @Override
    public boolean accept(Background background) {
        return background instanceof PatternBackground;
    }


    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    class PatternNewBackgroundPane extends PatternBackgroundPane {
        private PatternNewBackgroundPane(int nColumn) {
            super(nColumn);
        }

        protected LayoutManager layoutOfTypePane(int nColumn) {
            return new GridLayout(0, nColumn, 2, 2);
        }

        protected void initComponents(int nColumn) {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.setBorder(BorderFactory.createEmptyBorder());
            JPanel jPanel = new JPanel();
            jPanel.setLayout(layoutOfTypePane(nColumn));
            setChildrenOfTypePane(jPanel);

            foregroundColorPane = new ColorSelectBox(80);
            backgroundColorPane = new ColorSelectBox(80);
            foregroundColorPane.setSelectObject(Color.lightGray);
            backgroundColorPane.setSelectObject(Color.black);
            UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Pattern"));
            label.setVerticalAlignment(SwingConstants.TOP);
            Component[][] components = new Component[][]{
                    new Component[]{label, jPanel},
                    new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_ForeBackground_Color")), foregroundColorPane},
                    new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Pattern_Color")), backgroundColorPane}
            };
            JPanel centerPane = TableLayoutHelper.createGapTableLayoutPane(components, TableLayoutHelper.FILL_LASTCOLUMN,
                    IntervalConstants.INTERVAL_W4, IntervalConstants.INTERVAL_L1);
            JPanel jPanel1 = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
            jPanel1.add(centerPane);
            jPanel1.setBorder(BorderFactory.createEmptyBorder());
            this.add(jPanel1, BorderLayout.NORTH);
            this.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                    if (listener != null) {
                        listener.doChange();
                    }
                }
            });
        }
    }
}
