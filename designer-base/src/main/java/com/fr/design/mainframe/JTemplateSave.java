package com.fr.design.mainframe;

import com.fr.design.worker.save.CallbackSaveWorker;

/**
 * 模板保存接口
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/9
 */
public interface JTemplateSave {

    /**
     * 保存后需要根据是否成功做外部回调，可选保存位置
     *
     * @return
     */
    CallbackSaveWorker save();


    /**
     * 另存为后需要根据是否成功做外部回调，可选保存位置
     *
     * @return
     */
    CallbackSaveWorker saveAs();


    /**
     * 保存到当前工作目录(reportlets)后需要根据是否成功做外部回调
     *
     *
     * @return
     */
    CallbackSaveWorker save2Env();


    /**
     * 另存为到当前工作目录(reportlets)后需要根据是否成功做外部回调
     *
     * @return
     */
    CallbackSaveWorker saveAs2Env();

    /**D
     * 直接保存
      */
   void saveDirectly();


    /**
     * 直接另存为
     */
   void saveAsDirectly();

}
