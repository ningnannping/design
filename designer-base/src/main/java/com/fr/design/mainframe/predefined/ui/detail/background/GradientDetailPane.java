package com.fr.design.mainframe.predefined.ui.detail.background;

import com.fr.base.background.GradientBackground;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.style.background.gradient.GradientBar;
import com.fr.general.Background;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * 渐变色的面板，不是很pp，面板应用显得繁琐，有写可以写成控件类型，比如色彩选择的。。，可以做得花哨点
 *
 * @author ben
 */
public class GradientDetailPane extends AbstractBackgroundDetailPane<GradientBackground> implements UIObserver {
    private static final long serialVersionUID = -6854603990673031897L;
    private UIObserverListener listener;
    private UIRadioButton left2right, top2bottom;
    private GradientBar gradientBar;
    private ChangeListener changeListener = null;

    public GradientDetailPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        JPanel gradientPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        JPanel blankJp = new JPanel();
        gradientBar = new GradientBar(4, 140);
        blankJp.add(gradientBar);

        gradientPanel.add(gradientBar, BorderLayout.SOUTH);

        JPanel jp = new JPanel(new GridLayout(2, 1, 15, 10));


        left2right = new UIRadioButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Page_Setup_Horizontal"));
        jp.add(left2right);
        left2right.setSelected(true);
        left2right.addActionListener(reviewListener);

        top2bottom = new UIRadioButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Page_Setup_Vertical"));
        jp.add(top2bottom);
        top2bottom.addActionListener(reviewListener);

        ButtonGroup toggle = new ButtonGroup();
        toggle.add(left2right);
        toggle.add(top2bottom);

        Component[][] components = new Component[][]{
                new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Gradient_Setting")), gradientPanel},
                new Component[]{new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Background_Gradient_Color")), jp}
        };
        JPanel contentPane = TableLayoutHelper.createGapTableLayoutPane(components, TableLayoutHelper.FILL_LASTCOLUMN,
                IntervalConstants.INTERVAL_W4, IntervalConstants.INTERVAL_L1);
        this.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                if (listener != null) {
                    listener.doChange();
                }
            }
        });


        this.add(contentPane);
    }

    @Override
    public boolean accept(Background background) {
        return background instanceof GradientBackground;
    }



    public void populate(GradientBackground bg) {
        this.gradientBar.getSelectColorPointBtnP1().setColorInner(bg.getStartColor());
        this.gradientBar.getSelectColorPointBtnP2().setColorInner(bg.getEndColor());
        if (bg.getDirection() == GradientBackground.LEFT2RIGHT) {
            left2right.setSelected(true);
        } else {
            top2bottom.setSelected(true);
        }
        if (bg.isUseCell()) {
            return;
        }
        double startValue = (double) bg.getBeginPlace();
        double endValue = (double) bg.getFinishPlace();
        gradientBar.setStartValue(startValue);
        gradientBar.setEndValue(endValue);
        this.gradientBar.repaint();
    }

    public GradientBackground update() {
        GradientBackground gb = new GradientBackground(
                gradientBar.getSelectColorPointBtnP1().getColorInner(),
                gradientBar.getSelectColorPointBtnP2().getColorInner());
        if (left2right.isSelected()) {
            gb.setDirection(GradientBackground.LEFT2RIGHT);
        } else {
            gb.setDirection(GradientBackground.TOP2BOTTOM);
        }
        if (gradientBar.isOriginalPlace()) {
            gb.setUseCell(true);
        } else {
            gb.setUseCell(false);
            gb.setBeginPlace((float) gradientBar.getStartValue());
            gb.setFinishPlace((float) gradientBar.getEndValue());
        }
        return gb;
    }


    ActionListener reviewListener = new ActionListener() {
        public void actionPerformed(ActionEvent e) {
            fireChagneListener();
        }
    };

    public void addChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
        gradientBar.addChangeListener(changeListener);
    }

    public void fireChagneListener() {
        if (this.changeListener != null) {
            ChangeEvent evt = new ChangeEvent(this);
            this.changeListener.stateChanged(evt);
        }
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        this.listener = listener;
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Gradient_Color");
    }

}
