package com.fr.design.mainframe.predefined.ui.detail;

import com.fr.base.BaseUtils;
import com.fr.base.Style;
import com.fr.config.predefined.PredefinedCellStyle;
import com.fr.config.predefined.PredefinedCellStyleConfig;
import com.fr.design.actions.UpdateAction;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.NameInspector;
import com.fr.design.gui.controlpane.JListControlPane;
import com.fr.design.gui.controlpane.NameObjectCreator;
import com.fr.design.gui.controlpane.NameableCreator;
import com.fr.design.gui.controlpane.ShortCut4JControlPane;
import com.fr.design.gui.controlpane.UnrepeatedNameHelper;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.ilist.ListModelElement;
import com.fr.design.gui.ilist.ModNameActionListener;
import com.fr.design.gui.style.AlignmentPane;
import com.fr.design.gui.style.FormatPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.detail.cell.CustomPredefinedStylePane;
import com.fr.design.menu.ShortCut;
import com.fr.general.ComparatorUtils;
import com.fr.general.NameObject;
import com.fr.invoke.Reflect;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.Nameable;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by kerry on 2020-09-27
 */
public class CellStyleListControlPane extends JListControlPane {
    private boolean namePermitted = true;
    private AttributeChangeListener attributeChangeListener;



    public CellStyleListControlPane() {
        super();
        this.addModNameActionListener(new ModNameActionListener() {
            public void nameModed(int index, String oldName, String newName) {
                if (ComparatorUtils.equals(oldName, newName) || ComparatorUtils.equals(newName, NameInspector.ILLEGAL_NAME_HOLDER)) {
                    return;
                }
                namePermitted = true;
                String[] allNames = nameableList.getAllNames();
                allNames[index] = StringUtils.EMPTY;
                if (StringUtils.isEmpty(newName)) {
                    showTipDialogAndReset(Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Empty_Name"), index);
                    return;
                }
                if (isNameRepeated(new List[]{Arrays.asList(allNames)}, newName)) {
                    showTipDialogAndReset(Toolkit.i18nText("Fine-Design_Basic_Predefined_Style_Duplicate_Name", newName), index);
                    return;
                }
                populateSelectedValue();
            }
        });
    }



    public void registerAttrChangeListener(AttributeChangeListener listener){
        this.attributeChangeListener = listener;
    }


    private void showTipDialogAndReset(String content, int index) {
        nameableList.stopEditing();

        FineJOptionPane.showMessageDialog(SwingUtilities.getWindowAncestor(CellStyleListControlPane.this),
                content,
                Toolkit.i18nText("Fine-Design_Basic_Alert"),
                JOptionPane.WARNING_MESSAGE);
        setIllegalIndex(index);
        namePermitted = false;
    }

    @Override
    public NameableCreator[] createNameableCreators() {
        return new NameableCreator[]{
                new CellStyleNameObjectCreator(Toolkit.i18nText("Fine-Design_Predefined_Cell_New_Style"),
                        PredefinedCellStyle.class, CustomPredefinedStylePaneNoBorder.class) {
                    @Override
                    public boolean acceptDefaultNameObject(Object ob) {
                        return ((PredefinedCellStyle) ob).isDefaultStyle();
                    }
                },
                new CellStyleNameObjectCreator(Toolkit.i18nText("Fine-Design_Predefined_Cell_New_Style"),
                        PredefinedCellStyle.class, CustomPredefinedStylePane.class)};
    }

    @Override
    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        CustomPredefinedStylePane stylePane = (CustomPredefinedStylePane) super.createPaneByCreators(creator);
        stylePane.registerAttrChangeListener(attributeChangeListener);
        return stylePane;
    }



    @Override
    protected String title4PopupWindow() {
        return StringUtils.EMPTY;
    }

    protected void initComponentPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setCreators(this.createNameableCreators());
        initCardPane();
        JPanel leftPane = getLeftPane();
        JSeparator jSeparator = new JSeparator(SwingConstants.VERTICAL);
        leftPane.setPreferredSize(new Dimension(70, 0));
        jSeparator.setPreferredSize(new Dimension(2, 0));
        cardPane.setPreferredSize(new Dimension(238, 0));
        cardPane.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        JPanel mainSplitPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        mainSplitPane.add(leftPane, BorderLayout.WEST);
        mainSplitPane.add(jSeparator, BorderLayout.CENTER);
        mainSplitPane.add(cardPane, BorderLayout.EAST);

        this.add(mainSplitPane, BorderLayout.CENTER);
        this.checkButtonEnabled();
    }

    protected ShortCut4JControlPane[] createShortcuts() {
        return new ShortCut4JControlPane[]{
                createAddItemShortCut4JControlPane(),
                new RemoveItemShortCut4JControlPane(new RemoveItemAction())
        };
    }


    private static class CustomPredefinedStylePaneNoBorder extends CustomPredefinedStylePane {
        @Override
        protected List<BasicPane> initPaneList() {
            paneList = new ArrayList<BasicPane>();
            paneList.add(new FormatPane());
            paneList.add(new AlignmentPane());
            return paneList;
        }

    }


    private ShortCut4JControlPane createAddItemShortCut4JControlPane (){
        ShortCut shortCut =  shortCutFactory.createAddItemUpdateAction(new NameableCreator[]{
                new CellStyleNameObjectCreator(Toolkit.i18nText("Fine-Design_Predefined_Cell_New_Style"),
                        PredefinedCellStyle.class, CustomPredefinedStylePane.class)});
        return new AddItemShortCut4JControlPane(shortCut);
    }

    private class AddItemShortCut4JControlPane extends ShortCut4JControlPane{
        AddItemShortCut4JControlPane(ShortCut shortCut) {
            this.shortCut = shortCut;
        }


        @Override
        public void checkEnable() {
            this.shortCut.setEnabled(true);
        }
    }


    private class RemoveItemShortCut4JControlPane extends ShortCut4JControlPane {
        RemoveItemShortCut4JControlPane(ShortCut shortCut) {
            this.shortCut = shortCut;
        }

        @Override
        public void checkEnable() {
            ListModelElement selectModel = CellStyleListControlPane.this.getSelectedValue();
            if (selectModel != null) {
                NameObject selectNameObject = (NameObject) selectModel.wrapper;
                PredefinedCellStyle cellStyle = (PredefinedCellStyle) (selectNameObject.getObject());
                this.shortCut.setEnabled(!cellStyle.isBuiltIn() && !cellStyle.isDefaultStyle());
            } else {
                this.shortCut.setEnabled(false);
            }

        }
    }


    private class RemoveItemAction extends UpdateAction {
        RemoveItemAction() {
            this.setName(com.fr.design.i18n.Toolkit.i18nText(("Fine-Design_Basic_Action_Remove")));
            this.setMnemonic('R');
            this.setSmallIcon(BaseUtils
                    .readIcon("/com/fr/base/images/cell/control/remove.png"));
        }

        @Override
        public void actionPerformed(ActionEvent evt) {
            CellStyleListControlPane.this.onRemoveItem();
        }
    }


    class CellStyleNameObjectCreator extends NameObjectCreator {
        public CellStyleNameObjectCreator(String menuName, Class clazz, Class<? extends BasicBeanPane> updatePane) {
            super(menuName, clazz, updatePane);
        }

        public Nameable createNameable(UnrepeatedNameHelper helper) {
            Constructor<? extends PredefinedCellStyle> constructor = null;
            try {
                constructor = clazzOfInitCase.getConstructor();
                PredefinedCellStyle cellStyle = constructor.newInstance();

                cellStyle.setName(menuName);
                cellStyle.setStyle(Style.getInstance());
                return new NameObject(helper.createUnrepeatedName(this.menuName()), cellStyle);

            } catch (NoSuchMethodException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            } catch (InstantiationException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            } catch (InvocationTargetException e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
            return null;
        }
        /**
         *
         * @param ob
         * @return
         */
        public Object acceptObject2Populate(Object ob) {
            if (ob instanceof NameObject) {
                ob = ((NameObject) ob).getObject();
            }
            if (clazzOfObject != null && clazzOfObject.isInstance(ob) && acceptDefaultNameObject(ob)) {
                doSthChanged4Icon(ob);
                return ob;
            }
            return null;
        }

        public boolean acceptDefaultNameObject(Object ob) {
            return !((PredefinedCellStyle) ob).isDefaultStyle();
        }
    }


    /**
     * Populate
     */
    public void populateBean(PredefinedCellStyleConfig ob) {
        if (ob == null) {
            return;
        }

        List nameStyleList = new ArrayList();

        Iterator styleNameIterator = ob.getStyleNameIterator();
        while (styleNameIterator.hasNext()) {
            String name = (String) styleNameIterator.next();
            PredefinedCellStyle tmpStyle = ob.getStyle(name);

            if (tmpStyle != null) {
                nameStyleList.add(new NameObject(name, tmpStyle));
            }
        }

        NameObject[] nameObjects = new NameObject[nameStyleList.size()];
        nameStyleList.toArray(nameObjects);

        populate(nameObjects);
    }


    public PredefinedCellStyleConfig updateBean() {
        Nameable[] nameables = this.update();
        PredefinedCellStyleConfig styleConfig = new PredefinedCellStyleConfig();
        for (int i = 0; i < nameables.length; i++) {
            PredefinedCellStyle tmpStyle = (PredefinedCellStyle) ((NameObject) nameables[i]).getObject();
            tmpStyle.setName(nameables[i].getName());
            styleConfig.addStyle(tmpStyle);
        }
        return styleConfig;
    }


}
