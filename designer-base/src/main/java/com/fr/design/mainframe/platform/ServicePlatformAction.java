package com.fr.design.mainframe.platform;

import com.fr.design.actions.UpdateAction;
import com.fr.design.i18n.Toolkit;
import com.fr.design.utils.BrowseUtils;
import com.fr.general.CloudCenter;
import com.fr.log.FineLoggerFactory;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.net.URI;

/**
 * 帮助-服务平台
 *
 * @author Destiny.Lin
 * @version 11.0
 * created by Destiny.Lin on 2022-12-14
 */
public class ServicePlatformAction extends UpdateAction {
    public ServicePlatformAction() {
        this.setName(Toolkit.i18nText("Fine-Design_Basic_Service_Platform_Title"));
        this.setSmallIcon("/com/fr/design/images/platform/platform", false);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        BrowseUtils.browser(CloudCenter.getInstance().acquireUrlByKind("service.platform"));
    }
}
