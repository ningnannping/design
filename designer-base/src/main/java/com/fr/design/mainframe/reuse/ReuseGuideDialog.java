package com.fr.design.mainframe.reuse;

import com.fr.base.background.ColorBackground;
import com.fr.design.dialog.UIDialog;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.PromptWindow;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.general.IOUtils;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.geom.RoundRectangle2D;

public class ReuseGuideDialog extends UIDialog implements PromptWindow {
    InnerDialog innerDialog;
    private static final Dimension DEFAULT = new Dimension(735, 510);

    public ReuseGuideDialog(Frame parent) {
        super(parent);
    }

    @Override
    public void showWindow() {
        innerDialog = new InnerDialog(this);
        JPanel backGroundPane = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                Image icon = IOUtils.readImage("com/fr/base/images/share/background.png");// 003.jpg是测试图片在项目的根目录下
                g.drawImage(icon, 0, 0, getSize().width, getSize().height, this);// 图片会自动缩放
            }
        };
        add(backGroundPane, BorderLayout.CENTER);
        initStyle();
        innerDialog.showWindow();
    }

    private void initStyle() {
        setSize(DEFAULT);
        setUndecorated(true);
        setBackground(new Color(0, 0, 0, 0));
        GUICoreUtils.centerWindow(this);
    }

    @Override
    public void hideWindow() {
        ComponentReuseNotificationInfo.getInstance().updateLastGuidePopUpTime();
        this.setVisible(false);
        if (innerDialog != null) {
            innerDialog.setVisible(false);
            innerDialog.dispose();
            innerDialog = null;
        }
        this.dispose();
    }

    @Override
    public void checkValid() {

    }

    class InnerDialog extends UIDialog {
        private final Dimension DEFAULT = new Dimension(700, 475);
        private static final int TITLE_FONT_SIZE = 20;

        public InnerDialog(Dialog dialog) {
            super(dialog);
        }

        public void showWindow() {
            add(createCenterPanel(), BorderLayout.CENTER);
            add(createSouthPanel(), BorderLayout.SOUTH);
            add(createNorthPanel(), BorderLayout.NORTH);
            showDialog();
        }

        private JPanel createNorthPanel() {
            JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

            //右上角关闭按钮
            JButton button = new JButton(new ImageIcon(IOUtils.readImage("/com/fr/base/images/share/close.png").getScaledInstance(15, 15, Image.SCALE_SMOOTH)));
            button.setBorder(null);
            button.setOpaque(false);
            button.addActionListener(e -> ReuseGuideDialog.this.hideWindow());

            northPanel.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 15));
            northPanel.setOpaque(false);
            northPanel.add(button);
            return northPanel;
        }

        private JPanel createCenterPanel() {
            JPanel centerPanel = new JPanel(new BorderLayout());

            UILabel titleLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Drag_And_Make_Component"));
            UILabel imageLabel = new UILabel(new ImageIcon(IOUtils.readImage("com/fr/design/images/dashboard/guide.png").getScaledInstance(DEFAULT.width, DEFAULT.height, Image.SCALE_SMOOTH)));
            titleLabel.setFont(new Font(titleLabel.getFont().getName(), Font.BOLD, TITLE_FONT_SIZE));
            titleLabel.setBorder(BorderFactory.createEmptyBorder());

            JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            panel.setOpaque(false);
            panel.add(titleLabel);

            centerPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 10, 0));
            centerPanel.setOpaque(false);
            centerPanel.add(imageLabel, BorderLayout.CENTER);
            centerPanel.add(panel, BorderLayout.NORTH);
            return centerPanel;
        }

        private JPanel createSouthPanel() {
            JPanel southPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();

            JButton button = new JButton(Toolkit.i18nText("Fine-Design_Share_Try_Drag")) {
                @Override
                public void paint(Graphics g) {
                    ColorBackground buttonBackground = ColorBackground.getInstance(Color.decode("#419BF9"));
                    Graphics2D g2d = (Graphics2D) g;
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    buttonBackground.paint(g2d, new RoundRectangle2D.Double(0, 0, getWidth(), getHeight(), 8, 8));
                    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
                    super.paint(g);
                }
            };
            button.setBorder(null);
            button.setForeground(Color.WHITE);
            button.setOpaque(false);
            button.addActionListener(e -> ReuseGuideDialog.this.hideWindow());

            southPanel.setBorder(BorderFactory.createEmptyBorder(0, 290, 19, 290));
            southPanel.setPreferredSize(new Dimension(DEFAULT.width, 51));
            southPanel.setOpaque(false);
            southPanel.add(button);
            return southPanel;
        }

        /**
         * 显示窗口
         */
        private void showDialog() {
            setSize(DEFAULT);
            setUndecorated(true);
            GUICoreUtils.centerWindow(this);
            setModalityType(ModalityType.APPLICATION_MODAL);
            ReuseGuideDialog.this.setVisible(true);
            setVisible(true);
        }

        @Override
        public void checkValid() {

        }
    }
}
