package com.fr.design.mainframe.mobile.ui;

import com.fr.design.beans.BasicBeanPane;
import com.fr.form.ui.mobile.MobileBookMarkStyle;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2019/12/24
 */
public class DefaultMobileBookMarkStyleCustomDefinePane extends BasicBeanPane<MobileBookMarkStyle> {

    @Override
    public void populateBean(MobileBookMarkStyle ob) {

    }

    @Override
    public MobileBookMarkStyle updateBean() {
        return null;
    }

    @Override
    protected String title4PopupWindow() {
        return null;
    }
}
