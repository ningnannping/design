package com.fr.design.mainframe;

import javax.swing.JWindow;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/9
 * 只关心Window的显示和隐藏操作时可以实现该接口
 */
public interface PromptWindow {
    /**
     * 显示弹窗
     */
    void showWindow();

    /**
     * 隐藏弹窗
     */
    void hideWindow();
}
