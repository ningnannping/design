package com.fr.design.os.impl;

import com.fr.design.data.datapane.connect.ConnectionManagerPane;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.editlock.EditLockUtils;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.DesignerFrame;
import com.fr.file.ConnectionConfig;
import com.fr.report.LockItem;
import com.fr.stable.os.support.OSBasedAction;
import com.fr.transaction.CallBackAdaptor;
import com.fr.transaction.Configurations;
import com.fr.transaction.WorkerFacade;

import javax.swing.JOptionPane;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.fr.design.actions.server.ConnectionListAction.doWithDatasourceManager;

/**
 * 数据连接窗口
 *
 * @author pengda
 * @date 2019/10/9
 */
public class DatabaseDialogAction implements OSBasedAction {

    @Override
    public void execute(Object... objects) {
//        if (ServerPreferenceConfig.getInstance().isUseUniverseDBM() && !OperatingSystem.isLinux()) {
//            UniversalDatabaseOpener.showUniverseDatabaseDialog();
//        } else {
//        }
        // 直接这里屏蔽掉 防止有设置过 导致配置数据库值为true 即使设置界面屏蔽也没用
        openDesignDatabaseManager();

    }

    private void openDesignDatabaseManager() {
        DesignerFrame designerFrame = DesignerContext.getDesignerFrame();
        final ConnectionConfig datasourceManager = ConnectionConfig.getInstance();
        final ConnectionManagerPane databaseManagerPane = new ConnectionManagerPane() {
            public void complete() {
                ConnectionConfig connectionConfig = datasourceManager.mirror();
                populate(connectionConfig);
            }

            protected void renameConnection(String oldName, String newName) {
                datasourceManager.renameConnection(oldName, newName);
            }
        };
        final BasicDialog databaseListDialog = databaseManagerPane.showLargeWindow(designerFrame, null);
        databaseListDialog.addDialogActionListener(new DialogActionAdapter() {
            public void doOk() {
                if (!databaseManagerPane.isNamePermitted()) {
                    databaseListDialog.setDoOKSucceed(false);
                    return;
                }

                AtomicBoolean saved = new AtomicBoolean(true);
                Configurations.modify(new WorkerFacade(ConnectionConfig.class) {
                    @Override
                    public void run() {
                        try {
                            databaseManagerPane.update(datasourceManager);
                        } catch (Exception e) {
                            saved.set(false);
                            FineJOptionPane.showMessageDialog(databaseManagerPane, e.getMessage(), Toolkit.i18nText("Fine-Design_Basic_Error"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                }.addCallBack(new CallBackAdaptor() {
                    @Override
                    public boolean beforeCommit() {
                        //如果更新失败，则不关闭对话框，也不写xml文件，并且将对话框定位在请重命名的那个对象页面
                        return doWithDatasourceManager(datasourceManager, databaseManagerPane, databaseListDialog);
                    }

                    @Override
                    public void afterCommit() {
                        if (saved.get()) {
                            DesignerContext.getDesignerBean("databasename").refreshBeanElement();
                            // 定义数据连接弹窗关闭后，解锁
                            EditLockUtils.unlock(LockItem.CONNECTION);
                        } else {
                            databaseListDialog.setDoOKSucceed(false);
                        }
                    }
                }));
            }

            @Override
            public void doCancel() {
                super.doCancel();
                // 定义数据连接弹窗关闭后，解锁
                EditLockUtils.unlock(LockItem.CONNECTION);
            }
        });
        databaseListDialog.setVisible(true);
    }


}
