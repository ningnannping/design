package com.fr.design.dialog;


import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.notification.Notification;
import com.fr.design.notification.NotificationCenter;
import com.fr.module.ModuleContext;
import com.fr.stable.StringUtils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.UIManager;

/**
 * 带查看详情的简要通知框
 */
public class NotificationDialog extends JDialog {
    public static final int ERROR_MESSAGE = 0;
    public static final int NEW_MESSAGE = 1;
    public static final int WARNING_MESSAGE = 2;
    public static final String HTML_TAG_1 = "<html>";
    public static final String HTML_TAG_2 = "</html>";
    private static final String HIDE_MSG = "HIDE_MSG_TIMER";
    private UILabel messageText;
    private NotificationDialogAction notificationDialogAction;
    private ScheduledExecutorService TIMER;
    private Dimension dialogSize = new Dimension(230, 95);

    public NotificationDialog(Frame owner, String title, boolean isModal, int messageType, String message, NotificationDialogAction action) {
        this(owner, title, isModal, messageType, message, action, null);
    }

    public NotificationDialog(Frame owner, String title, boolean isModal, int messageType, String message, NotificationDialogAction action, Dimension dimension) {
        super(owner);
        if (dimension != null) {
            this.dialogSize = dimension;
        }
        setTitle(title);
        initComponents(StringUtils.EMPTY, messageType, message, isModal, action);
    }

    public NotificationDialog(Builder builder) {
        super(builder.owner);
        setTitle(builder.title);
        initComponents(builder.messageId, builder.messageType, builder.message, builder.modal, builder.action);
    }

    public void initComponents(String messageId, int messageType, String message, boolean isModal, NotificationDialogAction action) {
        NotificationCenter.getInstance().addNotification(new Notification(messageId, messageType, message, action));
        notificationDialogAction = action;
        setModal(isModal);
        setFocusable(false);
        setAutoRequestFocus(false);
        setResizable(false);
        JPanel body = FRGUIPaneFactory.createBorderLayout_L_Pane();
        body.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        //消息内容
        UILabel icon = new UILabel(getIconForType(messageType));
        JPanel iconPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        iconPanel.setBorder(BorderFactory.createEmptyBorder(0, 5, 10, 8));
        iconPanel.add(icon);
        body.add(iconPanel, BorderLayout.WEST);

        messageText = new UILabel(HTML_TAG_1 + message + HTML_TAG_2);
        messageText.setForeground(new Color(51, 51, 52));
        JPanel centerPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        centerPanel.setBorder(BorderFactory.createEmptyBorder(8, 0, 5, 20));
        JScrollPane jScrollPane = new JScrollPane(messageText, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane.setBorder(BorderFactory.createEmptyBorder());
        centerPanel.add(jScrollPane, BorderLayout.CENTER);
        centerPanel.setPreferredSize(dialogSize);
        body.add(centerPanel, BorderLayout.CENTER);

        //查看详情
        UILabel detailLabel = new UILabel();
        detailLabel.setText(Toolkit.i18nText("Fine_Designer_Look_Detail"));
        detailLabel.setForeground(Color.BLUE);
        JPanel detailPanel = FRGUIPaneFactory.createBorderLayout_L_Pane();
        detailPanel.add(detailLabel, BorderLayout.EAST);
        body.add(detailPanel, BorderLayout.SOUTH);

        detailLabel.addMouseListener(detailClickListener);
        messageText.addMouseListener(detailClickListener);
        addMouseListener(bodyMouseListener);

        add(body);
        Dimension dimension = body.getPreferredSize();
        setSize(dimension.width, dimension.height);
        setLocation((DesignerContext.getDesignerFrame().getWidth() - dimension.width - 30 + DesignerContext.getDesignerFrame().getX()),
                DesignerContext.getDesignerFrame().getY() + DesignerContext.getDesignerFrame().getHeight() - dimension.height - 30);
        disappear();
    }

    private MouseListener detailClickListener = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
            if (notificationDialogAction != null) {
                hideDialog();
                notificationDialogAction.doClick();
            }
        }
    };

    private MouseListener bodyMouseListener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            if (TIMER != null) {
                TIMER.shutdownNow();
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            disappear();
        }
    };

    public void disappear() {
        TIMER = createScheduleExecutorService();
        TIMER.schedule(new Runnable() {
            @Override
            public void run() {
                hideDialog();
            }
        }, 10000, TimeUnit.MILLISECONDS);
    }

    private ScheduledExecutorService createScheduleExecutorService() {
        return ModuleContext.getExecutor().newSingleThreadScheduledExecutor(new NamedThreadFactory(HIDE_MSG));
    }

    /**
     * 设置通知消息
     */
    public void setMessage(String message) {
        messageText.setText(HTML_TAG_1 + message + HTML_TAG_2);
    }

    private void hideDialog() {
        this.setVisible(false);
        this.dispose();
    }

    protected Icon getIconForType(int messageType) {
        String propertyName;
        switch (messageType) {
            case 0:
                propertyName = "OptionPane.circularErrorIcon";
                break;
            case 1:
                propertyName = "OptionPane.newMessageIcon";
                break;
            case 2:
                propertyName = "OptionPane.circularWarningIcon";
                break;
            default:
                return null;
        }
        return UIManager.getIcon(propertyName);
    }

    public static Builder Builder() {
        return new NotificationDialog.Builder();
    }

    public static final class Builder {
        public int messageType = WARNING_MESSAGE;
        public String messageId;
        public String message;
        public boolean modal = true;
        public Frame owner = null;
        public String title;
        public NotificationDialogAction action;

        private Builder() {

        }

        public NotificationDialog build() {
            return new NotificationDialog(this);
        }

        public Builder messageId(String messageId) {
            this.messageId = messageId;
            return this;
        }

        public Builder owner(Frame owner) {
            this.owner = owner;
            return this;
        }

        public Builder messageType(int messageType) {
            this.messageType = messageType;
            return this;
        }

        public Builder message(String message) {
            this.message = message;
            return this;
        }

        public Builder modal(boolean modal) {
            this.modal = modal;
            return this;
        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder notificationDialogAction(NotificationDialogAction action) {
            this.action = action;
            return this;
        }
    }

    public static void defaultPopup(String messageId, String title, String body, NotificationDialogAction action) {
        NotificationDialog.Builder()
                .messageId(messageId)
                .owner(DesignerContext.getDesignerFrame())
                .title(title)
                .modal(false)
                .messageType(NotificationDialog.NEW_MESSAGE)
                .message(body)
                .notificationDialogAction(action)
                .build()
                .setVisible(true);
    }
}
