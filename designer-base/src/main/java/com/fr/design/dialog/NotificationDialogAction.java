package com.fr.design.dialog;

public interface NotificationDialogAction {

    String name();

    void doClick();
}
