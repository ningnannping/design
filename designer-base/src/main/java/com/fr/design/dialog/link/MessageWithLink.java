package com.fr.design.dialog.link;

import com.fr.design.utils.LinkStrUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StringUtils;

import javax.swing.JEditorPane;
import javax.swing.event.HyperlinkEvent;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.net.URI;
import java.net.URL;

import static com.fr.design.utils.LinkStrUtils.LABEL;

/**
 * 用来构建JOptionPane带超链的消息提示
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2020/10/23
 */
public class MessageWithLink extends JEditorPane {

    private static final String HTML_BODY = "<body";
    private static final String HTML_STYLE_FORMAT = " style=\"%s\"";



    /**
     * 直接放入 html 内容
     * 如果有超链接标签， 如 <a href=""></a> 的话，将会自动点击匹配 url
     *
     * @param htmlText html内容
     */
    public MessageWithLink(String htmlText) {

        super("text/html", htmlText);
        initListener();
        setEditable(false);
        setBorder(null);
    }

    public MessageWithLink(String htmlText, Runnable action) {

        super("text/html", htmlText);
        initListener(action);
        setEditable(false);
        setBorder(null);
    }

    public MessageWithLink(String message, String linkName, String link) {
        this(message, linkName, link, LABEL.getBackground(), LABEL.getFont());
    }

    public MessageWithLink(String linkName, String link ) {
        this(StringUtils.EMPTY, linkName, link);
    }

    public MessageWithLink(String message, String linkName, String link, Color color) {
        this(message, linkName, link, color, LABEL.getFont());
    }

    public MessageWithLink(String frontMessage, String linkName, String link, String backMessage) {
        this(frontMessage, linkName, link, backMessage, LABEL.getBackground(), LABEL.getFont(), LABEL.getForeground());
    }

    public MessageWithLink(String message, String linkName, String link, Color color, Font font) {
        this(message, linkName, link, StringUtils.EMPTY, color, font, LABEL.getForeground());
    }

    public MessageWithLink(String frontMessage, String linkName, String link, String backMessage, Color color, Font font) {
        this(frontMessage, linkName, link, backMessage, color, font, LABEL.getForeground());
    }

    public MessageWithLink(String htmlText, Font font) {
        this(setHtmlFont(htmlText, font));
    }



    /**
     * 将指定的字体赋给html文本
     * 任何失败，返回原html文本
     * */
    private static String setHtmlFont(String htmlText, Font font) {

        try {
            int bodyIndex = htmlText.indexOf(HTML_BODY);
            StringBuilder sb = new StringBuilder();
            String left = htmlText.substring(0, bodyIndex + HTML_BODY.length());
            String right = htmlText.substring(bodyIndex + HTML_BODY.length());
            sb.append(left);
            sb.append(String.format(HTML_STYLE_FORMAT, LinkStrUtils.generateStyle(Color.WHITE, font, Color.BLACK)));
            sb.append(right);
            return sb.toString();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e, e.getMessage());
        }

        return htmlText;
    }

    public MessageWithLink(String frontMessage, String linkName, String link, String backMessage, Color backgroundColor, Font font, Color fontColor) {

        super("text/html", "<html><body style=\"" + LinkStrUtils.generateStyle(backgroundColor, font, fontColor) + "\">" + frontMessage + "<a href=\"" + link + "\">" + linkName + "</a>" + backMessage + "</body></html>");
        initListener(link);
        setEditable(false);
        setBorder(null);
    }

    protected void initListener() {

        addHyperlinkListener(hyperlinkEvent -> {
            try {
                if (hyperlinkEvent.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                    URL url = hyperlinkEvent.getURL();
                    Desktop.getDesktop().browse(url.toURI());
                }
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        });
    }

    protected void initListener(Runnable runnable) {

        addHyperlinkListener(e -> {
            if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
                runnable.run();
            }
        });
    }

    protected void initListener(String link) {

        initListener(() -> {
            try {
                Desktop.getDesktop().browse(URI.create(link));
            } catch (Exception e) {
                FineLoggerFactory.getLogger().error(e.getMessage(), e);
            }
        });
    }

}
