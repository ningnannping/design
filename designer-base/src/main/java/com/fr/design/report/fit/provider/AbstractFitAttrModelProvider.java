package com.fr.design.report.fit.provider;

import com.fr.stable.fun.mark.API;


@API(level = FitAttrModelProvider.CURRENT_LEVEL)
public abstract class AbstractFitAttrModelProvider implements FitAttrModelProvider {

    @Override
    public int currentAPILevel() {
        return CURRENT_LEVEL;
    }

    @Override
    public String mark4Provider() {
        return getClass().getName();
    }
}