package com.fr.design.report.fit.menupane;

import com.fr.design.actions.JTemplateAction;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.dialog.UIDialog;
import com.fr.design.i18n.DesignSizeI18nManager;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.menu.MenuKeySet;
import com.fr.report.fit.FitProvider;
import com.fr.report.fit.ReportFitAttr;

import javax.swing.KeyStroke;
import java.awt.event.ActionEvent;

/**
 * Created by Administrator on 2015/7/6 0006.
 */
public class ReportFitAttrAction extends JTemplateAction {
    private static final MenuKeySet REPORT_FIT_ATTR = new MenuKeySet() {
        @Override
        public char getMnemonic() {
            return 'T';
        }

        @Override
        public String getMenuName() {
            return Toolkit.i18nText("Fine-Designer_PC_Fit_Attr");
        }

        @Override
        public KeyStroke getKeyStroke() {
            return null;
        }
    };

    public ReportFitAttrAction(JTemplate jTemplate) {
        super(jTemplate);
        initMenuStyle();
    }

    private void initMenuStyle() {
        this.setMenuKeySet(REPORT_FIT_ATTR);
        this.setName(getMenuKeySet().getMenuKeySetName() + "...");
        this.setMnemonic(getMenuKeySet().getMnemonic());
        this.setSmallIcon("/com/fr/design/images/reportfit/fit");
    }

    /**
     * Action触发事件
     *
     * @param e 事件
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        final JTemplate jwb = getEditingComponent();
        if (jwb == null) {
            return;
        }
        final FitProvider wbTpl = (FitProvider) jwb.getTarget();
        ReportFitAttr fitAttr = wbTpl.getReportFitAttr();
        TemplateFitAttrPane templateFitAttrPane = new TemplateFitAttrPane(jwb);
        showReportFitDialog(fitAttr, jwb, wbTpl, templateFitAttrPane);
    }

    private void showReportFitDialog(ReportFitAttr fitAttr, final JTemplate jwb, final FitProvider wbTpl, final BasicBeanPane<ReportFitAttr> attrPane) {
        attrPane.populateBean(fitAttr);
        UIDialog dialog = attrPane.showWindowWithCustomSize(DesignerContext.getDesignerFrame(), new DialogActionAdapter() {
            @Override
            public void doOk() {
                wbTpl.setReportFitAttr(attrPane.updateBean());
                jwb.fireTargetModified();
            }
        }, DesignSizeI18nManager.getInstance().i18nDimension("com.fr.design.report.fit.templatePane.dialog"));
        dialog.setVisible(true);
    }

}
