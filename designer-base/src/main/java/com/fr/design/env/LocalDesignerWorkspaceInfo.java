package com.fr.design.env;

import com.fr.general.ComparatorUtils;
import com.fr.general.GeneralUtils;
import com.fr.locale.InterProviderFactory;
import com.fr.stable.CoreConstants;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;
import com.fr.stable.project.ProjectConstants;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.workspace.connect.WorkspaceConnectionInfo;

import com.fr.workspace.engine.exception.MainVersionNotMatchException;
import java.io.File;
import java.util.Properties;

/**
 * Created by juhaoyu on 2018/6/15.
 */
public class LocalDesignerWorkspaceInfo implements DesignerWorkspaceInfo {

    private static final String REPORT_ENGINE_KEY = "report-engine-key";
    private static final String REPORT_ENGINE_JAR;
    private static final String PROP_PATH = "/com/fr/env/jarVersion.properties";

    static {
        Properties properties = new Properties();
        try {
            properties.load(LocalDesignerWorkspaceInfo.class.getResourceAsStream(PROP_PATH));
        } catch (Exception ignored) {
        }
        REPORT_ENGINE_JAR = properties.getProperty(REPORT_ENGINE_KEY, "default.jar");
    }

    private String name;

    private String path;

    public static LocalDesignerWorkspaceInfo create(String name, String path) {

        LocalDesignerWorkspaceInfo info = new LocalDesignerWorkspaceInfo();
        info.name = name;
        info.path = path;
        return info;
    }

    @Override
    public DesignerWorkspaceType getType() {

        return DesignerWorkspaceType.Local;
    }

    @Override
    public String getName() {

        return name;
    }

    @Override
    public String getPath() {

        return path;
    }

    @Override
    public String getRemindTime() {
        return null;
    }

    @Override
    public WorkspaceConnectionInfo getConnection() {
        return null;
    }

    @Override
    public void readXML(XMLableReader reader) {

        if (reader.isAttr()) {
            this.name = reader.getAttrAsString("name", StringUtils.EMPTY);
            this.path = reader.getAttrAsString("path", StringUtils.EMPTY);
        }
    }

    @Override
    public void writeXML(XMLPrintWriter writer) {

        writer.attr("name", name);
        writer.attr("path", path);
    }


    @Override
    @SuppressWarnings("squid:S2975")
    public Object clone() throws CloneNotSupportedException {
        LocalDesignerWorkspaceInfo object = (LocalDesignerWorkspaceInfo)super.clone();

        return  object;
    }

    @Override
    public boolean checkValid(){
        File file = new File(this.path);
        //判断不是文件夹/路径不在WEB-INF下/代码启动三种情况
        if(!file.isDirectory() || !ComparatorUtils.equals(file.getName(), "WEB-INF") || this.path.startsWith(".")) {
            return false;
        }

        File engineLib = new File(StableUtils.pathJoin(this.path, ProjectConstants.LIB_NAME, REPORT_ENGINE_JAR));
        // 非安装版本允许自由切换
        boolean notExistLib = !CoreConstants.DOT.equals(StableUtils.getInstallHome())
                && !engineLib.exists();
        if (notExistLib) {
            throw new MainVersionNotMatchException();
        }

        return true;
    }
}
