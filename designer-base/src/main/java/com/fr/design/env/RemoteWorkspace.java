package com.fr.design.env;

import com.fr.cluster.engine.rpc.remote.ClusterOperator;
import com.fr.design.i18n.Toolkit;
import com.fr.base.operator.common.CommonOperator;
import com.fr.rpc.ExceptionHandler;
import com.fr.rpc.RPCInvokerExceptionInfo;
import com.fr.stable.AssistUtils;
import com.fr.workspace.WorkContext;
import com.fr.workspace.Workspace;
import com.fr.workspace.connect.WorkspaceClient;
import com.fr.workspace.connect.WorkspaceConnection;
import com.fr.workspace.connect.WorkspaceConnectionInfo;
import com.fr.workspace.server.authority.decision.DecisionOperator;
import com.fr.workspace.engine.rpc.WorkspaceProxyPool;
import com.fr.workspace.pool.WorkObjectPool;
import javax.swing.SwingWorker;

/**
 * Created by juhaoyu on 2018/6/14.
 * 远程工作目录
 */
public class RemoteWorkspace implements Workspace {

    private final WorkspaceClient client;

    private final String address;

    private final WorkspaceConnectionInfo connection;

    private volatile Boolean isRoot = null;

    private volatile Boolean cluster;

    private volatile Boolean warDeploy;

    private final SwingWorker<Void,Void> swingWorker;

    RemoteWorkspace(WorkspaceClient client, WorkspaceConnectionInfo connection) {

        this.client = client;
        this.address = connection.getUrl();
        this.connection = connection;
        this.swingWorker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                client.close();
                return null;
            }
        };
    }

    @Override
    public String getPath() {

        return address;
    }

    @Override
    public String getDescription() {

        return Toolkit.i18nText("Fine-Design_Basic_Remote_Env");
    }

    @Override
    public boolean isWarDeploy() {
        if (warDeploy == null) {
            warDeploy = WorkContext.getCurrent().get(CommonOperator.class, new ExceptionHandler<Boolean>() {
                @Override
                public Boolean callHandler(RPCInvokerExceptionInfo rpcInvokerExceptionInfo) {
                    return false;
                }
            }).isWarDeploy();
        }
        return warDeploy;
    }

    @Override
    public boolean isLocal() {

        return false;
    }

    @Override
    public boolean isRoot() {

        if (isRoot == null) {
            synchronized (this) {
                if (isRoot == null) {
                    isRoot = WorkContext.getCurrent().get(DecisionOperator.class).isRoot(getConnection().getUserName());
                }
            }
        }
        return isRoot;
    }

    @Override
    public boolean isCluster() {
        if (cluster == null) {
            cluster = WorkContext.getCurrent().get(ClusterOperator.class, new ExceptionHandler<Boolean>() {
                @Override
                public Boolean callHandler(RPCInvokerExceptionInfo rpcInvokerExceptionInfo) {
                    return false;
                }
            }).isCluster();
        }
        return cluster;
    }

    @Override
    public WorkspaceConnection getConnection() {

        return client.getConnection();
    }

    @Override
    public <T> T get(Class<T> type) {

        return client.getPool().get(type);
    }

    @Override
    public <T> T get(Class<T> type, ExceptionHandler exceptionHandler){
        if(exceptionHandler != null) {
            WorkObjectPool objectPool = client.getPool();
            if (objectPool instanceof WorkspaceProxyPool) {
                return ((WorkspaceProxyPool) objectPool).get(type, exceptionHandler);
            }else {
                return client.getPool().get(type);
            }
        }
        return client.getPool().get(type);
    }

    @Override
    public void close() {
        swingWorker.execute();
    }

    @Override
    public int hashCode() {

        return connection.hashCode();
    }

    @Override
    public boolean equals(Object obj) {

        return obj instanceof RemoteWorkspace
                && AssistUtils.equals(((RemoteWorkspace) obj).connection, this.connection)
                && AssistUtils.equals(((RemoteWorkspace) obj).client.getConnection(), this.client.getConnection());
    }

    public WorkspaceClient getClient(){
        return client;
    }
}
