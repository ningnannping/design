package com.fr.design.gui.itextfield;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;


/**
 * Number Field.
 */
public class EditTextField extends UIGridTextField {

	private static final String I18NProperty = "i18n";

	private int maxLength = 24;

	public EditTextField() {
		this(10000);
	}

	public EditTextField(int maxLength) {
		this.maxLength = maxLength;
		setDocument(new TextDocument());
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	@Override
	public void paint(Graphics g) {
		FRGraphics2D FRg2d = new FRGraphics2D((Graphics2D)g);
		super.paint(FRg2d);
	}

	class TextDocument extends PlainDocument {

		private Object defaultI18NProperty;

		public TextDocument() {
			defaultI18NProperty = getProperty(I18NProperty);
		}

		@Override
		public void insertString(int offset, String s, AttributeSet a) throws BadLocationException {
			String str = getText(0, getLength());
			if (str != null && str.length() > getMaxLength()) {
				Toolkit.getDefaultToolkit().beep();
				return;
			}
			putProperty(I18NProperty, defaultI18NProperty);
			super.insertString(offset, s, a);
		}
	}
}