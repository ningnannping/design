package com.fr.design.gui.controlpane;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.constants.UIConstants;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.ilist.JNameEdList;
import com.fr.design.gui.ilist.ListModelElement;
import com.fr.design.gui.ilist.UINameEdList;
import com.fr.form.event.Listener;
import com.fr.stable.ArrayUtils;
import com.fr.stable.Nameable;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.BorderLayout;
import java.awt.Point;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by plough on 2017/7/19.
 */

public abstract class UIListControlPane extends UIControlPane implements ListControlPaneProvider {
    private static final String LIST_NAME = "UIControl_List";

    protected UINameEdList nameableList;
    protected boolean isPopulating = false;
    private CommonShortCutHandlers commonHandlers;
    private ListControlPaneHelper helper;


    public UIListControlPane() {
        super();

    }

    private ListControlPaneHelper getHelper() {
        if (helper == null) {
            helper = ListControlPaneHelper.newInstance(this);
        }
        return helper;
    }

    private CommonShortCutHandlers getCommonHandlers() {
        if (commonHandlers == null) {
            commonHandlers = CommonShortCutHandlers.newInstance(this);
        }
        return commonHandlers;
    }

    @Override
    protected JPanel createControlUpdatePane() {
        return JControlUpdatePane.newInstance(this);
    }

    /**
     * 生成添加按钮的NameableCreator
     *
     * @return 按钮的NameableCreator
     */
    @Override
    public abstract NameableCreator[] createNameableCreators();


    @Override
    protected void initLeftPane(JPanel leftPane) {
        nameableList = createJNameList();
        nameableList.setName(LIST_NAME);
        nameableList.setSelectionBackground(UIConstants.ATTRIBUTE_PRESS);
        leftPane.add(new UIScrollPane(nameableList), BorderLayout.CENTER);


        nameableList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        nameableList.addMouseListener(getHelper().getListMouseListener(nameableList, this));
        nameableList.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                // richie:避免多次update和populate大大降低效率
                if (!evt.getValueIsAdjusting()) {
                    // shoc 切换的时候加检验
                    if (hasInvalid(false)) {
                        return;
                    }
                    ((JControlUpdatePane) UIListControlPane.this.controlUpdatePane).update();
                    ((JControlUpdatePane) UIListControlPane.this.controlUpdatePane).populate();
                    UIListControlPane.this.checkButtonEnabled();
                }
            }
        });
        if (isNewStyle()) {
            nameableList.getModel().addListDataListener(new ListDataListener() {
                @Override
                public void intervalAdded(ListDataEvent e) {
                    saveSettings();
                }

                @Override
                public void intervalRemoved(ListDataEvent e) {
                    saveSettings();
                }

                @Override
                public void contentsChanged(ListDataEvent e) {
                    saveSettings();
                }
            });
        }
    }

    private UINameEdList createJNameList() {
        UINameEdList nameEdList = new UINameEdList(new DefaultListModel()) {
            @Override
            protected void doAfterLostFocus() {
                UIListControlPane.this.updateControlUpdatePane();
            }
            @Override
            protected void doAfterStopEditing() {
                saveSettings();
            }
        };
        nameEdList.setCellRenderer(new UINameableListCellRenderer(this.isNewStyle(), this.creators));
        return nameEdList;
    }

    private void updateControlUpdatePane() {
        ((JControlUpdatePane) controlUpdatePane).update();
    }

    protected void setNameListEditable(boolean editable) {
        this.nameableList.setEditable(editable);
    }

    @Override
    public Nameable[] update() {
        return getHelper().update();
    }

    @Override
    public void populate(Nameable[] nameableArray) {
        isPopulating = true;  // 加一个标识位，避免切换单元格时，触发 saveSettings
        nameableList.getCellEditor().stopCellEditing();
        DefaultListModel listModel = (DefaultListModel) this.nameableList.getModel();
        listModel.removeAllElements();
        if (ArrayUtils.isEmpty(nameableArray)) {
            isPopulating = false;
            return;
        }

        listModel.setSize(nameableArray.length);
        for (int i = 0; i < nameableArray.length; i++) {
            listModel.set(i, new ListModelElement(nameableArray[i]));
        }

        if (listModel.size() > 0 || this.nameableList.getSelectedIndex() != 0) {
            this.nameableList.setSelectedIndex(0);
        }
        this.checkButtonEnabled();

        isPopulating = false;
    }


    /**
     * 添加 Nameable
     *
     * @param nameable 添加的Nameable
     * @param index    序号
     */
    public void addNameable(Nameable nameable, int index) {
        getHelper().addNameable(nameable, index);
        popupEditDialog();
    }

    public DefaultListModel getModel() {
        return (DefaultListModel) UIListControlPane.this.nameableList.getModel();
    }

    private void popupEditDialog() {
        popupEditDialog(null);
    }

    protected void popupEditDialog(Point mousePos) {
        if (isNewStyle()) {
            getHelper().popupEditDialog(mousePos, nameableList, this);
        }
    }


    /**
     * 生成不重复的名字
     *
     * @param prefix 名字前缀
     * @return 名字
     */
    @Override
    public String createUnrepeatedName(String prefix) {
        return getCommonHandlers().createUnrepeatedName(prefix);
    }

    @Override
    public void onAddItem(NameableCreator creator) {
        getCommonHandlers().onAddItem(creator);
    }

    @Override
    public void onRemoveItem() {
        getCommonHandlers().onRemoveItem();
    }

    @Override
    public void onCopyItem() {
        getCommonHandlers().onCopyItem();
    }

    @Override
    public void onMoveUpItem() {
        getCommonHandlers().onMoveUpItem();
    }

    @Override
    public void onMoveDownItem() {
        getCommonHandlers().onMoveDownItem();
    }

    @Override
    public void onSortItem(boolean isAtoZ) {
        getCommonHandlers().onSortItem(isAtoZ);
    }

    @Override
    public boolean isItemSelected() {
        return getModel().getSize() > 0 && nameableList.getSelectedIndex() != -1;
    }


    /**
     * 检查按钮可用状态 Check button enabled.
     */
    @Override
    public void checkButtonEnabled() {
        getHelper().checkButtonEnabled();
    }

    public BasicBeanPane createPaneByCreators(NameableCreator creator) {
        try {
            return creator.getUpdatePane().newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public BasicBeanPane createPaneByCreators(NameableCreator creator, String string) {
        Constructor constructor = null;
        try {
            constructor = creator.getUpdatePane().getDeclaredConstructor(new Class[]{String.class});
            constructor.setAccessible(true);
            return (BasicBeanPane) constructor.newInstance(string);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 检查是否符合规范
     *
     * @throws Exception
     */
    @Override
    public void checkValid() throws Exception {
        ((JControlUpdatePane) this.controlUpdatePane).checkValid();
    }

    @Override
    public boolean hasInvalid(boolean isAdd) {
        return getHelper().hasInvalid(isAdd);
    }
    /**
     * 设置选中项
     *
     * @param index 选中项的序列号
     */
    public void setSelectedIndex(int index) {
        nameableList.setSelectedIndex(index);
    }

    @Override
    public int getSelectedIndex() {
        return nameableList.getSelectedIndex();
    }

    @Override
    public ListModelElement getSelectedValue() {
        return (ListModelElement) this.nameableList.getSelectedValue();
    }

    @Override
    public JControlUpdatePane getControlUpdatePane() {
        return (JControlUpdatePane) controlUpdatePane;
    }

    @Override
    public JNameEdList getNameableList() {
        return nameableList;
    }

    @Override
    public void wrapperListener(Listener listener){

    }

}
