package com.fr.design.gui.chart;

import com.fr.chart.chartattr.ChartCollection;

import java.util.EventListener;

/**
 * @author shine
 * @version 10.0
 * Created by shine on 2021/5/26
 */
public interface ChartEditPaneActionListener extends EventListener {

    void attributeChange(ChartCollection chartCollection);

}
