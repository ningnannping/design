package com.fr.design.gui.controlpane;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.gui.ilist.JNameEdList;
import com.fr.design.gui.ilist.ListModelElement;
import com.fr.design.gui.ilist.UINameEdList;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.os.impl.PopupDialogSaveAction;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.event.Listener;
import com.fr.general.NameObject;
import com.fr.stable.Nameable;
import com.fr.stable.StringUtils;
import com.fr.stable.os.support.OSSupportCenter;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import java.awt.Component;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 存放一些公用的方法
 * Created by plough on 2018/8/13.
 */
class ListControlPaneHelper {
    private static final int EDIT_RANGE = 25;  // 编辑按钮的x坐标范围

    private ListControlPaneProvider listControlPane;

    private ListControlPaneHelper(ListControlPaneProvider listControlPane) {
        this.listControlPane = listControlPane;
    }

    public static ListControlPaneHelper newInstance(ListControlPaneProvider listControlPane) {
        return new ListControlPaneHelper(listControlPane);
    }

    public boolean hasInvalid(boolean isAdd) {

        int idx = getInValidIndex();
        if (isAdd || listControlPane.getSelectedIndex() != idx) {
            try {
                listControlPane.checkValid();
            } catch (Exception exp) {
                FineJOptionPane.showMessageDialog((Component) listControlPane, exp.getMessage());
                listControlPane.setSelectedIndex(idx);
                return true;
            }
        }
        return false;
    }

    private int getInValidIndex() {
        BasicBeanPane[] p = listControlPane.getControlUpdatePane().getUpdatePanes();
        if (p != null) {
            for (int i = 0; i < p.length; i++) {
                if (p[i] != null) {
                    try {
                        p[i].checkValid();
                    } catch (Exception e) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    public Nameable[] update() {
        java.util.List<Nameable> res = new java.util.ArrayList<Nameable>();
        listControlPane.getControlUpdatePane().update();
        DefaultListModel listModel = listControlPane.getModel();
        for (int i = 0, len = listModel.getSize(); i < len; i++) {
            res.add(((ListModelElement) listModel.getElementAt(i)).wrapper);
        }

        return res.toArray(new Nameable[0]);
    }

    /**
     * 获取选中的名字
     */
    public String getSelectedName() {
        ListModelElement el = listControlPane.getSelectedValue();
        return el == null ? StringUtils.EMPTY : el.wrapper.getName();
    }

    /**
     * 添加 Nameable
     *
     * @param nameable 添加的Nameable
     * @param index    序号
     */
    public void addNameable(Nameable nameable, int index) {
        JNameEdList nameableList = listControlPane.getNameableList();
        DefaultListModel model = listControlPane.getModel();

        ListModelElement el = new ListModelElement(nameable);
        model.add(index, el);
        nameableList.setSelectedIndex(index);
        nameableList.ensureIndexIsVisible(index);
        nameableList.repaint();
    }

    /**
     * 检查按钮可用状态 Check button enabled.
     */
    public void checkButtonEnabled() {

        int selectedIndex = listControlPane.getSelectedIndex();
        if (selectedIndex == -1) {
            listControlPane.showSelectPane();
        } else {
            listControlPane.showEditPane();
        }
        for (ShortCut4JControlPane sj : listControlPane.getShorts()) {
            sj.checkEnable();
        }
    }


    protected void popupEditDialog(Point mousePos, UINameEdList nameableList, UIControlPane controlPane) {
        int editingIndex = nameableList.getSelectedIndex();
        Rectangle currentCellBounds = nameableList.getCellBounds(editingIndex, editingIndex);
        if (editingIndex < 0 || (mousePos != null && !currentCellBounds.contains(mousePos))) {
            return;
        }
        Window popupEditDialog = controlPane.getPopupEditDialog();
        popupEditDialog.setLocation(getPopupDialogLocation(nameableList, popupEditDialog));
        if (popupEditDialog instanceof UIControlPane.PopupEditDialog) {
            ((UIControlPane.PopupEditDialog) popupEditDialog).setTitle(getSelectedName());
        }
        popupEditDialog.setVisible(true);
        PopupDialogSaveAction osBasedAction = OSSupportCenter.getAction(PopupDialogSaveAction.class);
        osBasedAction.register(controlPane, popupEditDialog);
    }

    private Point getPopupDialogLocation(UINameEdList nameableList, Window popupEditDialog) {
        Point resultPos = new Point(0, 0);
        Point listPos = nameableList.getLocationOnScreen();
        resultPos.x = listPos.x - popupEditDialog.getWidth();
        resultPos.y = listPos.y + (nameableList.getSelectedIndex() - 1) * EDIT_RANGE;

        // 当对象在屏幕上的位置比较靠下时，往下移动弹窗至与属性面板平齐
        Window frame = DesignerContext.getDesignerFrame();
        // 不能太低
        int maxY = frame.getLocationOnScreen().y + frame.getHeight() - popupEditDialog.getHeight();
        if (resultPos.y > maxY) {
            resultPos.y = maxY;
        }
        // 也不能太高
        int minY = frame.getLocationOnScreen().y + EDIT_RANGE;
        if (resultPos.y < minY) {
            resultPos.y = minY;
        }

        // 当在左侧显示不下时，在右侧弹出弹窗
        if (resultPos.x < 0) {
            resultPos.x = listPos.x + nameableList.getParent().getWidth();
        }
        // 如果右侧显示不下，可以向左移动
        int maxX = frame.getLocationOnScreen().x + frame.getWidth() - popupEditDialog.getWidth() - EDIT_RANGE;
        if (resultPos.x > maxX) {
            resultPos.x = maxX;
        }

        return resultPos;
    }

    /*
     * UINameEdList的鼠标事件
     */
    protected MouseListener getListMouseListener(UINameEdList nameableList,  UIControlPane controlPane) {
        return new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent evt) {
                nameableList.stopEditing();
                if (evt.getClickCount() >= 2
                        && SwingUtilities.isLeftMouseButton(evt) && evt.getX() > EDIT_RANGE) {
                    nameableList.editItemAt(nameableList.getSelectedIndex());
                } else if (SwingUtilities.isLeftMouseButton(evt) && evt.getX() <= EDIT_RANGE) {
                    popupEditDialog(evt.getPoint(), nameableList, controlPane);
                }

                // peter:处理右键的弹出菜单
                if (!SwingUtilities.isRightMouseButton(evt)) {
                    return;
                }

                // peter: 注意,在checkButtonEnabled()方法里面,设置了所有的Action的Enabled.
                checkButtonEnabled();

                // p:右键菜单.
                JPopupMenu popupMenu = new JPopupMenu();

                for (ShortCut4JControlPane sj : listControlPane.getShorts()) {
                    sj.getShortCut().intoJPopupMenu(popupMenu);
                }

                // peter: 只有弹出菜单有子菜单的时候,才需要弹出来.
                GUICoreUtils.showPopupMenu(popupMenu, nameableList, evt.getX() - 1,
                        evt.getY() - 1);
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                JList list = (JList) e.getSource();
                if (list.locationToIndex(e.getPoint()) == -1 && !e.isShiftDown()
                        && !isMenuShortcutKeyDown(e)) {
                    list.clearSelection();
                }
            }

            private boolean isMenuShortcutKeyDown(InputEvent event) {
                return (event.getModifiers() & Toolkit.getDefaultToolkit()
                        .getMenuShortcutKeyMask()) != 0;
            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        };
    }

    public Map<String, List<NameObject>> processCatalog(List<NameObject> nameObjectList) {
        Map<String, List<NameObject>> map = new HashMap<>();
        for (NameObject nameObject : nameObjectList) {
            Listener listener = (Listener) nameObject.getObject();
            if (StringUtils.isNotEmpty(listener.getName())) {
                nameObject.setName(listener.getName());
            }
            List<NameObject> list = map.computeIfAbsent(listener.getEventName(), k -> new ArrayList<>());
            list.add(nameObject);
        }
        return map;
    }



}
