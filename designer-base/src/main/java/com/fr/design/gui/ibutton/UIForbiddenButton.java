package com.fr.design.gui.ibutton;

import com.fr.base.CellBorderStyle;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.JTemplate;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.plaf.ButtonUI;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/9
 */
public class UIForbiddenButton extends UIButton {

    public UIForbiddenButton() {
        super();
    }

    public UIForbiddenButton(String string) {
        super(string);
    }

    public UIForbiddenButton(Icon icon) {
        super(icon);
    }

    public UIForbiddenButton(Action action) {
        super(action);
    }

    public UIForbiddenButton(String text, Icon icon) {
        super(text, icon);
    }

    public UIForbiddenButton(Icon normal, Icon rollOver, Icon pressed) {
        super(normal, rollOver, pressed);
    }

    public UIForbiddenButton(String resource, boolean needSetDisabledIcon) {
        super(resource, needSetDisabledIcon);
    }

    @Override
    public CellBorderStyle getBorderStyle() {
        return super.getBorderStyle();
    }

    @Override
    public boolean isEnabled() {
        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        boolean enabled = true;
        if (template != null) {
            enabled = super.isEnabled() && template.checkEnable();
        }
        return enabled;
    }

    @Override
    public ButtonUI getUI() {
        return new UIForbiddenButtonUI();
    }
}
