package com.fr.design.gui.ispinner.chart;

import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.itextfield.UINumberField;
import com.fr.design.gui.itextfield.UINumberFieldWithUnit;

/**
 * @author Bjorn
 * @version 10.0
 * Created by Bjorn on 2021-01-20
 */
public class UISpinnerWithUnit extends UISpinner {

    private String unit;

    public UISpinnerWithUnit(double minValue, double maxValue, double dierta, double defaultValue, String unit) {
        this.unit = unit;
        init(minValue, maxValue, dierta);
        getTextField().setValue(defaultValue);
    }

    @Override
    protected UINumberField initNumberField() {
        return new UINumberFieldWithUnit(3, unit);
    }
}
