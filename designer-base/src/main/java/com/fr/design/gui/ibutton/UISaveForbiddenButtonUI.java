package com.fr.design.gui.ibutton;

import java.awt.Graphics;
import javax.swing.JComponent;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/14
 */
public class UISaveForbiddenButtonUI extends UIButtonUI {

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        c.setEnabled(c.isEnabled());
    }
}
