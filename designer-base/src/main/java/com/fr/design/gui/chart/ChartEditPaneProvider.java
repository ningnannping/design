package com.fr.design.gui.chart;

/**
 * Created by daniel on 2016/11/2.
 */
public interface ChartEditPaneProvider {


    void gotoPane(String... id);

    void fire();

    default void addChartEditPaneActionListener(ChartEditPaneActionListener l) {
    }

    default void removeChartEditPaneActionListener(ChartEditPaneActionListener l) {
    }

    default void resetLastChartCollection() {
    }

}
