package com.fr.design.gui.ifilechooser;

import com.fr.design.i18n.Toolkit;
import com.fr.design.os.impl.SupportOSImpl;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/21
 */
public class FileChooserFactory {

    public static FileChooserProvider createFileChooser(FileChooserArgs fileChooserArgs) {
        if (SupportOSImpl.OLD_STYLE_CHOOSER.support()) {
            return new SwingFileChooser.Builder().
                    setFileSelectionMode(fileChooserArgs.getFileSelectionMode()).
                    setFileFilter(fileChooserArgs.getFilterDes(), fileChooserArgs.getExtensions()).
                    setSelectedFile(fileChooserArgs.getSelectedPath()).
                    setMultiSelectionEnabled(fileChooserArgs.isMultiSelectionEnabled()).
                    setTipText(fileChooserArgs.getTipText()).
                    setFileFilter(fileChooserArgs.getFilters()).build();
        } else {
            return new JavaFxNativeFileChooser.Builder().
                    fileSelectionMode(fileChooserArgs.getFileSelectionMode()).
                    filter(fileChooserArgs.getFilterDes(), fileChooserArgs.getExtensions()).
                    currentDirectory(fileChooserArgs.getSelectedPath()).
                    title(fileChooserArgs.getTipText()).
                    filters(fileChooserArgs.getFilters()).build();
        }
    }

    public static FileChooserProvider createImageFileChooser() {
        if (SupportOSImpl.OLD_STYLE_CHOOSER.support()) {
            return new SwingImageFileChooser();
        } else {
            return new JavaFxNativeFileChooser.Builder().
                    fileSelectionMode(FileSelectionMode.FILE).
                    title(Toolkit.i18nText("Fine-Design_Basic_Open")).
                    filter(Toolkit.i18nText("Fine-Design_Basic_Image_Image_Files"), "*.jpg", "*.gif", "*.png", "*.bmp").
                    build();
        }
    }

}
