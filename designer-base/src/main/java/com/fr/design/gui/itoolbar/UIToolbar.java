package com.fr.design.gui.itoolbar;

import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.JTemplate;
import java.awt.Component;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.JToolBar;

public class UIToolbar extends JToolBar {

    public UIToolbar() {
        this(FlowLayout.LEFT);
    }

    public UIToolbar(int align, UIToolBarUI uiToolBarUI) {
        super();
        setFloatable(false);
        setRollover(true);
        setLayout(new FlowLayout(align, 4, 0));
        setUI(uiToolBarUI);
        setBorderPainted(false);
    }

    public UIToolbar(int align) {
        this(align, new UIToolBarUI());
    }

    public void checkComponentsByNames(boolean flag, ArrayList<String> names) {
        for (int i = 0; i < getComponentCount(); i++) {
            Component component = getComponents()[i];
            if (names.contains(component.getName())) {
                component.setEnabled(flag);
            }
        }
    }

    public void refreshUIToolBar() {
        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        if (template != null) {
            for (int i = 0; i < getComponentCount(); i++) {
                Component component = getComponents()[i];
                component.setEnabled(template.checkEnable());
            }
        }
    }
}