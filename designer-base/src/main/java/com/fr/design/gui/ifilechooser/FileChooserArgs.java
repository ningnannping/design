package com.fr.design.gui.ifilechooser;

/**
 * 文件选择器可设置的参数集合
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/12/21
 */
public class FileChooserArgs {

    private final FileSelectionMode fileSelectionMode;
    private final String filterDes;
    private final String[] extensions;
    private final String selectedPath;
    private final ExtensionFilter[] filters;
    private final String tipText;
    private final boolean multiSelectionEnabled;

    public static Builder newBuilder() {
        return new Builder();
    }

    private FileChooserArgs(Builder builder) {
        this.fileSelectionMode = builder.fileSelectionMode;
        this.filterDes = builder.filterDes;
        this.extensions = builder.extensions;
        this.selectedPath = builder.selectedPath;
        this.filters = builder.filters;
        this.tipText = builder.tipText;
        this.multiSelectionEnabled = builder.multiSelectionEnabled;
    }

    public FileSelectionMode getFileSelectionMode() {
        return fileSelectionMode;
    }

    public String getFilterDes() {
        return filterDes;
    }

    public String[] getExtensions() {
        return extensions;
    }

    public String getSelectedPath() {
        return selectedPath;
    }

    public ExtensionFilter[] getFilters() {
        return filters;
    }

    public String getTipText() {
        return tipText;
    }

    public boolean isMultiSelectionEnabled() {
        return multiSelectionEnabled;
    }

    public static class Builder {

        private FileSelectionMode fileSelectionMode;
        private String filterDes;
        private String[] extensions;
        private String selectedPath;
        private ExtensionFilter[] filters = new ExtensionFilter[0];
        private String tipText;
        private boolean multiSelectionEnabled;

        public Builder setFileSelectionMode(FileSelectionMode fileSelectionMode) {
            this.fileSelectionMode = fileSelectionMode;
            return this;
        }

        public Builder setFilter(String filterDes, String... extensions) {
            this.filterDes = filterDes;
            this.extensions = extensions;
            return this;
        }

        public Builder setSelectedPath(String selectedPath) {
            this.selectedPath = selectedPath;
            return this;
        }

        public Builder setFilters(ExtensionFilter[] filters) {
            this.filters = filters;
            return this;
        }

        public Builder setTipText(String tipText) {
            this.tipText = tipText;
            return this;
        }

        public Builder setMultiSelectionEnabled(boolean multiSelectionEnabled) {
            this.multiSelectionEnabled = multiSelectionEnabled;
            return this;
        }

        public FileChooserArgs build() {
            return new FileChooserArgs(this);
        }
    }


}
