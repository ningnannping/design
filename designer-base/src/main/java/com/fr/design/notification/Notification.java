package com.fr.design.notification;

import com.fr.design.dialog.NotificationDialogAction;
import com.fr.stable.StringUtils;

public class Notification {
    private String messageId;
    private int type;
    private String message;
    private NotificationDialogAction notificationDialogAction;
    public static final int ERROR_MESSAGE = 0;
    public static final int NEW_MESSAGE = 1;
    public static final int WARNING_MESSAGE = 2;

    public Notification(String messageId, int type, String message, NotificationDialogAction notificationDialogAction) {
        this.messageId = messageId;
        this.type = type;
        this.message = message;
        this.notificationDialogAction = notificationDialogAction;
    }

    public Notification(String message) {
        this.type = WARNING_MESSAGE;
        this.message = message;
        this.notificationDialogAction = new NotificationDialogAction() {
            @Override
            public String name() {
                return StringUtils.EMPTY;
            }

            @Override
            public void doClick() {
                //do nothing
            }
        };
    }

    public NotificationDialogAction getNotificationDialogAction() {
        return notificationDialogAction;
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageId() {
        return messageId;
    }
}
