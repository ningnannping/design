package com.fr.design.notification;

import com.fr.plugin.context.PluginContext;

/**
 * created by Harrison on 2020/03/16
 **/
public interface SnapChatKey {

    String calc();

    /**
     * 插件刚被安装时不能通过PluginContexts.getContext()方法获取插件上下文，因此加一个接口
     */
    default String calc(PluginContext context) {
        throw new UnsupportedOperationException();
    }
}
