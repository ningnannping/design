package com.fr.design.notification;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/9/27
 */
public abstract class AbstractSnapChat implements SnapChat {
    @Override
    public boolean hasRead() {

        String calcKey = calcKey();
        Boolean val = SnapChatConfig.getInstance().hasRead(calcKey);
        return val == null ? defaultStatus() : val;
    }

    @Override
    public void markRead() {

        String calcKey = calcKey();
        SnapChatConfig.getInstance().markRead(calcKey);
    }
}
