package com.fr.design.mod.impl.change.formula;

import com.fr.base.Formula;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/27
 */
public class FormulaContentChange implements ContentChange<Formula> {

    private final Map<ChangeItem, ContentReplacer<Formula>> map;

    public FormulaContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.Formula4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.Formula4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return Formula.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<Formula>> changeInfo() {
        return map;
    }

}
