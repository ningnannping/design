package com.fr.design.mod.impl.repalce;

import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.ContentReplaceUtil;
import com.fr.js.JavaScriptImpl;
import com.fr.stable.StringUtils;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class JavaScriptContentReplacer implements ContentReplacer<JavaScriptImpl> {

    @Override
    public void replace(JavaScriptImpl javaScript, String oldName, String newName) {
        if (StringUtils.isNotEmpty(javaScript.getContent())) {
            javaScript.setContent(ContentReplaceUtil.replaceContent(javaScript.getContent(), oldName, newName));
        }
    }

    @Override
    public boolean contain(JavaScriptImpl javaScript, String name) {
        return ContentReplaceUtil.containsName(javaScript.getContent(), name);
    }
}
