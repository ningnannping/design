package com.fr.design.mod.impl.change;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.VanChartHtmlLabelContentReplacer;
import com.fr.plugin.chart.base.VanChartHtmlLabel;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/31
 */
public class VanChartHtmlLabelContentChange implements ContentChange<VanChartHtmlLabel> {

    private final Map<ChangeItem, ContentReplacer<VanChartHtmlLabel>> map;

    public VanChartHtmlLabelContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, new VanChartHtmlLabelContentReplacer());
        map.put(ChangeItem.TABLE_DATA_NAME, new VanChartHtmlLabelContentReplacer());
    }

    @Override
    public String type() {
        return VanChartHtmlLabel.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<VanChartHtmlLabel>> changeInfo() {
        return map;
    }
}
