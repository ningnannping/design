package com.fr.design.mod.impl.change.formula;

import com.fr.data.impl.FormulaDictionary;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * 数据字典——公式
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class FormulaDictionaryContentChange implements ContentChange<FormulaDictionary> {

    private final Map<ChangeItem, ContentReplacer<FormulaDictionary>> map;

    public FormulaDictionaryContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.FormulaDictionary4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.FormulaDictionary4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return FormulaDictionary.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FormulaDictionary>> changeInfo() {
        return map;
    }
}
