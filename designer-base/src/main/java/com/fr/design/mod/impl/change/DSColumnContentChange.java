package com.fr.design.mod.impl.change;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.DSColumn4TableDataNameContentReplacer;
import com.fr.design.mod.impl.repalce.DSColumn4WidgetNameContentReplacer;
import com.fr.report.cell.cellattr.core.group.DSColumn;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class DSColumnContentChange implements ContentChange<DSColumn> {

    private final Map<ChangeItem, ContentReplacer<DSColumn>> map;

    public DSColumnContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.TABLE_DATA_NAME, new DSColumn4TableDataNameContentReplacer());
        map.put(ChangeItem.WIDGET_NAME, new DSColumn4WidgetNameContentReplacer());
    }

    @Override
    public String type() {
        return DSColumn.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<DSColumn>> changeInfo() {
        return map;
    }
}
