package com.fr.design.mod.bean;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public enum ChangeItem {

    TABLE_DATA_NAME,

    WIDGET_NAME

}
