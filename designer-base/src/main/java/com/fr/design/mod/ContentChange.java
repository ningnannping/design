package com.fr.design.mod;

import com.fr.design.mod.bean.ChangeItem;

import java.util.Map;

/**
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/4/27
 */
public interface ContentChange<T> {

    /**
     * 标识内容替换类型
     *
     * @return
     */
    String type();

    /**
     * 替换详情信息
     *
     * @return
     */
    Map<ChangeItem, ContentReplacer<T>> changeInfo();

}
