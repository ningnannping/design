package com.fr.design.mod.event;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class TableDataModifyEvent extends ModifyEvent {

    public static final TableDataModifyEvent INSTANCE = new TableDataModifyEvent();

}
