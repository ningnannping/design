package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.form.ui.WidgetTitle;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class WidgetTitleContentChange implements ContentChange<WidgetTitle> {

    private final Map<ChangeItem, ContentReplacer<WidgetTitle>> map;

    public WidgetTitleContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.WidgetTitle4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.WidgetTitle4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return WidgetTitle.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<WidgetTitle>> changeInfo() {
        return map;
    }
}
