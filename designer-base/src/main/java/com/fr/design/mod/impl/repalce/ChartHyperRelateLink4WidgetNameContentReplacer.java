package com.fr.design.mod.impl.repalce;

import com.fr.chart.web.ChartHyperRelateLink;
import com.fr.design.mod.ContentReplacer;
import com.fr.general.ComparatorUtils;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class ChartHyperRelateLink4WidgetNameContentReplacer implements ContentReplacer<ChartHyperRelateLink> {

    @Override
    public void replace(ChartHyperRelateLink chartHyperRelateLink, String oldName, String newName) {
        if (ComparatorUtils.equals(chartHyperRelateLink.getRelateCCName(), oldName)) {
            chartHyperRelateLink.setRelateCCName(newName);
        }
    }

}
