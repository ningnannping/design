package com.fr.design.mod.impl.change.formula;

import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import com.fr.form.ui.CardSwitchButton;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/3
 */
public class CardSwitchButtonContentChange implements ContentChange<CardSwitchButton> {


    private final Map<ChangeItem, ContentReplacer<CardSwitchButton>> map;

    public CardSwitchButtonContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.CardSwitchButton4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.CardSwitchButton4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return CardSwitchButton.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<CardSwitchButton>> changeInfo() {
        return map;
    }
}
