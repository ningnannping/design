package com.fr.design.mod.impl.change.formula;

import com.fr.data.condition.FormulaCondition;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.FormulaReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * 公式条件
 *
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class FormulaConditionContentChange implements ContentChange<FormulaCondition> {

    private final Map<ChangeItem, ContentReplacer<FormulaCondition>> map;

    public FormulaConditionContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.WIDGET_NAME, FormulaReplacer.FormulaCondition4WidgetNameContentReplacer);
        map.put(ChangeItem.TABLE_DATA_NAME, FormulaReplacer.FormulaCondition4TableDataNameContentReplacer);
    }

    @Override
    public String type() {
        return FormulaCondition.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<FormulaCondition>> changeInfo() {
        return map;
    }
}
