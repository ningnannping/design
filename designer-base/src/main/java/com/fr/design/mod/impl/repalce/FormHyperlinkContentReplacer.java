package com.fr.design.mod.impl.repalce;

import com.fr.design.mod.ContentReplacer;
import com.fr.form.main.FormHyperlink;
import com.fr.general.ComparatorUtils;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class FormHyperlinkContentReplacer implements ContentReplacer<FormHyperlink> {

    @Override
    public void replace(FormHyperlink formHyperlink, String oldName, String newName) {
        if (ComparatorUtils.equals(formHyperlink.getRelateEditorName(), oldName)) {
            formHyperlink.setRelateEditorName(newName);
        }
    }
}
