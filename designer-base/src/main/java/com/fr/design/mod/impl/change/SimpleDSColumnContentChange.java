package com.fr.design.mod.impl.change;

import com.fr.data.SimpleDSColumn;
import com.fr.design.mod.ContentChange;
import com.fr.design.mod.ContentReplacer;
import com.fr.design.mod.bean.ChangeItem;
import com.fr.design.mod.impl.repalce.SimpleDSColumnContentReplacer;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/6/2
 */
public class SimpleDSColumnContentChange implements ContentChange<SimpleDSColumn> {

    private Map<ChangeItem, ContentReplacer<SimpleDSColumn>> map;

    public SimpleDSColumnContentChange() {
        map = new HashMap<>();
        map.put(ChangeItem.TABLE_DATA_NAME, new SimpleDSColumnContentReplacer());
    }

    @Override
    public String type() {
        return SimpleDSColumn.class.getName();
    }

    @Override
    public Map<ChangeItem, ContentReplacer<SimpleDSColumn>> changeInfo() {
        return map;
    }
}
