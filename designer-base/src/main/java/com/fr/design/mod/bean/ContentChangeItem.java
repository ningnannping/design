package com.fr.design.mod.bean;

import java.util.Map;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/5/28
 */
public class ContentChangeItem {

    private final Map<String, String> changeMap;
    private final ChangeItem changeItem;
    private final Object object;

    public ContentChangeItem(Map<String, String> changeMap,Object object, ChangeItem changeItem) {
        this.changeMap = changeMap;
        this.changeItem = changeItem;
        this.object = object;
    }

    public Map<String, String> getChangeMap() {
        return changeMap;
    }

    public ChangeItem getChangeItem() {
        return changeItem;
    }

    public Object getObject() {
        return object;
    }
}
