package com.fr.design.upm;

import com.fr.decision.webservice.v10.plugin.helper.category.impl.UpmResourceLoader;
import com.fr.design.DesignerEnvManager;
import com.fr.design.bridge.exec.JSBridge;
import com.fr.design.bridge.exec.JSCallback;
import com.fr.design.extra.PluginOperateUtils;
import com.fr.design.extra.PluginUtils;
import com.fr.design.extra.exe.GetInstalledPluginsExecutor;
import com.fr.design.extra.exe.GetPluginCategoriesExecutor;
import com.fr.design.extra.exe.GetPluginFromStoreExecutor;
import com.fr.design.extra.exe.GetPluginPrefixExecutor;
import com.fr.design.extra.exe.PluginLoginExecutor;
import com.fr.design.extra.exe.ReadUpdateOnlineExecutor;
import com.fr.design.extra.exe.SearchOnlineExecutor;
import com.fr.design.i18n.Toolkit;
import com.fr.design.upm.event.CertificateEvent;
import com.fr.design.upm.event.DownloadEvent;
import com.fr.design.upm.exec.NewUpmBrowserExecutor;
import com.fr.design.upm.task.UpmTaskWorker;
import com.fr.event.EventDispatcher;
import com.fr.general.GeneralUtils;
import com.fr.json.JSONObject;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.context.PluginMarker;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;
import com.teamdev.jxbrowser.js.JsAccessible;
import com.teamdev.jxbrowser.js.JsFunction;
import com.teamdev.jxbrowser.js.JsObject;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RunnableFuture;

/**
 * @author richie
 * @version 10.0
 * Created by richie on 2019-04-12
 * 桥接Java和JavaScript的类
 */
public class NewUpmBridge extends UpmBridge  {

    public static NewUpmBridge getBridge(JsObject jsObject) {
        return new NewUpmBridge(jsObject);
    }

    private JsObject jsObject;

    private NewUpmBridge(JsObject jsObject) {
        this.jsObject = jsObject;
    }

    /**
     * 更新插件管理中心资源文件，这个方法仅仅是为了语义上的作用（更新）
     *
     * @param callback 安装完成后的回调函数
     */
    @JSBridge
    @JsAccessible
    public void update(final JsFunction callback) {
        callback.invoke(jsObject, "start", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Start"));
        try {
            UpmResourceLoader.INSTANCE.download();
            UpmResourceLoader.INSTANCE.install();
            callback.invoke(jsObject, "success", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Success"));
            EventDispatcher.fire(DownloadEvent.UPDATE, "success");
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            callback.invoke(jsObject, "error", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Error"));
        }
    }

    /**
     * 下载并安装插件管理中心的资源文件
     *
     * @param callback 安装完成后的回调函数
     */
    @JSBridge
    @JsAccessible
    public void startDownload(final JsFunction callback) {
        callback.invoke(jsObject, "start", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Start"));
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                UpmResourceLoader.INSTANCE.download();
                UpmResourceLoader.INSTANCE.install();
                return null;
            }

            @Override
            protected void done() {
                try {
                    get();
                    callback.invoke(jsObject, "success", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Success"));
                    EventDispatcher.fire(DownloadEvent.SUCCESS, "success");
                } catch (Exception e) {
                    callback.invoke(jsObject, "error", Toolkit.i18nText("Fine-Design_Basic_Update_Plugin_Manager_Download_Error"));
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    EventDispatcher.fire(DownloadEvent.ERROR, "error");
                }
            }
        }.execute();
    }

    /**
     * 获取upm的版本信息
     *
     * @return 版本信息
     */
    @JSBridge
    @JsAccessible
    @Override
    public String getVersion() {
        return super.getVersion();
    }

    @JSBridge
    @JsAccessible
    @Override
    public String i18nText(String key) {
        return super.i18nText(key);
    }

    @JSBridge
    @JsAccessible
    @Override
    public void closeWindow() {
        super.closeWindow();
    }

    @JSBridge
    @JsAccessible
    @Override
    public boolean isDesigner() {
        return super.isDesigner();
    }

    @JSBridge
    @JsAccessible
    public void getPackInfo(final JsFunction callback) {
        callback.invoke(jsObject, StringUtils.EMPTY);
    }

    @JSBridge
    @JsAccessible
    public void getPluginPrefix(final JsFunction callback) {
        UpmTaskWorker<Void> task = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new GetPluginPrefixExecutor());
        task.execute();
    }

    /**
     * 在线获取插件分类
     *
     * @param callback 回调函数
     */
    @JSBridge
    @JsAccessible
    public void getPluginCategories(final JsFunction callback) {
        UpmTaskWorker<Void> task = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new GetPluginCategoriesExecutor());
        task.execute();
    }

    /**
     * 根据条件获取在线插件
     *
     * @param info     插件信息
     * @param callback 回调函数
     */
    @JSBridge
    @JsAccessible
    public void getPluginFromStoreNew(String info, final JsFunction callback) {
        UpmTaskWorker<Void> task = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new GetPluginFromStoreExecutor(new JSONObject(info)));
        task.execute();
    }

    /**
     * 已安装插件检查更新
     */
    @JSBridge
    @JsAccessible
    public void readUpdateOnline(final JsFunction callback) {
        UpmTaskWorker<Void> task = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new ReadUpdateOnlineExecutor());
        task.execute();
    }

    /**
     * 获取已经安装的插件的数组
     */
    @JSBridge
    @JsAccessible
    public void getInstalledPlugins(final JsFunction callback) {
        UpmTaskWorker<Void> task = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new GetInstalledPluginsExecutor());
        task.execute();
    }

    /**
     * 从插件服务器上更新选中的插件
     *
     * @param pluginIDs 插件集合
     */
    @JSBridge
    @JsAccessible
    public void updatePluginOnline(JsObject pluginIDs, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        List<PluginMarker> pluginMarkerList = new ArrayList<>();
        for (String key : pluginIDs.propertyNames()) {
            pluginIDs.property(key).ifPresent(v -> {
                pluginMarkerList.add(PluginUtils.createPluginMarker(GeneralUtils.objectToString(v)));
            });
        }
        PluginOperateUtils.updatePluginOnline(pluginMarkerList, jsCallback);
    }

    @JSBridge
    @JsAccessible
    public void updatePluginOnline(String pluginID, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        List<PluginMarker> pluginMarkerList = new ArrayList<>();
        pluginMarkerList.add(PluginUtils.createPluginMarker(pluginID));
        PluginOperateUtils.updatePluginOnline(pluginMarkerList, jsCallback);
    }

    /**
     * 搜索在线插件
     *
     * @param keyword 关键字
     */
    @JSBridge
    @JsAccessible
    public void searchPlugin(String keyword, final JsFunction callback) {
        UpmTaskWorker<Void> worker = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new SearchOnlineExecutor(keyword));
        worker.execute();
    }

    /**
     * 从磁盘上选择插件安装包进行安装
     *
     * @param filePath 插件包的路径
     */
    @JSBridge
    @JsAccessible
    public void installPluginFromDisk(final String filePath, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        File file = new File(filePath);
        PluginOperateUtils.installPluginFromDisk(file, jsCallback);
    }

    /**
     * 卸载当前选中的插件
     *
     * @param pluginInfo 插件信息
     */
    @JSBridge
    @JsAccessible
    public void uninstallPlugin(final String pluginInfo, final boolean isForce, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        PluginOperateUtils.uninstallPlugin(pluginInfo, isForce, jsCallback);
    }

    /**
     * 从插件服务器上安装插件
     *
     * @param pluginInfo 插件的ID
     * @param callback   回调函数
     */
    @JSBridge
    @JsAccessible
    public void installPluginOnline(final String pluginInfo, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        PluginMarker pluginMarker = PluginUtils.createPluginMarker(pluginInfo);
        PluginOperateUtils.installPluginOnline(pluginMarker, jsCallback);
    }

    /**
     * 从磁盘上选择插件安装包进行插件升级
     *
     * @param filePath 插件包的路径
     */
    @JSBridge
    @JsAccessible
    public void updatePluginFromDisk(String filePath, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        File file = new File(filePath);
        PluginOperateUtils.updatePluginFromDisk(file, jsCallback);
    }

    /**
     * 修改选中的插件的活跃状态
     *
     * @param pluginID 插件ID
     */
    @JSBridge
    @JsAccessible
    public void setPluginActive(String pluginID, final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        PluginOperateUtils.setPluginActive(pluginID, jsCallback);
    }

    /**
     * 选择文件对话框
     *
     * @return 选择的文件的路径
     */
    @JSBridge
    @JsAccessible
    @Override
    public String showFileChooser() {
        return super.showFileChooser();
    }

    /**
     * 选择文件对话框
     *
     * @param des    过滤文件描述
     * @param filter 文件的后缀
     * @return 选择的文件的路径
     * 这里换用JFileChooser会卡死,不知道为什么
     */
    @JSBridge
    @JsAccessible
    @Override
    public String showFileChooserWithFilter(final String des, final String filter) {
        return super.showFileChooserWithFilter(des, filter);
    }

    /**
     * 选择文件对话框
     *
     * @param des  过滤文件描述
     * @param args 文件的后缀
     * @return 选择的文件的路径
     */
    @JSBridge
    @JsAccessible
    public String showFileChooserWithFilters(final String des, final String args) {
        RunnableFuture<String> future = new FutureTask<>(() -> {
            JFileChooser fileChooser = new JFileChooser();
            List<String> filterList = new ArrayList<>();
            filterList.add(args);
            String[] filters = filterList.toArray(new String[0]);
            if (ArrayUtils.isNotEmpty(filters)) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter(des, UpmUtils.findMatchedExtension(filters));
                fileChooser.setFileFilter(filter);
            }
            int result = fileChooser.showOpenDialog(UpmFinder.getDialog());
            if (result == JFileChooser.APPROVE_OPTION) {
                return fileChooser.getSelectedFile().getAbsolutePath();
            }
            return null;
        });
        SwingUtilities.invokeLater(future);
        try {
            return future.get();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return null;
    }

    /**
     * 选择文件对话框
     *
     * @param des  过滤文件描述
     * @param args 文件的后缀
     * @return 选择的文件的路径
     */
    @JSBridge
    @JsAccessible
    public String showFileChooserWithFilters(final String des, final JsObject args) {
        RunnableFuture<String> future = new FutureTask<>(() -> {
            JFileChooser fileChooser = new JFileChooser();
            List<String> filterList = new ArrayList<>();
            for (String key : args.propertyNames()) {
                args.property(key).ifPresent(v -> {
                    filterList.add(GeneralUtils.objectToString(v));
                });
            }
            String[] filters = filterList.toArray(new String[0]);
            if (ArrayUtils.isNotEmpty(filters)) {
                FileNameExtensionFilter filter = new FileNameExtensionFilter(des, UpmUtils.findMatchedExtension(filters));
                fileChooser.setFileFilter(filter);
            }
            int result = fileChooser.showOpenDialog(UpmFinder.getDialog());
            if (result == JFileChooser.APPROVE_OPTION) {
                return fileChooser.getSelectedFile().getAbsolutePath();
            }
            return null;
        });
        SwingUtilities.invokeLater(future);
        try {
            return future.get();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return null;
    }

    ////////登录相关///////

    /**
     * 获取系统登录的用户名
     */
    @JSBridge
    @JsAccessible
    public String getLoginInfo(final JsFunction callback) {
        registerLoginInfo(callback);
        return DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
    }

    /**
     * 系统登录注册
     *
     * @param callback 回调函数
     */
    @JSBridge
    @JsAccessible
    public void registerLoginInfo(final JsFunction callback) {
        JSCallback jsCallback = new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback));
        String username = DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
        if (StringUtils.isEmpty(username)) {
            jsCallback.execute(StringUtils.EMPTY);
            EventDispatcher.fire(CertificateEvent.LOGOUT, StringUtils.EMPTY);
        } else {
            jsCallback.execute(username);
            EventDispatcher.fire(CertificateEvent.LOGIN, username);
        }
    }


    /**
     * 设计器端的用户登录
     *
     * @param username 用户名
     * @param password 密码
     * @param callback 回调函数
     */
    @JSBridge
    @JsAccessible
    public void defaultLogin(String username, String password, final JsFunction callback) {
        UpmTaskWorker<Void> worker = new UpmTaskWorker<>(new JSCallback(NewUpmBrowserExecutor.create(jsObject, callback)), new PluginLoginExecutor(username, password));
        worker.execute();
    }

    /**
     * 清除用户信息
     */
    @JsAccessible
    @JSBridge
    @Override
    public void clearUserInfo() {
        super.clearUserInfo();
    }

    /**
     * 打开论坛消息界面
     */
    @JSBridge
    @JsAccessible
    @Override
    public void getPriviteMessage() {
        super.getPriviteMessage();
    }

    /**
     * 忘记密码
     */
    @JSBridge
    @JsAccessible
    @Override
    public void forgetHref() {
        super.forgetHref();
    }

    /**
     * 立即注册
     */
    @JSBridge
    @JsAccessible
    @Override
    public void registerHref() {
        super.registerHref();
    }

    /**
     * 使用系统浏览器打开网页
     *
     * @param url 要打开的网页
     */
    @JSBridge
    @JsAccessible
    @Override
    public void openShopUrlAtWebBrowser(String url) {
        super.openShopUrlAtWebBrowser(url);
    }
}
