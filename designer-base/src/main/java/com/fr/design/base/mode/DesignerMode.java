package com.fr.design.base.mode;


import com.fr.design.mainframe.DesignerContext;

public enum DesignerMode {
    NORMAL,
    BAN_COPY_AND_CUT,
    VCS,
    AUTHORITY {
        @Override
        public void closeMode() {
            DesignerContext.getDesignerFrame().closeAuthorityMode();
        }
    },
    DUCHAMP;

    public void openMode() {

    }

    public void closeMode() {
    }

}
