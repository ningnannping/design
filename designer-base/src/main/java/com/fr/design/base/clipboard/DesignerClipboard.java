package com.fr.design.base.clipboard;

import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.FlavorListener;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * created by Harrison on 2020/05/11
 **/
public class DesignerClipboard extends Clipboard {
    
    private Clipboard clipboard;
    
    public DesignerClipboard(Clipboard clipboard) {
        super(clipboard.getName());
        this.clipboard = clipboard;
    }
    
    @Override
    public synchronized void setContents(Transferable contents, ClipboardOwner owner) {
        //处理 contents/owner
        Transferable filtered = ClipboardFilter.copy(contents);
        clipboard.setContents(filtered, owner);
    }
    
    @Override
    public synchronized Transferable getContents(Object requestor) {
        Transferable contents = clipboard.getContents(requestor);
        //处理 contents
        Transferable filtered = ClipboardFilter.paste(contents);
        return filtered;
    }
    
    @Override
    public DataFlavor[] getAvailableDataFlavors() {
        return clipboard.getAvailableDataFlavors();
    }
    
    @Override
    public boolean isDataFlavorAvailable(DataFlavor flavor) {
        return clipboard.isDataFlavorAvailable(flavor);
    }
    
    @Override
    public Object getData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
        return clipboard.getData(flavor);
    }
    
    @Override
    public synchronized void addFlavorListener(FlavorListener listener) {
        clipboard.addFlavorListener(listener);
    }
    
    @Override
    public synchronized void removeFlavorListener(FlavorListener listener) {
        clipboard.removeFlavorListener(listener);
    }
    
    @Override
    public synchronized FlavorListener[] getFlavorListeners() {
        return clipboard.getFlavorListeners();
    }
    
}
