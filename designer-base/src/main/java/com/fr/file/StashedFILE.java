package com.fr.file;

import com.fr.base.io.XMLEncryptUtils;
import com.fr.io.EncryptUtils;
import com.fr.io.FineEncryptUtils;
import com.fr.stable.StringUtils;
import javax.swing.Icon;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 切换环境用于暂存的文件类型
 */
public class StashedFILE extends AbstractFILE {

    private FILE file;
    private byte[] content;
    private String suffix;

    public StashedFILE(FILE file, byte[] content) {
        this(file, content, null);
    }

    public StashedFILE(FILE file, byte[] content, String suffix) {
        this.file = file;
        this.content = content;
        this.suffix = suffix;
    }

    @Override
    public String prefix() {
        return file.prefix();
    }

    @Override
    public boolean isDirectory() {
        return file.isDirectory();
    }

    @Override
    public String getName() {
        return file.getName();
    }

    @Override
    public Icon getIcon() {
        return file.getIcon();
    }

    @Override
    public String getPath() {
        return file.getPath();
    }

    @Override
    public boolean exists() {
        return false;
    }

    @Override
    public void closeTemplate() throws Exception {
        // do nothing
    }

    @Override
    public InputStream asInputStream() throws Exception {
        ByteArrayInputStream in = new ByteArrayInputStream(content);
        return needDecode() ? XMLEncryptUtils.decodeInputStream(EncryptUtils.decodeInputStream(FineEncryptUtils.decode(in))) : in;
    }

    private boolean needDecode() {
        return StringUtils.isNotEmpty(suffix) && (suffix.endsWith(".cpt") || suffix.endsWith(".frm"));
    }

    @Override
    public String getEnvFullName() {
        return file.getEnvFullName();
    }

    @Override
    public boolean isMemFile() {
        return true;
    }

    @Override
    public boolean isEnvFile() {
        return false;
    }

    @Override
    public String toString() {
        return FILEFactory.MEM_PREFIX + getName();
    }
}
