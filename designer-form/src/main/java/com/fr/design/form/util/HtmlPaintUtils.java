package com.fr.design.form.util;

import com.fr.base.Style;
import com.fr.general.FRFont;
import com.fr.log.FineLoggerFactory;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 富文本导出工具栏
 *
 * @author hades
 * @version 11.0
 * Created by hades on 2022/5/19
 */
public class HtmlPaintUtils {


    private static final Pattern FONT_SIZE_PATTERN = Pattern.compile(Pattern.quote("font-size:") + "(.*?)" + Pattern.quote("px"));

    /**
     * 设置单元格字体为富文本中的最大字体
     *
     * @param style
     */
    public static Style deriveMaxFontFromRichChar(Style style, String html) {
        int maxSize = style.getFRFont().getSize();
        Matcher matcher = FONT_SIZE_PATTERN.matcher(html);
        while (matcher.find()) {
            String value = matcher.group(1);
            try {
                double pxSize = Double.parseDouble(value);
                int ptSize = FontTransformUtil.roundUp(FontTransformUtil.px2pt(pxSize));
                maxSize = Math.max(maxSize, ptSize);
            } catch (Throwable e) {
                FineLoggerFactory.getLogger().debug(e.getMessage(), e);
            }
        }
        FRFont cellFont = style.getFRFont();
        return style.deriveFRFont(cellFont.applySize(maxSize));
    }
}
