package com.fr.design.mainframe.share.ui.online;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.sort.OnlineWidgetSortType;
import com.fr.design.mainframe.share.ui.base.FlexSearchFieldPane;
import com.fr.design.mainframe.share.ui.widgetfilter.FilterPane;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.utils.ShareUtils;
import com.fr.stable.StringUtils;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
 * Created by kerry on 2020-10-19
 */
public class OnlineWidgetShowPane extends AbstractOnlineWidgetShowPane {
    private String lastSearch = StringUtils.EMPTY;
    private String lastFilter = StringUtils.EMPTY;
    private int lastSortTabSelectedIndex = 0;

    public OnlineWidgetShowPane(OnlineShareWidget[] sharableWidgetProviders) {
        super(sharableWidgetProviders);
    }

    @Override
    public void initSearchTextFieldPaneListener(FlexSearchFieldPane searchFieldPane) {
        super.initSearchTextFieldPaneListener(searchFieldPane);

        searchFieldPane.registerSearchTextFieldFocusListener(new FocusAdapter() {
            private KeyListener keyListener = new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_DELETE) {
                        collectSearchText(false);
                    }
                }
            };
            public void focusGained(FocusEvent e) {
                searchFieldPane.addKeyListener(keyListener);
            }
            @Override
            public void focusLost(FocusEvent e) {
                searchFieldPane.removeKeyListener(keyListener);
                collectSearchText(true);

            }
        });

        searchFieldPane.registerDeleteIconMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                collectSearchText(false);
            }
        });

        searchFieldPane.registerFieldDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    lastSearch = e.getDocument().getText(0, e.getDocument().getLength());
                } catch (BadLocationException ex) {
                    ex.printStackTrace();
                }
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                textChanged(e);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                textChanged(e);
            }

            public void textChanged(DocumentEvent e) {};
        });
    }

    @Override
    public void initFilterPaneListener(FilterPane filterPane) {
        super.initFilterPaneListener(filterPane);
        filterPane.registerChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                lastFilter = e.getSource().toString();
            }
        });
        filterPane.addPopupStateChangeListener(new FilterPane.PopStateChangeListener() {
            @Override
            public void stateChange(boolean state) {
                if (!state) {
                    collectFilterContent();
                }
            }
        });
    }

    protected OnlineShareWidget[] getSharableWidgetArr( String filterStr){
        OnlineShareWidget[] onlineShareWidgets =  ShareUtils.getFilterWidgets(filterStr);
        OnlineWidgetSortType.values()[lastSortTabSelectedIndex].sort(onlineShareWidgets);
        return onlineShareWidgets;
    }

    @Override
    public void initSortTabPane(SortTabPane sortTabPane) {
        super.initSortTabPane(sortTabPane);
        lastSortTabSelectedIndex = sortTabPane.getIndex();
        sortTabPane.registerSortTabMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (sortTabPane.getIndex() != lastSortTabSelectedIndex) {
                    collectSortType(((UILabel) e.getSource()).getText());
                    lastSortTabSelectedIndex = sortTabPane.getIndex();
                }
            }
        });
    }

    private void collectSearchText(boolean isSaveInfo) {
        if (StringUtils.isNotEmpty(lastSearch)) {
            ComponentCollector.getInstance().collectSearchContent(this.lastSearch);
            if (isSaveInfo) {
                ComponentCollector.getInstance().saveInfo();
            }
            lastSearch = StringUtils.EMPTY;
        }
    }

    private void collectFilterContent() {
        if (StringUtils.isNotEmpty(lastFilter)) {
            ComponentCollector.getInstance().collectFilterContent(lastFilter);
            lastSearch = StringUtils.EMPTY;
        }
    }

    private void collectSortType(String sortType) {
        ComponentCollector.getInstance().collectSortType(sortType);
    }
}
