package com.fr.design.mainframe.share.select;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.FormSelection;
import com.fr.form.ui.Widget;
import com.fr.stable.AssistUtils;
import com.fr.third.org.apache.commons.lang3.tuple.Triple;
import org.jetbrains.annotations.Nullable;

import java.awt.Rectangle;

/**
 * created by Harrison on 2020/06/11
 **/
public class ComponentTransformerFactory {
    
    private TransformerKey lastKey;
    private Triple<Widget, XCreator, Rectangle> lastCache;
    
    private ComponentTransformer transformer = new ComponentTransformerImpl();
    
    private static class InstanceHolder {
        
        private static ComponentTransformerFactory INSTANCE = new ComponentTransformerFactory();
    }
    
    public static ComponentTransformerFactory getInstance() {
        
        return InstanceHolder.INSTANCE;
    }
    
    @Nullable
    public Triple<Widget, XCreator, Rectangle> transform(FormSelection selection) {
        
        if (selection == null) {
            return null;
        }
        TransformerKey transformerKey = new TransformerKey(selection);
        if (AssistUtils.equals(lastKey, transformerKey)) {
            return lastCache;
        }
        Triple<Widget, XCreator, Rectangle> triple = transformer.transform(selection);
        //缓存一下。
        cache(transformerKey, triple);
        return triple;
    }
    
    private void cache(TransformerKey transformerKey, Triple<Widget, XCreator, Rectangle> triple) {
        
        this.lastKey = transformerKey;
        this.lastCache = triple;
    }
}
