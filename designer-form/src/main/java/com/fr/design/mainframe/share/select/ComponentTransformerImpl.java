package com.fr.design.mainframe.share.select;

import com.fr.base.FRContext;
import com.fr.design.designer.beans.adapters.component.CompositeComponentAdapter;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.XWAbsoluteLayout;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.FormSelection;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.form.main.Form;
import com.fr.form.share.bean.ShareLayoutWidget;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.form.ui.container.WAbsoluteBodyLayout;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WBorderLayout;
import com.fr.form.ui.container.WFitLayout;
import com.fr.form.ui.container.WLayout;
import com.fr.form.ui.container.WParameterLayout;
import com.fr.form.ui.widget.CRBoundsWidget;
import com.fr.general.Inter;
import com.fr.log.FineLoggerFactory;
import com.fr.third.org.apache.commons.lang3.tuple.ImmutableTriple;
import com.fr.third.org.apache.commons.lang3.tuple.Triple;
import com.fr.web.FormCompVisibleUtils;
import org.jetbrains.annotations.Nullable;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

/**
 * created by Harrison on 2020/06/11
 **/
public class ComponentTransformerImpl implements ComponentTransformer {

    @Override
    @Nullable
    public Triple<Widget, XCreator, Rectangle> transform(FormSelection selection) {

        XCreator selectedCreator;
        Widget selectedWidget;
        if (selection == null) {
            return null;
        }
        Rectangle selectionBounds = selection.getSelctionBounds();
        Rectangle selectedTriple = new Rectangle(0, 0, selectionBounds.width, selectionBounds.height);
        XCreator[] xCreators = selection.getSelectedCreators();
        if (xCreators.length == 1) {
            selectedCreator = xCreators[0];
            selectedWidget = selectedCreator.toData();
        } else {

            WAbsoluteLayout wAbsoluteLayout = new WAbsoluteLayout("absolute");
            wAbsoluteLayout.setCompState(WAbsoluteLayout.STATE_FIT);
            Map<Integer, Widget> widgetMap = new TreeMap<>(
                    new Comparator<Integer>() {
                        @Override
                        public int compare(Integer o1, Integer o2) {
                            return o2.compareTo(o1);
                        }
                    }
            );
            //控件上下层关系需要继承下来
            for (XCreator xCreator : xCreators) {
                XLayoutContainer container = (XLayoutContainer) xCreator.getParent();
                int i = container.getComponentZOrder(xCreator);
                widgetMap.put(i, xCreator.toData());
            }
            JTemplate<?, ?> jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
            Form form = (Form) jt.getTarget();
            for (Widget innerWidget : widgetMap.values()) {
                WLayout parentWLayout = getParentLayout(form, innerWidget);
                if (parentWLayout == null){
                    continue;
                }
                try {
                    CRBoundsWidget boundsWidget = (CRBoundsWidget) parentWLayout.getBoundsWidget(innerWidget).clone();
                    adaptBounds(boundsWidget, selectionBounds);
                    wAbsoluteLayout.addWidget(boundsWidget);
                } catch (CloneNotSupportedException e1) {
                    FRContext.getLogger().error(e1.getMessage(), e1);
                }
            }
            wAbsoluteLayout.setDesigningResolution(Toolkit.getDefaultToolkit().getScreenSize());

            selectedCreator = new XWAbsoluteLayout(wAbsoluteLayout, new Dimension(selectedTriple.width, selectedTriple.height));
            selectedWidget = selectedCreator.toData();

            //将选中的类型初始化一下。
            CompositeComponentAdapter adapter = new CompositeComponentAdapter(WidgetPropertyPane.getInstance().getEditingFormDesigner(), selectedCreator);
            adapter.initialize();
            //并且初始化一次。缓存一下值。
            selectedCreator.repaint();
        }
        try {
            if (!supportShared(selectedWidget)) {
                return null;
            }
            if (!(selectedWidget instanceof AbstractBorderStyleWidget)) {
                selectedWidget = new ShareLayoutWidget(selectedWidget);
            }
            selectedWidget = (Widget) selectedWidget.clone();
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            FineJOptionPane.showMessageDialog(null, Inter.getLocText("Fine-Design_Share_Module_Failed"));
        }
        return new ImmutableTriple<>(selectedWidget, selectedCreator, selectedTriple);
    }

    //先从body中找，如果找不到再从参数面板中找
    @Nullable
    private WLayout getParentLayout(Form form, Widget innerWidget) {
        WLayout bodyLayout = (WLayout) ((WBorderLayout) form.getContainer()).getLayoutWidget(WBorderLayout.CENTER);
        WLayout paraLayout = (WLayout) ((WBorderLayout) form.getContainer()).getLayoutWidget(WBorderLayout.NORTH);
        WLayout parentLayout = FormCompVisibleUtils.findFitLayout(bodyLayout, innerWidget);
        if (parentLayout == null && paraLayout != null) {
            parentLayout = FormCompVisibleUtils.findFitLayout(paraLayout, innerWidget);
        }
        return parentLayout;
    }

    //组件集合产生绝对布局的时候，子组件的bounds需要去掉主框架的xy值，保证边界和绝对布局对齐
    private static void adaptBounds(CRBoundsWidget cRBoundsWidget, Rectangle delRec) {
        Rectangle rec = cRBoundsWidget.getBounds();
        int originX = rec.x;
        int originY = rec.y;
        int delX = delRec.x;
        int delY = delRec.y;
        cRBoundsWidget.setBounds(new Rectangle(originX - delX, originY - delY, rec.width, rec.height));
    }

    private static boolean supportShared(Widget widget) {

        return notBody(widget) && notForm(widget);
    }

    private static boolean notBody(Widget widget) {
        return !(widget instanceof WAbsoluteBodyLayout || widget instanceof WFitLayout || widget instanceof WParameterLayout);
    }

    private static boolean notForm(Widget widget) {
        if (!(widget instanceof WBorderLayout)) {
            return true;
        }
        Widget centerWidget = ((WBorderLayout) widget).getLayoutWidget(WBorderLayout.CENTER);
        return notBody(centerWidget);

    }
}
