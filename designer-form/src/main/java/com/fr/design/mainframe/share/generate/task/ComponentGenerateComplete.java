package com.fr.design.mainframe.share.generate.task;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.share.generate.ComponentBanner;

/**
 * created by Harrison on 2020/04/13
 **/
public class ComponentGenerateComplete implements ComponentBanner {
    
    public void execute() throws InterruptedException {
    }
    
    @Override
    public String getLoadingText() {
        
        return Toolkit.i18nText("Fine-Design_Share_Generate_Success");
    }
}
