package com.fr.design.mainframe.share.ui.base;

import com.fr.concurrent.NamedThreadFactory;
import com.fr.design.gui.iprogressbar.ProgressDialog;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.ui.util.UIUtil;
import com.fr.stable.StringUtils;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class ShareProgressBar {

    private static final int STEP = 20;

    private ProgressDialog progressBar = null;

    private LimitProgress loadingProgress = new LimitProgress();

    /**
     * 默认 40 ms更新进度
     */
    private int stepHeartbeat = 40;

    private volatile int progress = 0;

    private final AtomicBoolean TERMINATE = new AtomicBoolean(false);

    private final AtomicBoolean RUNNING = new AtomicBoolean(false);

    private ShareProgressBar() {
    }

    private static class InstanceHolder {

        private static final ShareProgressBar INSTANCE = new ShareProgressBar();
    }

    public static ShareProgressBar getInstance() {

        return InstanceHolder.INSTANCE;
    }

    public void prepare(int stepHeartbeat) {

        this.progressBar = createNewBar();
        this.stepHeartbeat = stepHeartbeat;
    }

    public void updateProgress(double rate, String loadingText) throws Exception {
        int maximum = progressBar.getProgressMaximum();
        double progress = maximum * rate;
        int maxProgress = (int) Math.floor(progress);
        loadingProgress.lock(maxProgress, loadingText);

        RUNNING.compareAndSet(false, true);
        //终止条件
        TERMINATE.compareAndSet(true, false);

        loadingProgress.get();

    }

    public void monitor() {

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1,
                new NamedThreadFactory("ShareProgressMonitor", true));
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (!isRunning()) {
                    return;
                }
                if (isComplete()) {
                    shutdown(scheduler);
                    return;
                }
                updateUI();
            }
        }, 0, stepHeartbeat, TimeUnit.MILLISECONDS);
    }


    /**
     * 延迟关闭
     */
    public void complete(String loadingText) throws Exception {

        updateProgress(1.0, loadingText);
    }

    /**
     * 立刻关闭
     */
    public void completeNow() {

        if (RUNNING.get()) {
            this.progress = progressBar.getProgressMaximum();
            this.TERMINATE.compareAndSet(false, true);
        }
    }

    /**
     * 完成条件。不只是达到目标值，还要确保当前已经可以终止了。
     *
     * @return 是否终止了。
     */
    public boolean isComplete() {

        return this.progress >= progressBar.getProgressMaximum() && this.TERMINATE.get();
    }

    private ProgressDialog createNewBar() {
        return new ProgressDialog(DesignerContext.getDesignerFrame());
    }

    /**
     * 小于当前最大时， 每次递增 1
     * 大于等于时， 停止
     *
     * @return 当前最大值
     */
    private int incrementProgress() {
        if (progress >= loadingProgress.getLimitKey()) {
            progress = loadingProgress.getLimitKey();
            loadingProgress.release();
            return progress;
        } else {
            progress += STEP;
            return progress;
        }
    }

    private String getLoadingText() {

        return loadingProgress.getLimitText();
    }


    private boolean isRunning() {

        return RUNNING.get();
    }


    private void shutdown(ScheduledExecutorService scheduler) {

        //停止运行
        RUNNING.compareAndSet(true, false);
        reset();
        scheduler.shutdown();
    }

    private void reset() {

        this.progress = 0;
        this.loadingProgress.reset();
        this.progressBar.setVisible(false);
        this.progressBar.dispose();
        this.progressBar = null;
    }

    private void updateUI() {
        String text = getLoadingText();
        int value = incrementProgress();
        UIUtil.invokeLaterIfNeeded(new Runnable() {
            @Override
            public void run() {
                if (!progressBar.isVisible()) {
                    progressBar.setVisible(true);
                }
                progressBar.updateLoadingText(text);
                progressBar.setProgressValue(value);
            }
        });
    }

    private static class LimitProgress {

        private int limitKey = 0;

        private String limitText = StringUtils.EMPTY;

        private FutureTask<Void> lock;

        public void lock(int key, String loadingText) {

            this.limitKey = key;
            this.limitText = loadingText;
            this.lock = new FutureTask<>(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    return null;
                }
            });
        }

        public void release() {

            this.lock.run();
        }

        public void get() throws Exception {

            this.lock.get();
        }

        public void reset() {

            this.limitKey = 0;
            this.limitText = StringUtils.EMPTY;
            //防止卡住。
            this.lock.run();
        }

        public int getLimitKey() {
            return limitKey;
        }

        public String getLimitText() {
            return limitText;
        }


    }

}
