package com.fr.design.mainframe.share.ui.online;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.sort.OnlineWidgetSortType;
import com.fr.design.mainframe.share.ui.base.FlexSearchFieldPane;
import com.fr.design.mainframe.share.ui.base.LoadingPane;
import com.fr.design.mainframe.share.ui.widgetfilter.FilterPane;
import com.fr.form.share.base.DataLoad;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.utils.ShareUtils;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kerry on 2020-10-19
 */
public abstract class AbstractOnlineWidgetShowPane extends JPanel {
    private static final String SEARCH_RESULT_PANE = "SEARCH_RESULT_PANE";
    private static final String MAIN_FILTER_TAB_PANE = "MAIN_FILTER_TAB_PANE";

    private JPanel componentSelectPane;
    private JPanel searchResultShowPane;
    private JPanel mainCenterPane;
    private FilterPane filterPane;
    private JPanel centerPane;
    private SortTabPane sortTabPane;

    private final JPanel loadingPane = new LoadingPane();

    private OnlineShareWidget[] sharableWidgetProviders;

    //缓存一份用于搜索
    private final OnlineShareWidget[] sharableWidgetCache;
    //主面板和搜索面板的cardLayout
    private CardLayout mainCardLayout;


    public AbstractOnlineWidgetShowPane(OnlineShareWidget[] sharableWidgetProviders) {
        this.sharableWidgetCache = sharableWidgetProviders;
        this.sharableWidgetProviders = sharableWidgetProviders;
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel contentPane = initContentPane();
        this.add(contentPane, BorderLayout.CENTER);
    }

    protected JPanel initContentPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        OnlineWidgetSortType.COMPOSITE.sort(sharableWidgetProviders);
        componentSelectPane = createOnlineWidgetSelectPane(sharableWidgetProviders);
        centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        centerPane.add(componentSelectPane, BorderLayout.CENTER);
        filterPane = createFilterPane();
        filterPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 8));
        initFilterPaneListener(filterPane);
        sortTabPane = new SortTabPane();
        initSortTabPane(sortTabPane);
        FlexSearchFieldPane flexSearchPane = new FlexSearchFieldPane(filterPane);
        initSearchTextFieldPaneListener(flexSearchPane);

        JPanel northPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        northPane.add(flexSearchPane, BorderLayout.CENTER);
        northPane.add(sortTabPane, BorderLayout.SOUTH);
        initNorthPane(jPanel, northPane);

        searchResultShowPane = createOnlineWidgetSelectPane(new OnlineShareWidget[]{});
        mainCardLayout = new CardLayout();
        mainCenterPane = new JPanel(mainCardLayout);
        mainCenterPane.add(centerPane, MAIN_FILTER_TAB_PANE);
        mainCenterPane.add(searchResultShowPane, SEARCH_RESULT_PANE);
        jPanel.add(mainCenterPane, BorderLayout.CENTER);

        filterPane.addPopupStateChangeListener(state -> setWidgetPaneScrollEnable(!state));
        return jPanel;
    }

    protected void initNorthPane(JPanel jPanel, JPanel northPane) {
        jPanel.add(northPane, BorderLayout.NORTH);
    }


    protected OnlineWidgetSelectPane createOnlineWidgetSelectPane(OnlineShareWidget[] sharableWidgetProviders) {
        return new OnlineWidgetSelectPane(sharableWidgetProviders, filterPane, 50);
    }

    protected OnlineWidgetSelectPane createOnlineWidgetSelectPane(DataLoad<OnlineShareWidget> dataLoad) {
        return new OnlineWidgetSelectPane(dataLoad, filterPane, 50);
    }

    protected FilterPane createFilterPane() {
        return FilterPane.createOnlineFilterPane();
    }

    protected OnlineShareWidget[] getSharableWidgetProviders() {
        return sharableWidgetProviders;
    }

    public void searchByKeyword(String text) {
        if (StringUtils.isEmpty(text)) {
            this.mainCardLayout.show(mainCenterPane, MAIN_FILTER_TAB_PANE);
            return;
        }
        List<OnlineShareWidget> widgets = new ArrayList<>();
        if (StringUtils.isNotEmpty(text)) {
            for (OnlineShareWidget provider : sharableWidgetCache) {
                if (provider.getName().toLowerCase().contains(text)) {
                    widgets.add(provider);
                }
            }
        }

        if (searchResultShowPane != null) {
            mainCenterPane.remove(searchResultShowPane);
        }
        this.searchResultShowPane = createOnlineWidgetSelectPane(widgets.toArray(new OnlineShareWidget[widgets.size()]));
        this.mainCenterPane.add(searchResultShowPane, SEARCH_RESULT_PANE);
        this.mainCardLayout.show(mainCenterPane, SEARCH_RESULT_PANE);
        this.validate();
        this.repaint();

    }

    public void initFilterPaneListener(FilterPane filterPane) {
        filterPane.registerChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(final ChangeEvent e) {
                String filterStr = e.getSource().toString();
                centerPane.remove(componentSelectPane);
                componentSelectPane = createOnlineWidgetSelectPane(() -> {
                    sharableWidgetProviders = new OnlineShareWidget[0];
                    sharableWidgetProviders = getSharableWidgetArr(filterStr);
                    return sharableWidgetProviders;
                });
                centerPane.add(componentSelectPane, BorderLayout.CENTER);
                AbstractOnlineWidgetShowPane.this.validate();
                AbstractOnlineWidgetShowPane.this.repaint();
            }
        });
    }

    protected OnlineShareWidget[] getSharableWidgetArr( String filterStr){
       return ShareUtils.getFilterWidgets(filterStr);
    }

    public void initSearchTextFieldPaneListener(FlexSearchFieldPane searchFieldPane) {
        searchFieldPane.registerChangeListener(event -> {
            Object object = event.getSource();
            if (object instanceof String) {
                String text = (String) object;
                sortTabPane.setVisible(StringUtils.isEmpty(text));
                searchByKeyword(text);
            }
        });
    }

    public void initSortTabPane(SortTabPane sortTabPane) {

    }

    private void setWidgetPaneScrollEnable(boolean enable) {
        if (componentSelectPane instanceof OnlineWidgetSelectPane) {
            ((OnlineWidgetSelectPane) componentSelectPane).setWidgetPaneScrollEnable(enable);
        }
    }

    public class SortTabPane extends JPanel {
        private final Color BLUE = Color.decode("#419BF9");
        private int index;
        private final UILabel[] labels;
        private final Map<String, Integer> recordMap;
        private List<MouseListener> mouseListeners = new ArrayList<>();

        public SortTabPane() {
            setLayout(new FlowLayout(FlowLayout.LEFT));
            setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));
            OnlineWidgetSortType[] sortTypes = OnlineWidgetSortType.values();
            recordMap = new HashMap<>();
            labels = new UILabel[sortTypes.length];
            int i = 0;
            for (OnlineWidgetSortType sortType : sortTypes) {
                labels[i] = createTab(sortType);
                recordMap.put(sortType.getDisplayName(), i);
                add(labels[i]);
                i++;
                if (i != sortTypes.length) {
                    add(createSeparator());
                }
            }
            setIndex(0);
        }

        public void setIndex(int index) {
            labels[this.index].setForeground(Color.black);
            labels[index].setForeground(BLUE);
            this.index = index;
            repaint();
        }

        public int getIndex() {
            return index;
        }

        private UILabel createTab(OnlineWidgetSortType sortType) {
            UILabel label = new UILabel(sortType.getDisplayName());
            label.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (sharableWidgetProviders.length == 0) {
                        return;
                    }
                    String title = label.getText();
                    if (index != recordMap.get(title)) {
                        setIndex(recordMap.get(title));
                    }
                    if (componentSelectPane != null) {
                        centerPane.remove(componentSelectPane);
                        centerPane.add(loadingPane);
                    }
                    AbstractOnlineWidgetShowPane.this.validate();
                    AbstractOnlineWidgetShowPane.this.repaint();
                    new SwingWorker<Boolean, Void>() {
                        @Override
                        protected Boolean doInBackground() {
                            sortType.sort(sharableWidgetProviders);
                            componentSelectPane = createOnlineWidgetSelectPane(sharableWidgetProviders);
                            return true;
                        }

                        @Override
                        protected void done() {
                            centerPane.removeAll();
                            centerPane.add(componentSelectPane, BorderLayout.CENTER);
                            AbstractOnlineWidgetShowPane.this.validate();
                            AbstractOnlineWidgetShowPane.this.repaint();
                        }
                    }.execute();

                    for (MouseListener listener : mouseListeners) {
                        listener.mouseClicked(e);
                    }
                }
            });
            return label;
        }

        private UILabel createSeparator() {
            UILabel label = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/separator.png"));
            label.setBorder(BorderFactory.createEmptyBorder(0, 2, 0, 2));
            return label;
        }

        public void registerSortTabMouseListener(MouseListener listener) {
            mouseListeners.add(listener);
        }
    }
}
