package com.fr.design.mainframe.share.generate.impl;

import com.fr.design.mainframe.share.Bean.ComponentGenerateInfo;
import com.fr.design.mainframe.share.generate.ComponentBanner;
import com.fr.design.mainframe.share.generate.ComponentCreatorProcessor;
import com.fr.design.mainframe.share.generate.ComponentGenerator;
import com.fr.design.mainframe.share.generate.ComponentTask;
import com.fr.design.mainframe.share.generate.task.ComponentCreator;
import com.fr.design.mainframe.share.ui.base.ShareProgressBar;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.report.ExtraReportClassManager;
import com.fr.third.org.apache.commons.lang3.time.StopWatch;

import javax.swing.SwingWorker;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;

/**
 * created by Harrison on 2020/04/16
 **/
public abstract class AbstractComponentGenerator implements ComponentGenerator {

    /**
     * 默认 40 ms
     */
    private static final int DEFAULT_SPEED = 40;

    /**
     * 进度 100
     */
    private static final int PROGRESS = 100;

    /**
     * 每一个任务平均耗时 2000 ms
     */
    private static final int PER_TIME = 2000;

    private final int speed;

    private ComponentGenerateInfo info;

    public AbstractComponentGenerator(ComponentGenerateInfo info, ComponentBanner... banners) {
        this.info = info;
        this.speed = calSpeed(banners);
    }

    public ComponentGenerateInfo getInfo() {
        return info;
    }

    private int calSpeed(ComponentBanner... banners) {

        if (banners == null || banners.length == 0) {
            return DEFAULT_SPEED;
        }
        return PER_TIME / (PROGRESS / banners.length);
    }

    /**
     * 进度条 100
     * 目前有 n 个任务
     * 每个任务至少 2000 ms
     * <p>
     * 则平均速度为 2000 / （100/n）
     */
    @Override
    public int speed() {

        return this.speed;
    }

    @Override
    public boolean generate() throws Throwable {

        try {
            return generate0();
        } catch (ExecutionException e) {
            //抛出根本原因
            throw e.getCause();
        } catch (Exception e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return false;
        }
    }

    protected abstract boolean generate0() throws InterruptedException, ExecutionException;

    protected <T> T execute(final ComponentTask<T> componentTask) throws InterruptedException, ExecutionException {

        final CyclicBarrier barrier = new CyclicBarrier(2);
        Thread UIThread = new Thread(new FutureTask<Void>(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                SwingWorker<Void, Boolean> worker = new SwingWorker<Void, Boolean>() {
                    @Override
                    protected Void doInBackground() throws Exception {

                        StopWatch stopWatch = new StopWatch();
                        stopWatch.start();

                        //ui更新
                        doUIUpdate();

                        stopWatch.stop();
                        long time = stopWatch.getTime(TimeUnit.MILLISECONDS);
                        if (time < 2000L) {
                            //不到 2 s
                            ShareUIUtils.wait((int) (2000L - time));
                        }

                        //完成更新
                        finishUpdate();
                        return null;
                    }

                    private int finishUpdate() throws InterruptedException, BrokenBarrierException {
                        return barrier.await();
                    }

                    private void doUIUpdate() throws Exception {
                        ShareProgressBar.getInstance().updateProgress(componentTask.indicator(), componentTask.getLoadingText());
                    }
                };
                worker.execute();
                return null;
            }
        }));
        UIThread.setName("Component-UIThread");

        FutureTask<T> task = new FutureTask<>(new Callable<T>() {
            @Override
            public T call() throws Exception {
                T result = componentTask.execute();
                barrier.await();
                return result;
            }
        });
        Thread workThread = new Thread(task);
        workThread.setName("Component-WorkThread");
        parallel(UIThread, workThread);
        return task.get();
    }

    private void parallel(Thread UIThread, Thread workThread) {

        UIThread.start();
        workThread.start();
    }


    public static ComponentCreatorProcessor getComponentCreator(){
        ComponentCreatorProcessor processor = ExtraReportClassManager.getInstance().getSingle(ComponentCreatorProcessor.MARK_STRING, ComponentCreator.class);
        return processor;
    }

}
