package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.design.i18n.Toolkit;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.bean.WidgetFilterInfo;
import com.fr.design.mainframe.share.constants.DisplayDevice;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/11
 */
public class LocalWidgetFilter {
    private List<WidgetFilterInfo> filterList;

    private LocalWidgetFilter() {
        filterList = new ArrayList<>();
    }

    private static class Holder {
        private static final LocalWidgetFilter HOLDER = new LocalWidgetFilter();
    }

    public static LocalWidgetFilter getInstance() {
        return Holder.HOLDER;
    }

    public void setFilterList(List<WidgetFilterInfo> filterList) {
        this.filterList = filterList;
    }

    public boolean shouldShow(SharableWidgetProvider provider) {
        if (filterList == null || filterList.size() == 0) {
            return true;
        }
        if (provider instanceof DefaultSharableWidget) {
            DefaultSharableWidget bindInfo = (DefaultSharableWidget) provider;
            //类型，即图表类型或报表类型
            boolean sameType = true;
            //展示终端，即PC端、移动端
            boolean sameDisplayDevice = true;
            //价格，即免费或者付费
            boolean sameSource = true;

            //如果有筛选条件，先初始化为false
            for (WidgetFilterInfo filterInfo : filterList) {
                if (StringUtils.equals(filterInfo.getType(), "chart") | StringUtils.equals(filterInfo.getType(), "report")) {
                    sameType = false;
                }
                if (StringUtils.equals(filterInfo.getType(), "displayDevice")) {
                    sameDisplayDevice = false;
                }
                if (StringUtils.equals(filterInfo.getType(), "source")) {
                    sameSource = false;
                }
            }
            //符合条件，设置为true
            for (WidgetFilterInfo filterInfo : filterList) {
                //类型
                sameType |= filterType(filterInfo, bindInfo);
                //展示终端
                sameDisplayDevice |= filterDisplayDevice(filterInfo, bindInfo);
                //来源
                sameSource |= filterSource(filterInfo, bindInfo);
            }
            return sameDisplayDevice & sameSource & sameType;
        }
        return false;
    }

    public SharableWidgetProvider[] filter(SharableWidgetProvider[] elCaseBindInfoList) {
        SharableWidgetProvider[] result = elCaseBindInfoList;
        for (SharableWidgetProvider provider : result) {
            if (!LocalWidgetFilter.getInstance().shouldShow(provider)) {
                result = ArrayUtils.removeElement(result, provider);
            }
        }
        return result;
    }

    public boolean hasFilter() {
        return filterList != null && filterList.size() != 0;
    }

    private boolean filterType(WidgetFilterInfo filterInfo, DefaultSharableWidget bindInfo) {
        //图表报表类型
        if (StringUtils.equals(filterInfo.getType(), "chart") | StringUtils.equals(filterInfo.getType(), "report")) {
            //图表、报表类型改名之后的兼容
            if (sameType(filterInfo, bindInfo)) {
                return true;
            }
            return StringUtils.equals(bindInfo.getChildClassify(), filterInfo.getName());
        }
        return false;
    }

    private boolean filterDisplayDevice(WidgetFilterInfo filterInfo, DefaultSharableWidget bindInfo) {
        if (StringUtils.equals(filterInfo.getType(), "displayDevice")) {
            boolean sameMobile = StringUtils.equals(filterInfo.getName(), "移动端") & DisplayDevice.supportMobile(bindInfo.getDisplayDevice());
            boolean samePC = StringUtils.equals(filterInfo.getName(), "PC端") & DisplayDevice.supportPC(bindInfo.getDisplayDevice());
            //filterInfo的设备信息 = bindInfo的设备信息，为true
            return sameMobile | samePC;
        }
        return false;
    }

    private boolean filterSource(WidgetFilterInfo filterInfo, DefaultSharableWidget bindInfo) {
        if (StringUtils.equals(filterInfo.getType(), "source")) {
            boolean sameMarket = StringUtils.equals(filterInfo.getName(), "商城") && StringUtils.equals(bindInfo.getSource(), DefaultSharableWidget.MARKET);
            boolean sameLocal = StringUtils.equals(filterInfo.getName(), "本地") && StringUtils.equals(bindInfo.getSource(), DefaultSharableWidget.LOCAL);
            return sameMarket | sameLocal;
        }
        return false;
    }

    private boolean sameType(WidgetFilterInfo filterInfo, DefaultSharableWidget bindInfo) {
        //旧：其他 新：其它图表
        boolean other = StringUtils.equals(filterInfo.getName(), Toolkit.i18nText("Fine-Design_Share_Type_Chart_Other"))
                && (StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Chart_Other"))
                || StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Others")));

        //旧：移动维度切换 新：多维度切换
        boolean dimensionChange = StringUtils.equals(filterInfo.getName(), Toolkit.i18nText("Fine-Design_Share_Type_Dimension_Change"))
                && (StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Mobile_Dimension_Change"))
                || StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Dimension_Change")));

        //旧：移动填报 新：填报
        boolean fill = StringUtils.equals(filterInfo.getName(), Toolkit.i18nText("Fine-Design_Share_Type_Fill"))
                && (StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Fill"))
                || StringUtils.equals(bindInfo.getChildClassify(), Toolkit.i18nText("Fine-Design_Share_Type_Mobile_Fill")));

        return other | dimensionChange | fill;
    }
}
