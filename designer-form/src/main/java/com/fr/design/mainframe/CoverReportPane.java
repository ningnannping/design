package com.fr.design.mainframe;

import com.fr.base.BaseUtils;
import com.fr.design.dialog.BasicPane;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.share.constants.ShareEntryKey;
import com.fr.form.ui.Widget;
import com.fr.stable.Constants;
import com.fr.stable.bridge.StableFactory;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.HashMap;

/**
 * 报表块的封面（如果后面所有的组件都有帮助信息的话就抽接口吧）
 * Coder: zack
 * Date: 2016/11/2
 * Time: 11:32
 */
public class CoverReportPane extends CoverPane{
    public static final int SHARE_CONF_BTN_W = 90;
    public static final int SHARE_CONF_BTN_H = 24;

    public static final Color SHARE_BTN_BG = new Color(152, 193, 250);

    public static void showShareConfig(Widget widget) {
        Object[] compositeArg = new Object[]{widget};
        HashMap<String, Class> compoClass = new HashMap<String, Class>();
        compoClass.put(Constants.ARG_0, Widget.class);
        BasicPane shareGuidePane = StableFactory.getMarkedInstanceObjectFromClass(ShareEntryKey.SHARE_CONFIG, compositeArg, compoClass, BasicPane.class);
        shareGuidePane.show();
    }

    public static void paintShareButton(Graphics g, Component component) {
        int x = component.getWidth() - SHARE_CONF_BTN_W;
        int y = 0;

        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(SHARE_BTN_BG);
        g2d.fillRoundRect(x ,y, SHARE_CONF_BTN_W, SHARE_CONF_BTN_H,5,5);
        g2d.setColor(Color.WHITE);

        // 画配置文字
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.drawString(Toolkit.i18nText("Fine-Design_Share_Help_Settings"),x + 30, 17);
        // 画图标
        BufferedImage image = BaseUtils.readImage("com/fr/base/images/share/config.png");
        g2d.drawImage(image, x + 10, 5, image.getWidth(), image.getHeight(), null);
    }

    private boolean isShared = false;

    public CoverReportPane() {
        this(false);
    }

    public CoverReportPane(boolean shared) {
        super();
        isShared = shared;
    }

    public boolean isShared() {
        return  isShared;
    }

    public void setShared(boolean shared) {
        isShared = shared;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
    }
}
