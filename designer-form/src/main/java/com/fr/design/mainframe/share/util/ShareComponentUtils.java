package com.fr.design.mainframe.share.util;

import com.fr.base.io.IOFile;
import com.fr.base.iofile.attr.ExtendSharableAttrMark;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.designer.creator.XLayoutContainer;
import com.fr.design.designer.creator.XWTitleLayout;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.JTemplate;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.bean.ShareLayoutWidget;
import com.fr.form.share.constants.ComponentPath;
import com.fr.form.ui.Widget;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;
import com.fr.workspace.WorkContext;
import org.jetbrains.annotations.NotNull;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

/**
 * Coder: zack
 * Date: 2016/9/20
 * Time: 12:00
 */
public class ShareComponentUtils {

    @NotNull
    public static XCreator createXCreator(Widget creatorSource, String shareId, SharableWidgetProvider provider) {

        XCreator xCreator;
        if (creatorSource instanceof ShareLayoutWidget) {
            xCreator = XCreatorUtils.createXCreator(((ShareLayoutWidget) creatorSource).getWidget(), new Dimension(provider.getWidth(), provider.getHeight()));
        } else {
            xCreator = XCreatorUtils.createXCreator(creatorSource, new Dimension(provider.getWidth(), provider.getHeight()));
        }
        xCreator.setBackupBound(new Rectangle(provider.getWidth(), provider.getHeight()));
        xCreator.setShareId(shareId);
        return xCreator;
    }

    /**
     * 检查readme.txt文件
     */
    public static void checkReadMe() {
        String shareDir = ComponentPath.SHARE_PATH.path();
        String readmePath = StableUtils.pathJoin(shareDir, "readme.txt");
        if (WorkContext.getWorkResource().exist(readmePath)) {
            return;
        }
        String msg = Toolkit.i18nText("Fine-Design_Share_Share_Read_Me_Tip");
        WorkContext.getWorkResource().write(readmePath, msg.getBytes());
    }

    public static String getWidgetId(Widget widget) {
        if (StringUtils.isEmpty(widget.getWidgetID())) {
            // 做下兼容处理，之前的插件创建的组件没有对应的widgetId
            return widget.getWidgetName();
        }
        return widget.getWidgetID();
    }

    public static String getCurrentTemplateID() {
        JTemplate jt = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        IOFile target = (IOFile) jt.getTarget();
        return target.getTemplateID();
    }

    public static boolean isShareWidget(XCreator xCreator) {
        if (xCreator == null) {
            return false;
        }
        if (xCreator.isShared()) {
            return true;
        }
        if (xCreator instanceof XLayoutContainer) {
            XLayoutContainer xLayoutContainer = (XLayoutContainer) xCreator;
            ExtendSharableAttrMark sharableAttrMark = xLayoutContainer.toData().getWidgetAttrMark(ExtendSharableAttrMark.XML_TAG);
            return sharableAttrMark != null && StringUtils.isNotEmpty(sharableAttrMark.getShareId());
        }
        return false;
    }

    public static boolean isShareWidgetWithChild(XCreator xCreator) {
        boolean result = isShareWidget(xCreator);
        if (result) {
            return true;
        }
        //做下兼容处理，老的组件结构的共享信息存储在子组件上
        if (xCreator instanceof XWTitleLayout) {
            XCreator child = ((XWTitleLayout) xCreator).getBodyCreator();
            return isShareWidget(child);
        }
        return false;
    }


    public static List<XCreator> getHelpConfigXCreatorList(XCreator root) {
        List<XCreator> helpConfigXCreatorList = new ArrayList<>();
        if (isShareWidget(root)) {
            helpConfigXCreatorList.add(root);
            return helpConfigXCreatorList;
        }
        Component[] components = root.getComponents();
        for (Component component : components) {
            if (component instanceof XCreator) {
                helpConfigXCreatorList.addAll(getHelpConfigXCreatorList((XCreator) component));
            }
        }
        return helpConfigXCreatorList;
    }
}
