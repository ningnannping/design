package com.fr.design.mainframe.share.encrypt.clipboard;

import com.fr.design.fun.impl.AbstractClipboardHandlerProvider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * created by Harrison on 2020/05/14
 **/
public abstract class CrossClipboardHandler<T> extends AbstractClipboardHandlerProvider<T> {
    
    private List<CrossClipboardState> states = new ArrayList<>(8);
    
    public CrossClipboardHandler(CrossClipboardState... states) {
        
        init(states);
    }
    
    private void init(CrossClipboardState... states) {
        
        if (states == null) {
            return;
        }
        Collections.addAll(this.states, states);
    }
    
    @Override
    public T cut(T selection) {
        
        mark();
        return cut0(selection);
    }
    
    protected T cut0(T selection) {
        
        return selection;
    }
    
    @Override
    public T copy(T selection) {
        
        mark();
        return copy0(selection);
    }
    
    protected T copy0(T selection) {
        
        return selection;
    }
    
    @Override
    public T paste(T selection) {
        
        return isBan() ? null : paste0(selection);
    }
    
    protected T paste0(T selection) {
        
        return selection;
    }
    
    private void mark() {
        
        for (CrossClipboardState state : states) {
            state.mark();
        }
    }
    
    private boolean isBan() {
        
        boolean isBan = false;
        for (CrossClipboardState state : states) {
            isBan |= state.isBan();
        }
        return isBan;
    }
}
