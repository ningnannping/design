package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.MouseClickListener;
import com.fr.form.share.bean.WidgetFilterTypeInfo;
import com.fr.form.share.utils.ShareUtils;
import com.fr.general.ComparatorUtils;
import com.fr.invoke.Reflect;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.Popup;
import javax.swing.PopupFactory;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.AWTEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * Created by kerry on 2020-10-21
 */
public class FilterPane extends JPanel {
    public static final String SOURCE_FILTER_KEY = "2@source";
    public static final String CHART_FILTER_KEY = "3@chart";
    public static final String REPORT_FILTER_KEY = "4@report";

    private static final Icon FILTER_COMBO = BaseUtils.readIcon("/com/fr/base/images/share/filter_combo.png");
    private static final Icon FILTER_COMBO_UP = BaseUtils.readIcon("/com/fr/base/images/share/filter_combo_up.png");
    private final UILabel filterLabel;
    private UILabel arrowButton;
    private Popup popup;
    private boolean showPopup = false;
    private FilterPopupPane filterPopupPane;
    private List<ChangeListener> changeListenerList = new ArrayList<>();
    private final AWTEventListener awtEventListener;
    List<PopStateChangeListener> listenerList = new ArrayList<>();

    private FilterPane() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        final JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setBorder(BorderFactory.createLineBorder(Color.decode("#D9DADD")));
        this.add(jPanel, BorderLayout.CENTER);
        jPanel.setBackground(Color.WHITE);
        this.filterLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Online_No_Filter"), SwingConstants.LEFT);
        filterLabel.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
        filterLabel.setPreferredSize(new Dimension(80, 20));
        filterLabel.setForeground(Color.decode("#8F8F92"));
        filterLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (filterPopupPane != null) {
                    if (filterPopupPane.hasFilter()) {
                        filterPopupPane.reset();
                    } else {
                        controlFilterPopUp();
                    }
                }

            }
        });
        jPanel.addMouseListener(
                new MouseClickListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        super.mouseClicked(e);
                        controlFilterPopUp();
                    }
                }
        );
        JPanel subPane = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
        subPane.add(filterLabel);
        subPane.setBackground(Color.WHITE);
        jPanel.add(subPane, BorderLayout.CENTER);
        arrowButton = new UILabel(FILTER_COMBO);
        awtEventListener = event -> {
            if (event instanceof MouseEvent) {
                MouseEvent mv = (MouseEvent) event;
                if (isCanHide(mv, jPanel)) {
                    hideFilterPopUp();
                }
            }

        };
        jPanel.add(arrowButton, BorderLayout.EAST);
    }

    public static FilterPane createLocalFilterPane() {
        FilterPane pane = new FilterPane();
        pane.filterPopupPane = new LocalFilterPopupPane(pane, LocalWidgetFilterCategory.getLocalCategory());
        return pane;
    }

    public static FilterPane createOnlineFilterPane() {
        FilterPane pane = new FilterPane();
        pane.filterPopupPane = new OnlineFilterPopupPane(pane, pane.loadFilterCategories());
        return pane;
    }

    public static FilterPane createOnlinePackageFilterPane() {
        FilterPane pane = new FilterPane() {
            @Override
            protected List<WidgetFilterTypeInfo> loadFilterCategories() {
                List<WidgetFilterTypeInfo> filterTypeInfos = super.loadFilterCategories();
                filterTypeInfos.removeIf(info -> ComparatorUtils.equals(FilterPane.CHART_FILTER_KEY, info.getKey())
                        || ComparatorUtils.equals(FilterPane.REPORT_FILTER_KEY, info.getKey()));
                return filterTypeInfos;
            }
        };
        pane.filterPopupPane = new LocalFilterPopupPane(pane, pane.loadFilterCategories());
        return pane;
    }

    public void changeFilterButtonStatus(boolean hasFilter) {
        if (hasFilter) {
            switchToClearFilter();
        } else {
            switchToNoFilter();
        }
    }

    public boolean isShowPopup() {
        return showPopup;
    }

    private void controlFilterPopUp() {
        if (!showPopup) {
            showFilterPopUp();
        } else {
            hideFilterPopUp();
        }
    }

    private synchronized void showFilterPopUp() {
        PopupFactory pf = PopupFactory.getSharedInstance();
        Point p = FilterPane.this.getLocationOnScreen();
        popup = pf.getPopup(FilterPane.this, filterPopupPane, p.x, p.y + FilterPane.this.getHeight());
        popup.show();
        filterPopupPane.setPreferredSize(new Dimension(228, filterPopupPane.getHeight()));
        showPopup = true;
        arrowButton.setIcon(FILTER_COMBO_UP);
        firePopupStateChange(true);
        java.awt.Toolkit.getDefaultToolkit().addAWTEventListener(awtEventListener, AWTEvent.MOUSE_EVENT_MASK);
    }

    private void hideFilterPopUp() {
        popup.hide();
        showPopup = false;
        arrowButton.setIcon(FILTER_COMBO);
        firePopupStateChange(false);
        java.awt.Toolkit.getDefaultToolkit().removeAWTEventListener(awtEventListener);
    }

    protected List<WidgetFilterTypeInfo> loadFilterCategories() {
        return ShareUtils.getWidgetFilterTypeInfos();
    }

    private boolean isCanHide(MouseEvent mv, JPanel jPanel) {
        if (mv.getClickCount() < 1) {
            return false;
        }
        // 表示Popup弹窗是否能被点击到
        boolean clickFlag = false;
        if (popup != null && showPopup) {
            Component component = Reflect.on(popup).call("getComponent").get();
            Point p = component.getLocationOnScreen();
            Point clickPoint = mv.getLocationOnScreen();
            clickFlag = clickPoint.getX() >= p.getX()
                    && clickPoint.getY() >= p.getY()
                    && clickPoint.getX() <= component.getWidth() + p.getX()
                    && clickPoint.getY() <= p.getY() + component.getHeight();

        }
        return popup != null && showPopup
                && !clickFlag && !ComparatorUtils.equals(mv.getSource(), jPanel)
                && !ComparatorUtils.equals(mv.getSource(), filterLabel);
    }

    public void registerChangeListener(ChangeListener changeListener) {
        changeListenerList.add(changeListener);
    }

    public void fireChangeListener(ChangeEvent e) {
        for (ChangeListener changeListener : changeListenerList) {
            changeListener.stateChanged(e);
        }
    }

    public void reset() {
        if (filterPopupPane != null && filterPopupPane.hasFilter()) {
            filterPopupPane.reset();
        }
        switchToNoFilter();
    }

    private void switchToNoFilter() {
        filterLabel.setText(Toolkit.i18nText("Fine-Design_Share_Online_No_Filter"));
        filterLabel.setForeground(Color.decode("#8F8F92"));
        filterLabel.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    private void switchToClearFilter() {
        filterLabel.setText(Toolkit.i18nText("Fine-Design_Share_Online_Clear_Filter"));
        filterLabel.setForeground(Color.decode("#419BF9"));
        filterLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    public void firePopupStateChange(boolean PopupState) {
        for (PopStateChangeListener listener : listenerList) {
            listener.stateChange(PopupState);
        }
    }

    public void addPopupStateChangeListener(PopStateChangeListener listener) {
        listenerList.add(listener);
    }

    public interface PopStateChangeListener extends EventListener {
        void stateChange(boolean state);
    }

}
