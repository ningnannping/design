package com.fr.design.mainframe.adaptve.config;


import com.fr.event.Event;

/**
 * Created by kerry on 4/29/21
 */
public interface TriggerPointProvider {
    /**
     * 触发后的操作
     */
   void triggerAction();

    /**
     * 触发事件
     * @return
     */
    Event triggerEvent();
}
