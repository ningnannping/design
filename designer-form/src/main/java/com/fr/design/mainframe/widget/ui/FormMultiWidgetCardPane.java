package com.fr.design.mainframe.widget.ui;

import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.gui.frpane.AttributeChangeListener;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UINumberField;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.design.mainframe.widget.arrangement.buttons.BottomAlignButton;
import com.fr.design.mainframe.widget.arrangement.buttons.HorizontalCenterButton;
import com.fr.design.mainframe.widget.arrangement.buttons.HorizontalDistributionButton;
import com.fr.design.mainframe.widget.arrangement.buttons.LeftAlignButton;
import com.fr.design.mainframe.widget.arrangement.buttons.RightAlignButton;
import com.fr.design.mainframe.widget.arrangement.buttons.TopAlignButton;
import com.fr.design.mainframe.widget.arrangement.buttons.VerticalCenterButton;
import com.fr.design.mainframe.widget.arrangement.buttons.VerticalDistributionButton;
import com.fr.general.IOUtils;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class FormMultiWidgetCardPane extends FormWidgetCardPane {
    private AttributeChangeListener listener;
    private MultiSelectionArrangement arrangement;

    public FormMultiWidgetCardPane(FormDesigner designer) {
        super(designer);
    }

    public void initPropertyPane() {
        arrangement = new MultiSelectionArrangement(designer);
        content.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 0));
        content.add(createArrangementLayoutPane(), BorderLayout.CENTER);
        this.listener = new AttributeChangeListener() {
            @Override
            public void attributeChange() {
                designer.getEditListenerTable().fireCreatorModified(DesignerEvent.CREATOR_RESIZED);
            }
        };
    }

    // 整个排列分布面板的layout，可以看成一个三行一列的表格，第一行是分布，第二行是自动间距，第三行是手动间距
    private JPanel createArrangementLayoutPane() {
        double[] rowSize = {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED};
        double[] columnSize = {TableLayout.PREFERRED};
        Component[][] components = new Component[][] {
                new Component[] {
                        createAlignmentPane()
                },
                new Component[] {
                        createAutoSpacingPane()
                }
        };
        return TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, 0, 16);
    }

    // 对齐
    private JPanel createAlignmentPane() {
        double[] rowSize = {TableLayout.PREFERRED};
        double[] columnSize = {
                TableLayout.PREFERRED,
                TableLayout.PREFERRED,
                TableLayout.PREFERRED,
                TableLayout.PREFERRED,
                TableLayout.PREFERRED,
                TableLayout.PREFERRED
        };
        Component[][] components = new Component[][] {
                new Component[] {
                        new LeftAlignButton(arrangement),
                        new HorizontalCenterButton(arrangement),
                        new RightAlignButton(arrangement),
                        new TopAlignButton(arrangement),
                        new VerticalCenterButton(arrangement),
                        new BottomAlignButton(arrangement)
                }
        };
        JPanel centerPane = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, 18, 0);
        return createTitleLayout(Toolkit.i18nText("Fine-Design_Multi_Selection_Align"), centerPane);
    }

    // 自动间距
    private JPanel createAutoSpacingPane() {
        double[] rowSize = {TableLayout.PREFERRED};
        double[] columnSize = {
                TableLayout.PREFERRED,
                TableLayout.PREFERRED
        };
        UIButton horizontalAutoSpacingBtn = new HorizontalDistributionButton(arrangement);
        UIButton verticalAutoSpacingBtn = new VerticalDistributionButton(arrangement);
        if (designer.getSelectionModel().getSelection().size() < 3) {
            horizontalAutoSpacingBtn.setEnabled(false);
            verticalAutoSpacingBtn.setEnabled(false);
        }
        Component[][] components = new Component[][] {
                new Component[] {
                        horizontalAutoSpacingBtn,
                        verticalAutoSpacingBtn
                }
        };
        JPanel centerPane = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, 18, 0);


        UILabel tip = new UILabel(IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_auto_spacing_tip.png"));
        tip.setToolTipText(Toolkit.i18nText("Fine-Design_Multi_Selection_Auto_Spacing_Tip"));
        Component[][] titleComponents = new Component[][] {
                new Component[] {
                        new UILabel(Toolkit.i18nText("Fine-Design_Multi_Selection_Auto_Spacing")),
                        tip
                }
        };
        JPanel northPane = TableLayoutHelper.createGapTableLayoutPane(titleComponents, rowSize, columnSize, 5, 0);

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 8));
        jPanel.add(northPane, BorderLayout.NORTH);
        jPanel.add(centerPane, BorderLayout.CENTER);
        return jPanel;
    }

    // 手动间距
    private JPanel createManualSpacingPane() {
        double[] rowSize = {TableLayout.PREFERRED, TableLayout.PREFERRED};
        double[] columnSize = {
                TableLayout.PREFERRED,
                TableLayout.FILL
        };
        UITextField horizontalSpacingNumberField = createIntNumberField(false, Toolkit.i18nText("Fine-Design_Multi_Selection_Manual_Horizontal_Spacing_Tip"));
        UITextField verticalSpacingNumberField = createIntNumberField(true, Toolkit.i18nText("Fine-Design_Multi_Selection_Manual_Vertical_Spacing_Tip"));
        Component[][] components = new Component[][] {
                new Component[] {
                        new UILabel(IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_horizontal_spacing.png")),
                        horizontalSpacingNumberField
                },
                new Component[] {
                        new UILabel(IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_vertical_spacing.png")),
                        verticalSpacingNumberField
                }
        };
        JPanel centerPane = TableLayoutHelper.createGapTableLayoutPane(components, rowSize, columnSize, 21, 9);
        return createTitleLayout(Toolkit.i18nText("Fine-Design_Multi_Selection_Manual_Spacing"), centerPane);
    }

    private UINumberField createIntNumberField(boolean isVertical, String tipText) {
        final UINumberField numberField = new UINumberField() {
            private static final long serialVersionUID = -448512934137620557L;

            public boolean shouldResponseChangeListener() {
                return false;
            }
        };
        numberField.setPlaceholder(tipText);
        numberField.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
                distributionDoChange(numberField, isVertical);
            }
        });
        numberField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    distributionDoChange(numberField, isVertical);
                }
            }
        });
        return numberField;
    }

    private void distributionDoChange(UINumberField numberField, boolean isVertical) {
        String text = numberField.getText();
        if (StringUtils.isNotEmpty(text) && StableUtils.isNumber(text)) {
            int gap = (int) Math.floor(Float.parseFloat(text));
            numberField.setValue(gap);
            if (isVertical) {
                arrangement.doArrangement(ArrangementType.VERTICAL_MANUAL_DISTRIBUTION, gap);
            } else {
                arrangement.doArrangement(ArrangementType.HORIZONTAL_MANUAL_DISTRIBUTION, gap);
            }
            attributeChanged();
        }
    }

    // 创建一个BorderLayout布局，上面是标题，例如“对齐”，下面是个容器
    private JPanel createTitleLayout(String title, JPanel centerPane) {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout(0, 8));
        jPanel.add(new UILabel(title), BorderLayout.NORTH);
        jPanel.add(centerPane, BorderLayout.CENTER);
        return jPanel;
    }

    @Override
    public void populate() {
        initListener(this);
        this.addAttributeChangeListener(listener);
    }
}
