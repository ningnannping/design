package com.fr.design.mainframe.widget.accessibles;

import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.mainframe.FormDesigner;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.mainframe.mobile.ui.MobileParamSettingPane;
import com.fr.design.mainframe.widget.wrappers.MobileParamWrapper;
import com.fr.form.ui.container.WParameterLayout;
import com.fr.form.ui.mobile.MobileParamStyle;
import com.fr.report.mobile.EmptyMobileParamStyle;
import javax.swing.SwingUtilities;

/**
 * @author hades
 * @version 10.0
 * Created by hades on 2021/1/4
 */
public class AccessibleMobileParamEditor extends UneditableAccessibleEditor {

    private MobileParamSettingPane mobileParamSettingPane;

    public AccessibleMobileParamEditor(MobileParamSettingPane mobileParamSettingPane) {
        super(new MobileParamWrapper());
        this.mobileParamSettingPane = mobileParamSettingPane;
    }

    @Override
    protected void showEditorPane() {
        mobileParamSettingPane.setPreferredSize(BasicDialog.MEDIUM);
        BasicDialog dlg = mobileParamSettingPane.showWindow(SwingUtilities.getWindowAncestor(this));
        dlg.addDialogActionListener(new DialogActionAdapter() {
            @Override
            public void doOk() {
                MobileParamStyle mobileParamStyle = mobileParamSettingPane.update();
                setValue(mobileParamStyle);
                FormDesigner formDesigner = WidgetPropertyPane.getInstance().getEditingFormDesigner();
                WParameterLayout wParameterLayout = (WParameterLayout) formDesigner.getSelectionModel().getSelection().getSelectedCreator().toData();
                if (mobileParamStyle instanceof EmptyMobileParamStyle) {
                    wParameterLayout.setProvider(((EmptyMobileParamStyle) mobileParamStyle).getProvider());
                }
                fireStateChanged();
            }
        });
        mobileParamSettingPane.populate((MobileParamStyle) getValue());
        dlg.setVisible(true);
    }
}
