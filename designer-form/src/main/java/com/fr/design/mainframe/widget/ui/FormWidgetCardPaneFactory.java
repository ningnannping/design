package com.fr.design.mainframe.widget.ui;

import com.fr.design.mainframe.FormDesigner;

public class FormWidgetCardPaneFactory {
    public static FormWidgetCardPane create(FormDesigner designer) {
        if (designer.isMultiSelection()) {
            return new FormMultiWidgetCardPane(designer);
        } else {
            return new FormSingleWidgetCardPane(designer);
        }
    }
}
