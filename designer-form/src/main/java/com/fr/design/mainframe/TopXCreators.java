package com.fr.design.mainframe;

import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.designer.beans.models.SelectionModel;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.designer.creator.cardlayout.XWCardMainBorderLayout;

import javax.swing.*;
import java.awt.*;


/**
 * 需要显示顶层的组件层
 *
 * @Author: Yuan.Wang
 * @Date: 2020/8/25
 */
public class TopXCreators extends JComponent {
    final private FormDesigner designer;

    public TopXCreators(FormDesigner designer) {
        this.designer = designer;
        init();
    }

    private void init() {
        setLayout(null);
        setVisible(false);
        setBackground(null);
        setOpaque(false);
        designer.addDesignerEditListener(e -> {
            if (e.getCreatorEventID() == DesignerEvent.CREATOR_SELECTED || e.getCreatorEventID() == DesignerEvent.CREATOR_CUTED) {
                refresh();
            }
        });
    }

    /**
     * 选中的组件有变化时刷新
     */
    public void refresh() {
        removeAll();
        addXCreators();
    }

    @Override
    public void paint(Graphics g) {
        setSize(designer.getSize());
        resizeTopXCreators();
        super.paint(g);
    }

    /**
     * 加入被选择的组件
     */
    private void addXCreators() {
        SelectionModel selectionModel = designer.getSelectionModel();
        XCreator[] xCreators = selectionModel.getSelection().getSelectedCreators();
        for (XCreator creator : xCreators) {
            if (!creator.isTopable()) {
                continue;
            }
            XWCardMainBorderLayout topXMainBorderLayout = XCreatorUtils.getTopXMainBorderLayout(creator);
            add(new TopXCreator(designer, creator));
            if (topXMainBorderLayout != null) {
                add(new TopXCreator(designer, topXMainBorderLayout));
            }

        }
    }

    /**
     * 更新顶层组件的位置和大小
     */
    private void resizeTopXCreators() {
        for (int i = 0, count = getComponentCount(); i < count; i++) {
            TopXCreator topXCreator = (TopXCreator) getComponent(i);
            topXCreator.resizeTopXCreator();
        }
    }
}
