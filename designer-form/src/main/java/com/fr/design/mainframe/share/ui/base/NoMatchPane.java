package com.fr.design.mainframe.share.ui.base;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

/**
 * @Author: Yuan.Wang
 * @Date: 2021/1/14
 */
public class NoMatchPane extends JPanel {
    public NoMatchPane() {
        init();
    }

    private void init() {
        JPanel panel = FRGUIPaneFactory.createVerticalFlowLayout_Pane(true, FlowLayout.LEADING, 0, 5);
        UILabel picLabel = new UILabel();
        picLabel.setIcon(BaseUtils.readIcon("com/fr/base/images/share/no_match_icon.png"));
        picLabel.setHorizontalAlignment(SwingConstants.CENTER);
        picLabel.setPreferredSize(new Dimension(240, 100));
        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Share_No_Match_Result"), SwingConstants.CENTER);
        label.setBorder(BorderFactory.createEmptyBorder(5, 0, 0, 0));
        label.setForeground(Color.gray);
        label.setPreferredSize(new Dimension(240, 20));
        label.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(picLabel);
        panel.add(label);
        panel.setBorder(BorderFactory.createEmptyBorder(250, 0, 0, 0));
        this.setLayout(new BorderLayout());
        this.add(panel, BorderLayout.CENTER);
    }
}
