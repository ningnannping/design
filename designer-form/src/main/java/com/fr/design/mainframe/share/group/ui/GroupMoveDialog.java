package com.fr.design.mainframe.share.group.ui;

import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.form.share.group.DefaultShareGroupManager;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/9
 */
abstract public class GroupMoveDialog extends BaseGroupDialog {
    private UIComboBox selectGroupBox;

    public GroupMoveDialog(Frame frame) {
        super(frame);
        this.setLayout(new BorderLayout());
        this.setModal(true);

        setTitle(Toolkit.i18nText("Fine-Design_Share_Group_Move"));
        // 标签
        UILabel newNameLabel = creteNewNameLabel();
        // 输入框
        createSwitchGroupBox();

        // 确认按钮
        UIButton confirmButton = createConfirmButton();
        confirmButton.setEnabled(true);
        // 取消按钮
        UIButton cancelButton = createCancelButton();

        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
        topPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
        topPanel.add(newNameLabel);
        topPanel.add(selectGroupBox);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
        bottomPanel.setPreferredSize(new Dimension(340, 45));
        bottomPanel.add(confirmButton);
        bottomPanel.add(cancelButton);
        this.add(topPanel, BorderLayout.CENTER);
        this.add(bottomPanel, BorderLayout.SOUTH);

        initStyle();


    }

    private void createSwitchGroupBox() {
        // 文件名输入框
        selectGroupBox = new UIComboBox(DefaultShareGroupManager.getInstance().getAllGroup());
        selectGroupBox.setPreferredSize(new Dimension(225, 20));
    }


    private UILabel creteNewNameLabel() {
        // 输入框前提示
        UILabel newNameLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Group_Select"));
        newNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        newNameLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        newNameLabel.setPreferredSize(new Dimension(60, 16));
        return newNameLabel;
    }

    protected UIComboBox getSelectGroupBox() {
        return selectGroupBox;
    }

}
