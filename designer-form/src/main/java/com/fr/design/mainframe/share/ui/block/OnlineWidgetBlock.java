package com.fr.design.mainframe.share.ui.block;

import com.fr.base.iofile.attr.SharableAttrMark;
import com.fr.design.DesignerEnvManager;
import com.fr.design.base.mode.DesignModeContext;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.form.util.XCreatorConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.login.DesignerLoginHelper;
import com.fr.design.login.DesignerLoginSource;
import com.fr.design.mainframe.WidgetToolBarPane;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.form.share.group.DefaultShareGroup;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetRepoPane;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetSelectPane;
import com.fr.design.mainframe.share.util.DownloadUtils;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.ui.util.UIUtil;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.form.share.utils.ShareUtils;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.general.IOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.StableUtils;
import com.fr.stable.StringUtils;

import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import java.awt.AlphaComposite;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.dnd.DnDConstants;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * Created by kerry on 2020-10-19
 * 商城组件块
 */
public class OnlineWidgetBlock extends AbstractOnlineWidgetBlock {
    private boolean isMouseEnter = false;
    private boolean downloading = false;
    private static final Color COVER_COLOR = Color.decode("#333334");
    protected MouseEvent lastPressEvent;
    private double process = 0D;
    private static final BufferedImage WIDGET_INSTALLED_ICON = IOUtils.readImage("/com/fr/base/images/share/widget_installed.png");
    private static final BufferedImage WIDGET_DOWNLOAD_ICON = IOUtils.readImage("/com/fr/base/images/share/download.png");
    private static final BufferedImage WIDGET_DOWNLOADING_ICON = IOUtils.readImage("/com/fr/base/images/share/downloading.png");

    public OnlineWidgetBlock(OnlineShareWidget widget, OnlineWidgetSelectPane parentPane) {
        super(widget, parentPane);
        this.add(createSouthPane(widget), BorderLayout.SOUTH);
        new DragAndDropDragGestureListener(this, DnDConstants.ACTION_COPY_OR_MOVE);
    }

    protected JPanel createSouthPane(OnlineShareWidget widget) {
        JPanel southPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel label = new UILabel(widget.getName(), UILabel.CENTER);
        label.setToolTipText(widget.getName());
        label.setHorizontalTextPosition(SwingConstants.LEFT);
        southPane.add(label, BorderLayout.CENTER);
        UILabel emptyLabel = new UILabel();
        emptyLabel.setPreferredSize(new Dimension(25, 20));
        southPane.add(emptyLabel, BorderLayout.EAST);
        southPane.setBackground(Color.WHITE);
        return southPane;
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        super.mouseEntered(e);
        this.isMouseEnter = true;
        this.repaint();
    }

    @Override
    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        this.isMouseEnter = false;
        this.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        this.lastPressEvent = e;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (!checkWidgetInstalled() && getDownloadIconRec().contains(e.getX(), e.getY())) {
            downLoadWidget();
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (DesignModeContext.isAuthorityEditing() || !checkWidgetInstalled()) {
            return;
        }
        if (lastPressEvent == null) {
            return;
        }
        ComponentCollector.getInstance().collectPopupJump();
        Object source = e.getSource();
        Widget creatorSource;
        String shareId;
        if (source instanceof OnlineWidgetBlock) {
            OnlineWidgetBlock no = (OnlineWidgetBlock) e.getSource();
            if (no == null) {
                return;
            }
            shareId = widget.getUuid();
            creatorSource = ShareUtils.getElCaseEditorById(shareId);
            if (creatorSource == null) {
                ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Drag_Error_Info"));
                return;
            }
            creatorSource.setWidgetID(UUID.randomUUID().toString());
            ((AbstractBorderStyleWidget) creatorSource).addWidgetAttrMark(new SharableAttrMark(true));
            SharableWidgetProvider bindInfo = ShareUtils.getElCaseBindInfoById(shareId);
            //tab布局WCardMainBorderLayout通过反射出来的大小是960*480
            XCreator xCreator = ShareComponentUtils.createXCreator(creatorSource, shareId, bindInfo);
            WidgetToolBarPane.getTarget().startDraggingBean(xCreator);
            lastPressEvent = null;
            this.setBorder(null);
        }
    }

    private String createLocalReuFilename() {
        String filename = widget.getFileLoca();
        if (StringUtils.isEmpty(filename) || !filename.endsWith(".reu")) {
            filename = widget.getName() + "." + widget.getUuid() + ".reu";
        }
        return filename;
    }

    private void downLoadWidget() {
        if (OnlineWidgetRepoPane.getInstance().isShowPackagePanel()) {
            ComponentCollector.getInstance().collectDownloadPktNum();
        }
        final WidgetDownloadProcess process = new WidgetDownloadProcess();
        downloading = true;
        process.process(0.0D);
        String userName = DesignerEnvManager.getEnvManager().getDesignerLoginUsername();
        if (StringUtils.isEmpty(userName)) {
            DesignerLoginHelper.showLoginDialog(DesignerLoginSource.NORMAL);
            downloading = false;
            return;
        }

        new SwingWorker<Boolean, Void>() {

            @Override
            protected Boolean doInBackground() {
                String filePath;
                try {
                    String filename = createLocalReuFilename();
                    filePath = DownloadUtils.download(widget.getId(), filename, process);
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    return false;
                }
                ShareComponentUtils.checkReadMe();
                //安装
                File file = new File(filePath);
                try {
                    if (file.exists() && getDefaultGroup().installUniqueIdModule(file)) {
                        ShareUtils.recordInstallTime(file.getName(), System.currentTimeMillis());
                        ComponentCollector.getInstance().collectCmpDownLoad(widget.getUuid());
                    }
                } catch (IOException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                } finally {
                    //删掉下载组件的目录
                    StableUtils.deleteFile(file);
                }
                return true;

            }

            @Override
            protected void done() {
                downloading = false;
                OnlineWidgetBlock.this.process = 0.0D;
                try {
                    if (get()) {
                        LocalWidgetRepoPane.getInstance().refreshShowPanel();
                    } else {
                        ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Download_Failed"));
                    }
                } catch (InterruptedException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                    Thread.currentThread().interrupt();
                } catch (ExecutionException e) {
                    FineLoggerFactory.getLogger().error(e.getMessage(), e);
                }
            }
        }.execute();


    }


    @Override
    public void mouseMoved(MouseEvent e) {
        super.mouseMoved(e);
        if (checkWidgetInstalled()) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        } else if (getDownloadIconRec().contains(e.getX(), e.getY())) {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        } else {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    private Rectangle getDownloadIconRec() {
        return new Rectangle(ShareComponentConstants.SHARE_THUMB_WIDTH / 2 - 12, ShareComponentConstants.SHARE_BLOCK_HEIGHT / 2 - 16, 24, 24);
    }

    private boolean checkWidgetInstalled() {
        return ShareUtils.getElCaseBindInfoById(widget.getUuid()) != null;
    }

    private Group getDefaultGroup() {
        return DefaultShareGroupManager.getInstance().getGroup(DefaultShareGroup.GROUP_NAME);
    }

    public void paint(Graphics g) {
        super.paint(g);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_LCD_HRGB);
        int x = 0;
        int y = 0;
        int w = getWidth();
        int h = getHeight();
        if (process == 1 || checkWidgetInstalled()) {
            Graphics2D g2d = (Graphics2D) g;
            g2d.drawImage(
                    WIDGET_INSTALLED_ICON,
                    w - 20,
                    h - 20,
                    WIDGET_INSTALLED_ICON.getWidth(),
                    WIDGET_INSTALLED_ICON.getHeight(),
                    null,
                    this
            );
            return;
        }
        //如果鼠标移动到布局内且布局不可编辑，画出编辑蒙层
        if (isMouseEnter || downloading) {
            Graphics2D g2d = (Graphics2D) g;
            Composite oldComposite = g2d.getComposite();
            //画白色的编辑层
            g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 20 / 100.0F));
            g2d.setColor(COVER_COLOR);
            g2d.fillRect(x, y, w, h);
            g2d.setComposite(oldComposite);
            //画编辑按钮图标
            BufferedImage image = (process > 0 || downloading) ? WIDGET_DOWNLOADING_ICON : WIDGET_DOWNLOAD_ICON;
            g2d.drawImage(
                    image,
                    (x + w / 2 - 12),
                    (y + h / 2 - 16),
                    image.getWidth(),
                    image.getHeight(),
                    null,
                    this
            );
            Stroke oldStroke = g2d.getStroke();
            g2d.setStroke(XCreatorConstants.STROKE);
            g2d.setColor(Color.decode("#419BF9"));
            double arcAngle = downloading ? (36 + 360 * 0.9 * process) : 0.0;
            g2d.drawArc(x + w / 2 - 12, y + h / 2 - 16, 24, 24, 90, -(int) arcAngle);
            g2d.setColor(Color.WHITE);
            g2d.setStroke(oldStroke);
        }
    }


    class WidgetDownloadProcess implements com.fr.design.extra.Process<Double> {

        @Override
        public void process(Double aDouble) {
            OnlineWidgetBlock.this.process = aDouble;
            final Dimension dimension = OnlineWidgetBlock.this.getSize();
            UIUtil.invokeAndWaitIfNeeded(() -> OnlineWidgetBlock.this.paintImmediately(0, 0, dimension.width, dimension.height));
        }
    }

}
