package com.fr.design.mainframe.widget.ui;

import com.fr.design.dialog.AttrScrollPane;
import com.fr.design.dialog.BasicScrollPane;
import com.fr.design.gui.frpane.AbstractAttrNoScrollPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.FormDesigner;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import java.awt.BorderLayout;

/**
 * Created by ibm on 2017/7/25.
 */
public class FormWidgetCardPane extends AbstractAttrNoScrollPane {
    protected FormDesigner designer;
    protected JPanel content;

    public FormWidgetCardPane(FormDesigner designer) {
        this.designer = designer;
        this.initLayout();
        this.initScrollPane();
        this.initPropertyPane();
    }

    public void initPropertyPane() {

    }

    @Override
    protected JPanel createContentPane() {
        return null;
    }

    private void initScrollPane() {
        final JPanel jPanel = this.createContentPaneInScrollPane();
        BasicScrollPane basicScrollPane = new AttrScrollPane() {
            private static final long serialVersionUID = 3117877968639659022L;

            @Override
            protected JPanel createContentPane() {
                return jPanel;
            }
        };
        this.add(basicScrollPane, BorderLayout.CENTER);
        this.content = jPanel;
    }

    private void initLayout() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        // 跟上方tab标题“属性”那一栏间距10
        this.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
    }

    protected JPanel createContentPaneInScrollPane() {
        return FRGUIPaneFactory.createBorderLayout_S_Pane();
    }

    public void populate() {

    }
}
