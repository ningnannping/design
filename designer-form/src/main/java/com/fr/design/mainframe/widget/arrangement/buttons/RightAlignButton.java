package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.general.IOUtils;

import javax.swing.Icon;

public class RightAlignButton extends AbstractMultiSelectionArrangementButton {
    private static final long serialVersionUID = -8698936349956288409L;

    public RightAlignButton(MultiSelectionArrangement arrangement) {
        super(arrangement);
    }

    @Override
    public Icon getIcon() {
        return IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_right_align.png");
    }

    @Override
    public String getTipText() {
        return Toolkit.i18nText("Fine-Design_Multi_Selection_Right_Align");
    }

    @Override
    public ArrangementType getArrangementType() {
        return ArrangementType.RIGHT_ALIGN;
    }
}
