package com.fr.design.mainframe.share.Bean;

import com.fr.design.mainframe.share.constants.ComponentTypes;
import com.fr.form.share.bean.WidgetDeviceBean;
import com.fr.form.share.bean.WidgetTypeBean;
import com.fr.design.mainframe.share.constants.DisplayDevice;
import com.fr.stable.AssistUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 下拉框与下拉复现框联动状态
 * @author hades
 * @version 10.0
 * Created by hades on 2020/6/9
 */
public class LinkageChangeBean {

    private static final Map<LinkageChangeBean, List<String>> STATE_MAP  = new HashMap<>();

    static {
        // all or default
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, false), new WidgetTypeBean(false, false)), ComponentTypes.allTypes());
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, true), new WidgetTypeBean(true, true)), ComponentTypes.allTypes());
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, false), new WidgetTypeBean(true, true)), ComponentTypes.allTypes());
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, true), new WidgetTypeBean(false, false)), ComponentTypes.allTypes());

        // pc
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, true), new WidgetTypeBean(false, false)), ComponentTypes.allTypesByDevice(DisplayDevice.PC.getType()));
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, true), new WidgetTypeBean(true, true)), ComponentTypes.allTypesByDevice(DisplayDevice.PC.getType()));

        // mobile
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, false), new WidgetTypeBean(false, false)), ComponentTypes.allTypesByDevice(DisplayDevice.MOBILE.getType()));
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, false), new WidgetTypeBean(true, true)), ComponentTypes.allTypesByDevice(DisplayDevice.MOBILE.getType()));

        // report
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, false), new WidgetTypeBean(true, false)), ComponentTypes.REPORT.types());
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, true), new WidgetTypeBean(true, false)), ComponentTypes.REPORT.types());

        // chart
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, false), new WidgetTypeBean(false, true)), ComponentTypes.CHART.types());
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, true), new WidgetTypeBean(false, true)), ComponentTypes.CHART.types());

        // pc-report
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, true), new WidgetTypeBean(true, false)), ComponentTypes.REPORT.children(DisplayDevice.PC.getType()));

        // pc-chart
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(false, true), new WidgetTypeBean(false, true)), ComponentTypes.CHART.children(DisplayDevice.PC.getType()));

        // mobile-report
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, false), new WidgetTypeBean(true, false)), ComponentTypes.REPORT.children(DisplayDevice.MOBILE.getType()));

        // mobile-chart
        STATE_MAP.put(new LinkageChangeBean(new WidgetDeviceBean(true, false), new WidgetTypeBean(false, true)), ComponentTypes.CHART.children(DisplayDevice.MOBILE.getType()));

    }

    public static List<String> getComboBoxItems(LinkageChangeBean bean) {
        return STATE_MAP.get(bean);
    }

    private LinkageChangeBean(WidgetDeviceBean widgetDeviceBean, WidgetTypeBean widgetTypeBean) {
        this.widgetDeviceBean = widgetDeviceBean;
        this.widgetTypeBean = widgetTypeBean;
    }

    private WidgetTypeBean widgetTypeBean;
    private WidgetDeviceBean widgetDeviceBean;

    public static class Builder {
        private boolean report;
        private boolean chart;
        private boolean mobile;
        private boolean pc;

        public Builder setReport(boolean report) {
            this.report = report;
            return this;
        }

        public Builder setChart(boolean chart) {
            this.chart = chart;
            return this;
        }

        public Builder setMobile(boolean mobile) {
            this.mobile = mobile;
            return this;
        }

        public Builder setPc(boolean pc) {
            this.pc = pc;
            return this;
        }

        public LinkageChangeBean build() {
            return new LinkageChangeBean(new WidgetDeviceBean(this.mobile, this.pc), new WidgetTypeBean(this.report, this.chart));
        }
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof LinkageChangeBean
                && AssistUtils.equals(this.widgetDeviceBean, ((LinkageChangeBean) o).widgetDeviceBean)
                && AssistUtils.equals(this.widgetTypeBean, ((LinkageChangeBean) o).widgetTypeBean);
    }

    @Override
    public int hashCode() {
        return AssistUtils.hashCode(this.widgetDeviceBean, this.widgetTypeBean);
    }
}
