package com.fr.design.mainframe.share.generate.impl;

import com.fr.design.mainframe.share.Bean.ComponentGenerateInfo;
import com.fr.design.mainframe.share.generate.ComponentCreatorProcessor;
import com.fr.design.mainframe.share.generate.ComponentTaskAdaptor;
import com.fr.design.mainframe.share.generate.task.ComponentGenerateComplete;
import com.fr.form.share.bean.ComponentReuBean;

import java.util.concurrent.ExecutionException;

/**
 * created by Harrison on 2020/04/16
 **/
public class ComponentPureGenerator extends AbstractComponentGenerator {

    private ComponentCreatorProcessor creator;

    private ComponentGenerateComplete complete;

    private ComponentPureGenerator(ComponentGenerateInfo info, ComponentCreatorProcessor creator, ComponentGenerateComplete complete) {

        super(info, creator, complete);
        this.creator = creator;
        this.complete = complete;
    }

    public static ComponentPureGenerator create(ComponentGenerateInfo info) {

        ComponentCreatorProcessor creator = getComponentCreator();
        ComponentGenerateComplete complete = new ComponentGenerateComplete();
        return new ComponentPureGenerator(info, creator, complete);
    }

    @Override
    protected boolean generate0() throws InterruptedException, ExecutionException {

        final ComponentGenerateInfo info = getInfo();
        ComponentTaskAdaptor<ComponentReuBean> createTest = new ComponentTaskAdaptor<ComponentReuBean>(1.0, creator.getLoadingText()) {
            @Override
            public ComponentReuBean execute() throws Exception {
                return creator.create(info.getJt(), info.getParaMap(), info.getWidget(), info.getInfo());
            }
        };
        ComponentReuBean bean = execute(createTest);

        ComponentTaskAdaptor<Void> completeTask = new ComponentTaskAdaptor<Void>(1.0, complete.getLoadingText()) {
            @Override
            public Void execute() throws Exception {
                complete.execute();
                return null;
            }
        };
        Void result = execute(completeTask);
        return true;
    }

}
