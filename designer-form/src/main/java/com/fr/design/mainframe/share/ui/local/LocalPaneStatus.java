package com.fr.design.mainframe.share.ui.local;

import com.fr.base.BaseUtils;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.share.ui.base.LoadingPane;
import com.fr.design.mainframe.share.ui.base.NoMatchPane;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/1
 */
enum LocalPaneStatus {

    //无匹配
    NO_MATCH {
        @Override
        public JPanel getPanel() {
            return new NoMatchPane();
        }
    },

    //正常
    NORMAL {
        @Override
        public JPanel getPanel() {
            return new JPanel();
        }
    },

    //空
    EMPTY {
        @Override
        public JPanel getPanel() {
            JPanel panel = FRGUIPaneFactory.createBorderLayout_S_Pane();
            UILabel picLabel = new UILabel();
            picLabel.setIcon(BaseUtils.readIcon("com/fr/base/images/share/empty.png"));
            picLabel.setHorizontalAlignment(SwingConstants.CENTER);

            panel.add(picLabel, BorderLayout.CENTER);

            JPanel labelPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
            UILabel topLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Empty_First"), SwingConstants.CENTER);
            topLabel.setForeground(Color.GRAY);
            UILabel bottomLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Empty_Second"), SwingConstants.CENTER);
            bottomLabel.setForeground(Color.GRAY);
            labelPanel.add(topLabel, BorderLayout.CENTER);
            labelPanel.add(bottomLabel, BorderLayout.SOUTH);
            labelPanel.setPreferredSize(new Dimension(240, 40));
            panel.add(labelPanel, BorderLayout.SOUTH);

            panel.setPreferredSize(new Dimension(240, 180));


            JPanel emptyPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
            emptyPanel.setBorder(BorderFactory.createEmptyBorder(100, 0, 0, 0));
            emptyPanel.add(panel, BorderLayout.NORTH);
            return emptyPanel;
        }
    },

    //加载
    LOADING {
        @Override
        public JPanel getPanel() {
            return new LoadingPane();
        }
    },

    //安装
    INSTALLING {
        @Override
        public JPanel getPanel() {
            return new LoadingPane(Toolkit.i18nText("Fine-Design_Share_installing"));
        }
    };

    public abstract JPanel getPanel();
}
