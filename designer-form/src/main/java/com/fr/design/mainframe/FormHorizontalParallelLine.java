package com.fr.design.mainframe;

import java.awt.Point;

public class FormHorizontalParallelLine extends AbstractFormParallelLine {
    public FormHorizontalParallelLine(int parallelValue, int startPosition, int endPosition) {
        super(parallelValue, startPosition, endPosition);
    }

    @Override
    public Point getStartPointOnVerticalCenterLine() {
        Point point = new Point();
        point.setLocation(getCenterPosition(), parallelValue);
        return point;
    }

    @Override
    public Point getEndPointOnVerticalCenterLine(int parallelValue) {
        Point point = new Point();
        point.setLocation(getCenterPosition(), parallelValue);
        return point;
    }

    @Override
    public Point getExtendedLineStartPoint(AbstractFormParallelLine parallelLine) {
        Point point = new Point();
        if (parallelLine.isVerticalCenterLineBeforeTheParallelLine(this)) {
            point.setLocation(getStartPosition(), getParallelValue());
        } else if (parallelLine.isVerticalCenterLineBehindTheParallelLine(this)) {
            point.setLocation(getEndPosition(), getParallelValue());
        }
        return point;
    }

    @Override
    public Point getExtendedLineEndPoint(AbstractFormParallelLine parallelLine) {
        Point point = new Point();
        if (parallelLine.isVerticalCenterLineBeforeTheParallelLine(this)) {
            point.setLocation(parallelLine.getStartPosition(), getParallelValue());
        } else if (parallelLine.isVerticalCenterLineBehindTheParallelLine(this)) {
            point.setLocation(parallelLine.getEndPosition(), getParallelValue());
        }
        return point;
    }
}
