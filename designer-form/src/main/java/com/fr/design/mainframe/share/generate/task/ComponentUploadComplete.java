package com.fr.design.mainframe.share.generate.task;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.share.generate.ComponentBanner;

/**
 * created by Harrison on 2020/06/16
 **/
public class ComponentUploadComplete implements ComponentBanner {
    
    public boolean execute() throws InterruptedException {
        return true;
    }
    
    @Override
    public String getLoadingText() {
        return Toolkit.i18nText("Fine-Design_Share_Upload_Success");
    }
}
