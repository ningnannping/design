package com.fr.design.mainframe.share.ui.local;

import com.fr.design.mainframe.share.AbstractWidgetSelectPane;
import com.fr.design.mainframe.share.ui.block.LocalWidgetBlock;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.Group;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.general.ComparatorUtils;
import com.fr.stable.ArrayUtils;

import javax.swing.BorderFactory;
import java.awt.AWTEvent;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.event.AWTEventListener;
import java.awt.event.MouseEvent;

/**
 * created by Harrison on 2020/03/23
 **/
public class LocalWidgetSelectPane extends AbstractWidgetSelectPane {
    private LocalWidgetBlock selectedBlock;
    private final Group group;

    public LocalWidgetSelectPane(Group group, SharableWidgetProvider[] providers, boolean isEdit) {
        this.group = group;
        // 表示Popup弹窗是否能被点击到
        AWTEventListener awtEventListener = new AWTEventListener() {
            @Override
            public void eventDispatched(AWTEvent event) {
                if (event instanceof MouseEvent) {
                    MouseEvent mv = (MouseEvent) event;
                    if (isCanCancelSelect(mv, selectedBlock)) {
                        LocalWidgetSelectPane.this.selectedBlock = null;
                        LocalWidgetSelectPane.this.repaint();
                    }
                }

            }

            private boolean isCanCancelSelect(MouseEvent mv, LocalWidgetBlock selectedBlock) {
                if (mv.getClickCount() < 1) {
                    return false;
                }
                // 表示Popup弹窗是否能被点击到
                boolean clickFlag = false;
                if (selectedBlock != null) {
                    if (!selectedBlock.isShowing()) {
                        return false;
                    }
                    Point p = selectedBlock.getLocationOnScreen();
                    Point clickPoint = mv.getLocationOnScreen();
                    clickFlag = clickPoint.getX() >= p.getX()
                            && clickPoint.getY() >= p.getY()
                            && clickPoint.getX() <= selectedBlock.getWidth() + p.getX()
                            && clickPoint.getY() <= p.getY() + selectedBlock.getHeight();

                }
                return selectedBlock != null && !clickFlag && !ComparatorUtils.equals(mv.getSource(), selectedBlock);
            }
        };

        java.awt.Toolkit.getDefaultToolkit().addAWTEventListener(awtEventListener, AWTEvent.MOUSE_EVENT_MASK);

        // 设置面板的边框 ，距离上、左、下、右 的距离
        this.setBorder(BorderFactory.createEmptyBorder(0, 5, 0, 0));
        if (ArrayUtils.isNotEmpty(providers)) {
            showComponent(providers, isEdit);
        } else {
            showEmpty();
        }
    }

    public Group getGroup() {
        return group;
    }

    private void showComponent(SharableWidgetProvider[] providers, boolean isEdit) {

        int rowCount = (providers.length + 1) / 2;
        int vgap = 10;
        this.setLayout(new FlowLayout(FlowLayout.LEFT, 5, vgap));
        for (SharableWidgetProvider provider : providers) {
            LocalWidgetBlock widgetButton = new LocalWidgetBlock((DefaultSharableWidget) provider, this);
            widgetButton.setElementCaseEdit(isEdit);
            this.add(widgetButton);
        }
        this.setPreferredSize(new Dimension(240, rowCount * (ShareComponentConstants.SHARE_BLOCK_HEIGHT + vgap)));
    }

    private void showEmpty() {
        this.setPreferredSize(new Dimension(240, 0));
    }

    public void setSelectBlock(LocalWidgetBlock block) {
        this.selectedBlock = block;
        this.repaint();
    }

    public LocalWidgetBlock getSelectedBlock() {
        return selectedBlock;
    }

    @Override
    protected Container getParentContainer() {
        return this.getParent().getParent();
    }
}
