package com.fr.design.mainframe.share.util;

import com.fr.design.mainframe.reuse.SnapChatKeys;
import com.fr.design.notification.SnapChat;
import com.fr.design.notification.SnapChatFactory;
import com.fr.form.share.constants.ComponentPath;
import com.fr.form.share.group.filter.DirFilter;
import com.fr.form.share.group.filter.ReuFilter;
import com.fr.io.utils.ResourceIOUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StableUtils;
import com.fr.workspace.WorkContext;

import java.io.InputStream;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/4
 */
public class InstallComponentHelper {

    private static final String PRE_INSTALL_PATH = "/com/fr/form/share/components";
    private static final String[] PRE_INSTALL_COMPONENTS = new String[]{
            "单行指标卡.f3df58b3-4302-4cab-ab77-caaf225de60a.reu",
            "分层雷达图-深色.49f8397c-e6a6-482a-acc7-46d8cec353a4.reu",
            "红绿灯表格-浅色.d0466992-328a-4ccf-ad67-6cbc844d669c.reu",
            "进度表格-深色.de4141ce-5c25-4506-9424-f5aa15fbf6d0.reu",
            "三列指标卡.61a83d18-a162-4dc3-aa57-3b954edaf82e.reu",
            "透明按钮切换图表.e373e13a-3da0-4c29-91bc-9ae804241023.reu"
    };

    public static void installPreComponent() {
        if (needPreInstallComponent()) {
            for (String componentPath : PRE_INSTALL_COMPONENTS) {
                try {
                    InputStream inputStream = InstallComponentHelper.class.getResourceAsStream(StableUtils.pathJoin(PRE_INSTALL_PATH, componentPath));
                    byte[] data = ResourceIOUtils.inputStream2Bytes(inputStream);
                    WorkContext.getWorkResource().write(StableUtils.pathJoin(ComponentPath.SHARE_PATH.path(), componentPath), data);
                } catch (Exception e) {
                    FineLoggerFactory.getLogger().error("install Component filed" + e.getMessage(), e);
                }
            }
        }

    }

    private static boolean needPreInstallComponent() {
        try {
            //老用户或者组件库里已有组件,不预装组件
            SnapChat snapChat = SnapChatFactory.createSnapChat(false, SnapChatKeys.COMPONENT);
            return !snapChat.hasRead() && !hasComponentInstalled();
        } catch (Throwable e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
        }
        return false;
    }

    /**
     * 判断是否已有组件被安装
     */
    private static boolean hasComponentInstalled() {
        String sharePath = ComponentPath.SHARE_PATH.path();
        String[] components = WorkContext.getWorkResource().list(sharePath, new ReuFilter());
        String[] dirs = WorkContext.getWorkResource().list(sharePath, new DirFilter());
        return components != null && ArrayUtils.isNotEmpty(components)
                || (dirs != null && ArrayUtils.isNotEmpty(dirs));
    }
}
