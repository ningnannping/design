package com.fr.design.mainframe.share.ui.block;

import com.fr.base.FRContext;
import com.fr.base.GraphHelper;
import com.fr.base.iofile.attr.SharableAttrMark;
import com.fr.design.actions.UpdateAction;
import com.fr.design.base.mode.DesignModeContext;
import com.fr.design.constants.UIConstants;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.dialog.FineJOptionPane;
import com.fr.design.form.util.XCreatorConstants;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.imenu.UIPopupMenu;
import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.DesignerContext;
import com.fr.design.mainframe.WidgetToolBarPane;
import com.fr.design.mainframe.share.collect.ComponentCollector;
import com.fr.design.mainframe.share.group.ui.GroupMoveDialog;
import com.fr.design.mainframe.share.ui.base.PopupMenuItem;
import com.fr.design.mainframe.share.ui.local.LocalWidgetRepoPane;
import com.fr.design.mainframe.share.ui.local.LocalWidgetSelectPane;
import com.fr.design.mainframe.share.ui.local.WidgetSelectedManager;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.design.mainframe.share.util.ShareUIUtils;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.form.share.DefaultSharableWidget;
import com.fr.form.share.SharableWidgetProvider;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.form.share.Group;
import com.fr.form.share.record.ShareWidgetInfoManager;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.Widget;
import com.fr.general.ComparatorUtils;
import com.fr.general.IOUtils;
import com.fr.stable.Constants;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.dnd.DnDConstants;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.util.UUID;

/**
 * created by Harrison on 2020/06/12
 * 本地组件块
 **/
public class LocalWidgetBlock extends PreviewWidgetBlock<DefaultSharableWidget> {
    private static final int MARK_START_X = 83;

    private boolean mouseHover = false;
    private final LocalWidgetSelectPane parentPane;

    private MouseEvent lastPressEvent;
    private boolean isEdit;
    private boolean isMarked;
    private boolean pressed;
    private boolean hover;
    private final Icon markedMode = IOUtils.readIcon("/com/fr/base/images/share/marked.png");
    private final Icon unMarkedMode = IOUtils.readIcon("/com/fr/base/images/share/unmarked.png");
    private final Icon incompatibleMarker = IOUtils.readIcon("/com/fr/base/images/share/marker_incompatible.png");

    public LocalWidgetBlock(DefaultSharableWidget provider, LocalWidgetSelectPane parentPane) {
        super(provider);
        this.parentPane = parentPane;
        new DragAndDropDragGestureListener(this, DnDConstants.ACTION_COPY_OR_MOVE);
        initUI();
    }


    private void initUI() {
        JPanel labelPane = new JPanel(new BorderLayout());
        UILabel label = new UILabel(this.getBindInfo().getName(), UILabel.CENTER);
        label.setPreferredSize(new Dimension(ShareComponentConstants.SHARE_THUMB_WIDTH, ShareComponentConstants.SHARE_BLOCK_LABEL_HEIGHT));
        labelPane.setBackground(Color.WHITE);
        labelPane.add(label, BorderLayout.CENTER);
        this.add(labelPane, BorderLayout.SOUTH);
    }

    public void setElementCaseEdit(boolean isEdit) {
        this.isEdit = isEdit;
        if (isEdit) {
            isMarked = WidgetSelectedManager.getInstance().isSelected(getGroup().getGroupName(), this.getWidget().getId());
        }
        repaint();
    }

    public String getFileName() {
        //插件里面， 所有的值都是插件值。可以强转的。
        DefaultSharableWidget provider = (DefaultSharableWidget) getBindInfo();
        return provider.getFileName();
    }

    public SharableWidgetProvider getBindInfo() {
        return this.getWidget();
    }

    @Override
    protected void showPreview(DefaultSharableWidget widget) {
        this.parentPane.showPreviewPane(this, widget.getId());
    }

    @Override
    protected void hidePreview() {
        this.parentPane.hidePreviewPane();
    }

    @Override
    @NotNull
    public Image getCoverImage() {
        return this.getWidget().getCover();
    }

    @Override
    protected String getWidgetUuid() {
        return widget.getId();
    }

    @Nullable
    public Image getPreviewImage() {
        String id = this.getWidget().getId();
        return getGroup().getPreviewImage(id);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getButton() == MouseEvent.BUTTON3 && !isEdit) {
            this.parentPane.hidePreviewPane();
            UIPopupMenu popupMenu = new UIPopupMenu();
            popupMenu.setOnlyText(true);
            popupMenu.setBackground(UIConstants.DEFAULT_BG_RULER);
            popupMenu.add(new PopupMenuItem(new MoveGroupAction()));
            popupMenu.add(new PopupMenuItem(new RemoveAction()));
            GUICoreUtils.showPopupMenu(popupMenu, this, e.getX(), e.getY());
        }

    }

    @Override
    public void mousePressed(MouseEvent e) {
        super.mousePressed(e);
        lastPressEvent = e;
        pressed = true;
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        super.mouseReleased(e);
        if (pressed && hover) {
            dealClickAction(e);
        }
        pressed = false;
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        super.mouseMoved(e);
        this.mouseHover = true;
        if (!isEdit) {
            setCursor(new Cursor(Cursor.MOVE_CURSOR));
        }
        this.repaint();
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        super.mouseEntered(e);
        hover = true;
    }

    @Override
    public void mouseExited(MouseEvent e) {
        super.mouseExited(e);
        this.mouseHover = false;
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        hover = false;
        this.repaint();
    }


    @Override
    public void mouseDragged(MouseEvent e) {
        if (DesignModeContext.isAuthorityEditing() || lastPressEvent == null || isEdit) {
            return;
        }
        hidePreview();
        ComponentCollector.getInstance().collectPopupJump();
        Object source = e.getSource();
        Widget creatorSource;
        String shareId;
        if (source instanceof LocalWidgetBlock) {
            LocalWidgetBlock widgetBlock = (LocalWidgetBlock) e.getSource();
            if (widgetBlock == null) {
                return;
            }
            SharableWidgetProvider widget = widgetBlock.getWidget();
            if (widget == null) {
                return;
            }
            if (!widget.isCompatibleWithCurrentEnv()) {
                FineJOptionPane.showMessageDialog(DesignerContext.getDesignerFrame(),
                        Toolkit.i18nText("Fine-Design_Share_Drag_And_Make_Incompatible_Component_Tip"),
                        Toolkit.i18nText("Fine-Design_Basic_Error"),
                        JOptionPane.ERROR_MESSAGE,
                        UIManager.getIcon("OptionPane.errorIcon")
                );
                return;
            }
            shareId = widgetBlock.getBindInfo().getId();
            creatorSource = getGroup().getElCaseEditorById(shareId);
            if (creatorSource == null) {
                ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Drag_Error_Info"));
                return;
            }
            creatorSource.setWidgetID(UUID.randomUUID().toString());
            ((AbstractBorderStyleWidget) creatorSource).addWidgetAttrMark(new SharableAttrMark(true));
            //tab布局WCardMainBorderLayout通过反射出来的大小是960*480
            XCreator xCreator = ShareComponentUtils.createXCreator(creatorSource, shareId, widgetBlock.getBindInfo());
            WidgetToolBarPane.getTarget().startDraggingBean(xCreator);
            lastPressEvent = null;
            this.setBorder(null);
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2d = (Graphics2D) g;

        boolean isUnusable = !getWidget().isCompatibleWithCurrentEnv();
        if (isUnusable) {
            paintUnusableMask(g2d);
        }

        //绘制删除标志
        if (isEdit) {
            Icon icon = isMarked ? markedMode : unMarkedMode;
            icon.paintIcon(this, g, MARK_START_X, 0);
        }
        if (ComparatorUtils.equals(this, this.parentPane.getSelectedBlock()) || this.mouseHover) {
            g.setColor(XCreatorConstants.FORM_BORDER_COLOR);
            Rectangle rectangle = new Rectangle();
            rectangle.width = this.getWidth();
            rectangle.height = this.getHeight();
            GraphHelper.draw(g, rectangle, Constants.LINE_LARGE);
        }
    }

    protected void paintUnusableMask(Graphics2D g2d) {
        Color oldColor = g2d.getColor();
        Font oldFont = g2d.getFont();

        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Dimension coverDim = getCoverDimension();
        double canvasX = 0;
        double canvasY = 0;
        double canvasW = coverDim.getWidth();
        double canvasH = coverDim.getHeight();

        g2d.setColor(new Color(0.0F, 0.0F, 0.0F, 0.4F));
        GraphHelper.fillRect(g2d, canvasX, canvasY, canvasW, canvasH);

        g2d.setColor(new Color(0.0F, 0.0F, 0.0F, 0.5F));
        GraphHelper.fillRect(g2d, canvasX, canvasH - 16, canvasW, 16);

        String tipText = Toolkit.i18nText("Fine-Design_Share_Incompatible_Version_Tip");
        Font tipFont = FRContext.getDefaultValues().getFRFont().deriveFont(8.0F);
        FontRenderContext frc = g2d.getFontRenderContext();
        double tipTextWidth = GraphHelper.stringWidth(tipText, tipFont, frc);
        LineMetrics metrics = tipFont.getLineMetrics(tipText, frc);
        double tipTextHeight = metrics.getHeight();
        g2d.setColor(Color.WHITE);
        g2d.setFont(tipFont);
        GraphHelper.drawString(g2d, tipText, canvasX + (canvasW - tipTextWidth) / 2.0F, canvasY + canvasH - (16 - tipTextHeight) / 2.0F);

        int markerX = (int) (canvasX + (canvasW - incompatibleMarker.getIconWidth()) / 2);
        int markerY = (int) (canvasY + (canvasH - incompatibleMarker.getIconHeight()) / 2);
        incompatibleMarker.paintIcon(this, g2d, markerX, markerY);

        g2d.setColor(oldColor);
        g2d.setFont(oldFont);
    }

    /**
     * 由鼠标释放时调用该方法来触发左键点击事件
     */
    private void dealClickAction(MouseEvent e) {
        this.parentPane.setSelectBlock(this);
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (isEdit) {
                if (isMarked) {
                    WidgetSelectedManager.getInstance().removeSelect(getGroup().getGroupName(), this.getWidget().getId());
                    isMarked = false;
                } else {
                    WidgetSelectedManager.getInstance().addSelect(getGroup().getGroupName(), this.getWidget().getId());
                    isMarked = true;
                }
            }
            //没有组件被选择则禁用按钮
            LocalWidgetRepoPane.getInstance().setManageButtonEnable(hasComponentSelected());
            repaint();
        }
    }

    private Group getGroup() {
        return parentPane.getGroup();
    }

    private boolean hasComponentSelected() {
        return !WidgetSelectedManager.getInstance().isSelectEmpty();
    }

    private class MoveGroupAction extends UpdateAction {
        public MoveGroupAction() {
            this.putValue(Action.SMALL_ICON, null);
            this.setName(Toolkit.i18nText("Fine-Design_Share_Group_Move"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            new GroupMoveDialog(DesignerContext.getDesignerFrame()) {
                @Override
                protected void confirmClose() {
                    this.dispose();
                    Group group = (Group) getSelectGroupBox().getSelectedItem();
                    assert group != null;
                    if (WidgetSelectedManager.getInstance().moveSelect(getGroup().getGroupName(), group.getGroupName(), getWidget().getId())) {
                        ShareWidgetInfoManager.getInstance().saveXmlInfo();
                        LocalWidgetRepoPane.getInstance().refreshShowPanel(false);
                    } else {
                        ShareUIUtils.showErrorMessageDialog(Toolkit.i18nText("Fine-Design_Share_Group_Move_Fail_Message"));
                    }
                }

            };
        }
    }

    private class RemoveAction extends UpdateAction {
        public RemoveAction() {
            this.putValue(Action.SMALL_ICON, null);
            this.setName(Toolkit.i18nText("Fine-Design_Share_Remove"));
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            int rv = FineJOptionPane.showConfirmDialog(DesignerContext.getDesignerFrame(),
                    Toolkit.i18nText("Fine-Design_Share_Remove_Info"),
                    Toolkit.i18nText("Fine-Design_Share_Group_Confirm"),
                    FineJOptionPane.YES_NO_OPTION
            );
            if (rv == FineJOptionPane.YES_OPTION) {
                if (!WidgetSelectedManager.getInstance().unInstallSelect(getGroup().getGroupName(), getWidget().getId())) {
                    FineJOptionPane.showMessageDialog(null, Toolkit.i18nText("Fine-Design_Share_Remove_Failure"));
                }
                LocalWidgetRepoPane.getInstance().refreshShowPanel(getGroup());
            }
        }
    }

}
