package com.fr.design.mainframe.share.generate;

/**
 * created by Harrison on 2020/04/16
 **/
public interface ComponentGenerator {
    
    /**
     * 生成组件
     *
     * @return 组件
     */
    boolean generate() throws Throwable;
    
    /**
     * 耗时/速率
     *
     * @return 时间
     */
    int speed();

}
