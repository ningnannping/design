package com.fr.design.mainframe.share.encrypt.clipboard.impl;

import com.fr.base.io.BaseBook;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.share.encrypt.clipboard.AbstractCrossClipBoardState;
import com.fr.design.mainframe.share.encrypt.clipboard.RestrictTemplateSet;
import com.fr.form.main.Form;

/**
 * created by Harrison on 2020/05/18
 **/
public class CrossTemplateClipBoardState extends AbstractCrossClipBoardState {
    
    @Override
    protected String currentId() {
        
        JTemplate<?, ?> template = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        BaseBook book = template.getTarget();
        if (book instanceof Form) {
            Form form = (Form) book;
            RestrictTemplateSet.monitorGracefully(form);
        }
        return book.getTemplateID();
    }
    
    @Override
    protected boolean isRestrict(String sourceId) {
        
        return RestrictTemplateSet.isRestrict(sourceId);
    }
}
