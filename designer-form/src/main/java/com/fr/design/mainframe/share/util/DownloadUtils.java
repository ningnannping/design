package com.fr.design.mainframe.share.util;

import com.fr.design.DesignerEnvManager;
import com.fr.design.extra.PluginConstants;
import com.fr.form.share.base.CancelCheck;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.ftp.util.Base64;
import com.fr.general.CloudCenter;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ProductConstants;
import com.fr.stable.StableUtils;
import com.fr.third.org.apache.http.HttpEntity;
import com.fr.third.org.apache.http.HttpException;
import com.fr.third.org.apache.http.HttpStatus;
import com.fr.third.org.apache.http.client.config.CookieSpecs;
import com.fr.third.org.apache.http.client.config.RequestConfig;
import com.fr.third.org.apache.http.client.methods.CloseableHttpResponse;
import com.fr.third.org.apache.http.client.methods.HttpUriRequest;
import com.fr.third.org.apache.http.client.methods.RequestBuilder;
import com.fr.third.org.apache.http.impl.client.BasicCookieStore;
import com.fr.third.org.apache.http.impl.client.CloseableHttpClient;
import com.fr.third.org.apache.http.impl.client.HttpClients;
import org.jetbrains.annotations.NotNull;

import javax.crypto.Cipher;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * created by Harrison on 2020/05/27
 **/
public class DownloadUtils {
    private static final String REUSES_URL = ShareComponentConstants.REU_INFO_PATH + "file/download";
    private static final String PACKAGE_REUSES_URL = ShareComponentConstants.REU_INFO_PATH + "package/download/";
    private static final String CERTIFICATE_PUBLIC_KEY = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCtsz62CPSWXZE/IYZRiAuTSZkw\n" +
            "1WOwer8+JFktK0uKLAUuQoBr+UjAMFtRA8W7JgKMDwZy/2liEAiXEOSPU/hrdV8D\n" +
            "tT541LnGi1X/hXiRwuttPWYN3L2GYm/d5blU+FBNwghBIrdAxXTzYBc6P4KL/oYX\n" +
            "nMdTIrkz8tYkG3QoFQIDAQAB";


    private static CloseableHttpClient createClient() {

        BasicCookieStore cookieStore = new BasicCookieStore();
        return HttpClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                        .setCookieSpec(CookieSpecs.STANDARD).build())
                .setDefaultCookieStore(cookieStore)
                .build();
    }

    @NotNull
    public static String download(String id, String fileName, com.fr.design.extra.Process<Double> process) throws Exception {
        CloseableHttpResponse fileRes = postDownloadHttpResponse(REUSES_URL, id);
        if (fileRes.getStatusLine().getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
            fileRes = getDownloadHttpResponse(fileRes.getHeaders("Location")[0].getValue());
        }
        if (fileRes.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            String realPath = StableUtils.pathJoin(ProductConstants.getEnvHome(), ShareComponentConstants.PLUGIN_CACHE, ShareComponentConstants.DOWNLOAD_SHARE);
            String filePath;

            HttpEntity entity = fileRes.getEntity();
            long totalSize = entity.getContentLength();

            InputStream content = entity.getContent();
            filePath = StableUtils.pathJoin(realPath, fileName);
            StableUtils.makesureFileExist(new File(filePath));
            FileOutputStream writer = new FileOutputStream(filePath);
            byte[] data = new byte[PluginConstants.BYTES_NUM];
            int bytesRead;
            int totalBytesRead = 0;

            while ((bytesRead = content.read(data)) > 0) {
                writer.write(data, 0, bytesRead);
                data = new byte[PluginConstants.BYTES_NUM];
                totalBytesRead += bytesRead;
                process.process(totalBytesRead / (double) totalSize);
            }
            content.close();
            writer.flush();
            writer.close();
            FineLoggerFactory.getLogger().info("download widget{} success", id);
            return filePath;
        } else {
            FineLoggerFactory.getLogger().info("download widget{} failed", id);
            throw new HttpException();
        }
    }

    public static String downloadPackage(String id, String fileName, CancelCheck cancelCheck) throws Exception {

        Map<String, String> params = new HashMap<>();
        params.put("designerVersion", ProductConstants.RELEASE_VERSION);
        CloseableHttpResponse fileRes = postDownloadHttpResponse(PACKAGE_REUSES_URL, id, params);
        if (fileRes.getStatusLine().getStatusCode() == HttpStatus.SC_MOVED_TEMPORARILY) {
            fileRes = getDownloadHttpResponse(fileRes.getHeaders("Location")[0].getValue());
        }
        String realPath = StableUtils.pathJoin(ProductConstants.getEnvHome(), ShareComponentConstants.PLUGIN_CACHE, ShareComponentConstants.DOWNLOAD_PACKAGE_SHARE);
        String filePath;
        if (fileRes.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            HttpEntity entity = fileRes.getEntity();
            filePath = StableUtils.pathJoin(realPath, fileName + ".reus");
            StableUtils.makesureFileExist(new File(filePath));

            InputStream content = entity.getContent();
            FileOutputStream writer = new FileOutputStream(filePath);

            byte[] data = new byte[PluginConstants.BYTES_NUM * 20];
            int bytesRead;

            while (!cancelCheck.isCanceled() && (bytesRead = content.read(data)) > 0) {
                writer.write(data, 0, bytesRead);
                data = new byte[PluginConstants.BYTES_NUM * 20];
            }

            content.close();
            writer.flush();
            writer.close();
            if (cancelCheck.isCanceled()) {
                FineLoggerFactory.getLogger().info("download widget{} canceled", id);
            } else {
                FineLoggerFactory.getLogger().info("download widget{} failed", id);
            }
            return filePath;
        } else {
            FineLoggerFactory.getLogger().info("download widget{} failed", id);
            throw new HttpException();
        }
    }

    private static CloseableHttpResponse postDownloadHttpResponse(String url, String id) throws Exception {
        return postDownloadHttpResponse(url, id, new HashMap<>());
    }

    private static CloseableHttpResponse postDownloadHttpResponse(String url, String id, Map<String, String> params) throws Exception {
        //先登录一下。不然可能失败
        CloseableHttpClient client = createClient();
        FineLoggerFactory.getLogger().info("login fr-market");
        FineLoggerFactory.getLogger().info("start download widget {}", id);
        RequestBuilder builder = RequestBuilder.post()
                .setHeader("User-Agent", "Mozilla/5.0")
                .setUri(url)
                .addParameter("id", encrypt(id))
                .addParameter("userId", String.valueOf(DesignerEnvManager.getEnvManager().getDesignerLoginUid()));

        if (params != null) {
            Set<String> keys =  params.keySet();
            for (String key: keys) {
                builder.addParameter(key, params.get(key));
            }
        }

        return client.execute(builder.build());
    }


    private static CloseableHttpResponse getDownloadHttpResponse(String url) throws Exception {
        //先登录一下。不然可能失败
        CloseableHttpClient client = createClient();
        HttpUriRequest file = RequestBuilder.get()
                .setUri(url)
                .build();
        return client.execute(file);
    }


    private static String encrypt(String str) throws Exception {
        //base64编码的公钥
        byte[] decoded = Base64.decodeBase64(CERTIFICATE_PUBLIC_KEY);
        RSAPublicKey pubKey = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(decoded));
        //RSA加密
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, pubKey);
        String outStr = Base64.encodeBase64String(cipher.doFinal(str.getBytes(StandardCharsets.UTF_8)));
        return outStr;
    }


}
