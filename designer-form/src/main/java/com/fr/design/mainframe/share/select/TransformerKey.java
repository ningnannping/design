package com.fr.design.mainframe.share.select;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.mainframe.FormSelection;
import com.fr.form.ui.Widget;
import com.fr.general.ComparatorUtils;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.AssistUtils;
import org.jetbrains.annotations.Nullable;

import java.awt.Rectangle;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

/**
 * created by Harrison on 2020/06/11
 **/
public class TransformerKey {

    private Rectangle selectionBounds;

    private Widget[] widgets;

    public TransformerKey(FormSelection selection) {
        this.selectionBounds = selection.getSelctionBounds();
        this.widgets = deepData(selection.getSelectedCreators());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransformerKey that = (TransformerKey) o;
        return Objects.equals(selectionBounds, that.selectionBounds) &&
                Arrays.equals(widgets, that.widgets);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(selectionBounds);
        result = 31 * result
                + Arrays.hashCode(widgets);
        return result;
    }

    private Widget[] deepData(XCreator[] xCreators) {
        if (xCreators == null) {
            return new UniqueWidgetWrapper[0];
        }
        Widget[] widgets = new Widget[xCreators.length];
        int i = 0;
        for (XCreator xCreator : xCreators) {
            widgets[i++] = new UniqueWidgetWrapper(cloneWidget(xCreator));
        }
        return widgets;
    }

    @Nullable
    private Widget cloneWidget(XCreator xCreator) {

        try {
            return xCreator.toData() != null ? (Widget) xCreator.toData().clone() : null;
        } catch (CloneNotSupportedException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return null;
        }
    }

    private static class UniqueWidgetWrapper extends Widget {
        private String uuid;
        private Widget widget;

        public UniqueWidgetWrapper(Widget widget) {
            this.widget = widget;
            this.uuid = UUID.randomUUID().toString();
        }

        @Override
        public String getXType() {
            return "UniqueWidgetWrapper";
        }

        @Override
        public boolean isEditor() {
            return false;
        }

        @Override
        public String[] supportedEvents() {
            return new String[0];
        }

        /**
         * hash码
         *
         * @return 返回int
         */
        @Override
        public int hashCode() {
            return 31 * super.hashCode() + AssistUtils.hashCode(this.uuid, this.widget);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof UniqueWidgetWrapper
                    && super.equals(obj)
                    && ComparatorUtils.equals(((UniqueWidgetWrapper) obj).uuid, this.uuid)
                    && ComparatorUtils.equals(((UniqueWidgetWrapper) obj).widget, this.widget);
        }
    }
}
