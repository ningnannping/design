package com.fr.design.mainframe.share.ui.base;

import com.fr.base.BaseUtils;
import com.fr.design.event.ChangeEvent;
import com.fr.design.event.ChangeListener;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UINumberField;
import com.fr.stable.ArrayUtils;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by kerry on 2020-10-20
 */
public class PagingFiledPane extends JPanel {
    private static final Dimension PAGING_BTN_SIZE = new Dimension(20, 20);
    private int currentPageNum = 1;
    private int totalPageNum;
    private int numPerPage;

    private ChangeListener changeListener;

    private UIButton lastPageBtn;
    private UIButton nextPageBtn;
    private UINumberField pagingEditField;

    public PagingFiledPane(int totalItems, int numPerPage) {
        this.setOpaque(false);
        this.numPerPage = numPerPage;
        this.totalPageNum = totalItems / numPerPage + ((totalItems % numPerPage) == 0 ? 0 : 1);
        this.totalPageNum = this.totalPageNum > 0 ? this.totalPageNum : 1;
        initPane(totalPageNum);
    }

    private void initPane(int totalPageNum) {

        this.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 15));
        initLastPageBtn();

        JSeparator jSeparator1 = new JSeparator();

        pagingEditField = new UINumberField();
        registerPagingEditFieldListener();
        pagingEditField.canFillNegativeNumber(false);
        pagingEditField.setMinValue(1);
        pagingEditField.setMaxValue(totalPageNum);
        pagingEditField.setPreferredSize(new Dimension(50, 20));

        UILabel totalPageLabel = new UILabel("/" + totalPageNum);

        JSeparator jSeparator2 = new JSeparator();

        initNextPageBtn();

        checkPageStatus();

        this.add(lastPageBtn);
        this.add(jSeparator1);
        this.add(pagingEditField);
        this.add(totalPageLabel);
        this.add(jSeparator2);
        this.add(nextPageBtn);
    }

    public void setEnable(boolean enable) {
        lastPageBtn.setEnabled(enable);
        nextPageBtn.setEnabled(enable);
        pagingEditField.setEnabled(enable);
    }

    private void initLastPageBtn() {
        lastPageBtn = new UIButton(BaseUtils.readIcon("/com/fr/base/images/share/left_page_normal.png"));
        lastPageBtn.setRolloverEnabled(true);
        lastPageBtn.setFocusPainted(false);
        lastPageBtn.setContentAreaFilled(true);
        lastPageBtn.setMargin(new Insets(0, 0, 0, 0));
        lastPageBtn.setPressedIcon(BaseUtils.readIcon("/com/fr/base/images/share/left_page_click.png"));
        lastPageBtn.setRolloverIcon(BaseUtils.readIcon("/com/fr/base/images/share/left_page_hover.png"));
        lastPageBtn.setDisabledIcon(BaseUtils.readIcon("/com/fr/base/images/share/left_page_disable.png"));
        lastPageBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clickLastPage();
            }
        });
        lastPageBtn.setPreferredSize(PAGING_BTN_SIZE);

    }

    private void initNextPageBtn() {
        nextPageBtn = new UIButton(BaseUtils.readIcon("/com/fr/base/images/share/right_page_normal.png"));
        nextPageBtn.setRolloverEnabled(true);
        nextPageBtn.setFocusPainted(false);
        nextPageBtn.setContentAreaFilled(true);
        nextPageBtn.setMargin(new Insets(0, 0, 0, 0));
        nextPageBtn.setPressedIcon(BaseUtils.readIcon("/com/fr/base/images/share/right_page_click.png"));
        nextPageBtn.setRolloverIcon(BaseUtils.readIcon("/com/fr/base/images/share/right_page_hover.png"));
        nextPageBtn.setDisabledIcon(BaseUtils.readIcon("/com/fr/base/images/share/right_page_disable.png"));
        nextPageBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clickNextPage();
            }
        });
        nextPageBtn.setPreferredSize(PAGING_BTN_SIZE);

    }

    private void registerPagingEditFieldListener() {
        pagingEditField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent evt) {
                int code = evt.getKeyCode();
                if (code == KeyEvent.VK_ENTER) {
                    jumpPage((int) pagingEditField.getValue());
                }
            }
        });
        pagingEditField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                jumpPage((int) pagingEditField.getValue());
            }
        });
    }

    public int getCurrentPageNum() {
        return currentPageNum;
    }

    public void registerChangeListener(ChangeListener changeListener) {
        this.changeListener = changeListener;
    }

    private void checkPageStatus() {
        lastPageBtn.setEnabled(currentPageNum > 1);
        nextPageBtn.setEnabled(currentPageNum < totalPageNum);
        pagingEditField.setText(String.valueOf(currentPageNum));
        if (changeListener != null) {
            changeListener.fireChanged(new ChangeEvent(currentPageNum));
        }
    }

    private void clickNextPage() {
        if (currentPageNum < totalPageNum) {
            currentPageNum++;
            checkPageStatus();
        }
    }

    private void clickLastPage() {
        if (currentPageNum > 1) {
            currentPageNum--;
            checkPageStatus();
        }
    }

    private void jumpPage(int pageNum) {
        if (pageNum > 0 && pageNum <= totalPageNum) {
            currentPageNum = pageNum;
            checkPageStatus();
        }
    }

    public <T> T[] getShowItems(T[] items) {
        int startIndex = Math.max(0, currentPageNum - 1);
        T[] resultArr = ArrayUtils.subarray(items, startIndex * this.numPerPage, this.currentPageNum * this.numPerPage);
        return resultArr;
    }


    /**
     * 测试程序
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame("");
        frame.setSize(400, 320);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        frame.setLocation((d.width - frame.getSize().width) / 2, (d.height - frame.getSize().height) / 2);
        PagingFiledPane tt = new PagingFiledPane(10, 3);
        frame.getContentPane().add(tt);
        frame.setVisible(true);
    }

}
