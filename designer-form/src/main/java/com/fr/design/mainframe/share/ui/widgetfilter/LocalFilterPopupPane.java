package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.form.share.bean.WidgetFilterTypeInfo;
import com.fr.stable.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/11
 */
public class LocalFilterPopupPane extends FilterPopupPane {
    public LocalFilterPopupPane(FilterPane pane, List<WidgetFilterTypeInfo> loadFilterCategories) {
        super(pane, loadFilterCategories);
    }

    @Override
    protected String assembleFilter() {
        LocalWidgetFilter.getInstance().setFilterList(getFilterList() == null ? new ArrayList<>() : getFilterList());
        return StringUtils.EMPTY;
    }
}
