package com.fr.design.mainframe.share.util;

import com.fr.form.share.group.DefaultShareGroup;
import com.fr.form.share.group.DefaultShareGroupManager;
import com.fr.form.share.Group;
import com.fr.form.share.group.TempFileOperator;
import com.fr.form.share.utils.ReuxUtils;
import com.fr.form.share.utils.ShareUtils;
import com.fr.io.repository.base.fs.FileSystemRepository;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.ProductConstants;
import com.fr.stable.StableUtils;
import com.fr.stable.project.ProjectConstants;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/12/21
 */
public class InstallUtils {

    private static final String DOT = ".";

    /**
     * 安装组件包
     */
    public static boolean installReusFile(File chosenFile, long installTime, List<String> list) {
        return installReusFile(chosenFile, installTime, list, o -> {
            //do nothing
        }).installStatus;
    }

    /**
     * 安装组件包
     */
    public static InstallResult installReusFile(File chosenFile, long installTime, List<String> list, com.fr.design.extra.Process<Double> process) {
        Group group = createComponentGroup(chosenFile.getName());
        if (group == null) {
            return new InstallResult(false, null);
        }
        int totalFileNum;
        int installedFileNum = 0;
        try (TempFileOperator tempFilOperator = new TempFileOperator(getUnZipCacheDir())) {
            File[] files = unZipReuxsFile(chosenFile, tempFilOperator.getTempFilePath());
            totalFileNum = files.length;
            boolean installStatus = true;
            for (File file : files) {
                boolean success = installReuFile(group, file, installTime);
                if (!success) {
                    list.add(getFilePrefix(chosenFile) + ": " + getFilePrefix(file));
                }
                installedFileNum++;
                process.process(installedFileNum / (double) totalFileNum);
                installStatus &= success;
            }
            return new InstallResult(installStatus, group);
        } catch (Exception e) {
            process.process(1.0);
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return new InstallResult(false, group);
        }

    }

    /**
     * 安装组件
     */
    public static boolean installReuFile(File chosenFile, long installTime, List<String> list) {
        Group defaultGroup = DefaultShareGroupManager.getInstance().getGroup(DefaultShareGroup.GROUP_NAME);
        if (defaultGroup == null) {
            return false;
        }
        boolean success = installReuFile(defaultGroup, chosenFile, installTime);
        if (!success) {
            list.add(getFilePrefix(chosenFile));
        }
        return success;
    }

    @Nullable
    private static Group createComponentGroup(String fileName) {
        String groupName = createGroupName(fileName);

        //有重名分组则加后缀
        if (DefaultShareGroupManager.getInstance().getGroup(groupName) != null) {
            int suffix = 1;
            while (DefaultShareGroupManager.getInstance().getGroup(groupName + suffix) != null) {
                suffix++;
            }
            groupName = groupName + suffix;
        }

        if (!DefaultShareGroupManager.getInstance().createGroup(groupName)) {
            return null;
        }
        return DefaultShareGroupManager.getInstance().getGroup(groupName);
    }

    private static String createGroupName(String fileName) {
        for (String suffix : ReuxUtils.SUPPORT_REUS_SUFFIX) {
            if (fileName.endsWith(suffix)) {
                return fileName.substring(0, fileName.indexOf(DOT + suffix));
            }
        }
        return fileName;
    }

    private static boolean installReuFile(Group group, File chosenFile, long installTime) {
        try {
            if (!group.installUniqueIdModule(chosenFile)) {
                return false;
            }
            ShareUtils.recordInstallTime(chosenFile.getName(), installTime);
            return true;
        } catch (IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return false;
        }
    }

    private static File[] unZipReuxsFile(File chosenFile, String tempFilePath) throws IOException {
        List<File> files = new ArrayList<>();
        ReuxUtils.unzipRueFile(chosenFile, tempFilePath);
        String[] components = FileSystemRepository.getSingleton().list(tempFilePath, s -> s.endsWith(ProjectConstants.REU));
        for (String component : components) {
            files.add(new File(StableUtils.pathJoin(tempFilePath, component)));
        }
        return files.toArray(new File[files.size()]);
    }

    private static String getUnZipCacheDir() {
        return StableUtils.pathJoin(ProductConstants.getEnvHome(), "plugin_cache", "reu_share_temp");
    }

    private static String getFilePrefix(File file) {
        String fileName = file.getName();
        return fileName.contains(DOT) ? fileName.substring(0, fileName.indexOf(DOT)) : fileName;
    }

    /**
     * 安装的返回结果
     */
    public static class InstallResult {
        public final boolean installStatus;
        public final Group group;

        public InstallResult(boolean installStatus, Group group) {
            this.installStatus = installStatus;
            this.group = group;
        }
    }


}
