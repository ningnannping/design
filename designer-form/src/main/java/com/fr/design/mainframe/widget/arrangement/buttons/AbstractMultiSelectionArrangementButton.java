package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.event.UIObserver;
import com.fr.design.event.UIObserverListener;
import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.mainframe.MultiSelectionArrangement;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class AbstractMultiSelectionArrangementButton extends UIButton implements MultiSelectionArrangementButton, UIObserver {
    private static final long serialVersionUID = -2114423583742242771L;
    protected MultiSelectionArrangement arrangement;
    protected UIObserverListener uiObserverListener;

    public AbstractMultiSelectionArrangementButton(MultiSelectionArrangement arrangement) {
        super();
        this.arrangement = arrangement;
        this.setNormalPainted(false);
        this.setBorderPaintedOnlyWhenPressed(true);
        this.setIcon(getIcon());
        this.setToolTipText(getTipText());
        this.addActionListener(getActionListener());
    }

    @Override
    public void registerChangeListener(UIObserverListener listener) {
        uiObserverListener = listener;
    }

    @Override
    public boolean shouldResponseChangeListener() {
        return true;
    }

    @Override
    public ActionListener getActionListener() {
        return new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                arrangement.doArrangement(getArrangementType());
                uiObserverListener.doChange();
            }
        };
    }
}
