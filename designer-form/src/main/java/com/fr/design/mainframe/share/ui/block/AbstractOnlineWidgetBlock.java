package com.fr.design.mainframe.share.ui.block;

import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.share.ui.online.OnlineResourceManager;
import com.fr.design.mainframe.share.ui.online.OnlineWidgetSelectPane;
import com.fr.design.mainframe.share.ui.online.ResourceLoader;
import com.fr.form.share.bean.OnlineShareWidget;
import com.fr.form.share.constants.ShareComponentConstants;
import com.fr.log.FineLoggerFactory;
import com.fr.stable.EncodeConstants;
import com.fr.third.springframework.web.util.UriUtils;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;

/**
 * Created by kerry on 2020-11-22
 */
public abstract class AbstractOnlineWidgetBlock extends PreviewWidgetBlock<OnlineShareWidget> implements ResourceLoader {

    private final OnlineWidgetSelectPane parentPane;
    private UILabel coverLabel;

    public AbstractOnlineWidgetBlock(OnlineShareWidget widget, OnlineWidgetSelectPane parentPane) {
        super(widget);
        this.parentPane = parentPane;
    }

    protected UILabel initCoverLabel(Image image) {
        coverLabel = new UILabel(new ImageIcon(image));
        return coverLabel;
    }

    @Override
    protected String getWidgetUuid() {
        return widget.getUuid();
    }

    protected void showPreview(OnlineShareWidget widget) {
        parentPane.showPreviewPane(this, widget.getId());
    }

    protected void hidePreview() {
        parentPane.hidePreviewPane();
    }

    @Override
    @NotNull
    protected Image getCoverImage() {
        OnlineResourceManager.getInstance().addLoader(this);
        return getDefaultDisplayImage();
    }

    public Image getPreviewImage() {
        try {
            return ImageIO.read(new URL(UriUtils.encodePath(widget.getPicPath(), EncodeConstants.ENCODING_UTF_8)));
        } catch (IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            return getDefaultDisplayImage();
        }
    }

    public void load() {
        Image image;
        try {

            Dimension coverDimension = getCoverDimension();
            String previewURI = UriUtils.encodePath(widget.getPicPath(), EncodeConstants.ENCODING_UTF_8) + "?x-oss-process=image/resize,m_fixed," + "w_" + coverDimension.width +
                    ",h_" + coverDimension.height;
            image = ImageIO.read(new URL(previewURI));
        } catch (IOException e) {
            FineLoggerFactory.getLogger().error(e.getMessage(), e);
            image = getDefaultDisplayImage();
        }
        resetCover(image);
    }

    private Image getDefaultDisplayImage(){
        return ShareComponentConstants.DEFAULT_COVER;
    }


    public void resetCover(Image image) {
        coverLabel.setIcon(new ImageIcon(image));
        this.parentPane.validate();
        this.parentPane.repaint();
    }

}
