package com.fr.design.mainframe.share.ui.base;

import com.fr.base.BaseUtils;
import com.fr.design.event.ChangeEvent;
import com.fr.design.event.ChangeListener;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.general.IOUtils;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kerry on 2020-10-19
 */
public class FlexSearchFieldPane extends JPanel {
    private static final String SEARCH = "SEARCH";
    private static final String OTHER = "OTHER";

    private static final Color SEARCH_BORDER_COLOR = Color.decode("#F5F5F7");
    private static final Color SEARCH_BORDER_INPUT_COLOR = Color.decode("#419BF9");
    private UITextField searchTextField;
    private CardLayout cardLayout;
    private JPanel centerPane;
    private List<ChangeListener> changeListenerList = new ArrayList<>();
    private List<FocusListener> focusListeners =  new ArrayList<>();
    private List<MouseListener> deleteIconMouseListener =  new ArrayList<>();
    private List<DocumentListener> fieldDocumentListener =  new ArrayList<>();

    public FlexSearchFieldPane(JPanel otherPane) {
        JPanel searchFieldPane = createSearchField();
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.add(otherPane, BorderLayout.CENTER);
        jPanel.add(createSearchLabel(), BorderLayout.EAST);
        cardLayout = new CardLayout();
        centerPane = new JPanel(cardLayout);
        centerPane.add(searchFieldPane, SEARCH);
        centerPane.add(jPanel, OTHER);
        cardLayout.show(centerPane, OTHER);
        this.add(centerPane, BorderLayout.CENTER);
    }


    private JPanel createSearchLabel() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        UILabel label = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/search_icon.png"));
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jPanel.add(label, BorderLayout.EAST);
        label.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                cardLayout.show(centerPane, SEARCH);
                searchTextField.requestFocus();
            }
        });
        return jPanel;
    }

    private JPanel createSearchField() {
        final JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        jPanel.setPreferredSize(new Dimension(228, 20));
        jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_COLOR));
        jPanel.setBackground(Color.WHITE);
        UILabel label = new UILabel(IOUtils.readIcon("/com/fr/base/images/share/search_icon.png"));
        label.setBorder(BorderFactory.createEmptyBorder(0, 8, 0, 0));
        jPanel.add(label, BorderLayout.WEST);
        this.searchTextField = new UITextField();
        this.searchTextField.setBorderPainted(false);
        this.searchTextField.setPlaceholder(Toolkit.i18nText("Fine-Design_Basic_Plugin_Search"));
        this.searchTextField.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_INPUT_COLOR));
                jPanel.repaint();
                for (FocusListener focusListener : focusListeners) {
                    focusListener.focusGained(e);
                }
            }

            @Override
            public void focusLost(FocusEvent e) {
                jPanel.setBorder(BorderFactory.createLineBorder(SEARCH_BORDER_COLOR));
                jPanel.repaint();
                for (FocusListener focusListener : focusListeners) {
                    focusListener.focusLost(e);
                }
            }
        });
        this.searchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterByName();
                for (DocumentListener listener : fieldDocumentListener) {
                    listener.insertUpdate(e);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterByName();
                for (DocumentListener listener : fieldDocumentListener) {
                    listener.removeUpdate(e);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterByName();
                for (DocumentListener listener : fieldDocumentListener) {
                    listener.changedUpdate(e);
                }
            }
        });
        jPanel.add(this.searchTextField, BorderLayout.CENTER);
        UILabel xLabel = new UILabel(BaseUtils.readIcon("/com/fr/design/images/buttonicon/close_icon.png"));
        xLabel.addMouseListener(new MouseClickListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                searchTextField.setText(StringUtils.EMPTY);
                cardLayout.show(centerPane, OTHER);
                for (MouseListener listener : deleteIconMouseListener) {
                    listener.mouseClicked(e);
                }
            }
        });
        xLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        jPanel.add(xLabel, BorderLayout.EAST);


        return jPanel;
    }

    private void filterByName() {
        String text = searchTextField.getText();
        for (ChangeListener listener : changeListenerList) {
            listener.fireChanged(new ChangeEvent(text));
        }
    }

    public void registerChangeListener(ChangeListener changeListener) {
        changeListenerList.add(changeListener);
    }

    public void registerSearchTextFieldFocusListener(FocusListener focusListener) {
        focusListeners.add(focusListener);
    }

    public void registerDeleteIconMouseListener(MouseListener mouseListener) {
        deleteIconMouseListener.add(mouseListener);
    }

    public void registerFieldDocumentListener(DocumentListener documentListener) {
        fieldDocumentListener.add(documentListener);
    }


}
