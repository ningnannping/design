package com.fr.design.mainframe.widget.arrangement.buttons;

import com.fr.design.i18n.Toolkit;
import com.fr.design.mainframe.ArrangementType;
import com.fr.design.mainframe.MultiSelectionArrangement;
import com.fr.general.IOUtils;

import javax.swing.Icon;

public class TopAlignButton extends AbstractMultiSelectionArrangementButton {
    private static final long serialVersionUID = 2896267783075870924L;

    public TopAlignButton(MultiSelectionArrangement arrangement) {
        super(arrangement);
    }

    @Override
    public Icon getIcon() {
        return IOUtils.readIcon("/com/fr/design/images/buttonicon/multi_selection_top_align.png");
    }

    @Override
    public String getTipText() {
        return Toolkit.i18nText("Fine-Design_Multi_Selection_Top_Align");
    }

    @Override
    public ArrangementType getArrangementType() {
        return ArrangementType.TOP_ALIGN;
    }
}
