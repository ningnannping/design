package com.fr.design.mainframe.share.ui.widgetfilter;

import com.fr.design.i18n.Toolkit;
import com.fr.form.share.bean.WidgetFilterInfo;
import com.fr.form.share.bean.WidgetFilterTypeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/11/12
 */
public class LocalWidgetFilterCategory {

    public static List<WidgetFilterTypeInfo> getLocalCategory() {
        List<WidgetFilterTypeInfo> category = new ArrayList<>();

        WidgetFilterTypeInfo source = new WidgetFilterTypeInfo();
        source.setTitle(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Source"));
        source.setKey("2@source");
        source.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Local"), "1", "source"));
        source.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Shop"), "2", "source"));
        source.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "source"));

        WidgetFilterTypeInfo displayDevice = new WidgetFilterTypeInfo();
        displayDevice.setTitle(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Show_Device"));
        displayDevice.setKey("1@displayDevice");
        displayDevice.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_PC"), "1", "displayDevice"));
        displayDevice.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Mobile"), "2", "displayDevice"));
        displayDevice.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "displayDevice"));

        WidgetFilterTypeInfo fee = new WidgetFilterTypeInfo();
        fee.setTitle(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Price"));
        fee.setKey("2@fee");
        fee.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Pay"), "2", "fee"));
        fee.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Free"), "1", "fee"));
        fee.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "fee"));

        WidgetFilterTypeInfo chart = new WidgetFilterTypeInfo();
        chart.setTitle(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Basic_Element"));
        chart.setKey("3@chart");
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Bar_Chart"), "1", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Line_Chart"), "3", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Combination_Chart"), "4", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Pie_Chart"), "2", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Dashboard_Chart"), "5", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Map"), "6", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Other_Chart"), "7", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Detail_List"), "8", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Basic_Widget"), "9", "chart"));
        chart.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "chart"));

        WidgetFilterTypeInfo report = new WidgetFilterTypeInfo();
        report.setTitle(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Comprehensive_Application"));
        report.setKey("4@report");
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Indicator_Card"), "1", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Title_Head"), "2", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Special_Function_Card"), "4", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Multi_Dimensional_Switch"), "5", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Move_Directory_Navigation"), "6", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Write_Report"), "8", "report"));
        report.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "report"));

        WidgetFilterTypeInfo style = new WidgetFilterTypeInfo();
        style.setTitle(Toolkit.i18nText("Fine-Design_Share_Filter_Style"));
        style.setKey("5@style");
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Simple_Fresh"), "1", "style"));
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Business_Stable"), "2", "style"));
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Lively_And_Bright"), "3", "style"));
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Cool_Technology"), "4", "style"));
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_Other_Style"), "5", "style"));
        style.addFilterItem(new WidgetFilterInfo(Toolkit.i18nText("Fine-Design_Share_Local_Filter_Item_All"), "0", "style"));

        category.add(displayDevice);
        category.add(source);
        category.add(chart);
        category.add(report);
        return category;

    }

}
