package com.fr.design.mainframe.share.ui.base;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * created by Harrison on 2020/04/21
 **/
public class LeftWordsTextArea extends PlaceholderTextArea {
    
    /**
     * 默认值 200
     */
    private int limitedLen = 200;
    
    public LeftWordsTextArea() {
    }
    
    public LeftWordsTextArea(String s, String placeholder) {
        super(s, placeholder);
    }
    
    public void setLimitedLen(int len) {
        this.limitedLen = len;
    }
    
    @Override
    protected void paintComponent(Graphics pG) {
        super.paintComponent(pG);
    
        char[] text = getText().toCharArray();
        int currentLen = text.length;
        int leftLen = limitedLen - currentLen;
        String leftWordsLen = String.valueOf(leftLen);
    
        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        g.drawString(leftWordsLen, getWidth() - getInsets().right - leftWordsLen.length() * 8 - 5, getHeight() - getInsets().bottom - 5);
    }
}
