package com.fr.design.mainframe.widget.accessibles;

import com.fr.design.Exception.ValidationException;
import com.fr.design.designer.properties.Decoder;
import com.fr.design.designer.properties.Encoder;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.xpane.PredefinedComponentStyleSettingPane;
import com.fr.design.mainframe.widget.editors.ITextComponent;
import com.fr.design.mainframe.widget.renderer.EncoderCellRenderer;
import com.fr.form.ui.NameLayoutBorderStyle;

import javax.swing.SwingUtilities;
import java.awt.Dimension;

/**
 * Created by kerry on 2020-09-07
 */
public class AccessibleBorderStyleEditor extends UneditableAccessibleEditor {
    private PredefinedComponentStyleSettingPane borderPane;

    public AccessibleBorderStyleEditor() {
        super(new ComponentStyleWrapper());
    }

    @Override
    protected ITextComponent createTextField() {
        return new RendererField(new ComponentStyleRenderer());
    }

    @Override
    protected void showEditorPane() {
        if (borderPane == null) {
            borderPane = new PredefinedComponentStyleSettingPane();
            borderPane.setPreferredSize(new Dimension(600, 400));
        }
        BasicDialog dlg = borderPane.showWindow(SwingUtilities.getWindowAncestor(this));
        dlg.addDialogActionListener(new DialogActionAdapter() {

            @Override
            public void doOk() {
                setValue(borderPane.updateBean());
                fireStateChanged();
            }
        });
        borderPane.populateBean((NameLayoutBorderStyle) getValue());
        dlg.setVisible(true);
    }


    private static class ComponentStyleWrapper implements Encoder, Decoder {
        public ComponentStyleWrapper() {

        }

        /**
         * 将属性转化成字符串
         *
         * @param v 属性对象
         * @return 字符串
         */
        public String encode(Object v) {
            if (v == null) {
                return null;
            }
            NameLayoutBorderStyle style = (NameLayoutBorderStyle) v;
            return style.usePredefinedStyle() ? com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Predefined") :
                    com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Basic_Preference_Custom");
        }

        /**
         * 将字符串转化成属性
         *
         * @param txt 字符串
         * @return 属性对象
         */
        public Object decode(String txt) {
            return null;
        }

        /**
         * 符合规则
         *
         * @param txt 字符串
         * @throws ValidationException 抛错
         */
        public void validate(String txt) throws ValidationException {

        }

    }

    private static class ComponentStyleRenderer extends EncoderCellRenderer {

        public ComponentStyleRenderer() {
            super(new ComponentStyleWrapper());
        }

    }
}
