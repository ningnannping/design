package com.fr.design.mainframe.share.group.ui;

import com.fr.design.gui.ibutton.UIButton;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.stable.StringUtils;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @Author: Yuan.Wang
 * @Date: 2020/10/29
 */
abstract public class GroupFileDialog extends BaseGroupDialog {

    private UITextField nameField;

    private UILabel warnLabel;

    private final UIButton confirmButton;

    public GroupFileDialog(Frame frame, String title) {
        this(frame, title, StringUtils.EMPTY);

    }

    public GroupFileDialog(Frame frame, String title, String defaultName) {
        super(frame);
        this.setLayout(new BorderLayout());
        this.setModal(true);

        setTitle(title);
        // 标签
        UILabel newNameLabel = creteNewNameLabel();
        // 输入框
        createNameField(defaultName);
        // 重名提示
        createWarnLabel();
        // 确认按钮
        confirmButton = createConfirmButton();
        // 取消按钮
        UIButton cancelButton = createCancelButton();

        JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 5));
        topPanel.setBorder(BorderFactory.createEmptyBorder(15, 15, 0, 15));
        topPanel.add(newNameLabel);
        topPanel.add(nameField);

        JPanel midPanel = new JPanel(new BorderLayout());
        midPanel.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
        midPanel.add(warnLabel, BorderLayout.WEST);

        JPanel bottomPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.setBorder(BorderFactory.createEmptyBorder(0, 15, 0, 15));
        bottomPanel.add(confirmButton);
        bottomPanel.add(cancelButton);

        this.add(
                TableLayoutHelper.createTableLayoutPane(
                        new Component[][]{
                                new Component[]{topPanel},
                                new Component[]{midPanel},
                                new Component[]{bottomPanel}
                        },
                        new double[]{TableLayout.FILL, TableLayout.FILL, TableLayout.FILL},
                        new double[]{TableLayout.FILL}
                ), BorderLayout.CENTER);
        initStyle();
    }

    protected String getFileName() {
        return nameField.getText().trim();
    }

    protected void validInput() {
        //抽成模板方法
        String userInput = getFileName();

        if (StringUtils.isEmpty(userInput)) {
            confirmButton.setEnabled(false);
            return;
        }
        String name = nameField.getText().trim();

        if (isDuplicate(name)) {
            nameField.selectAll();
            // 如果文件名已存在，则灰掉确认按钮
            warnLabel.setText(Toolkit.i18nText("Fine-Design_Share_Group_Repeat_Name_Info"));
            warnLabel.setVisible(true);
            confirmButton.setEnabled(false);
        } else {
            warnLabel.setVisible(false);
            confirmButton.setEnabled(true);
        }
    }

    /**
     * 点击确定后的处理方法
     */
    abstract protected void confirmClose();

    /**
     * 是否重名
     */
    abstract protected boolean isDuplicate(String fileName);


    private UILabel creteNewNameLabel() {
        // 输入框前提示
        UILabel newNameLabel = new UILabel(Toolkit.i18nText("Fine-Design_Share_Group_Enter_New_Folder_Name"));
        newNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        newNameLabel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 10));
        newNameLabel.setPreferredSize(new Dimension(84, 16));
        return newNameLabel;
    }

    private void createNameField(String name) {
        // 文件名输入框
        nameField = new UITextField(name);
        nameField.getDocument().addDocumentListener(new DocumentListener() {

            public void changedUpdate(DocumentEvent e) {
                validInput();
            }

            public void insertUpdate(DocumentEvent e) {
                validInput();
            }

            public void removeUpdate(DocumentEvent e) {
                validInput();
            }
        });
        nameField.selectAll();
        nameField.setPreferredSize(new Dimension(200, 20));
        // 增加enter以及esc快捷键的支持
        nameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    dispose();
                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (confirmButton.isEnabled()) {
                        confirmClose();
                    }
                }
            }
        });
    }

    private void createWarnLabel() {
        warnLabel = new UILabel();
        warnLabel.setPreferredSize(new Dimension(300, 30));
        warnLabel.setHorizontalAlignment(SwingConstants.LEFT);
        warnLabel.setForeground(Color.RED);
        warnLabel.setVisible(false);
    }
}