package com.fr.design.mainframe.share.ui.base.ui;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

/**
 * created by Harrison on 2020/04/22
 **/
public abstract class PlaceHolderUI<T extends JComponent> extends ComponentUI {

    private static final Color DEFAULT_COLOR = new Color(143, 142, 139);

    private String placeholder;


    public PlaceHolderUI(String placeholder) {
        this.placeholder = placeholder;
    }

    @Override
    public void paint(Graphics pG, JComponent c) {
        @SuppressWarnings("unchecked") T realType = (T) c;
        if (placeholder.length() == 0 || validate(realType)) {
            return;
        }
        final Graphics2D g = (Graphics2D) pG;
        g.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(getDisabledTextColor());
        g.drawString(placeholder, c.getInsets().left + 10, pG.getFontMetrics()
                .getMaxAscent() + c.getInsets().top + 3);

    }


    protected abstract boolean validate(T t);

    protected Color getDisabledTextColor() {

        return DEFAULT_COLOR;
    };

}
