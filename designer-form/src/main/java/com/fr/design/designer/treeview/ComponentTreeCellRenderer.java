package com.fr.design.designer.treeview;

import com.fr.base.BaseUtils;
import com.fr.design.constants.UIConstants;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XCreatorUtils;
import com.fr.design.mainframe.share.util.ShareComponentUtils;
import com.fr.log.FineLoggerFactory;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.Component;
import java.awt.Graphics;

public class ComponentTreeCellRenderer extends DefaultTreeCellRenderer {
    private boolean needAddShareIcon = false;
    private static final Icon SHARE_ICON = BaseUtils.readIcon("/com/fr/design/images/toast/reuse_icon.png");

    public ComponentTreeCellRenderer() {
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded,
                                                  boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        if (value instanceof XCreator) {
            String name = ((XCreator) value).toData().getWidgetName();
            setText(name);
            Icon icon = null;
            try {
                icon = XCreatorUtils.getCreatorIcon((XCreator) value);
            } catch (Exception e) {
                FineLoggerFactory.getLogger().info("{} has not icon or has been deleted", name);
            }
            if (icon != null) {
                setIcon(icon);
                this.needAddShareIcon = ShareComponentUtils.isShareWidgetWithChild((XCreator) value);
            }

        }
        this.setBorder(BorderFactory.createEmptyBorder(1, 0, 1, 0));
        this.setBackgroundNonSelectionColor(UIConstants.TREE_BACKGROUND);
        return this;
    }


    public void paint(Graphics g) {
        super.paint(g);
        if (needAddShareIcon) {
            SHARE_ICON.paintIcon(this, g, 10, 0);
        }
    }

    @Override
    public Icon getClosedIcon() {
        return getIcon();
    }

    @Override
    public Icon getLeafIcon() {
        return getIcon();
    }

    @Override
    public Icon getOpenIcon() {
        return getIcon();
    }
}
