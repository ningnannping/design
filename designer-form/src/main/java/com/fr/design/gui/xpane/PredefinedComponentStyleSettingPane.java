package com.fr.design.gui.xpane;

import com.fr.config.predefined.PredefinedComponentStyle;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.predefined.ui.PredefinedStyleSettingPane;
import com.fr.design.mainframe.predefined.ui.detail.component.ComponentFrameStylePane;
import com.fr.design.mainframe.predefined.ui.detail.component.ComponentTitleStylePane;
import com.fr.design.mainframe.predefined.ui.preview.StyleSettingPreviewPane;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.form.ui.NameLayoutBorderStyle;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

/**
 * Created by kerry on 2020-09-02
 */
public class PredefinedComponentStyleSettingPane extends PredefinedStyleSettingPane<NameLayoutBorderStyle> {
    private StyleSettingPane styleSettingPane;
    private TinyFormulaPane formulaPane;

    @Override
    protected StyleSettingPreviewPane createPreviewPane() {
        return new PreviewPane();
    }

    protected JPanel createPredefinedSettingPane() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 10));
        jPanel.add(new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Content")));
        formulaPane = new TinyFormulaPane();
        formulaPane.setPreferredSize(new Dimension(158, 30));
        jPanel.add(formulaPane);
        return jPanel;

    }

    @Override
    protected JPanel createCustomDetailPane() {
        JPanel jPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        styleSettingPane = new StyleSettingPane();
        jPanel.add(styleSettingPane, BorderLayout.CENTER);
        return jPanel;
    }

    @Override
    public void populateBean(NameLayoutBorderStyle ob) {
        this.setPopulating(true);
        super.populate(ob);
        this.formulaPane.populateBean(ob.getTitleText().toString());
        styleSettingPane.populateBean(ob);
        this.previewPane.refresh();
        this.setPopulating(false);
    }


    @Override
    public NameLayoutBorderStyle updateBean() {
        if (predefinedRadioBtn.isSelected()) {
            return updatePredefinedStyle();
        }
        return styleSettingPane.updateBean();
    }

    private NameLayoutBorderStyle updatePredefinedStyle() {
        NameLayoutBorderStyle layoutBorderStyle = NameLayoutBorderStyle.createPredefinedStyle(getPredefinedStyleName());
        layoutBorderStyle.setTitleText(formulaPane.updateBean());
        return layoutBorderStyle;
    }

    protected void populateCustomPane() {
        this.styleSettingPane.populateBean(updatePredefinedStyle());
    }

    @Override
    public String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Style");
    }

    class StyleSettingPane extends BasicBeanPane<NameLayoutBorderStyle> {
        private ComponentFrameStylePane frameStylePane;
        private ComponentTitleStylePane titleStylePane;

        public StyleSettingPane() {
            initPane();
        }

        private void initPane() {
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            JPanel frame = FRGUIPaneFactory.createTitledBorderNoGapPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Frame"));
            frameStylePane = new ComponentFrameStylePane();
            frameStylePane.setPreferredSize(new Dimension(233, 225));
            frame.add(frameStylePane);

            JPanel title = FRGUIPaneFactory.createTitledBorderNoGapPane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title"));
            titleStylePane = ComponentTitleStylePane.createStyleSettingPane();
            titleStylePane.setPreferredSize(new Dimension(233, 220));
            title.add(titleStylePane);

            this.add(frame, BorderLayout.NORTH);
            this.add(title, BorderLayout.CENTER);
        }

        @Override
        public void populateBean(NameLayoutBorderStyle ob) {
            PredefinedComponentStyle componentStyle = new PredefinedComponentStyle();
            componentStyle.setBorderStyle(ob.createRealStyle());
            frameStylePane.populate(componentStyle);
            titleStylePane.populate(componentStyle);
        }

        @Override
        public NameLayoutBorderStyle updateBean() {
            PredefinedComponentStyle componentStyle = update();
            NameLayoutBorderStyle nameLayoutBorderStyle = NameLayoutBorderStyle.createCustomStyle(componentStyle.getBorderStyle());
            return nameLayoutBorderStyle;
        }

        public PredefinedComponentStyle update() {
            PredefinedComponentStyle componentStyle = new PredefinedComponentStyle();
            frameStylePane.update(componentStyle);
            titleStylePane.update(componentStyle);
            return componentStyle;
        }

        @Override
        protected String title4PopupWindow() {
            return null;
        }
    }

    class PreviewPane extends StyleSettingPreviewPane {
        private LayoutBorderPreviewPane layoutBorderPreviewPane;

        public PreviewPane() {
            this.setPreferredSize(new Dimension(390, 511));
            this.setLayout(FRGUIPaneFactory.createBorderLayout());
            this.layoutBorderPreviewPane = new LayoutBorderPreviewPane(new LayoutBorderStyle());
            this.add(this.layoutBorderPreviewPane, BorderLayout.CENTER);
        }

        public void refresh() {
            NameLayoutBorderStyle componentStyle = PredefinedComponentStyleSettingPane.this.updateBean();
            this.layoutBorderPreviewPane.repaint((LayoutBorderStyle) componentStyle.createRealStyle());
        }

    }
}
