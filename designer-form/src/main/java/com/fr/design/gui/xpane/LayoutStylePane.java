package com.fr.design.gui.xpane;

import com.fr.base.Utils;
import com.fr.base.svg.IconUtils;
import com.fr.design.beans.BasicBeanPane;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.file.HistoryTemplateListCache;
import com.fr.design.formula.TinyFormulaPane;
import com.fr.design.gui.frpane.UIPercentDragPane;
import com.fr.design.gui.ibutton.UIButtonGroup;
import com.fr.design.gui.ibutton.UIColorButton;
import com.fr.design.gui.ibutton.UIToggleButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.icombobox.UIComboBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.ispinner.UISpinner;
import com.fr.design.gui.style.BackgroundSpecialPane;
import com.fr.design.gui.style.FRFontPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.JForm;
import com.fr.design.mainframe.JTemplate;
import com.fr.design.mainframe.backgroundpane.GradientBackgroundQuickPane;
import com.fr.design.widget.FRWidgetFactory;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.form.ui.WidgetTitle;
import com.fr.general.Background;
import com.fr.general.FRFont;
import com.fr.general.IOUtils;
import com.fr.general.act.TitlePacker;
import com.fr.stable.Constants;

import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;

/**
 * @author Starryi
 * @version 10.0.18
 * Created by Starryi on 2021/6/25
 *
 * 可配置图片类型边框的样式设置面板
 */
public class LayoutStylePane extends BasicBeanPane<LayoutBorderStyle> {
    public static final String[] BORDER_STYLE = new String[]{
            com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Common"),
            com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Shadow")
    };
    private static final Dimension BUTTON_SIZE = new Dimension(20, 20);
    public static final int SETTING_LABEL_WIDTH = 60;

    protected LayoutBorderStyle style = new LayoutBorderStyle();

    //渲染风格：有无阴影
    protected UIComboBox borderStyleCombo;
    // 含图片类型边框的边框配置面板(图片类型边框 + 阴影时存在默认的阴影颜色）
    protected BorderLineAndImagePane borderLineAndImagePane;
    //边框圆角或圆角裁剪
    protected UISpinner cornerSpinner;
    //主体背景
    protected BackgroundSpecialPane backgroundPane;
    //主体背景透明度
    protected UIPercentDragPane backgroundOpacityPane;

    // 标题可见
    protected UICheckBox titleVisibleCheckbox;
    //标题文字内容
    protected TinyFormulaPane titleTextPane;
    //标题字体格式
    protected UIComboBox titleFontFamilyComboBox;
    //标题字体大小
    protected UIComboBox titleFontSizeComboBox;
    //标题字体颜色
    protected UIColorButton titleFontColorSelectPane;
    //标题字体特殊效果：粗体、斜体、下划线
    private UIToggleButton titleFontBoldButton;
    private UIToggleButton titleFontItalicButton;
    private UIToggleButton titleFontUnderlineButton;
//    private LineComboBox titleFontUnderlineCombo; // 目前前端仅支持短横线类型的下划线，因此设计器端暂时就不展示线型选择框了，待后续优化
    // 标题图文混排
    protected TitleInsetImagePane titleInsetImagePane;
    //对齐方式
    protected UIButtonGroup titleAlignPane;
    //标题整体背景
    protected BackgroundSpecialPane titleBackgroundPane;
    //标题背景透明度
    protected UIPercentDragPane titleBackgroundOpacityPane;

    private boolean supportBorderImage = false;

    public LayoutStylePane() {
        this(false);
    }

    public LayoutStylePane(boolean supportBorderImage) {
        this.supportBorderImage = supportBorderImage;
        this.initLayout();
    }

    protected void initLayout() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        this.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));

        JPanel container = FRGUIPaneFactory.createBorderLayout_S_Pane();

        JTemplate currentEditingTemplate = HistoryTemplateListCache.getInstance().getCurrentEditingTemplate();
        boolean currentIsRootLayout = currentEditingTemplate != null && !currentEditingTemplate.isJWorkBook() && ((JForm)currentEditingTemplate).isSelectRootPane();

        JPanel titlePane = createTitleStylePane();
        JPanel bodyContentPane = currentIsRootLayout ? createBodyContentPane4RootLayout() : createBodyContentPane();
        JPanel backgroundPane = createBackgroundStylePane(true);

        if (titlePane != null) {
            container.add(titlePane, BorderLayout.NORTH);
            if (currentIsRootLayout) {
                titlePane.setVisible(false);
            }
        }
        JPanel nextContainerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        container.add(nextContainerPane, BorderLayout.CENTER);

        if (bodyContentPane != null) {
            //界面上表单主体只有背景和透明度可以设置
            nextContainerPane.add(bodyContentPane, BorderLayout.NORTH);
        }
        if (backgroundPane != null) {
            nextContainerPane.add(backgroundPane, BorderLayout.CENTER);
            if (currentIsRootLayout) {
                backgroundPane.setVisible(false);
            }
        }

        this.add(container, BorderLayout.CENTER);
    }

    protected JPanel createBackgroundStylePane(boolean supportCornerRadius) {
        borderStyleCombo = new UIComboBox(BORDER_STYLE);
        borderLineAndImagePane = new BorderLineAndImagePane(this.supportBorderImage);
        cornerSpinner = new UISpinner(0,1000,1,0);

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = supportCornerRadius ? new double[] {p, p, p, p} : new double[]{p, p, p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        UILabel uiLabel = FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Background_Style"));
        Font font = uiLabel.getFont().deriveFont(Font.BOLD);
        uiLabel.setFont(font);
        uiLabel.setForeground(new Color(143, 143, 146));

        JPanel contentPane = TableLayoutHelper.createGapTableLayoutPane(new JComponent[][]{
                        {uiLabel, null},
                        {FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Render_Style")), createBorderLayoutNorthPaneWithComponent(borderStyleCombo)},
                        {this.borderLineAndImagePane, null},
                        {FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Radius")), cornerSpinner},
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1);
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

        JPanel container = FRGUIPaneFactory.createBorderLayout_S_Pane();
        container.setBorder(new BottomLineBorder());
        container.add(contentPane, BorderLayout.NORTH);

        return container;
    }

    protected JPanel createBodyContentPane() {
        backgroundPane = new LayoutBackgroundSpecialPane();
        backgroundOpacityPane = new UIPercentDragPane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p, p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        JPanel bodyBackground = createBackgroundAndOpacityPane(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Body_Fill"),
                this.backgroundPane,
                this.backgroundOpacityPane);

        UILabel uiLabel = FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Body_Content"));
        Font font = uiLabel.getFont().deriveFont(Font.BOLD);
        uiLabel.setFont(font);
        uiLabel.setForeground(new Color(143, 143, 146));
        JPanel contentPane = TableLayoutHelper.createGapTableLayoutPane(new JComponent[][]{
                        {uiLabel, null},
                        {bodyBackground, null},
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1);
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

        JPanel container = FRGUIPaneFactory.createBorderLayout_S_Pane();
        container.setBorder(new BottomLineBorder());
        container.add(contentPane, BorderLayout.NORTH);

        return container;
    }

    protected JPanel createBodyContentPane4RootLayout() {
        backgroundPane = new LayoutBackgroundSpecialPane();
        backgroundOpacityPane = new UIPercentDragPane();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        JPanel bodyBackground = createBackgroundAndOpacityPane(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Body_Fill"),
                this.backgroundPane,
                this.backgroundOpacityPane);

        JPanel contentPane = TableLayoutHelper.createGapTableLayoutPane(new JComponent[][]{
                        {bodyBackground, null},
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1);
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

        JPanel container = FRGUIPaneFactory.createBorderLayout_S_Pane();
        container.setBorder(new BottomLineBorder());
        container.add(contentPane, BorderLayout.NORTH);

        return container;
    }

    protected void initTitleComponents() {
        titleVisibleCheckbox = new UICheckBox();

        titleTextPane = new TinyFormulaPane();

        titleFontFamilyComboBox = new UIComboBox(Utils.getAvailableFontFamilyNames4Report());
        TitlePacker title = style.getTitle();
        if (title != null) {
            FRFont frFont = title.getFrFont();
            if (frFont != null) {
                String fontFamily = frFont.getFamily();
                // 使用style中默认的字体初始化titleFontFamilyComboBox，保证UI和数据的一致性
                this.titleFontFamilyComboBox.setSelectedItem(fontFamily);
            }
        }
        titleFontFamilyComboBox.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Family"));

        titleFontSizeComboBox = new UIComboBox(FRFontPane.FONT_SIZES);
        titleFontSizeComboBox.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Size"));

        titleFontColorSelectPane = new UIColorButton();
        titleFontColorSelectPane.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Foreground"));
        titleFontColorSelectPane.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Foreground"));

        titleFontBoldButton = new UIToggleButton(IOUtils.readIcon("/com/fr/design/images/m_format/cellstyle/bold.png"));
        titleFontBoldButton.setPreferredSize(BUTTON_SIZE);
        titleFontBoldButton.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Bold"));
        titleFontBoldButton.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Bold"));

        titleFontItalicButton = new UIToggleButton(IOUtils.readIcon("/com/fr/design/images/m_format/cellstyle/italic.png"));
        titleFontItalicButton.setPreferredSize(BUTTON_SIZE);
        titleFontItalicButton.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Italic"));
        titleFontItalicButton.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Italic"));

        titleFontUnderlineButton = new UIToggleButton(IOUtils.readIcon("/com/fr/design/images/m_format/cellstyle/underline.png"));
        titleFontUnderlineButton.setPreferredSize(BUTTON_SIZE);
        titleFontUnderlineButton.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Underline"));
        titleFontUnderlineButton.setToolTipText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Underline"));

//        titleFontUnderlineCombo = new LineComboBox(UIConstants.BORDER_LINE_STYLE_ARRAY);
//        titleFontUnderlineCombo.setGlobalName(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_FRFont_Line_Style"));

        titleInsetImagePane = new TitleInsetImagePane();

        titleAlignPane = new UIButtonGroup<Integer>(
                new Icon[] {
                        IconUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_left_normal.png"),
                        IconUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_center_normal.png"),
                        IconUtils.readIcon("/com/fr/design/images/m_format/cellstyle/h_right_normal.png")
                },
                new Integer[]{Constants.LEFT, Constants.CENTER, Constants.RIGHT});
        titleAlignPane.setAllToolTips(
                new String[] {
                        com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Left"),
                        com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Center"),
                        com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_StyleAlignment_Right")
                });

        titleBackgroundPane = new LayoutBackgroundSpecialPane();

        titleBackgroundOpacityPane = new UIPercentDragPane();
    }

    protected JPanel createTitleStylePane() {
        initTitleComponents();

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p,p,p,p,p,p};
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        final JPanel bottomPane = TableLayoutHelper.createCommonTableLayoutPane( new JComponent[][]{
                        {FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Content")), titleTextPane},
                        {FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Format")), createBorderLayoutNorthPaneWithComponent(titleFontFamilyComboBox)},
                        {null, createTitleFontButtonPane()},
                        {titleInsetImagePane, null},
                        {FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Text_Align")), titleAlignPane},
                        {this.createTitleBackgroundAndOpacityPane(), null},
                },
                rowSize, columnSize, IntervalConstants.INTERVAL_L1);
        bottomPane.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        bottomPane.setVisible(false);

        JPanel visibleComposedPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        
        titleVisibleCheckbox.setSelected(false);
        visibleComposedPane.add(titleVisibleCheckbox, BorderLayout.WEST);
        visibleComposedPane.add(FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Visible")), BorderLayout.CENTER);

        UILabel uiLabel = FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title"));
        Font font = uiLabel.getFont().deriveFont(Font.BOLD);
        uiLabel.setFont(font);
        uiLabel.setForeground(new Color(143, 143, 146));
        JPanel topPane = TableLayoutHelper.createCommonTableLayoutPane( new JComponent[][] {
                {uiLabel, null},
                {visibleComposedPane, null}
        }, new double[]{p, p}, new double[]{SETTING_LABEL_WIDTH, p}, IntervalConstants.INTERVAL_L1);
        topPane.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));

        JPanel container = FRGUIPaneFactory.createBorderLayout_S_Pane();
        container.setBorder(new BottomLineBorder());
        container.add(topPane, BorderLayout.NORTH);
        container.add(bottomPane, BorderLayout.CENTER);

        titleVisibleCheckbox.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                bottomPane.setVisible(titleVisibleCheckbox.isSelected());
            }
        });

        return container;
    }

    private JPanel createBorderLayoutNorthPaneWithComponent(JComponent content) {
        JPanel jPanel = new JPanel(new BorderLayout());
        jPanel.add(content, BorderLayout.NORTH);
        return jPanel;
    }

    protected JPanel createTitleFontButtonPane(){
        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] rowSize = {p};
        double[] columnSize = {f, p, p, p, p};

        JPanel buttonPane = TableLayoutHelper.createCommonTableLayoutPane( new JComponent[][] {
                {titleFontSizeComboBox, titleFontColorSelectPane, titleFontItalicButton, titleFontBoldButton, titleFontUnderlineButton},
        }, rowSize, columnSize, IntervalConstants.INTERVAL_W0);

        JPanel containerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        containerPane.add(buttonPane, BorderLayout.NORTH);
//        containerPane.add(titleFontUnderlineCombo, BorderLayout.CENTER);

//        titleFontUnderlineCombo.setVisible(false);
//        titleFontUnderlineButton.addChangeListener(new ChangeListener() {
//            @Override
//            public void stateChanged(ChangeEvent e) {
//                titleFontUnderlineCombo.setVisible(titleFontUnderlineButton.isSelected());
//            }
//        });

        return containerPane;
    }

    protected JPanel createTitleBackgroundAndOpacityPane() {
        return createBackgroundAndOpacityPane(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style_Title_Background"),
                titleBackgroundPane,
                titleBackgroundOpacityPane);
    }

    @Override
    public LayoutBorderStyle updateBean() {
        LayoutBorderStyle style = new LayoutBorderStyle();
        updateMain(style);
        updateTitle(style);
        return style;
    }

    protected void updateMain(LayoutBorderStyle style) {
        if (borderStyleCombo != null) {
            style.setBorderStyle(borderStyleCombo.getSelectedIndex());
        }
        if (cornerSpinner != null) {
            style.setBorderRadius((int) cornerSpinner.getValue());
        }
        if (borderLineAndImagePane != null) {
            borderLineAndImagePane.updateBean(style);
        }
        if (backgroundPane != null) {
            style.setBackground(backgroundPane.update());
        }
        if (backgroundOpacityPane != null) {
            style.setAlpha((float)backgroundOpacityPane.updateBean());
        }
    }

    protected void updateTitle(LayoutBorderStyle style) {
        style.setType(titleVisibleCheckbox != null && titleVisibleCheckbox.isSelected() ? LayoutBorderStyle.TITLE : LayoutBorderStyle.STANDARD);
        TitlePacker title = style.getTitle() == null ? new WidgetTitle() : style.getTitle();
        title.setTextObject(titleTextPane.updateBean());
        FRFont frFont = title.getFrFont();
        frFont = frFont.applySize((Integer) titleFontSizeComboBox.getSelectedItem());
        frFont = frFont.applyName(titleFontFamilyComboBox.getSelectedItem().toString());
        frFont = frFont.applyForeground(titleFontColorSelectPane.getColor());
        frFont = updateTitleFontItalicBold(frFont);
//        int line = titleFontUnderlineButton.isSelected() ? this.titleFontUnderlineCombo.getSelectedLineStyle() : Constants.LINE_NONE;
        int line = titleFontUnderlineButton.isSelected() ? Constants.LINE_THIN : Constants.LINE_NONE;
        frFont = frFont.applyUnderline(line);
        title.setFrFont(frFont);
        title.setPosition((Integer) titleAlignPane.getSelectedItem());
        titleInsetImagePane.updateBean(title);
        title.setBackground(titleBackgroundPane.update());
        title.setBackgroundOpacity((float)titleBackgroundOpacityPane.updateBean());
        style.setTitle(title);
    }

    private FRFont updateTitleFontItalicBold(FRFont frFont) {
        int italic_bold = frFont.getStyle();
        boolean isItalic = italic_bold == Font.ITALIC || italic_bold == (Font.BOLD + Font.ITALIC);
        boolean isBold = italic_bold == Font.BOLD || italic_bold == (Font.BOLD + Font.ITALIC);
        if (titleFontItalicButton.isSelected() && !isItalic) {
            italic_bold += Font.ITALIC;
        } else if (!titleFontItalicButton.isSelected() && isItalic) {
            italic_bold -= Font.ITALIC;
        }
        frFont = frFont.applyStyle(italic_bold);
        if (titleFontBoldButton.isSelected() && !isBold) {
            italic_bold += Font.BOLD;
        } else if (!titleFontBoldButton.isSelected() && isBold) {
            italic_bold -= Font.BOLD;
        }
        frFont = frFont.applyStyle(italic_bold);
        return frFont;
    }

    @Override
    public void populateBean(LayoutBorderStyle style) {
        if(this.style == null) {
            this.style = new LayoutBorderStyle();
        }
        this.style.setStyle(style);

        populateMain();
        populateTitle();
    }

    protected void populateMain() {
        if (this.borderStyleCombo != null) {
            this.borderStyleCombo.setSelectedIndex(style.getBorderStyle());
        }
        if (this.borderLineAndImagePane != null) {
            this.borderLineAndImagePane.populateBean(style);
        }
        if (this.backgroundPane != null) {
            this.backgroundPane.populateBean(style.getBackground());
            if (this.backgroundOpacityPane != null) {
                this.backgroundOpacityPane.populateBean(style.getAlpha());
            }
        }
        if (this.cornerSpinner != null) {
            this.cornerSpinner.setValue(style.getBorderRadius());
        }
    }

    protected void populateTitle() {
        TitlePacker widgetTitle = style == null ? new WidgetTitle() : style.getTitle();
        widgetTitle = widgetTitle == null ? new WidgetTitle() : widgetTitle;
        titleVisibleCheckbox.setSelected(style.getType() != LayoutBorderStyle.STANDARD);

        this.titleTextPane.populateBean(widgetTitle.getTextObject().toString());

        FRFont frFont = widgetTitle.getFrFont();
        this.titleFontSizeComboBox.setSelectedItem(frFont.getSize());
        this.titleFontFamilyComboBox.setSelectedItem(frFont.getFamily());
        this.titleFontColorSelectPane.setColor(frFont.getForeground());
        this.titleFontColorSelectPane.repaint();
        titleFontBoldButton.setSelected(frFont.isBold());
        titleFontItalicButton.setSelected(frFont.isItalic());

        int line = frFont.getUnderline();
        if (line == Constants.LINE_NONE) {
            titleFontUnderlineButton.setSelected(false);
//            titleFontUnderlineCombo.setVisible(false);
        } else {
            titleFontUnderlineButton.setSelected(true);
//            titleFontUnderlineCombo.setVisible(true);
//            this.titleFontUnderlineCombo.setSelectedLineStyle(line);
        }

        titleAlignPane.setSelectedItem(widgetTitle.getPosition());
        titleInsetImagePane.populateBean(widgetTitle);
        titleBackgroundPane.populateBean(widgetTitle.getBackground());
        titleBackgroundOpacityPane.populateBean(widgetTitle.getBackgroundOpacity());
    }

    @Override
    protected String title4PopupWindow() {
        return com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget_Style");
    }

    private static JPanel createBackgroundAndOpacityPane(String name, BackgroundSpecialPane backgroundPane, UIPercentDragPane opacityPane) {
        JPanel container = new JPanel();
        container.setLayout(new BorderLayout(0, 6));

        double p = TableLayout.PREFERRED;
        double f = TableLayout.FILL;
        double[] columnSize = {SETTING_LABEL_WIDTH, f};

        // 确保BackgroundSpecialPane高度变化时，Label依然保持与其顶部对齐
        JPanel backgroundLabelPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        backgroundLabelPane.setBorder(BorderFactory.createEmptyBorder(7, 0, 0, 0));
        backgroundLabelPane.add(FRWidgetFactory.createLineWrapLabel(name), BorderLayout.NORTH);

        JPanel backgroundComposedPane = TableLayoutHelper.createGapTableLayoutPane(
                new JComponent[][]{
                        {backgroundLabelPane, backgroundPane}
                },
                new double[]{p}, columnSize, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1);

        JPanel opacityComposedPane = TableLayoutHelper.createGapTableLayoutPane(
                new JComponent[][]{
                        {new UILabel(""), FRWidgetFactory.createLineWrapLabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Widget-Style_Alpha"))},
                        {new UILabel(""), opacityPane}
                },
                new double[]{p, p}, columnSize, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1);

        container.add(backgroundComposedPane, BorderLayout.NORTH, 0);
        container.add(opacityComposedPane, BorderLayout.CENTER, 1);

        opacityComposedPane.setVisible(false);

        backgroundPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                Background background = backgroundPane.update();
                opacityComposedPane.setVisible(background != null);
            }
        });

        return container;
    }

    protected static class BottomLineBorder extends LineBorder {

        public BottomLineBorder() {
            super(new Color(217, 218, 221), 1);
        }

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2d = (Graphics2D) g;

            Color oldColor = g2d.getColor();
            Stroke oldStroke = g2d.getStroke();

            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g2d.setColor(getLineColor());
            g2d.setStroke(new BasicStroke(getThickness() * 2));
            g2d.drawLine(0, height, width, height);

            g2d.setStroke(oldStroke);
            g2d.setColor(oldColor);
            g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
        }
    }

    protected static class LayoutBackgroundSpecialPane extends BackgroundSpecialPane {
        @Override
        protected GradientBackgroundQuickPane createGradientBackgroundQuickPane() {
            return new GradientBackgroundQuickPane(140);
        }
    }
}
