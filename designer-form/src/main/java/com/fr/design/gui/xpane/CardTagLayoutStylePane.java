/*
 * Copyright(c) 2001-2010, FineReport Inc, All Rights Reserved.
 */
package com.fr.design.gui.xpane;

import com.fr.form.ui.LayoutBorderStyle;

import javax.swing.JPanel;

/**
 * CardTagLayoutBorderPane Pane.
 */
public class CardTagLayoutStylePane extends LayoutStylePane {

	private LayoutBorderStyle backupStyleFromPopulating = new LayoutBorderStyle();

	@Override
	protected JPanel createTitleStylePane(){
		JPanel panel = super.createTitleStylePane();
		panel.setVisible(false);
		return panel;
	}

	@Override
	protected JPanel createBackgroundStylePane(boolean supportCornerRadius) {
		return super.createBackgroundStylePane(false);
	}

	@Override
	public void populateBean(LayoutBorderStyle style) {
		this.backupStyleFromPopulating = style;
		super.populateBean(style);
	}

	@Override
	public LayoutBorderStyle updateBean() {
		LayoutBorderStyle style = super.updateBean();
		style.setTitle(backupStyleFromPopulating.getTitle());
		style.setType(backupStyleFromPopulating.getType());
		return style;
	}
}
