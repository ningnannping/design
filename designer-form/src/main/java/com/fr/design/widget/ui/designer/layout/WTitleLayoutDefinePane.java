package com.fr.design.widget.ui.designer.layout;

import com.fr.design.designer.creator.XCreator;
import com.fr.design.foldablepane.UIExpandablePane;
import com.fr.design.gui.xpane.LayoutStylePane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.widget.ui.designer.AbstractDataModify;
import com.fr.form.ui.AbstractBorderStyleWidget;
import com.fr.form.ui.LayoutBorderStyle;
import com.fr.general.ComparatorUtils;

import javax.swing.*;
import java.awt.*;


/**
 * Created by ibm on 2017/8/3.
 */
public abstract class WTitleLayoutDefinePane<T extends AbstractBorderStyleWidget>  extends AbstractDataModify<T> {
    private LayoutStylePane stylePane;

    public WTitleLayoutDefinePane(XCreator xCreator) {
        super(xCreator);
        initComponent();
    }

    public void initComponent() {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());
        JPanel advancePane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        stylePane = new LayoutStylePane(true);
        advancePane.add(stylePane, BorderLayout.NORTH);
        JPanel centerPane = createCenterPane();
        if(centerPane!=null){
            advancePane.add(centerPane, BorderLayout.CENTER);
        }
        UIExpandablePane advanceExpandablePane = new UIExpandablePane(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Report_Advanced"), 280, 20, advancePane);

        this.add(advanceExpandablePane);

    }

    protected JPanel createCenterPane(){
        return null;
    }


    @Override
    public String title4PopupWindow() {
        return "titleLayout";
    }

    @Override
    public void populateBean(T ob) {
        populateSubBean(ob);
        stylePane.populateBean((LayoutBorderStyle) ob.getBorderStyle());
    }


    @Override
    public T updateBean() {
        T e = updateSubBean();
        LayoutBorderStyle style = stylePane.updateBean();
        if(!ComparatorUtils.equals(style, e.getBorderStyle())){
            e.setBorderStyle(style);
        }
        return e;
    }

    protected abstract T updateSubBean();

    protected abstract void populateSubBean(T ob);

}
