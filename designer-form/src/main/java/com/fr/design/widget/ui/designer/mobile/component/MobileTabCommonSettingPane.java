package com.fr.design.widget.ui.designer.mobile.component;

import com.fr.design.constants.LayoutConstants;
import com.fr.design.designer.IntervalConstants;
import com.fr.design.designer.beans.events.DesignerEvent;
import com.fr.design.dialog.BasicPane;
import com.fr.design.gui.ibutton.ModeButtonGroup;
import com.fr.design.gui.ibutton.UIRadioButton;
import com.fr.design.gui.icheckbox.UICheckBox;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.i18n.Toolkit;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayout;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.WidgetPropertyPane;
import com.fr.design.style.color.NewColorSelectBox;
import com.fr.general.cardtag.mobile.MobileTemplateStyle;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JComponent;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MobileTabCommonSettingPane extends BasicPane {

    private UICheckBox showTabTitleCheck;
    private UICheckBox tabSlideCheck;
    private UICheckBox showTabDotIndicatorCheck;
    private ModeButtonGroup<Integer> buttonGroup;
    private NewColorSelectBox initDotColorBox;
    private NewColorSelectBox selectDotColorBox;
    private boolean isPopulate;

    public MobileTabCommonSettingPane(boolean isTopComponent) {
        initComponent(isTopComponent);
    }

    private void initComponent(boolean isTopComponent) {
        this.setLayout(FRGUIPaneFactory.createBorderLayout());

        this.showTabTitleCheck = new UICheckBox(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Tab_Show_Title"), true) {
            @Override
            protected void initListener() {
                this.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        attributeChange();
                    }
                });
            }
        };
        this.tabSlideCheck = new UICheckBox(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Tab_Slide"), true) {
            @Override
            protected void initListener() {
                this.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        attributeChange();
                    }
                });
            }
        };
        this.showTabDotIndicatorCheck = new UICheckBox(
                com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Tab_Show_Indicator"), true) {
            @Override
            protected void initListener() {
                this.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        attributeChange();
                    }
                });
            }
        };

        UILabel label = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Tab_Show_Indicator_type"));
        JPanel dotIndicatorShowTypePane = FRGUIPaneFactory.createLeftFlowZeroGapBorderPane();
        addIndicatorShowTypeButton(dotIndicatorShowTypePane);

        UILabel initColorLabel = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Init_Fill"));
        UILabel selectColor = new UILabel(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Select_Fill"));
        initDotColorBox = new NewColorSelectBox(0){
            @Override
            protected void iniListener() {
                this.addSelectChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        if (!isPopulate) {
                            attributeChange();
                        }
                    }
                });
            }
        };
        selectDotColorBox = new NewColorSelectBox(0){
            @Override
            protected void iniListener() {
                this.addSelectChangeListener(new ChangeListener() {
                    @Override
                    public void stateChanged(ChangeEvent e) {
                        if (!isPopulate) {
                            attributeChange();
                        }
                    }
                });
            }
        };
        JPanel initDotColorPane = TableLayoutHelper.createGapTableLayoutPane(new Component[][]{new Component[]{initColorLabel, initDotColorBox}}, TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_L1, LayoutConstants.VGAP_MEDIUM);
        JPanel selectDotColorPane = TableLayoutHelper.createGapTableLayoutPane(new Component[][]{new Component[]{selectColor, selectDotColorBox}}, TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_L1, LayoutConstants.VGAP_MEDIUM);

        double[] rowSize = {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED};
        double[] verticalGaps = {IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1, IntervalConstants.INTERVAL_L1};
        double[] dotSettingColumnSize = {TableLayout.PREFERRED, TableLayout.FILL};

        final JPanel tabBaseConfigPanel = createTableConfigJPanel();

        JPanel dotIndicatorSettingPanel = TableLayoutHelper.createDiffVGapTableLayoutPane(new JComponent[][]{
                {label, dotIndicatorShowTypePane},
                {initColorLabel, initDotColorPane},
                {selectColor, selectDotColorPane}}, rowSize, dotSettingColumnSize, 0, verticalGaps);
        dotIndicatorSettingPanel.setBorder(
                BorderFactory.createEmptyBorder(0, IntervalConstants.INTERVAL_L2, IntervalConstants.INTERVAL_L1, 0)
        );

        final JPanel tableConfigJPanel = FRGUIPaneFactory.createBorderLayout_S_Pane();
        if (isTopComponent) {
            tableConfigJPanel.setBorder(BorderFactory.createEmptyBorder(IntervalConstants.INTERVAL_L1, 0, 0, 0));
        }
        tableConfigJPanel.add(tabBaseConfigPanel, BorderLayout.NORTH);
        tableConfigJPanel.add(dotIndicatorSettingPanel, BorderLayout.CENTER);
        this.add(tableConfigJPanel, BorderLayout.CENTER);

        showTabDotIndicatorCheck.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                dotIndicatorSettingPanel.setVisible(showTabDotIndicatorCheck.isSelected());
            }
        });
    }

    public void populate(MobileTemplateStyle mobileTemplateStyle) {
        this.showTabTitleCheck.setSelected(mobileTemplateStyle.isShowTabTitle());
        this.showTabDotIndicatorCheck.setSelected(mobileTemplateStyle.isShowDotIndicator());
        this.tabSlideCheck.setSelected(mobileTemplateStyle.canSlide());
        Color initDotColor = mobileTemplateStyle.getIndicatorInitialColor();
        Color selectDotColor = mobileTemplateStyle.getIndicatorSelectColor();
        int dotIndicatorShowType = mobileTemplateStyle.getDotIndicatorShowType();
        this.populateColorBox(initDotColorBox, initDotColor, MobileTemplateStyle.DEFAULT_INITIAL_DOT_COLOR);
        this.populateColorBox(selectDotColorBox, selectDotColor, MobileTemplateStyle.DEFAULT_SELECT_DOT_COLOR);
        if (dotIndicatorShowType != buttonGroup.getCurrentSelected()) {
            this.buttonGroup.setSelectButton(dotIndicatorShowType);
        }
    }

    public void update(MobileTemplateStyle mobileTemplateStyle) {
        mobileTemplateStyle.setShowTabTitle(showTabTitleCheck.isSelected());
        mobileTemplateStyle.setShowDotIndicator(showTabDotIndicatorCheck.isSelected());
        mobileTemplateStyle.setCanSlide(tabSlideCheck.isSelected());
        mobileTemplateStyle.setIndicatorInitialColor(initDotColorBox.getSelectObject());
        mobileTemplateStyle.setIndicatorSelectColor(selectDotColorBox.getSelectObject());
        mobileTemplateStyle.setDotIndicatorShowType(buttonGroup.getCurrentSelected());
        WidgetPropertyPane.getInstance().getEditingFormDesigner().getEditListenerTable().fireCreatorModified(DesignerEvent.CREATOR_EDITED);
    }

    @Override
    protected String title4PopupWindow() {
        return "MobileTabCommonSettingPane";
    }

    private void addIndicatorShowTypeButton(JPanel dotIndicatorShowTypePane) {
        UIRadioButton holderPlaceButton = new UIRadioButton(Toolkit.i18nText("Fine-Design_Mobile_Tab_Holder_Place"));
        holderPlaceButton.setSelected(true);
        UIRadioButton floatButton = new UIRadioButton(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Mobile_Tab_Float"));
        dotIndicatorShowTypePane.add(holderPlaceButton);
        dotIndicatorShowTypePane.add(floatButton);
        dotIndicatorShowTypePane.setBorder(
                BorderFactory.createEmptyBorder(0, IntervalConstants.INTERVAL_L1, 0, 0)
        );
        buttonGroup = new ModeButtonGroup<>();
        buttonGroup.put(MobileTemplateStyle.TYPE_PLACEHOLDER_DOT_INDICATOR, holderPlaceButton);
        buttonGroup.put(MobileTemplateStyle.TYPE_FLOAT_DOT_INDICATOR, floatButton);
    }

    private void populateColorBox(NewColorSelectBox colorBox, Color color, Color defaultColor) {
        if (color == null) {
            color = defaultColor;
        }
        if (color != colorBox.getSelectObject()) {
            isPopulate = true;
            colorBox.setSelectObject(color);
            isPopulate = false;
        }
    }

    private JPanel createTableConfigJPanel() {
        final JPanel jPanel = new JPanel();
        BorderLayout borderLayout = new BorderLayout(0, IntervalConstants.INTERVAL_L1);
        jPanel.setLayout(borderLayout);
        jPanel.add(this.showTabTitleCheck, BorderLayout.NORTH);
        jPanel.add(this.tabSlideCheck, BorderLayout.CENTER);
        jPanel.add(this.showTabDotIndicatorCheck, BorderLayout.SOUTH);
        jPanel.setBorder(BorderFactory.createEmptyBorder(0, 0, IntervalConstants.INTERVAL_L1, 0));
        return  jPanel;
    }
}
