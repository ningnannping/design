package com.fr.design.widget.ui.designer.layout;

import com.fr.design.designer.IntervalConstants;
import com.fr.design.designer.creator.CRPropertyDescriptor;
import com.fr.design.designer.creator.PropertyGroupPane;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XElementCase;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.layout.TableLayoutHelper;
import com.fr.design.mainframe.widget.accessibles.AccessibleElementCaseToolBarEditor;
import com.fr.design.widget.ui.designer.component.PaddingBoundPane;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.web.FormToolBarManager;
import com.fr.general.ComparatorUtils;

import javax.swing.*;
import java.awt.*;

/**
 * Created by ibm on 2017/8/7.
 */
public class ChartEditorDefinePane extends WTitleLayoutDefinePane<ChartEditor> {
    private PaddingBoundPane paddingBoundPane;

    public ChartEditorDefinePane(XCreator xCreator) {
        super(xCreator);

    }

    protected JPanel createCenterPane() {
        JPanel centerPane = FRGUIPaneFactory.createBorderLayout_S_Pane();
        paddingBoundPane = new PaddingBoundPane();
        Component[][] components = new Component[][]{
                new Component[]{paddingBoundPane, null},
        };
        JPanel panel = TableLayoutHelper.createGapTableLayoutPane(components, TableLayoutHelper.FILL_LASTCOLUMN, IntervalConstants.INTERVAL_W0, IntervalConstants.INTERVAL_L1);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 0, 10, 0));
        centerPane.add(panel, BorderLayout.NORTH);
        return centerPane;
    }

    protected ChartEditor updateSubBean() {
        ChartEditor chartEditor = (ChartEditor) creator.toData();
        if (ComparatorUtils.equals(getGlobalName(), com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Layout_Padding_Duplicate"))) {
            paddingBoundPane.update(chartEditor);
        }

        return chartEditor;
    }

    protected void populateSubBean(ChartEditor ob) {
        paddingBoundPane.populate(ob);
    }

}
