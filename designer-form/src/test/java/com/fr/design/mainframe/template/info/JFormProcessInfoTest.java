package com.fr.design.mainframe.template.info;

import com.fr.base.background.ColorBackground;
import com.fr.chart.chartattr.ChartCollection;
import com.fr.form.main.Form;
import com.fr.form.ui.CardSwitchButton;
import com.fr.form.ui.ChartEditor;
import com.fr.form.ui.ElementCaseEditor;
import com.fr.form.ui.FreeButton;
import com.fr.form.ui.container.WAbsoluteBodyLayout;
import com.fr.form.ui.container.WAbsoluteLayout;
import com.fr.form.ui.container.WBorderLayout;
import com.fr.form.ui.container.WCardLayout;
import com.fr.form.ui.container.WFitLayout;
import com.fr.form.ui.container.WParameterLayout;
import com.fr.form.ui.container.cardlayout.WCardTagLayout;
import com.fr.invoke.Reflect;
import com.fr.json.JSONArray;
import com.fr.plugin.chart.vanchart.VanChart;
import com.fr.report.cell.DefaultTemplateCellElement;
import com.fr.report.worksheet.FormElementCase;
import org.junit.Assert;
import org.junit.Test;

import java.awt.Color;

/**
 * Created by kerry on 2020-05-08
 */
public class JFormProcessInfoTest {
    @Test
    public void testHasTestECReport() {
        Form form = new Form();
        WFitLayout wFitLayout = new WFitLayout();
        ElementCaseEditor editor = new ElementCaseEditor();
        FormElementCase elementCase = new FormElementCase();
        editor.setElementCase(elementCase);
        wFitLayout.addWidget(editor);
        form.setContainer(wFitLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        boolean result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertTrue(result);

        elementCase.addCellElement(new DefaultTemplateCellElement());
        result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertTrue(result);

        DefaultTemplateCellElement templateCellElement = new DefaultTemplateCellElement();
        templateCellElement.setValue(123);

        elementCase.addCellElement(templateCellElement);
        result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertFalse(result);

        elementCase.removeCellElement(templateCellElement);
        result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertTrue(result);

        editor.setBackground(ColorBackground.getInstance(Color.WHITE));
        result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertFalse(result);


        editor.setBackground(null);
        result = Reflect.on(jFormProcessInfo).call("hasTestECReport").get();
        Assert.assertTrue(result);
    }



    @Test
    public void testHasTestChart() {
        Form form = new Form();
        WFitLayout wFitLayout = new WFitLayout();
        ChartEditor editor = new ChartEditor();
        ChartCollection collection = new ChartCollection();
        editor.resetChangeChartCollection(collection);
        wFitLayout.addWidget(editor);
        form.setContainer(wFitLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        boolean result1 = Reflect.on(jFormProcessInfo).call("hasTestChart").get();
        Assert.assertTrue(result1);

        collection.addChart(new VanChart());
        boolean result2 = Reflect.on(jFormProcessInfo).call("hasTestChart").get();
        Assert.assertTrue(result2);

    }

    @Test
    public void testHasTestTabLayout() {
        Form form = new Form();
        WFitLayout wFitLayout = new WFitLayout();
        WCardLayout editor = new WCardLayout();
        wFitLayout.addWidget(editor);
        form.setContainer(wFitLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        boolean result1 = Reflect.on(jFormProcessInfo).call("hasTestTabLayout").get();
        Assert.assertFalse(result1);

        WCardTagLayout tagLayout = new WCardTagLayout();
        CardSwitchButton button = new CardSwitchButton();
        button.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Title") + "0");
        tagLayout.addWidget(button);
        editor.addWidget(tagLayout);
        boolean result2 = Reflect.on(jFormProcessInfo).call("hasTestTabLayout").get();
        Assert.assertTrue(result2);

        button.setText(com.fr.design.i18n.Toolkit.i18nText("Fine-Design_Form_Title") + "test");
        boolean result3 = Reflect.on(jFormProcessInfo).call("hasTestTabLayout").get();
        Assert.assertFalse(result3);

    }

    @Test
    public void testHasTestAbsolutePane() {
        Form form = new Form();
        WFitLayout wFitLayout = new WFitLayout();
        WAbsoluteLayout editor = new WAbsoluteLayout();
        wFitLayout.addWidget(editor);
        form.setContainer(wFitLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        boolean result1 = Reflect.on(jFormProcessInfo).call("hasTestAbsolutePane").get();
        Assert.assertTrue(result1);

        editor.addWidget(new FreeButton());
        boolean result2 = Reflect.on(jFormProcessInfo).call("hasTestAbsolutePane").get();
        Assert.assertFalse(result2);
    }

    @Test
    public void testHasEmptyParaPane() {
        Form form = new Form();
        WBorderLayout borderLayout = new WBorderLayout();
        form.setContainer(borderLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        boolean result1 = Reflect.on(jFormProcessInfo).call("hasEmptyParaPane").get();
        Assert.assertFalse(result1);

        borderLayout.addNorth(new WParameterLayout());
        boolean result2 = Reflect.on(jFormProcessInfo).call("hasEmptyParaPane").get();
        Assert.assertTrue(result2);
    }

    @Test
    public void testUseParaPane() {
        Form form = new Form();
        WBorderLayout borderLayout = new WBorderLayout();
        form.setContainer(borderLayout);
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        Assert.assertFalse(jFormProcessInfo.useParaPane());

        borderLayout.addNorth(new WParameterLayout());
        Assert.assertTrue(jFormProcessInfo.useParaPane());

    }

    @Test
    public void testGetComponentsInfo() {
        Form form = new Form();
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        FreeButton button1 = new FreeButton();
        FreeButton button2 = new FreeButton();
        button1.setWidgetID("xxxx1");
        button2.setWidgetID("xxxx2");
        Reflect.on(jFormProcessInfo).call("addComponentCreateInfo", new ComponentCreateOperate(button1).toJSONObject());
        Reflect.on(jFormProcessInfo).call("addComponentRemoveInfo", new ComponentDeleteOperate(button2).toJSONObject());
        JSONArray ja = jFormProcessInfo.getComponentsInfo();
        Assert.assertEquals(2, ja.size());

        Reflect.on(jFormProcessInfo).call("addComponentCreateInfo", new ComponentCreateOperate(button1).toJSONObject());
        FreeButton button3 = new FreeButton();
        Reflect.on(jFormProcessInfo).call("addComponentRemoveInfo", new ComponentDeleteOperate(button3).toJSONObject());
        Assert.assertEquals(1, jFormProcessInfo.getComponentsInfo().size());

        ja = jFormProcessInfo.getComponentsInfo();
        Assert.assertEquals(0, ja.size());
    }

    @Test
    public void testGetWidgetCount() {
        Form form = new Form();
        WBorderLayout borderLayout = new WBorderLayout();
        form.setContainer(borderLayout);
        WFitLayout fitLayout = new WFitLayout();
        borderLayout.addCenter(fitLayout);
        fitLayout.addWidget(new FreeButton());
        fitLayout.addWidget(new ElementCaseEditor());
        JFormProcessInfo jFormProcessInfo = new JFormProcessInfo(form);
        Assert.assertEquals(2, jFormProcessInfo.getWidgetCount());

        fitLayout.addWidget(new ChartEditor());
        Assert.assertEquals(3, jFormProcessInfo.getWidgetCount());

        fitLayout.addWidget(new WAbsoluteBodyLayout());
        Assert.assertEquals(3, jFormProcessInfo.getWidgetCount());

        fitLayout.addWidget(new CardSwitchButton());
        Assert.assertEquals(3, jFormProcessInfo.getWidgetCount());

        fitLayout.addWidget(new ChartEditor());
        Assert.assertEquals(4, jFormProcessInfo.getWidgetCount());
    }
}
