package com.fr.design.mainframe;

import com.fr.design.mainframe.reuse.ComponentReuseNotificationInfo;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kerry on 5/10/21
 */
public class ReuseTriggerPointManagerTest {
    private static final long ONE_WEEK_TIME = 7 * 24 * 3600 * 1000L;


    @Test
    public void testNeedTrigger() {
        ComponentReuseNotificationInfo notificationInfo = ComponentReuseNotificationInfo.getInstance();
        notificationInfo.setNotifiedNumber(0);
        Assert.assertTrue(ReuseTriggerPointManager.getInstance().needTrigger());

        notificationInfo.setNotifiedNumber(1);
        Assert.assertTrue(ReuseTriggerPointManager.getInstance().needTrigger());

        notificationInfo.setNotifiedNumber(2);
        Assert.assertFalse(ReuseTriggerPointManager.getInstance().needTrigger());

        notificationInfo.setNotifiedNumber(0);
        notificationInfo.setLastNotifyTime(System.currentTimeMillis());
        Assert.assertFalse(ReuseTriggerPointManager.getInstance().needTrigger());

        notificationInfo.setNotifiedNumber(1);
        notificationInfo.setLastNotifyTime(System.currentTimeMillis());
        Assert.assertFalse(ReuseTriggerPointManager.getInstance().needTrigger());


        notificationInfo.setNotifiedNumber(1);
        notificationInfo.setLastNotifyTime(System.currentTimeMillis() - ONE_WEEK_TIME - 1);
        Assert.assertTrue(ReuseTriggerPointManager.getInstance().needTrigger());


        notificationInfo.setNotifiedNumber(2);
        notificationInfo.setLastNotifyTime(System.currentTimeMillis());
        Assert.assertFalse(ReuseTriggerPointManager.getInstance().needTrigger());
    }

}
