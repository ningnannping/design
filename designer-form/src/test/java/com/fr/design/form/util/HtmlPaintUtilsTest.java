package com.fr.design.form.util;

import com.fr.base.Style;
import junit.framework.TestCase;
import org.junit.Assert;


/**
 * @author hades
 * @version 11.0
 * Created by hades on 2022/5/25
 */
public class HtmlPaintUtilsTest extends TestCase {

    public void testDeriveMaxFontFromRichChar() {
        // 富文本字体size更大
        String testHtml0 = "<span style=\"font-size:21px;font-family:'宋体';\">这是一条测试数据</span>";
        Style style0 = Style.DEFAULT_STYLE;
        Assert.assertEquals(16, HtmlPaintUtils.deriveMaxFontFromRichChar(style0, testHtml0).getFRFont().getSize());

        // 单元格字体size更大
        String testHtml1 = "<span style=\"font-size:7px;font-family:'宋体';\">这是一条测试数据</span>";
        Style style1 = Style.DEFAULT_STYLE;
        int oldFontSize = style1.getFRFont().getSize();
        Assert.assertEquals(oldFontSize, HtmlPaintUtils.deriveMaxFontFromRichChar(style1, testHtml1).getFRFont().getSize());

        // 富文本字体size更大 不同文本 有不同size
        String testHtml2 = "<span style=\"font-size:21px;font-family:'宋体';\">这是一条测</span><span style=\"font-size:31px;font-family:'宋体';\">试数</span><span style=\"font-size:21px;font-family:'宋体';\">据</span>";
        Style style2 = Style.DEFAULT_STYLE;
        Assert.assertEquals(23, HtmlPaintUtils.deriveMaxFontFromRichChar(style2, testHtml2).getFRFont().getSize());


        // 异常场景1
        String testHtml3 = "xxxx奇怪的格式xxxx";
        Style style3 = Style.DEFAULT_STYLE;
        oldFontSize = style1.getFRFont().getSize();
        Assert.assertEquals(oldFontSize, HtmlPaintUtils.deriveMaxFontFromRichChar(style3, testHtml3).getFRFont().getSize());

        // 异常场景2
        String testHtml4 = "<span style=\"font-size:xxxxpx;font-family:'宋体';\">这是一条测试数据</span>";
        Style style4 = Style.DEFAULT_STYLE;
        oldFontSize = style1.getFRFont().getSize();
        Assert.assertEquals(oldFontSize, HtmlPaintUtils.deriveMaxFontFromRichChar(style4, testHtml4).getFRFont().getSize());


    }


}