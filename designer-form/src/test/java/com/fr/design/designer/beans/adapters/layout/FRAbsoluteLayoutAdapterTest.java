package com.fr.design.designer.beans.adapters.layout;

import com.fr.config.dao.DaoContext;
import com.fr.config.dao.impl.LocalClassHelperDao;
import com.fr.config.dao.impl.LocalEntityDao;
import com.fr.config.dao.impl.LocalXmlEntityDao;
import com.fr.design.designer.creator.XButton;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.designer.creator.XWAbsoluteLayout;
import com.fr.form.ui.Button;
import com.fr.form.ui.container.WAbsoluteLayout;
import java.awt.Dimension;
import junit.framework.TestCase;
import org.junit.Assert;

public class FRAbsoluteLayoutAdapterTest extends TestCase {
    @Override
    protected void setUp() throws Exception {
        DaoContext.setEntityDao(new LocalEntityDao());
        DaoContext.setClassHelperDao(new LocalClassHelperDao());
        DaoContext.setXmlEntityDao(new LocalXmlEntityDao());
    }

    public void testFix(){
        WAbsoluteLayout layout =new WAbsoluteLayout();
        XWAbsoluteLayout container =new XWAbsoluteLayout(layout);
        container.setBounds(0,320,400,160);
        FRAbsoluteLayoutAdapter frAbsoluteLayoutAdapter = new FRAbsoluteLayoutAdapter(container);
        XCreator button = new XButton(new Button(),new Dimension(88,40));

        frAbsoluteLayoutAdapter.fix(button,-1,-1);
        Assert.assertEquals(0,button.getX());
        Assert.assertEquals(0,button.getY());

        frAbsoluteLayoutAdapter.fix(button,350,200);
        Assert.assertEquals(312,button.getX());
        Assert.assertEquals(120,button.getY());
    }

    @Override
    protected void tearDown() throws Exception {
        DaoContext.setEntityDao(null);
        DaoContext.setClassHelperDao(null);
        DaoContext.setXmlEntityDao(null);
    }
}
