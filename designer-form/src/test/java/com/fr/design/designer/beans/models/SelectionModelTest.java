package com.fr.design.designer.beans.models;

import com.fr.design.ExtraDesignClassManager;
import com.fr.design.designer.beans.events.CreatorEventListenerTable;
import com.fr.design.designer.creator.XCreator;
import com.fr.design.fun.ClipboardHandlerProvider;
import com.fr.design.fun.impl.AbstractClipboardHandlerProvider;
import com.fr.design.mainframe.FormDesigner;
import com.fr.plugin.injectable.PluginModule;
import com.fr.stable.fun.mark.Mutable;
import org.easymock.EasyMock;
import org.easymock.IAnswer;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.HashSet;
import java.util.Set;

@PrepareForTest(PluginModule.class)
@SuppressStaticInitializationFor({"com.fr.design.mainframe.FormDesigner"})
@PowerMockIgnore("javax.swing.*")
@RunWith(PowerMockRunner.class)
public class SelectionModelTest {
    
    @Before
    public void setUp() throws Exception {
        
        AbstractClipboardHandlerProvider provider = EasyMock.mock(AbstractClipboardHandlerProvider.class);
        EasyMock.expect(provider.cut(EasyMock.anyObject())).andAnswer(new IAnswer<Object>() {
            @Override
            public Object answer() throws Throwable {
                return null;
            }
        }).anyTimes();
        EasyMock.expect(provider.copy(EasyMock.anyObject())).andAnswer(new IAnswer<Object>() {
            @Override
            public Object answer() throws Throwable {
                return EasyMock.getCurrentArguments()[0];
            }
        }).anyTimes();
        EasyMock.expect(provider.support(EasyMock.anyObject())).andReturn(true).anyTimes();
        EasyMock.expect(provider.paste(EasyMock.anyObject())).andReturn(null).anyTimes();
        EasyMock.replay(provider);
    
        Set<Mutable> providers = new HashSet<>();
        providers.add(provider);
    
        ExtraDesignClassManager designManager = EasyMock.mock(ExtraDesignClassManager.class);
        EasyMock.expect(designManager.getArray(ClipboardHandlerProvider.XML_TAG))
                .andReturn(providers)
                .anyTimes();
        EasyMock.expect(designManager.getArray("DesignerEditListenerProvider"))
                .andReturn(new HashSet<Mutable>())
                .anyTimes();
        EasyMock.replay(designManager);
    
        PowerMock.mockStatic(PluginModule.class);
        EasyMock.expect(PluginModule.getAgent(PluginModule.ExtraDesign))
                .andReturn(designManager)
                .anyTimes();
        PowerMock.replayAll();
    }
    
    @After
    public void tearDown() throws Exception {
        
        PowerMock.resetAll();
    }
    
    @Test
    public void testPaste() {
        
        FormDesigner formDesigner = EasyMock.partialMockBuilder(FormDesigner.class).createMock();
        XCreator xCreator = EasyMock.mock(XCreator.class);
        EasyMock.expect(xCreator.acceptType(EasyMock.anyObject(Class[].class))).andReturn(true).anyTimes();
        CreatorEventListenerTable table = new CreatorEventListenerTable();
        Whitebox.setInternalState(formDesigner, "edit", table);
        EasyMock.replay(formDesigner, xCreator);
        
        SelectionModel model = new SelectionModel(formDesigner);
        model.cutSelectedCreator2ClipBoard();
        boolean paste1 = model.pasteFromClipBoard();
        Assert.assertFalse(paste1);
        
        model.setSelectedCreator(xCreator);
        model.copySelectedCreator2ClipBoard();
        boolean paste2 = model.pasteFromClipBoard();
        Assert.assertFalse(paste2);
    }
    
}