package com.fr.design.designer.creator;

import com.fr.form.ui.EditorHolder;
import org.junit.Assert;
import org.junit.Test;

public class XCreatorUtilsTest {
    
    @Test
    public void testCreateXCreator() throws Exception {
        
        XCreator xCreator = XCreatorUtils.createXCreator(new EditorHolder() {
        });
        
        Assert.assertFalse(xCreator instanceof NullCreator);
    }
}